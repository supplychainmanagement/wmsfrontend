<?php
/**
 * Created by PhpStorm.
 * User: Khaled
 * Date: 8/14/2016
 * Time: 5:15 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HttpRequest\HttpRequest;
use App\Http\Requests;

class StorageJobController extends Controller{
    public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function createIndex()
    {
        $url = $this->url.'/generatestoragejob';
        $response = HttpRequest::get($url);
        $body = json_decode($response['body'],true);
        $body = $body[0];
        return View('GoodsIn.StorageJob.create')->with('STR_number',$body);
    }

    public function getDataForStorageJob()
    {
        $url = $this->url.'/storagejobdata';
        $response = HttpRequest::get($url);
        $body = json_decode($response['body'],true);
        return $body;
    }

    public function index()
    {
        $url = $this->url.'/storagejob';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if ($status_code == 404) return redirect('/home')->with('message', 'This URI is Not Found')->
        with('messagetype', 'danger');
        if ($status_code == 500) return redirect('/home')->with('message', $body['errors'][0])->with('messagetype', 'danger');
        if ($status_code == 200)
        {
            return View('GoodsIn.StorageJob.index')->with('storageJobs',$body);
        }
    }
    public function getStorageLocationsForStrategy(Request $request)
    {
        $url = $this->url.'/storagejobstrategy';
        $fields = $request->all();
        $response = HttpRequest::post($url,$fields);
        $body = json_decode($response['body'],true);
        $status_code = $response['info']['http_code'];
        $return_data = array();
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors']
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>[['Storage Locations Successfully Loaded according to strategy']],
            'data'=>$body
            );
            return $return_data;
        }
    }

    public function generateStorageJobName()
    {
        $url = $this->url.'/generatestoragejob';
        $response = HttpRequest::get($url);
        $body = json_decode($response['body'],true);
        return $body;
    }

    public function submitStorageJob(Request $request)
    {
        $url = $this->url.'/submitstoragejob';
        $fields = $request->all();
        $response = HttpRequest::post($url,$fields);
        $body = json_decode($response['body'],true);
        $status_code = $response['info']['http_code'];
        //return  $response['body'];
        // if($status_code != 201)return $body['errors'][0];
        // return View('/home')->with('message','Storage Job Has Been Submitted Successfully')
        //     ->with('messagetype','success');
        $return_data = array();
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors']
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>[['Storage Job Successfully Submitted']],
            'data'=>$body
            );
            return $return_data;
        }
    }

}