<?php

namespace App\Http\Controllers;

use App\HttpRequest\HttpRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class RackController extends Controller
{
  public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function index()
    {
    	$url = $this->url . '/rack';
    	$return_code = $this->return_code;
       $response = HttpRequest::get($url);
       $status_code = $response['info']['http_code'];
       //return $response['body'];
       if($status_code != $return_code['ok'])
       {
       		return Redirect::route('home');
       }
       elseif($status_code == 204)
       {
       	return view('Locations.rack')
       	->with('racks',array())->with('zones',array())
       	->with('storagelocationtypes',array())->with('functionalareas',array());
       }
       else
       {
       	$racks = array();
       	$body = $response['body'];
       	$backendArray = json_decode($body, true);
       	$counter = 0;
        foreach ($backendArray['racks'] as $key => $value) {
            $racks[$counter]['id'] = $value['rack']['id'];
            $racks[$counter]['rack_name'] = $value['rack']['RACK_name'];
            $racks[$counter]['row'] = $value['rack']['RACK_row'];
            $racks[$counter]['column'] = $value['rack']['RACK_column'];
            $racks[$counter]['depth'] = $value['rack']['RACK_depth'];
            $racks[$counter]['zone_name'] = $value["zone_name"];
            $racks[$counter]['created_at'] = $value['rack']['created_at'];
            $racks[$counter]['updated_at'] = $value['rack']['updated_at'];
            $counter++;

        }

        $zones = array();
        $counter = 0;
        foreach ($backendArray['zones'] as $key => $value) {
        	$zones[$counter] = $value['ZONE_name'];
        	$counter++;
        }

        $storagelocationtypes = array();
        $counter = 0;
        foreach ($backendArray['storagelocationtypes'] as $key => $value) {
        	$storagelocationtypes[$counter] = $value['STL_Type_name'];
        	$counter++;
        }

        $functionalareas = array();
        $counter = 0;
        foreach ($backendArray['functionalareas'] as $key => $value) {
        	$functionalareas[$counter] = $value['FA_name'];
        	$counter++;
        }
        $sites = array();
        $counter = 0;
        foreach ($backendArray['sites'] as $key => $value) {
          $sites[$counter] = $value['SITE_name'];
          $counter++;
        }



    	return view('Locations.rack')->with('racks',$racks)
    	->with('zones',$zones)->with('storagelocationtypes',$storagelocationtypes)
    	->with('functionalareas',$functionalareas)->with('sites',$sites);
       }
    }

    public function create(Request $request)
    {
    	$fields = array();
      	$fields['RACK_name'] = $request->rack_name;
      	$fields['ZONE_name'] = $request->zone_name;
      	$fields['RACK_column'] = $request->column;
      	$fields['RACK_row'] = $request->row;
      	$fields['RACK_depth'] = $request->depth;
      	$fields['STL_Type_name'] = $request->storagelocationtype_name;
      	$fields['FA_name'] = $request->functionalarea_name;
        $fields['SITE_name'] = $request->SITE_name;
      	$url = $this->url . '/rack/create'; 
      	$return_code = $this->return_code;
      	$response = HttpRequest::post($url ,$fields);
      	$statusCode = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
      	if($statusCode == $return_code['bad_request'])
		{
			return redirect('/rack') 
			        -> with('message', $body['errors'])->with('messagetype','danger');
		}
		elseif($statusCode == $return_code['internal_server_error'])
		{
      return $response['body'];
			return redirect('/rack') 
			        -> with('message', $body)->with('messagetype','danger');
		}  
		elseif($statusCode == $return_code['created'])
		{
			return redirect('/rack') 
			        -> with('message', $body)->with('messagetype','success');
		}  
    }

    public function update(Request $request)
    {
    	$fields = array();
    	$fields['id'] = $request->id;
      	$fields['RACK_name'] = $request->rack_name;
      	$fields['ZONE_name'] = $request->zone_name;
      	$fields['RACK_column'] = $request->column;
      	$fields['RACK_row'] = $request->row;
      	$fields['RACK_depth'] = $request->depth;
	    //return $fields;
        $url = $this->url . '/rack/update'; 
        $return_code = $this->return_code;  
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if($statusCode == $return_code['bad_request'])
		{
			return redirect('/rack') 
			        -> with('message', $body['errors'])->with('messagetype','danger');
		}
		elseif($statusCode == $return_code['internal_server_error'])
		{
			return redirect('/rack') 
			        -> with('message', $body)->with('messagetype','danger');
		}  
		elseif($statusCode == $return_code['ok'])
		{
			return redirect('/rack') 
			        -> with('message', $body)->with('messagetype','success');
		}         	
    }

    public function delete(Request $request)
    {
    	$fields =array(
          'id'=>$request->id);
      	$url = $this->url . '/rack/delete';  
      	$return_code = $this->return_code; 
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
    	if($statusCode == $return_code['bad_request'])
		{
			return redirect('/rack') 
			        -> with('message', $body['errors'])->with('messagetype','danger');
		}
		elseif($statusCode == $return_code['internal_server_error'])
		{
			return redirect('/rack') 
			        -> with('message', $body)->with('messagetype','danger');
		}  
		elseif($statusCode == $return_code['ok'])
		{
			return redirect('/rack') 
			        -> with('message', $body)->with('messagetype','success');
		}   	
    }

    public function getprop(Request $request)
    {

       $url = $this->url . '/rackprop';
       $id = $request->id;
       //return $id;
       $fields = array('id'=>$id);
       $response = HttpRequest::post($url,$fields);
       //return $response;
       $status_code = $response['info']['http_code'];
       //return $status_code;
       if($status_code != 200)
       {
           return Redirect::route('rack');
       }
       else
       {
            $body = $response['body'];
            $dataArray = json_decode($body, true);
            
            $zoneArray = array();
            // $counter = 0;
            //return $dataArray;
            $counter = 0;
            foreach ($dataArray['zones'] as $key => $value) {
                $zoneArray[$counter] = $value["ZONE_name"];
                $counter++;

            }
            $dataFrontEndArray = ['rack'=>$dataArray['rack'],'rack_zone'=>$dataArray['rack_zone'],'admin'=>$dataArray['admin'],'zones'=>$zoneArray];
            return response($dataFrontEndArray);
       }
       // else if($status_code == 204)
       // {
       //    $zone = array();
       //    return response($zone);
       // }
    }
    public function getZones(Request $request)
    {
      $url = $this->url .'/getzonesbysitename';
      $fields = array('SITE_name'=>$request->value);
      //return $fields;
      $response = HttpRequest::post($url,$fields);
      $status_code = $response['info']['http_code'];
      $body = json_decode($response['body'], true);
      //return $response['body'];
      $return_data = array();
      if($status_code == 400)
      { 
        $return_data = array(
          'error'=>true,
          'message'=>'Missing Inputs'
          );
          return $return_data;
      }
      elseif($status_code == 500)
      {
        $return_data = array(
          'error'=>true,
          'message'=>'Server Encountered an error'
          );
          return $return_data;
      }
      else
      {
      $return_data = array(
      'error'=>false,
      'message'=>'Zones Data Pulled',
      'zones'=>$body
      );
        return $return_data;
      }
    }
}
