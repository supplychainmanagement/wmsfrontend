<?php

namespace App\Http\Controllers;

use App\HttpRequest\HttpRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class UnitloadTypeController extends Controller
{
  public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function index()
    {
    	$url = $this->url . '/unitloadtype';
    	$return_code = $this->return_code;
       $response = HttpRequest::get($url);
       $status_code = $response['info']['http_code'];
       if($status_code != $return_code['ok'] && $status_code != $return_code['no_content'])
       {
       		return Redirect::route('login');
       }
       elseif($status_code == $return_code['no_content'])
       {
       	return view('types.unitloadtype')->with('message','No Content')
       	->with('unitloadtypes',array());
       }
       else
       {
       	$unitloadtypes = array();
       	$body = $response['body'];
       	$backendArray = json_decode($body, true);
       	$counter = 0;
        foreach ($backendArray['unitloadType'] as $key => $value) {
            $unitloadtypes[$counter]['id'] = $value['id'];
            $unitloadtypes[$counter]['type_name'] = $value["ULT_Type_name"];
            $unitloadtypes[$counter]['created_at'] = $value['created_at'];
            $unitloadtypes[$counter]['updated_at'] = $value['updated_at'];
            $counter++;

        }

        return view('types.unitloadtype')->with('unitloadtypes',$unitloadtypes);
       }
    }

    public function create(Request $request)
    {
    	$fields = array();
      	$fields['ULT_Type_name'] = $request->type_name;

      	$url = $this->url . '/unitloadtype/create'; 
      	$return_code = $this->return_code;
      	$response = HttpRequest::post($url ,$fields);
      	$statusCode = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
      	if($statusCode == $return_code['bad_request'])
		{
			return redirect('/unitloadtype') 
			        -> with('message', $body['errors'])->with('messagetype','danger');
		}
		elseif($statusCode == $return_code['internal_server_error'])
		{
			return redirect('/unitloadtype') 
			        -> with('message', $body)->with('messagetype','danger');
		}  
		elseif($statusCode == $return_code['created'])
		{
			return redirect('/unitloadtype') 
			        -> with('message',$body)->with('messagetype','success');
		}    	
    }


    public function update(Request $request)
    {
    	$fields =array(
          'id'=>$request->id,
          'ULT_Type_name'=>$request->type_name,
          );
        $url = $this->url . '/unitloadtype/update'; 
        $return_code = $this->return_code;  
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if($statusCode == $return_code['bad_request'])
		{
			return redirect('/unitloadtype') 
			        -> with('message',$body['errors'])->with('messagetype','danger');
		}
		elseif($statusCode == $return_code['internal_server_error'])
		{
			return redirect('/unitloadtype') 
			        -> with('message', $body)->with('messagetype','danger');
		} 
		elseif($statusCode == $return_code['ok'])
		{
			return redirect('/unitloadtype') 
			        -> with('message',$body)->with('messagetype','success');
		}    
    }

    public function delete(Request $request)
    {
    	$fields =array(
          'id'=>$request->id);
      	$url = $this->url . '/unitloadtype/delete';  
      	$return_code = $this->return_code; 
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
		if($statusCode == $return_code['bad_request'])
		{
			return redirect('/unitloadtype') 
			        -> with('message', $body['errors'])->with('messagetype','danger');
		}
		
		elseif($statusCode == $return_code['internal_server_error'])
		{
			return redirect('/unitloadtype') 
			        -> with('message', $body)->with('messagetype','danger');
		}
		elseif($statusCode == $return_code['ok'])
		{
			return redirect('/unitloadtype') 
			        -> with('message', $body)->with('messagetype','success');
		}   
    }
}
