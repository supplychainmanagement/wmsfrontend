<?php
/**
 * Created by PhpStorm.
 * User: Khaled
 * Date: 8/8/2016
 * Time: 3:47 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HttpRequest\HttpRequest;
use App\Http\Requests;

class CustomerController extends Controller{
    public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }

    public function index()
    {
        $url = $this->url.'/customer';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        $customers = $body['customers'];
        $clients = $body['clients'];
        if($status_code == 200 || $status_code == 204)return View('Customer.customer')->with('customers',$customers)
            ->with('clients',$clients);
    }

    public function getprop(Request $request)
    {
        $url = $this->url .'/customer/prop';
        $fields = ['id'=>$request->id];
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        return $body;
        $customer = $body['customer'];
        $clients = $body['clients'];
        $customer_clients = $body['customer_clients'];

        if($status_code == 200)
        {
            $dataFrontEnd = ['customer'=>$customer,'clients'=>$clients,'customer_clients'=>$customer_clients];
            return response($dataFrontEnd);
        }elseif($status_code == 204)
        {
            $dataFrontEnd = array();
            return $dataFrontEnd;
        }else{
            return redirect('/login');
        }
    }

    public function create(Request $request)
    {
        $url = $this->url.'/customer/create';
        $fields = array(
            'CST_name'=>$request->CST_name,
            'CST_CLI_name'=>$request->CST_CLI,
            'CST_phone'=>$request->CST_phone,
            'CST_email'=>$request->CST_email,
            'CST_address'=>$request->CST_address,
            'CST_branch'=>$request->CST_branch
        );
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        if($status_code == 201)return redirect('/customer')->with('message','Customer Have Been Created Successfully')
            ->with('messagetype','success');
        if($status_code == 400)return redirect('/customer')->with('message',$body['errors'][0])->with('messagetype','danger');
        if($status_code == 500)return redirect('/customer')->with('message','Customer Has Failed to Be Created')
            ->with('messagetype','danger');
    }

    public function delete(Request $request)
    {
        $url = $this->url.'/customer/delete';
        $fields = ['id'=>$request->id];
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        if($status_code == 200)return redirect('/customer')->with('message','Customer Has Been Deleted Successfully')
            ->with('messagetype','success');
        if($status_code == 400)return redirect('/customer')->with('message',$body['errors'][0])->with('messagetype','danger');
        if($status_code == 500)return redirect('/customer')->with('message','Customer Has Failed to Be Deleted')
            ->with('messagetype','danger');
    }

    public function update(Request $request)
    {
        $url = $this->url.'/customer/update';
        $fields = array(
            'id'=>$request->id,
            'CST_name'=>$request->CST_name,
            'CST_email'=>$request->CST_email,
            'CST_phone'=>$request->CST_phone,
            'CST_address'=>$request->CST_address,
            'CST_branch'=>$request->CST_branch,
            'CST_CLI'=>$request->CST_CLI_name
        );
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        if($status_code == 200)return redirect('/customer')->with('message','Client has Been Updated Successfully')
            ->with('messagetype','success');
        if($status_code == 400)return redirect('/customer')->with('message',$body['errors'][0])->with('messagetype','danger');
        if($status_code == 500)return redirect('/customer')->with('message','Customer Has Failed to Be Updated')
            ->with('messagetype','danger');
    }

    
}