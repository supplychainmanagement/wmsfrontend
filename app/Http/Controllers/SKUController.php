<?php
/**
 * Created by PhpStorm.
 * User: Khaled
 * Date: 8/1/2016
 * Time: 6:19 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HttpRequest\HttpRequest;
use App\Http\Requests;
use Illuminate\Http\Response;

class SKUController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function index()
    {
        $url = $this->url.'/sku';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        //return $response['body'];
        $sku = $this->getskuArray($response);
        //return $sku;
        $skuType = $this->getskuType($response);
        $clients = $this->getClients($response);
        $suppliers = $this->getSuppliers($response);
        $permission = $this->getPermission($response);
        $systemClient = $this->getSystemClient($response);
        if($status_code == 200 || $status_code == 204)return View('SKU.sku')->with('skus',$sku)
            ->with('skuType',$skuType)->with('clients',$clients)
            ->with('suppliers',$suppliers)->with('permission',$permission)->with('systemClient',$systemClient);
        else{
            return redirect('/login');
        }


    }

    public function  getPermission($response)
    {
        if($response['body']!=null)
        {
            $body = json_decode($response['body'],true);
            return $body['permission'] ? true : false;
        }
    }

    public function getSystemClient($response)
    {
        if($response['body']!=null)
        {
            $body = json_decode($response['body'],true);
            return $body['systemClient'] ? true : false;
        }
    }

    public function getSuppliers($response)
    {
        $suppliers = array();
        if($response['body']!=null)
        {
            $body = json_decode($response['body'],true);
            $counter = 0;
            if($body['all_suppliers']) {
                foreach ($body['all_suppliers'] as $supplier) {
                    $suppliers[$counter]['id'] = $supplier['id'];
                    $suppliers[$counter]['SUP_name'] = $supplier['SUP_name'];
                    $suppliers[$counter]['SUP_phone'] = $supplier['SUP_phone'];
                    $suppliers[$counter]['SUP_address'] = $supplier['SUP_address'];
                    $suppliers[$counter]['SUP_email'] = $supplier['SUP_email'];
                    $counter++;
                }
            }
        }
        return $suppliers;
    }

    public function getClients($response)
    {
        $clientsArray = array();
        if($response['body']!=null)
        {
            $body = json_decode($response['body'],true);
            $counter =0;
            if($body['all_clients']!=null) {
                foreach ($body['all_clients'] as $client) {
                    $clientsArray[$counter]['id'] = $client['id'];
                    $clientsArray[$counter]['CLI_name'] = $client['CLI_name'];
                    $clientsArray[$counter]['CLI_email'] = $client['CLI_email'];
                    $clientsArray[$counter]['CLI_address'] = $client['CLI_address'];
                    $clientsArray[$counter]['CLI_phone'] = $client['CLI_phone'];
                    $counter++;
                }
            }
        }
        return $clientsArray;
    }

    public function getskuArray($response)
    {
        $skuArray = array();
        if($response['body']!=null)
        {
            $body = $response['body'];
            $someArray = json_decode($body,true);
            $counter = 0;
            if($someArray['skus']!=null) {
                foreach ($someArray['skus'] as $sku) {
                    $skuArray[$counter] = $sku;
                    $counter++;
                }
            }
        }
        return $skuArray;
    }

    public function getskuType($response)
    {
        $skuType = array();
        if($response['body'] != null)
        {
            $body = json_decode($response['body'],true);
            $counter = 0;
            if($body['all_skuTypes']!=null) {
                foreach ($body['all_skuTypes'] as $type) {
                    $skuType[$counter] = $type;
                    $counter++;
                }
            }
        }
        return $skuType;
    }

    public function update(Request $request)
    {
        $url = $this->url . '/sku/update';
        $fields = array(
            'id'=>$request->id,
            'SKU_name'=>$request->SKU_name,
            'SKU_SKU_Type'=>$request->SKU_SKU_Type,
            'SKU_CLI'=>$request->SKU_CLI_name,
            'SKU_item_no'=>$request->SKU_item_no,
        );
        $fields['SKU_suppliers'] = null;
        $fields['SKU_stackingNumber'] = null;
        $fields['SKU_lifeTime'] = null;
        if($request->has('SUP_name')) $fields['SKU_suppliers']=$request->SUP_name;
        if($request->has('SKU_stackingNumber')) $fields['SKU_stackingNumber']=$request->SKU_stackingNumber;
        if($request->has('SKU_lifeTime')) $fields['SKU_lifeTime']=$request->SKU_lifeTime;

        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        if($status_code == 400)return redirect('/sku')->with('message',$body['errors'])->with('messagetype','danger');
        elseif($status_code == 200)return redirect('/sku')->with('message',$body)->with('messagetype','success');
        elseif($status_code == 500)return redirect('/sku')->with('message',$body)->with('messagetype','danger');
    }

    public function delete(Request $request)
    {
        $fields = array('id'=>$request->id);
        $url = $this->url . '/sku/delete';
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        if($status_code == 400)return redirect('/sku')->with('message',$body['errors'])->with('messagetype','danger');
        elseif($status_code == 200)return redirect('/sku')->with('message',$body)->with('messagetype','success');
        elseif($status_code == 500)return redirect('/sku')->with('message',$body)->with('messagetype','danger');
    }

    public function create(Request $request)
    {
        $url = $this->url .'/sku/create';
        $fields = array();
        $fields['SKU_name'] = $request['SKU_name'];
        $fields['SKU_SKU_Type'] = $request['SKU_SKU_Type_name'];
        $fields['SKU_CLI'] = $request['SKU_CLI_name'];
        $fields['SKU_item_no'] = $request['SKU_item_no'];
        $fields['SKU_lifeTime'] = null;
        $fields['SKU_stackingNumber'] = null;
        if($request->has('SKU_lifeTime')) $fields['SKU_lifeTime'] = $request['SKU_lifeTime'];
        if($request->has('SKU_stackingNumber')) $fields['SKU_stackingNumber'] = $request['SKU_stackingNumber'];
        if($request->has('SKU_SUP_name'))
        {
            for($i = 0;$i<sizeof($request['SKU_SUP_name']);$i++)
            {
                $fields['SKU_suppliers'][$i] = $request['SKU_SUP_name'][$i];
            }
        }
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        if($status_code == 400)
        {
            return redirect('/sku')
                ->with('message',$body['errors'])->with('messagetype','danger');
        }elseif($status_code == 201)
        {
            return redirect('/sku')->with('message',$body)->with('messagetype','success');
        }elseif($status_code == 500)
        {
            return redirect('/sku')->with('message',$body)->with('messagetype','danger');
        }
    }

    public function getprop(Request $request)
    {
        $url = $this->url .'/sku/prop';
        $fields = array('id'=>$request->id);
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $clients = array();
        $types = array();
        $suppliers = array();
        $body = json_decode($response['body'],true);
        $counter =0;
        return $body;
        foreach($body['all_clients'] as $client)
        {
            $clients[$counter]=$client['CLI_name'];
            $counter++;
        }
        $counter = 0;
        foreach($body['all_sku_types'] as $type)
        {
            $types[$counter] = $type['SKU_Type_name'];
            $counter++;
        }
        $counter = 0;
        foreach($body['all_suppliers'] as $supplier)
        {
            $suppliers[$counter] = $supplier['SUP_name'];
            $counter++;
        }
        if($status_code == 200)
        {
            $dataFrontEnd = ['clients'=>$clients,'Types'=>$types,'suppliers'=>$suppliers,
            'skuClient'=>$body['sku_client'],'skuType'=>$body['sku_sku_type'],'skuSuppliers'=>$body['sku_suppliers'],'sku'=>$body['sku']
            ,'permission'=>$body['permission'],'attached'=>$body['attached']];

            return response ($dataFrontEnd);
        }elseif($status_code == 204)
        {
            $body = array();
            return response($body);
        }else{
            redirect::route('home');
        }
    }


}