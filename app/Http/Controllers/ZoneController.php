<?php
/**
 * Created by PhpStorm.
 * User: Khaled
 * Date: 8/1/2016
 * Time: 7:18 PM
 */
namespace App\Http\Controllers;

use App\HttpRequest\HttpRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
class ZoneController extends Controller
{
  public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function index()
    {
       $url = $this->url . '/zone';
       $response = HttpRequest::get($url);
       $status_code = $response['info']['http_code'];
       $zones = $this->getZoneArray($response);
       //return $zones;
       $permission =$this->getPermission($response);
       $sites = $this->getSites($response);
       $systemClient = $this->getSystemClient($response);
       $siteBased =$this->siteBased($response);
       $role = $this->role($response);
       if($status_code == 200)
       {
            return view('Locations.zone')->with('zones',$zones)
            ->with('sites',$sites)->with('permission',$permission)
            ->with('systemClient',$systemClient)->with('siteBased',$siteBased)
            ->with('role',$role);
            //return $zones;
       }
       else if($status_code == 204)
       {
           return view('Locations.zone')->with('message','No Content')
           ->with('zones',$zones)->with('sites',$sites)
           ->with('permission',$permission)
           ->with('systemClient',$systemClient)->with('siteBased',$siteBased)
           ->with('role',$role);
       }
       else
       {
           return Redirect::route('login');
       }
    }
    public function create(Request $request){
      $fields = array();
      $fields['ZONE_name'] = $request->name;
        if($request->has('site'))
            $fields['ZONE_site']= $request->site;
      $url = $this->url . '/zone/create';   
      $response = HttpRequest::post($url ,$fields);
      $statusCode = $response['info']['http_code'];
      $body = json_decode($response['body'], true);
      if($statusCode == 400)
      {
        return redirect('/zone') 
                -> with('message',$body['errors'])->with('messagetype','danger');
      }
      elseif($statusCode == 201)
      {
        return redirect('/zone') 
                -> with('message',$body)->with('messagetype','success');
      }
      elseif($statusCode == 500)
      {
        return redirect('/zone') 
                -> with('message', $body)->with('messagetype','danger');
      }

    }
    function update(Request $request)
    {
        $fields =array(
          'id'=>$request->id,
          'ZONE_name'=>$request->zone_name,
          'ZONE_site'=>$request->zone_site
          );
        //return $request;
        $url = $this->url . '/zone/update';   
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
         if($statusCode == 400)
      {
        return redirect('/zone') 
                -> with('message', $body['errors'])->with('messagetype','danger');
      }
      elseif($statusCode == 200)
      {
        return redirect('/zone') 
                -> with('message', $body)->with('messagetype','success');
      }
      elseif($statusCode == 500)
      {
        return redirect('/zone') 
                -> with('message', $body)->with('messagetype','danger');
      }

    }
    function delete(Request $request)
    {
      $fields =array(
          'id'=>$request->id);
      $url = $this->url . '/zone/delete';   
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
         if($statusCode == 400)
      {
        return redirect('/zone') 
                -> with('message', $body['errors'])->with('messagetype','danger');
      }
      elseif($statusCode == 200)
      {
        return redirect('/zone') 
                -> with('message', $body)->with('messagetype','success');
      }
      elseif($statusCode == 500)
      {
        return redirect('/zone') 
                -> with('message', $body)->with('messagetype','danger');
      }

    }
    //get Zones for View
    public function getZoneArray($response){
        $zoneArray = array();
        if($response['body'] != null){
            $body = $response['body'];
            $someArray = json_decode($body, true);
            if($someArray['zones']!=null) {
                $counter = 0;
                foreach ($someArray['zones'] as $key => $value) {
                    $zoneArray[$counter]['id'] = $value['zone']['id'];
                    $zoneArray[$counter]['ZONE_name'] = $value['zone']["ZONE_name"];
                    $zoneArray[$counter]['SITE_name'] = $value["site_name"];
                    $zoneArray[$counter]['ZONE_OWN_id'] = $value['zone']["ZONE_OWN_id"];
                    $counter++;

                }
            }
        }
        return $zoneArray;
    }

    ///get permission
    public function getPermission($response)
    {
        $body = $response['body'];
        $jsonArray = json_decode($body, true);
        return $jsonArray['permission'] ? 'true' : 'false';
    }

    //getSites
    public function getSites($response){
        $siteArray = array();
        if($response['body'] != null){
            $body = $response['body'];
            $someArray = json_decode($body, true);
            $counter = 0;
            return $someArray['sites'];
            foreach ($someArray['sites'] as $key => $value) {
                $siteArray[$counter]['id'] = $value["id"];
                $siteArray[$counter]['SITE_name'] = $value["SITE_name"];
                $counter++;

            }
        }
        return $siteArray;
    }

    public function getprop(Request $request)
    {

       $url = $this->url . '/zoneprop';
       $id = $request->id;
       //return $id;
       $fields = array('id'=>$id);
       $response = HttpRequest::post($url,$fields);
       //return $response['body'];
       $status_code = $response['info']['http_code'];
       //return $status_code;
       if($status_code == 200)
       {
            $body = $response['body'];
            $dataArray = json_decode($body, true);
            $siteArray = array();
            $counter = 0;
            //return $dataArray;
            $counter = 0;
            foreach ($dataArray['sites'] as $key => $value) {
                $siteArray[$counter] = $value["SITE_name"];
                $counter++;

            }
            $dataFrontEndArray = ['zone'=>$dataArray['zones'],'siteArray'=>$siteArray,'admin'=>$dataArray['admin'],'site'=>$dataArray['site'],'zonesite'=>$dataArray['zonesite']];
            return response($dataFrontEndArray);
       }
       else if($status_code == 204)
       {
          $zone = array();
          return response($zone);
       }
       else
       {
           return Redirect::route('login');
       }
     }
    public function getSystemClient($response){
        $body = $response['body'];
        $someArray = json_decode($body, true);
        return $someArray['systemClient'];
    }
    public function siteBased($response){
        $body = $response['body'];
        $someArray = json_decode($body, true);
        return $someArray['siteBased'];
    }
    public function role($response){
        $body = $response['body'];
        $someArray = json_decode($body, true);
        return $someArray['role'];
    }
  }
