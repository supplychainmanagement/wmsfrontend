<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HttpRequest\HttpRequest;
use App\Http\Requests;
use PDF;
use Session;

// Final

class PickOrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function pickorderindex()
    {
    	$url = $this->url . '/issueorders';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if ($status_code == 404) return redirect('/home')->with('message', 'This URI is Not Found')->
        with('messagetype', 'danger');
        if ($status_code == 500) return redirect('/home')->with('message', $body['errors'][0])->with('messagetype', 'danger');
        if ($status_code == 200)
        {
            return View('GoodsOut.pick.pickrequest')->with('date',date("Y-m-d"))->
                with('issueOrders',$body);
        }
    }
    public function getItems(Request $request)
    {
    	$url = $this->url.'/issueorderbyname';
    	$fields = array(
    		'ISO_number' => $request->value
    	);
    	$response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if($status_code == 400)
        {	
        	$return_data = array(
        		'error'=>true,
        		'message'=>$body['errors']
        		);
            return $return_data;
        }
        elseif($status_code == 500)
        {
        	$return_data = array(
        		'error'=>true,
        		'message'=>$body
        		);
            return $return_data;
        }
        else
        {
    		$return_data = array(
    		'error'=>false,
    		'message'=>['Items Pulled'],
    		'data'=>$body
    		);
       		return $return_data;
        }
    }
    function getItemAmount(Request $request)
    {
    	$url = $this->url.'/issueorderitembyname';
    	$fields = array(
    		'ISI_number' => $request->issueorderitemnumber
    	);
    	//return $fields;
    	$response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if($status_code == 400)
        {	
        	$return_data = array(
        		'error'=>true,
        		'message'=>$body['errors']
        		);
            return $return_data;
        }
        elseif($status_code == 500)
        {
        	$return_data = array(
        		'error'=>true,
        		'message'=>$body
        		);
            return $return_data;
        }
        else
        {
    		$return_data = array(
    		'error'=>false,
    		'message'=>['Items Pulled'],
    		'data'=>$body
    		);
       		return $return_data;
        }
    }
    public function getStorageUnitloads(Request $request)
    {
        $url = $this->url.'/storageunitloads';
        $fields = array(
            'ISI_number'=>$request->value
        );
        //return $fields;
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors'],
                'body'=>$body
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>['Items Pulled'],
            'data'=>$body
            );
            return $return_data;
        }
    }

    public function pickInUnitloads(Request $request)
    {
        $url = $this->url.'/matchedgoodsoutunitloads';
        $fields = array(
            'ISI_number'=>$request->value
        );
        //return $fields;
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors'],
                'body'=>$body
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>['Items Pulled'],
            'data'=>$body
            );
            return $return_data;
        }
    }
    public function createPickRequest(Request $request)
    {
        $url = $this->url.'/submitpickrequest';
        $fields = array(
            'ISI_number'=>$request->isinumber,
            'Unitloads_amount_storage_array'=>$request->value[0]['pickout'],
            'Unitloads_amount_goodsout_array'=>$request->value[0]['pickin']
        );
        //return $fields;
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        //return $response;
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors'],
                'body'=>$body
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>['Pick Request Created'],
            );
            return $return_data;
        }
    }
    public function getPickInUnitloadAmount(Request $request)
    {
        $url = $this->url.'/unitloadfreecapacitybyname';
        $fields = array(
            'UL_name'=>$request->value
        );
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors'],
                'body'=>$body
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>['Items Pulled'],
            'data'=>$body
            );
            return $return_data;
        }
    }
    public function getPickOutUnitloadAmount(Request $request)
    {
        $url = $this->url.'/unitloadamountbyname';
        $fields = array(
            'UL_name'=>$request->value
        );
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors'],
                'body'=>$body
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>['Items Pulled'],
            'data'=>$body
            );
            return $return_data;
        }
    }
    public function suggestPickOut(Request $request)
    {
        $url =$this->url.'/setmatchedstorageunitloads';
        $fields = array(
            'ISI_number'=>$request->issueorderitem
        );
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors'],
                'body'=>$body
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>['Items Pulled'],
            'data'=>$body
            );
            return $return_data;
        }
    }
    public function pickRequestIndex()
    {
        $url = $this->url.'/pickrequest';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if ($status_code == 404) return redirect('/home')->with('message', 'This URI is Not Found')->
        with('messagetype', 'danger');
        if ($status_code == 500) return redirect('/home')->with('message', $body['errors'][0])->with('messagetype', 'danger');
        if ($status_code == 200)
        {
            return View('GoodsOut.pick.index')->with('date',date("Y-m-d"))->
                with('pickRequests',$body);
        }
    }
    public function pickRequestItemIndex()
    {
        $url = $this->url.'/pickrequestitem';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if ($status_code == 404) return redirect('/home')->with('message', 'This URI is Not Found')->
        with('messagetype', 'danger');
        if ($status_code == 500) return redirect('/home')->with('message', $body['errors'][0])->with('messagetype', 'danger');
        if ($status_code == 200)
        {
            return View('GoodsOut.pickitem.index')->with('date',date("Y-m-d"))->
                with('pickRequestItems',$body);
        }
    }

    public function pickJobIndex()
    {
        $url = $this->url.'/pickjob';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        $url = $this->url.'/pickjob/generatepickjobname';
        $response = HttpRequest::get($url);
        $PJ_Number = json_decode($response['body'],true);
        $PJ_Number = $PJ_Number[0];
        if ($status_code == 404) return redirect('/home')->with('message', 'This URI is Not Found')->
        with('messagetype', 'danger');
        if ($status_code == 500) return redirect('/home')->with('message', $body['errors'][0])->with('messagetype', 'danger');
        if ($status_code == 200)
        {
            return View('GoodsOut.pickjob.index')->with('date',date("Y-m-d"))->
                with('pickJobs',$body['pickjobs'])->with('pickrequests',$body['pickrequests'])->
                with('PJ_number',$PJ_Number);
        }
    }
    public function pickJobItems(Request $request)
    {
        $url = $this->url.'/getpickrequestbyname';
        $fields = array(
            'PR_number'=>$request->value
        );
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors'],
                'body'=>$body
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>['Items Pulled'],
            'data'=>$body
            );
            return $return_data;
        }
    }
    public function createPickJob(Request $request)
    {
        $url = $this->url.'/submitpickjob';
        $fields = array(
            'PRI_number'=>$request->pickrequestitem,
            'PJ_number'=>$request->PJ_number
        );
         $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if ($status_code == 404) return redirect('/pickorder/pickjobindex')->with('message', 'This URI is Not Found')->
        with('messagetype', 'danger');
        if ($status_code == 500) return redirect('/pickorder/pickjobindex')->with('message', $body)->with('messagetype', 'danger');
        if ($status_code == 400) return redirect('/pickorder/pickjobindex')->with('message', $body['errors'])->with('messagetype', 'danger');
        if ($status_code == 201)
        {
              return redirect('/pickorder/pickjobindex') 
                    -> with('message', 'Pick Job Created')->with('messagetype', 'success');
        }
    }
    public function pdf(Request $request)
    {
        $url = $this->url .'/getpickrequestforprint';
        $fields = array('PR_number'=>$request->PR_number);
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        //return $response;
        if($status_code == 200)
        {
            $this->CreatePDF($body);
        }
        else{
            return redirect()->route('pickorder.pickrequestindex');
        }
    }
    public function CreatePDF($response)
    {
        PDF::setAuthor('me-wims');
        PDF::SetTitle($response['pickrequest']['PR_number']);
        PDF::SetSubject('PickRequest Order');
        PDF::SetMargins(15,10,10,true);
        PDF::setFooterCallback(function($pdf) {
             $pdf->SetY(-15);
            $pdf->writeHTML('<h5>Printed at '.date("Y-m-d").'</h5>',true,false,false,false,'R');
            $pdf->writeHTML('<h5>Powered By Micro Engineering roboVics<h5>',true,false,false,false,'R');
        });
                //PDF::Footer();
        PDF::AddPage();
        $style = array(
            'position' => 'L',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );
        PDF::writeHtml('<h1>'.Session::get('warehouseName').' warehouse'.'</h1>',true,false,false,false,'C');
        PDF::writeHtml('<h3>'.$response['SITE_name'].'</h3>',true,false,false,false,'C');
        PDF::writeHtml('<h4>16 Abd El Razk el Sanhoury,Cairo,Egypt</h4>',true,false,false,false,'C');
        PDF::writeHTml('</br><h2>PickRequest</h2>',true,false,false,false,'C');
         PDF::writeHTML('<br/>',true,false,false,false,'R');
        PDF::writeHTML('<strong>Pick Request:</strong>'.$response['pickrequest']['PR_number'].'<br/><br/>',false,false,false,false,'L');
        //PDF::writeHTML('<strong>Pick Request Date:</strong>'.$response['pickrequest']['PR_date'].'<br/><br/>',false,false,false,false,'R');

        PDF::write1DBarcode($response['pickrequest']['PR_number'].$response['pickrequest']['id'], 'C128', '', '', '', 18, 0.4, $style, '');

        //PDF::writeHTML('<strong>Issue Order Item:</strong>'.$response['ISI_number'],false,false,false,false,'R');
        PDF::writeHTML('<br/><br/>',true,false,false,false,'R');
        PDF::writeHTML('<hr/>',false,false,false,false,'R');
        PDF::writeHTML('<br/>',true,false,false,false,'R');
        PDF::writeHTML('<h4>info:</h4><br/>',true,false,false,false,'L');
        PDF::writeHTML('
                        <table>
                            <tr>
                                <td>Issue Order:</td>
                                <td>'.$response['ISO_number'].'<br/></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Issue Order Item:</td>
                                <td>'.$response['ISI_number'].'<br/></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Pick Request Date:</td>
                                <td>'.$response['pickrequest']['PR_date'].'<br/></td>
                                <td></td>
                            </tr>
                        </table>
                        ',false,false,false,false,'L');
        PDF::writeHTML('<br/>',true,false,false,false,'R');
        PDF::writeHTML('<hr/>',false,false,false,false,'R');
         PDF::writeHTML('<br/>',true,false,false,false,'R');
        PDF::writeHTML('<h4>Pick Out Data:</h4><br/>',true,false,false,false,'L');
        PDF::writeHTML('<br/>',true,false,false,false,'R');
        $tablehtml = '<table>
                          <tr>
                            <th><strong>Unitload</strong></th>
                            <th><strong>Route</strong></th> 
                            <th><strong>Amount</strong></th>
                          </tr>
                          <br/>';
        $tabledata ='';
        $totalamount = 0;
        for($i=0 ;$i<sizeof($response['pickrequestoutitems_data']);$i++)
        {
            $tabledata .= '<tr>'
                .'<td>'.$response['pickrequestoutitems_data'][$i]['UL_name'].'</td>'
                .'<td>'.$response['pickrequestoutitems_data'][$i]['UL_route'].'</td>'
                .'<td>'.$response['pickrequestoutitems_data'][$i]['pickrequestitem']['PRI_amount'].'</td>'
                .'</tr>';

                $totalamount += $response['pickrequestoutitems_data'][$i]['pickrequestitem']['PRI_amount'];
        }
        $tabledata .='</table>';
        $tablehtml .= $tabledata;
        PDF::writeHTML($tablehtml,false,false,false,false,'');
        PDF::writeHTML('<br/>',true,false,false,false,'R');
        PDF::writeHTML('<hr/>',true,false,false,false,'R');
        PDF::writeHTML('<h4>Total Amount: '.$totalamount.'</h4><br/>',true,false,false,false,'R');
        //PDF::writeHTML('<br/>',true,false,false,false,'R');
        PDF::writeHTML('<hr/>',true,false,false,false,'R');

        PDF::writeHTML('<h4>Pick In Data:</h4><br/>',true,false,false,false,'L');
        PDF::writeHTML('<br/>',true,false,false,false,'R');
        $tablehtml = '<table>
                          <tr>
                            <th><strong>Unitload</strong></th>
                            <th><strong>Route</strong></th> 
                            <th><strong>Amount</strong></th>
                          </tr>
                          <br/>';
        $tabledata ='';
        $totalamount = 0;
        for($i=0 ;$i<sizeof($response['pickrequestinitems_data']);$i++)
        {
            $tabledata .= '<tr>'
                .'<td>'.$response['pickrequestinitems_data'][$i]['UL_name'].'</td>'
                .'<td>'.$response['pickrequestinitems_data'][$i]['UL_route'].'</td>'
                .'<td>'.$response['pickrequestinitems_data'][$i]['pickrequestitem']['PRI_amount'].'</td>'
                .'</tr>';

                $totalamount += $response['pickrequestinitems_data'][$i]['pickrequestitem']['PRI_amount'];
        }
        $tabledata .='</table>';
        $tablehtml .= $tabledata;
        PDF::writeHTML($tablehtml,false,false,false,false,'');
        PDF::writeHTML('<br/>',true,false,false,false,'R');
        PDF::writeHTML('<hr/>',true,false,false,false,'R');
        PDF::writeHTML('<h4>Total Amount: '.$totalamount.'</h4><br/>',true,false,false,false,'R');
        PDF::Output($response['pickrequest']['PR_number'].'_'.$response['pickrequest']['id'].'.pdf');
    }
    
}
