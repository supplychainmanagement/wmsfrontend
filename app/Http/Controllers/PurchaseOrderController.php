<?php
/**
 * Created by PhpStorm.
 * User: Khaled
 * Date: 8/10/2016
 * Time: 9:04 AM
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HttpRequest\HttpRequest;
use App\Http\Requests;
use PDF;
use Session;

class PurchaseOrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function create(Request $request)
    {
        $url = $this->url.'/createpurchaseorder';
        $fields = $request->all();
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        if($status_code == 201)return redirect('/purchaseorderitem/create')->with('message','Purchase Order Has Been Created Successfully')
            ->with('messagetype','success');
        else return redirect('/purchaseorder')->with('message',$body['errors'][0])
            ->with('messagetype','danger');
    }

    // public function createIndex()
    // {
    //     $url = $this->url.'/purchaseorderdata';
    //     $response = HttpRequest::get($url);
    //     $status_code = $response['info']['http_code'];
    //     $body = json_decode($response['body'],true);
    //     if($status_code == 200)return View('GoodsIn.PurchaseOrder.create')->with('client_names',$body['client_names'])
    //         ->with('site_names',$body['site_names'])->with('date',date("Y-m-d"));
    //     else return View('GoodsIn.Purchase.index');
    // }

    public function index($PO_number = null)
    {
        $url = $this->url . '/purchaseorders';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if ($status_code == 404) return redirect('/home')->with('message', 'This URI is Not Found')->
        with('messagetype', 'danger');
        if ($status_code == 500) return redirect('/home')->with('message', $body['errors'][0])->with('messagetype', 'danger');
        if ($status_code == 200)
        {
            return View('GoodsIn.PurchaseOrder.index')->with('date',date("Y-m-d"))->
                with('purchaseOrders',$body)->with('PO_number',$PO_number);
        }
    }

     public function createorderindex()
    {
        //get site client and strategys
        $url = $this->url.'/purchaseorderdata';
        //return $url;
        $response = HttpRequest::get($url);
        $purchaseOrderData_status_code = $response['info']['http_code'];
        $purchaseOrderDataBody = json_decode($response['body'], true);
        //get purchase order names
        $url = $this->url.'/purchaseordernames';
        $response = HttpRequest::get($url);
        $purchaseOrderNames_status_code = $response['info']['http_code'];
        $purchaseOrderNamesBody = json_decode($response['body'], true);


        if ($purchaseOrderData_status_code == 404 || $purchaseOrderNames_status_code == 404) return redirect('/home')->with('message', 'This URI is Not Found')->
        with('messagetype', 'danger');
        if ($purchaseOrderData_status_code == 500 || $purchaseOrderNames_status_code == 500) return redirect('/home')->with('message', $body['errors'][0])->with('messagetype', 'danger');
        if ($purchaseOrderData_status_code == 200 && $purchaseOrderNames_status_code == 200)
        {
            return View('GoodsIn.PurchaseOrderWizard.wizard')->with('client_names',$purchaseOrderDataBody['client_names'])
            ->with('site_names',$purchaseOrderDataBody['site_names'])->with('purchaseorder_names',$purchaseOrderNamesBody)->with('date',date("Y-m-d"));
        }
    }
    public function getEntry(Request $request)
    {
        $url = $this->url.'/purchaseorderbynamecreate';
        $fields = array(
            'PO_number'=> $request->value
        );
        //return $url;
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        $return_data = array();
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors']
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>'Purchase Order Data Pulled',
            'purchaseorder'=>$body
            );
            return $return_data;
        }
    }

    public function CreatePDF($response)
    {
        //return $response;

        PDF::setAuthor('me-wims');
        PDF::SetTitle($response['purchaseorder']['PO_number']);
        PDF::SetSubject('Purchase Order');
        PDF::SetMargins(15,10,10,true);
        PDF::setFooterCallback(function($pdf) {
            $pdf->SetY(-15);
            $pdf->writeHTML('<h5>Printed at '.date("Y-m-d").'</h5>',true,false,false,false,'R');
            $pdf->writeHTML('<h5>Powered By Micro Engineering roboVics<h5>',true,false,false,false,'R');
        });
        PDF::AddPage();
        $style = array(
            'position' => 'L',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );
        PDF::writeHtml('<h1>'.Session::get('warehouseName').' warehouse'.'</h1>',true,false,false,false,'C');
        PDF::writeHtml('<h3>'.$response['sitename'].'</h3>',true,false,false,false,'C');
        PDF::writeHtml('<h4>16 Abd El Razk el Sanhoury,Cairo,Egypt</h4>',true,false,false,false,'C');
        PDF::writeHTml('</br><h2>PurchaseOrder</h2>',true,false,false,false,'C');
        PDF::writeHTML('<br/>',true,false,false,false,'R');
        PDF::writeHTML('<strong>Purchase Order:</strong>'.$response['purchaseorder']['PO_number'],false,false,false,false,'L');
        PDF::writeHTML('<strong>Delivery Date:</strong>'.$response['PO_dateOfDelivery'].'<br/><br/>',false,false,false,false,'R');
        PDF::write1DBarcode($response['purchaseorder']['PO_number'].$response['purchaseorder']['id'], 'C128', '', '', '', 18, 0.4, $style, 'N');
        PDF::writeHTML('<br/>',true,false,false,false,'R');
        PDF::writeHTML('<hr/>',false,false,false,false,'R');
        PDF::writeHTML('<br/>',true,false,false,false,'R');
        PDF::writeHTML('<h4>To:</h4><br/>',true,false,false,false,'L');
        PDF::writeHTML('
                        <table>
                            <tr>
                                <td>Client Company:</td>
                                <td>'.$response['client']['CLI_name'].'<br/></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Address:</td>
                                <td>'.$response['client']['CLI_address'].'<br/></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Phone:</td>
                                <td>'.$response['client']['CLI_phone'].'<br/></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Email:</td>
                                <td>'.$response['client']['CLI_email'].'</td>
                                <td></td>
                            </tr>
                        </table>
                        ',false,false,false,false,'L');
        PDF::writeHTML('<br/>',true,false,false,false,'R');
        PDF::writeHTML('<hr/>',false,false,false,false,'R');
        PDF::writeHTML('<br/>',true,false,false,false,'R');
        $tablehtml = '<table>
                          <tr>
                            <th><strong>Purchase Order Item</strong></th>
                            <th><strong>SKU</strong></th> 
                            <th><strong>SKU number</strong></th>
                            <th><strong>Amount</strong></th>
                            <th><strong>Provider</strong></th>
                          </tr>
                          <br/>';
        $tabledata ='';

        for($i=0 ;$i<sizeof($response['purchaseorderitems_names']);$i++)
        {
            $tabledata .= '<tr>'
                .'<td>'.$response['purchaseorderitems_names'][$i]['name'].'</td>'
                .'<td>'.$response['purchaseorderitems_names'][$i]['sku'].'</td>'
                .'<td>'.$response['purchaseorderitems_names'][$i]['skunumber'].'</td>'
                .'<td>'.$response['purchaseorderitems_names'][$i]['amount'].'</td>'
                .'<td>'.$response['purchaseorderitems_names'][$i]['provider'].'</td>'
                .'</tr>';
        }
        $tabledata .='</table>';
        $tablehtml .= $tabledata;
        PDF::writeHTML($tablehtml,false,false,false,false,'');
        PDF::writeHTML('<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>',true,false,false,false,'R');
        PDF::Output($response['purchaseorder']['PO_number'].'.pdf');
    }
    public function pdf(Request $request)
    {
        //return 'i am here';
        $url = $this->url .'/purchaseorderbyname';
        $fields = ['PO_number'=>$request->PO_number];
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        //return $response['body'];
        if($status_code == 200)
        {
            $this->CreatePDF($body);
        }
        else{
            return redirect()->route('purchaseorder');
        }
        

    }
    public function getProviders(Request $request)
    {
        $url = $this->url.'/clientproviders';
        $fields = array(
            'CLI_name'=>$request->value
        );
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        $return_data = array();
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors']
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            if(sizeof($body)>0){
                $return_data = array(
                'error'=>false,
                'message'=>'Providers for this Client have Been Pulled',
                'providers'=>$body
                );
                return $return_data;
            }
            else
            {
                $return_data = array(
                'error'=>true,
                'message'=>'No Providers For this client'
                );
                return $return_data;
            }
            
        }
    }
    public function save(Request $request)
    {
        $purchaseorder = $request->purchaseorder;

        if($purchaseorder == 'New Purchase Order')
        {
            $url = $this->url.'/createpurchaseorder';
            $fields = array(
                'SITE_name'=>$request->site,
                'CLI_name'=>$request->client,
                'PO_dateOfDelivery'=>$request->dateofdelivery
            );
            $response = HttpRequest::post($url,$fields);
            $status_code = $response['info']['http_code'];
            $body = json_decode($response['body'], true);
            $return_data = array();
            if($status_code == 400)
            {   
                $return_data = array(
                    'error'=>true,
                    'message'=>$body['errors']
                    );
                return $return_data;
            }
            elseif($status_code == 500)
            {
                $return_data = array(
                    'error'=>true,
                    'message'=>$body
                    );
                return $return_data;
            }
            else
            {
                $return_data = array(
                'error'=>false,
                'message'=>'Save Successfull You May Proceed to Add items to this Purchase order',
                'purchaseorder'=>$body,
                'state'=>'new'
                );
                return $return_data;
            }
        }
        else
        {
            $url = $this->url.'/purchaseorder/update';
            $fields = array(
                'SITE_name'=>$request->site,
                'CLI_name'=>$request->client,
                'PO_dateOfDelivery'=>$request->dateofdelivery,
                'PO_number'=>$request->purchaseorder
            );
            
            $response = HttpRequest::post($url,$fields);
            $status_code = $response['info']['http_code'];
            $body = json_decode($response['body'], true);
            $return_data = array();
            if($status_code == 400)
            {   
                $return_data = array(
                    'error'=>true,
                    'message'=>$body['errors']
                    );
                return $return_data;
            }
            elseif($status_code == 500)
            {
                $return_data = array(
                    'error'=>true,
                    'message'=>$body
                    );
                return $return_data;
            }
            else
            {
                $return_data = array(
                'error'=>false,
                'message'=>'Update Successfull You May Proceed to Add items to this Purchase order',
                'purchaseorder'=>$body,
                'state'=>'old'
                );
                return $return_data;
            }

        } 
    }
    public function getSKUs(Request $request)
    {
        $url = $this->url.'/clientskus';
        $fields = array(
            'CLI_name'=>$request->value
        );
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        $return_data = array();
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors']
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            if(sizeof($body)>0){
                $return_data = array(
                'error'=>false,
                'message'=>'SKUs for this Client have Been Pulled',
                'skus'=>$body
                );
                return $return_data;
            }
            else
            {
                $return_data = array(
                'error'=>true,
                'message'=>'No SKUs For this client'
                );
                return $return_data;
            }
            
        }
    }
    public function saveItem(Request $request)
    {
        $url = $this->url.'/createpurchaseorderitem';
        $fields = array(
            'PO_number'=>$request->purchaseordername,
            'POI_amount'=>$request->amount,
            'SKU_name'=>$request->sku,
            'PRV_name'=>$request->provider,
        );
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        $return_data = array();
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors']
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            
                $return_data = array(
                'error'=>false,
                'message'=>'Purchase Item Added',
                'purchaseitem'=>$body
                );
                return $return_data;    
        }
    }
    public function removeItem(Request $request)
    {

    }
    public function finishPurchaseOrder(Request $request)
    {
        return redirect()->route('purchaseorder',array('search'=>$request->purchaseordernamex))->with('message','Purchase Order Created Successfully')->with('messagetype','success');
    }
}