<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HttpRequest\HttpRequest;
use App\Http\Requests;
use Illuminate\Http\Response as httpResponse;

class RegisterController extends Controller
{
    public function __construct()
    {
      parent::__construct();
    }
    public function index()
    {
      return View('main&templates.register');
    }

    public function supportIndex()
    {
      return View('main&templates.supportcenter');
    }
	/**
     * Register warehouse.
     *
     * @param  Array containing data for registeration
     * @return error or redirect to subdomain
     */
    public function register(Request $request){
    	//register post url
    	$url = $this->url . '/register';

    	//fields that will be sent to backend when registering
    	$fields = array(
           'email' => $request->email,
           'password' => $request->password,
           'username'=>$request->password,
           'firstName'=>$request->firstname,
           'lastName'=>$request->lastname,
           'warehouseName'=>$request->warehouseName,
           'phone'=>$request->phone,
           'address'=>$request->address
       	);

    	$response = HttpRequest::post($url ,$fields);
    	$statusCode = $response['info']['http_code'];
    	$body = json_decode($response['body'], true);
    	if($statusCode == httpResponse::HTTP_INTERNAL_SERVER_ERROR)
    	{
    		return $body;
    		//return redirect('/')->with('message','We have encountered an error Saving this data, try again later');
    	}
    	else if($statusCode == httpResponse::HTTP_BAD_REQUEST)
    	{
    		return redirect('/')->with('errors',$body['errors']);
    	}
    	else if($statusCode == httpResponse::HTTP_CREATED)
    	{
    		//redirect to the subdomain of the created warehouse
    		$link = url('');
            $basiclink = str_replace('www.','',$link);
            $subdomainlink = str_replace('http://','http://'.$request->warehouseName.'.',$basiclink);
            return redirect($subdomainlink)->with('username',$request->username)->with('password',$request->password);
    	}
    }
    public function verifyCode(Request $request)
    {
       $url = $this->url.'/verifycode';//activatecode
        $fields = array(
            'code'=> $request->code
        );
        //return $url;
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        $return_data = array();
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors']
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>'code verified',
            'data'=>$body
            );
            return $return_data;
        }
    }
    public function invitation(Request $request)
    {
        $url = $this->url.'/activatecode';//activatecode
        $fields = array(
            'email'=> $request->emailjoin,
            'warehouseName'=>$request->warehouseName,
            'password'=>$request->password,
            'username'=>$request->username,
            'code'=>$request->codehidden
        );
        //return $url;
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        $return_data = array();
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors']
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>'Warehouse Created',
            'data'=>$body
            );
            return $return_data;
        }
    }
    public function RFQ(Request $request)
    {
      $url = $this->url.'/createrfq';//activatecode
        $fields = array(
            'RFQ_name'=> $request->name,
            'RFQ_email'=>$request->emailrfq,
            'RFQ_phone'=>$request->phone,
            'RFQ_companyName'=>$request->companyName,
            'RFQ_industry'=>$request->industry,
            'RFQ_ERPPlatform'=>$request->ERPPlatform,
            'RFQ_noOfSites'=>$request->noOfSites,
            'RFQ_numberOfEmployees'=>$request->noOfEmployees,
            'RFQ_noOfOperationsPerMonth'=>$request->noOfTransactionsPerMonth
        );
        //return $url;
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        $return_data = array();
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors']
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>'RFQ sent',
            'data'=>$body
            );
            return $return_data;
        }
    }
}
