<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HttpRequest\HttpRequest;
use App\Http\Requests;
use Session;
use Illuminate\Http\Response as httpResponse;

class SpriteController extends Controller
{
  	public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function index()
    {

                $url = $this->url.'/getsitesmonitoring';
                $response = HttpRequest::get($url);
                $status_code = $response['info']['http_code'];
                $body = json_decode($response['body'], true);
                if ($status_code == 404)
                {
                    return view('sprite.sprite')->with('message','url not found');
                }
                if ($status_code == 500)
                {
                     return view('sprite.sprite')->with('message','Internal Server Error');
                }
                if ($status_code == 200)
                {
                     return view('sprite.sprite')->with('sites',$body);
                }


    }
    public function getWarehouseData(Request $request)
    {
    	
    	$url = $this->url.'/getwarehousedata';
        $fields = array(
            'sitename'=>$request->sitename
        );
    	$response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        //return $response['body'];
        $body = json_decode($response['body'], true);
        if ($status_code == 404)
        {
        	$return_data = array(
        		'error'=>true,
        		'message'=>'Not Found'
        		);
            return $return_data;
        }
        if ($status_code == 500)
        {
        	$return_data = array(
        		'error'=>true,
        		'message'=>'Internal Server Error'
        		);
            return $return_data;
        }
        if ($status_code == 200)
        {
            $return_data = array(
        		'error'=>false,
        		'message'=>'Data Pulled',
        		'data'=>$body
        		);
            return $return_data;
        }
    }
    public function getUnitloadLocation(Request $request)
    {
        $url = $this->url.'/getunitloadlocation';
        $fields = array(
            'sitename'=>$request->sitename,
            'zonename'=>$request->zonename,
            'rackname'=>$request->rackname
        );
        //return $fields;
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if ($status_code == 404)
        {
            $return_data = array(
                'error'=>true,
                'message'=>'Not Found'
                );
            return $return_data;
        }
        if ($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>'Internal Server Error'
                );
            return $return_data;
        }
        if ($status_code == 200)
        {
            $return_data = array(
                'error'=>false,
                'message'=>'Data Pulled',
                'data'=>$body
                );
            return $return_data;
        }

    }
}
