<?php

namespace App\Http\Controllers;

use App\HttpRequest\HttpRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class CapacityLocationController extends Controller
{
   public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function index()
    {
    	$url = $this->url . '/capacitylocation';
    	$return_code = $this->return_code;
       $response = HttpRequest::get($url);
       $status_code = $response['info']['http_code'];
       if($status_code != $return_code['ok'] && $status_code != $return_code['no_content'])
       {
       		return Redirect::route('login');
       }
       elseif($status_code == $return_code['no_content'])
       {
       	return view('capacities.capacitylocation')->with('message','No Content')
       	->with('capacitylocations',array())->with('unitloadtypes',array())
       	->with('storagelocationtypes',array());
       }
       else
       {
       	$capacitylocations = array();
       	$body = $response['body'];
       	$backendArray = json_decode($body, true);
       	$counter = 0;
        foreach ($backendArray['CapacityLocation'] as $key => $value) {
            $capacitylocations[$counter]['id'] = $value['capacity']['id'];
            $capacitylocations[$counter]['unitloadtype_name'] = $value["CL_ULT_Type_name"];
            $capacitylocations[$counter]['storagelocationtype_name'] = $value["CL_STL_Type_name"];
            $capacitylocations[$counter]['allocation'] = $value['capacity']["CL_allocation"];
            $capacitylocations[$counter]['created_at'] = $value['capacity']['created_at'];
            $capacitylocations[$counter]['updated_at'] = $value['capacity']['updated_at'];
            $counter++;

        }

        $unitloadtypes = array();
        $counter = 0;
        foreach ($backendArray['ULT'] as $key => $value) {
        	$unitloadtypes[$counter] = $value['ULT_Type_name'];
        	$counter++;
        }

        $storagelocationtypes = array();
        $counter = 0;
        foreach ($backendArray['STLT'] as $key => $value) {
        	$storagelocationtypes[$counter] = $value['STL_Type_name'];
        	$counter++;
        }



    	return view('capacities.capacitylocation')->with('capacitylocations',$capacitylocations)
    	->with('unitloadtypes',$unitloadtypes)
       	->with('storagelocationtypes',$storagelocationtypes);
       }
    }

    public function create(Request $request)
    {
    	$fields = array();
      	$fields['CL_ULT_Type_name'] = $request->unitloadtype_name;
      	$fields['CL_STL_Type_name'] = $request->storagelocationtype_name;
      	$fields['CL_allocation'] = $request->allocation;

      	$url = $this->url . '/capacitylocation/create'; 
      	$return_code = $this->return_code;
      	$response = HttpRequest::post($url ,$fields);
      	$statusCode = $response['info']['http_code'];

      	if($statusCode == $return_code['bad_request'])
		{
			return redirect('/capacitylocation') 
			        -> with('message', $response['body'])->with('messagetype','danger');
		}
		elseif($statusCode == $return_code['internal_server_error'])
		{
			return redirect('/capacitylocation') 
			        -> with('message', 'Internal Server Error')->with('messagetype','danger');
		}  
		elseif($statusCode == $return_code['created'])
		{
			return redirect('/capacitylocation') 
			        -> with('message', 'Capacity of Location Rule Successfully Created')->with('messagetype','success');
		}  
    }

    public function update(Request $request)
    {
    	$fields = array();
    	$fields['id'] = $request->id;
      	$fields['ULT_Type_name'] = $request->unitloadtype_name;
      	$fields['STL_Type_name'] = $request->storagelocationtype_name;
      	$fields['allocation'] = $request->allocation;
	
        $url = $this->url . '/capacitylocation/update'; 
        $return_code = $this->return_code;  
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];

        if($statusCode == $return_code['bad_request'])
		{
			return redirect('/capacitylocation') 
			        -> with('message', $response['body'])->with('messagetype','danger');
		}
		elseif($statusCode == $return_code['internal_server_error'])
		{
			return redirect('/capacitylocation') 
			        -> with('message', 'Internal Server Error')->with('messagetype','danger');
		}  
		elseif($statusCode == $return_code['ok'])
		{
			return redirect('/capacitylocation') 
			        -> with('message', 'Capacity of Location Rule Successfully Updated')->with('messagetype','success');
		}         	
    }

    public function delete(Request $request)
    {
    	$fields =array(
          'id'=>$request->id);
      	$url = $this->url . '/capacitylocation/delete';  
      	$return_code = $this->return_code; 
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];

    	if($statusCode == $return_code['bad_request'])
  		{
  			return redirect('/capacitylocation') 
  			        -> with('message', $response['body'])->with('messagetype','danger');
  		}
  		elseif($statusCode == $return_code['internal_server_error'])
  		{
  			return redirect('/capacitylocation') 
  			        -> with('message', 'Internal Server Error')->with('messagetype','danger');
  		}  
  		elseif($statusCode == $return_code['ok'])
  		{
  			return redirect('/capacitylocation') 
  			        -> with('message', 'Capacity of Location Rule Successfully Deleted')->with('messagetype','success');
  		}   	
    }
}
