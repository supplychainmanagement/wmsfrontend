<?php

namespace App\Http\Controllers;

use App\HttpRequest\HttpRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class FunctionalAreaController extends Controller
{
  public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function index()
    {    	
    	$url = $this->url . '/functionalarea';
    	$return_code = $this->return_code;
       $response = HttpRequest::get($url);
       $status_code = $response['info']['http_code'];
       if($status_code != $return_code['ok'] && $status_code != $return_code['no_content'])
       {
       		return Redirect::route('login');
       }
       elseif($status_code == $return_code['no_content'])
       {
       	return view('Locations.functionalarea')->with('message','No Content')
       	->with('functionalareas',array());
       }
       else
       {
       	$functionalareas = array();
       	$body = $response['body'];
       	$backendArray = json_decode($body, true);
       	$counter = 0;
        foreach ($backendArray['functionalArea'] as $key => $value) {
            $functionalareas[$counter]['id'] = $value['id'];
            $functionalareas[$counter]['area_name'] = $value["FA_name"];
            $functionalareas[$counter]['goodsin'] = $value["FA_GoodsIn"];
            $functionalareas[$counter]['goodsout'] = $value["FA_GoodsOut"];
            $functionalareas[$counter]['storage'] = $value["FA_Storage"];
            $functionalareas[$counter]['created_at'] = $value['created_at'];
            $functionalareas[$counter]['updated_at'] = $value['updated_at'];
            $counter++;

        }


        return view('Locations.functionalarea')
        ->with('functionalareas',$functionalareas);
       }
    }

    public function create(Request $request)
    {
    	$fields = array();
      	$fields['FA_name'] = $request->area_name;
      	$fields['FA_GoodsIn']  = 0;
      	$fields['FA_GoodsOut']  = 0;
      	$fields['FA_Storage']  = 0;
		if(isset($_POST['goodsin']))
                {$fields['FA_GoodsIn']  = 1;}
        if(isset($_POST['goodsout']))
            {$fields['FA_GoodsOut']  = 1;}
        if(isset($_POST['storage']))
            {$fields['FA_Storage']  = 1;}

      	$url = $this->url . '/functionalarea/create'; 
      	$return_code = $this->return_code;
      	$response = HttpRequest::post($url ,$fields);
      	$statusCode = $response['info']['http_code'];

      	if($statusCode == $return_code['bad_request'])
    		{
    			return redirect('/functionalarea') 
    			        -> with('message', $response['body'])->with('messagetype','danger');
    		}
    		elseif($statusCode == $return_code['internal_server_error'])
    		{
    			return redirect('/functionalarea') 
    			        -> with('message', 'Internal Server Error')->with('messagetype','danger');
    		}  
    		elseif($statusCode == $return_code['created'])
    		{
    			return redirect('/functionalarea') 
    			        -> with('message', 'Functional Area Successfully Created')->with('messagetype','success');
    		}    	
    }

    public function update(Request $request)
    {
    	$fields =array();
    	$fields['id'] = $request->id;
    	$fields['FA_name'] = $request->area_name;
    	$fields['FA_GoodsIn']  = 0;
      	$fields['FA_GoodsOut']  = 0;
      	$fields['FA_Storage']  = 0;
      	if(isset($_POST['goodsin']))
                {$fields['FA_GoodsIn']  = 1;}
        if(isset($_POST['goodsout']))
            {$fields['FA_GoodsOut']  = 1;}
        if(isset($_POST['storage']))
            {$fields['FA_Storage']  = 1;}

        $url = $this->url . '/functionalarea/update'; 
        $return_code = $this->return_code;  
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];

        if($statusCode == $return_code['bad_request'])
    		{
    			return redirect('/functionalarea') 
    			        -> with('message', $response['body'])->with('messagetype','danger');
    		}
    		elseif($statusCode == $return_code['internal_server_error'])
    		{
    			return redirect('/functionalarea') 
    			        -> with('message', 'Internal Server Error')->with('messagetype','danger');
    		} 
    		elseif($statusCode == $return_code['ok'])
    		{
    			return redirect('/functionalarea') 
    			        -> with('message', 'Functional Area Successfully Updated')->with('messagetype','success');
    		}    

    }

    public function delete(Request $request)
    {

    	$fields =array(
          'id'=>$request->id);
      	$url = $this->url . '/functionalarea/delete';  
      	$return_code = $this->return_code; 
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];

    		if($statusCode == $return_code['bad_request'])
    		{
    			return redirect('/functionalarea') 
    			        -> with('message', $response['body'])->with('messagetype','danger');
    		}
    		
    		elseif($statusCode == $return_code['internal_server_error'])
    		{
    			return redirect('/functionalarea') 
    			        -> with('message', 'Internal Server Error')->with('messagetype','danger');
    		}
    		elseif($statusCode == $return_code['ok'])
    		{
    			return redirect('/functionalarea') 
    			        -> with('message', 'Functional Area Successfully Deleted')->with('messagetype','success');
    		}    
        	
    }
}
