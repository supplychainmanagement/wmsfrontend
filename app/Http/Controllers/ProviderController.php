<?php
/**
 * Created by PhpStorm.
 * User: Khaled
 * Date: 8/8/2016
 * Time: 7:33 PM
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HttpRequest\HttpRequest;
use App\Http\Requests;

class ProviderController extends Controller{
    public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function index()
    {
        $url = $this->url.'/provider';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        $providers = $body['providers'];
        $clients = $body['clients'];
        if($status_code == 200 || $status_code == 204)return View('Provider.provider')->with('providers',$providers)
            ->with('clients',$clients);
        else{
            return redirect('/login');
        }

    }
    public function create(Request $request)
    {
        $url = $this->url.'/provider/create';
        $fields = array(
            'PRV_name'=>$request->PRV_name,
            'PRV_phone'=>$request->PRV_phone,
            'PRV_email'=>$request->PRV_email,
            'PRV_address'=>$request->PRV_address,
            'PRV_clients'=>$request->PRV_CLI

        );
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        //return $response['body'];
        if($status_code == 201)return redirect('/provider')->with('message','Provider Has Been Created Successfully')
            ->with('messagetype','success');
        if($status_code == 401)return redirect('/login');
        if($status_code == 400)return redirect('/provider')->with('message',$body['errors'][0])->with('messagetype','danger');
        if($status_code == 500)return redirect('/provider')->with('message','Provider Has Failed To Be Created')
            ->with('messagetype','danger');
    }

    public function update(Request $request)
    {
        $url = $this->url.'/provider/update';
        $fields = array(
            'id'=>$request->id,
            'PRV_name'=>$request->PRV_name,
            'PRV_phone'=>$request->PRV_phone,
            'PRV_email'=>$request->PRV_email,
            'PRV_address'=>$request->PRV_address,
            'PRV_clients'=>$request->PRV_CLI_name
        );
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        //return $response['body'];
        if($status_code == 200)return redirect('/provider')->with('message','Provider Has Been Updated Successfully')
            ->with('messagetype','success');
        if($status_code == 400)return redirect('/provider')->with('message',$body['errors'][0])
            ->with('messagetype','danger');
        if($status_code == 500)return redirect('/provider')->with('message','Provider Has Been Failed To Be Updated')
            ->with('messagetype','danger');
    }

    public function delete(Request $request)
    {
        $url = $this->url.'/provider/delete';
        $fields = ['id'=>$request->id];
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        if($status_code == 200)return redirect('/provider')->with('message','Provider Has Been Deleted Successfully')
            ->with('messagetype','success');
        if($status_code == 400)return redirect('/provider')->with('message','Invalid Index')->with('messagetype','danger');
        if($status_code == 500)return redirect('/provider')->with('message','Provider Has Failed To Be Deleted')
            ->with('messagetype','danger');
    }

    public function getprop(Request $request)
    {
        $url = $this->url.'/provider/providerprop';
        $fields = array(
            'id'=>$request->id,
        );
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        $provider = $body['provider'];
        $clients = $body['clients'];
        $provider_clients = $body['provider_clients'];
        if($status_code == 200)
        {
            $dataFrontEnd = ['id'=>$provider['id'],'provider'=>$provider,'clients'=>$clients,'provider_clients'=>$provider_clients];
            return response($dataFrontEnd);
        }elseif($status_code == 204)
        {
            $dataFrontEnd = array();
            return $dataFrontEnd;
        }else{
            return redirect('/login');
        }
    }
}