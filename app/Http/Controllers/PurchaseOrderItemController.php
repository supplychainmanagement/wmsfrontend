<?php
/**
 * Created by PhpStorm.
 * User: Khaled
 * Date: 8/10/2016
 * Time: 2:21 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HttpRequest\HttpRequest;
use App\Http\Requests;

class PurchaseOrderItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function create(Request $request)
    {
        $url = $this->url.'/createpurchaseorderitem';
        $fields = $request->all();
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        if($status_code == 201)return redirect('/purchaseorderitem')->with('message','Purchase Order Item Has Been Created Successfully')
            ->with('messagetype','success');
        else return redirect()->route('PurchaseOrderItemCreate')->with('message',$body['errors'][0])
            ->with('messagetype','danger');
    }

    public function index($POI_number = null)
    {
        $url = $this->url . '/purchaseorderitem';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if ($status_code == 404) return redirect('/home')->with('message', 'This URI is Not Found')->
        with('messagetype', 'danger');
        if ($status_code == 500) return redirect('/home')->with('message', $body['errors'][0])->with('messagetype', 'danger');
        if ($status_code == 200)
        {
            $respond = array();
            $counter = 0;
            foreach($body as $POI)
            {
                $respond[$counter]['id'] = $POI['purchaseorderitem']['id'];
                $respond[$counter]['PO_number'] = $POI['purchaseorder_name'];
                $respond[$counter]['SKU_name'] = $POI['sku_name'];
                $respond[$counter]['POI_number'] = $POI['purchaseorderitem']['POI_number'];
                $respond[$counter]['amount'] = $POI['purchaseorderitem']['POI_amount'];
                $respond[$counter]['state'] = $POI['purchaseorderitem']['POI_state'];
                $respond[$counter]['received'] = $POI['purchaseorderitem']['POI_receivedamount'];
                $respond[$counter]['productionDate'] = $POI['purchaseorderitem']['POI_productiondate'];
                $counter++;
            }
            return View('GoodsIn.PurchaseOrderItem.index')->with('data',$respond)->with('POI_number',$POI_number)->with('date',date("Y-m-d"));
        }
    }

    public function createIndex()
    {
        $url = $this->url.'/pois';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        if($status_code == 200)return View('GoodsIn.PurchaseOrderItem.create')->with('PO_names',$body['purchaseorders'])
            ->with('skus',$body['skus'])->with('date',date("Y-m-d"));
        else return View('GoodsIn.PurchaseOrderItem.index');
    }
}