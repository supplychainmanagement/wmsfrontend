<?php
/**
 * Created by PhpStorm.
 * User: Khaled
 * Date: 8/11/2016
 * Time: 10:40 AM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HttpRequest\HttpRequest;
use App\Http\Requests;

class ReceivingJobController extends Controller{
    public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function createIndex()
    {
        // $url = $this->url.'/purchaseordernames';
        // $response = HttpRequest::get($url);
        // $url = $this->url.'/generatereceivingjob';
        // $response2 = HttpRequest::get($url);
        // $RCJ_number = json_decode($response2['body'],true);
        // $RCJ_number = $RCJ_number[0];
        // $PO_names = json_decode($response['body'],true);
        // return View('GoodsIn.ReceivingJob.create')->with('PO_names',$PO_names)->with('RCJ_number',$RCJ_number);


        //get site client and strategys
        $url = $this->url.'/purchaseordernames';
        //return $url;
        $response = HttpRequest::get($url);
        $purchaseOrderNames_status_code = $response['info']['http_code'];
        $purchaseOrderNamesBody = json_decode($response['body'], true);
        //get purchase order names
        $url = $this->url.'/generatereceivingjob';
        $response = HttpRequest::get($url);
        $receivingjob_status_code = $response['info']['http_code'];
        $receivingjob_Body = json_decode($response['body'], true);

        if ($purchaseOrderNames_status_code == 404 || $receivingjob_status_code == 404) return redirect('/home')->with('message', 'This URI is Not Found')->
        with('messagetype', 'danger');
        if ($purchaseOrderNames_status_code == 500 || $receivingjob_status_code == 500) return redirect('/home')->with('message', $body)->with('messagetype', 'danger');
        if ($purchaseOrderNames_status_code == 200 && $receivingjob_status_code == 200)
        {
            return View('GoodsIn.ReceivingJob.create')->with('PO_names',$purchaseOrderNamesBody)->with('RCJ_number',$receivingjob_Body[0]);
        }
    }
    public function index()
    {
        $url = $this->url . '/receivingjob';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        //return $response;
        if ($status_code == 404) return redirect('/home')->with('message', 'This URI is Not Found')->
        with('messagetype', 'danger');
        if ($status_code == 500) return redirect('/home')->with('message', $body)->with('messagetype', 'danger');
        if ($status_code == 200)
        {
            return View('GoodsIn.ReceivingJob.index')->with('receivingJobs',$body);
        }
    }
    public function getPurchaseOrderByName(Request $request)
    {
        // $url = $this->url.'/purchaseorderbyname';
        // $fields = $request->all();
        // $response = HttpRequest::post($url,$fields);
        // $body = json_decode($response['body'],true);
        // return $body['purchaseorderitems_names'];


        $url = $this->url.'/purchaseorderbyname';
        $fields = $request->all();
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        $return_data = array();
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors']
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>['Purchase Order Items Pulled'],
            'data'=>$body['purchaseorderitems_names']
            );
            return $return_data;
        }
    }

    public function getPurchaseOrderItemByName(Request $request)
    {
        $url = $this->url . '/purchaseorderitembyname';
        $fields = $request->all();
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        //return $body;
        $return_data = array();
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors']
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>[['Purchase Order Item Data Pulled']],
            'data'=>$body
            );
            return $return_data;
        }
    }

    public function getMatchedUnitloads(Request $request)
    {
        $url = $this->url.'/receivingjobmatchedunitloads';
        $field = $request->all();
        $response = HttpRequest::post($url,$field);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        //return $body;
        $return_data = array();
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors']
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>[['Matched Unitloads Data Pulled']],
            'data'=>$body
            );
            return $return_data;
        }
    }

    public function getRCJNumber()
    {
        $url = $this->url.'/generatereceivingjob';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        $return_data = array();
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors']
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>[['Receiving Job Number Pulled']],
            'data'=>$body
            );
            return $return_data;
        }

    }

    public function submitReceivingJob(Request $request)
    {
        $url = $this->url.'/submitreceivingjob';
        $fields = $request->all();
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        //if($status_code == 201)return redirect('/home')->with('message','Receiving Job Has Successfully Submitted')
            //->with('messagetype','success');
        //return $status_code.'   '.$response['body'];
        $return_data = array();
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors']
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>[['Receiving Job Has Successfully Submitted']],
            'data'=>$body
            );
            return $return_data;
        }
    }
}