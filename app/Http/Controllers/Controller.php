<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Support\Facades\Session;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;
    public $url = '';
    public function __construct()
    {
        if (strpos(url(''), 'localhost') !== false)
        {
            $this->url = 'http://localhost:8888';
        }
        else
        {
            $this->url = 'http://me-wims.com:9090';
        }
    }

    public $return_code = array(
    	'ok' => 200 , 
    	'no_content' => 204 , 
    	'bad_request' => 400 , 
    	'internal_server_error' => 500 , 
    	'created' => 201  
    	);

    public function getRole()
    {
    	$role = 'none';
        $system_client = false;
    	if (Session::has('user_role')) {
            $role = Session::get('user_role');
        }
        if (Session::has('system_client')) {
            $system_client = Session::get('system_client');
        }
        $data = array('role' =>$role , 'system_client' =>$system_client);
    	return response($data);
    }
}
