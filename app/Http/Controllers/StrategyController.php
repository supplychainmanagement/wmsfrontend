<?php
/**
 * Created by PhpStorm.
 * User: Khaled
 * Date: 8/8/2016
 * Time: 10:12 AM
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HttpRequest\HttpRequest;
use App\Http\Requests;
use Illuminate\Http\Response;

class StrategyController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function index()
    {
        $url = $this->url.'/strategy';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        if($status_code == 200||$status_code==204)
        {
            return View('Strategy.strategy')->with('strategies',$body);
        }else{
            return redirect('/login');
        }
    }
}