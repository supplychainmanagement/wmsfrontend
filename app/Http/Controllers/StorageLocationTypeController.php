<?php

namespace App\Http\Controllers;

use App\HttpRequest\HttpRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class StorageLocationTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function index()
    {    	
    	$url = $this->url . '/storagelocationtype';
    	$return_code = $this->return_code;
       $response = HttpRequest::get($url);
       $status_code = $response['info']['http_code'];
       if($status_code != $return_code['ok'] && $status_code != $return_code['no_content'])
       {
       		return Redirect::route('login');
       }
       elseif($status_code == $return_code['no_content'])
       {
       	return view('types.storagelocationtype')->with('message','No Content')
       	->with('storagelocationtypes',array());
       }
       else
       {
       	$storagelocationtypes = array();
       	$body = $response['body'];
       	$backendArray = json_decode($body, true);
       	$counter = 0;
        foreach ($backendArray['storageLocationType'] as $key => $value) {
            $storagelocationtypes[$counter]['id'] = $value['id'];
            $storagelocationtypes[$counter]['type_name'] = $value["STL_Type_name"];
            $storagelocationtypes[$counter]['created_at'] = $value['created_at'];
            $storagelocationtypes[$counter]['updated_at'] = $value['updated_at'];
            $counter++;

        }


        return view('types.storagelocationtype')
        ->with('storagelocationtypes',$storagelocationtypes);
       }
    }

    public function create(Request $request)
    {
    	$fields = array();
      	$fields['STL_Type_name'] = $request->type_name;

      	$url = $this->url . '/storagelocationtype/create'; 
      	$return_code = $this->return_code;
      	$response = HttpRequest::post($url ,$fields);
      	$statusCode = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
      	if($statusCode == $return_code['bad_request'])
    		{
    			return redirect('/storagelocationtype') 
    			        -> with('message', $body['errors'])->with('messagetype','danger');
    		}
    		elseif($statusCode == $return_code['internal_server_error'])
    		{
    			return redirect('/storagelocationtype') 
    			        -> with('message', $body)->with('messagetype','danger');
    		}  
    		elseif($statusCode == $return_code['created'])
    		{
    			return redirect('/storagelocationtype') 
    			        -> with('message', $body)->with('messagetype','success');
    		}    	
    }

    public function update(Request $request)
    {
    	$fields =array(
          'id'=>$request->id,
          'STL_Type_name'=>$request->type_name,
          );
        $url = $this->url . '/storagelocationtype/update'; 
        $return_code = $this->return_code;  
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if($statusCode == $return_code['bad_request'])
    		{
    			return redirect('/storagelocationtype') 
    			        -> with('message',$body['errors'])->with('messagetype','danger');
    		}
    		elseif($statusCode == $return_code['internal_server_error'])
    		{
    			return redirect('/storagelocationtype') 
    			        -> with('message', $body)->with('messagetype','danger');
    		} 
    		elseif($statusCode == $return_code['ok'])
    		{
    			return redirect('/storagelocationtype') 
    			        -> with('message', $body)->with('messagetype','success');
    		}    

    }

    public function delete(Request $request)
    {

    	$fields =array(
          'id'=>$request->id);
      	$url = $this->url . '/storagelocationtype/delete';  
      	$return_code = $this->return_code; 
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];
		    $body = json_decode($response['body'],true);

    		if($statusCode == $return_code['bad_request'])
    		{
    			return redirect('/storagelocationtype') 
    			        -> with('message', $body['errors'])->with('messagetype','danger');
    		}
    		
    		elseif($statusCode == $return_code['internal_server_error'])
    		{
    			return redirect('/storagelocationtype') 
    			        -> with('message', $body)->with('messagetype','danger');
    		}
    		elseif($statusCode == $return_code['ok'])
    		{
    			return redirect('/storagelocationtype') 
    			        -> with('message', $body)->with('messagetype','success');
    		}    
        	
    }

}
