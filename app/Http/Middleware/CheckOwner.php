<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\HttpRequest\HttpRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
class CheckOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $warehouseName = Session::get('warehouseName');
        $enable = Session::get('warehouseEnable');
        $frontendURL = url('');
        $frontendURL = str_replace("www.","",$frontendURL);
        $pos = strpos($frontendURL,'.');
        $link = substr($frontendURL,7,$pos-7);
        $actualURL = str_replace($link.'.',"",url(''));
        if($link != $warehouseName || $enable != 1)
        {
            return redirect($actualURL)->with('message','u tried to access a subdomain that u are not registered in');
        } 
        return $next($request);
    }
}
