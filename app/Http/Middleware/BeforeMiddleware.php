<?php

namespace App\Http\Middleware;

use Closure;

class BeforeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $url = $account;
        //$link = $account;
        $link2 = str_replace("www.","",$url);
        $pos = strpos($link2,'.');
        $link = substr($link2,7,$pos-7);
        $basicurl = str_replace($link.'.',"",$url);
        if ($link == 'me-wims') {
            return view('newmain1');
        }
        return $next($request);
    }
}
