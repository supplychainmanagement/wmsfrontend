<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\HttpRequest\HttpRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
class CheckSubdomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    //public $url = 'http://localhost:8888';
    public function handle($request, Closure $next)
    {
        if (strpos(url(''), 'localhost') !== false)
        {
            $url = 'http://localhost:8888/checksubdomain';
        }
        else
        {
            $url = 'http://me-wims.com:9090/checksubdomain';
        }
        
        $frontendURL = url('');
        $frontendURL = str_replace("www.","",$frontendURL);
        $pos = strpos($frontendURL,'.');
        $link = substr($frontendURL,7,$pos-7);
        $actualURL = str_replace($link.'.',"",url(''));
        $fields = array(
            'warehousename'=>$link
        );
        $response = HttpRequest::post($url,$fields);
        //return $response;
        $status_code = $response['info']['http_code'];
        //return $response;
        $body = json_decode($response['body'], true);
        if($status_code == 401)
            return redirect($actualURL)->with('message','Subdomain inactive or doesn\'t exist');
        else if($status_code == 500)
            return redirect($actualURL)->with('message','Server is not responding please try again later');
        
        return $next($request);
    }
}
