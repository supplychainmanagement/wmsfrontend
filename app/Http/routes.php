<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/






////get subdomain url
$link = url('');
// if the url contains localhost
if (strpos($link, 'localhost') !== false)
{
	// echo 'die';
	// die();
	if(strpos($link, '.') !== false)
	{
		$pos = strrpos($link,'.');
		$subdomain = substr($link,7,$pos-7);
		$basedomain = substr($link,0,$pos+1);
		$endwebsitenamepos = strripos($link,':');
		$websitename= '';
		if($endwebsitenamepos != 4)
			$websitename = substr($link,$pos,$endwebsitenamepos-$pos);

		else
			$websitename = substr($link,$pos,strlen($link)-$pos);
		$websitename = str_replace('.','',$websitename);
	}
	else
	{
		$subdomain = null;
		$websitename = null;
	}
}
//doesn't contain localhost
else
{
	// echo 'die';
	// die();
	$substrings = explode('.',$link);
	// me-wims.com
	if(sizeof($substrings) == 2){
		$subdomain = null ;
		$websitename =null;
	}
	//www or a.me-wims.com
	else if(sizeof($substrings) == 3){

		$substrings2 = explode('://',$substrings[0]);
		
		$websitename  = $substrings[1].'.'.$substrings[2];
		$subdomain = $substrings2[1];
		if($subdomain == 'www')
		{
			$websitename = null;
			$subdomain = null;
		}
	}
}

/////////////////////

///Subdomain Route group
Route::group(array('domain' => $subdomain.'.'.$websitename), function() {

	Route::get('/', 'LoginController@index');
	//Route::get('/login', 'LoginController@index');
	Route::post('/login',['as'=>'login','uses'=>'LoginController@login']);
	Route::get('/logout',['as'=>'logout','uses'=>'LoginController@logout']);
	
	Route::get('/home' , ['as' => 'home' ,'uses' => 'HomeController@index']);
	Route::get('/home/getskus' , ['as' => 'home.getSkus' ,'uses' => 'HomeController@getSKUs']);
	Route::get('/homedata','HomeController@homeData');
	Route::get('/zone','ZoneController@index');

	Route::get('/getrole','Controller@getRole');


	Route::group(['prefix' => '/zone'], function () {
		Route::get('/' , ['as' => 'zone' ,'uses' => 'ZoneController@index']);
		Route::post('/' , 'ZoneController@getprop');
		Route::post('/create',['as'=>'zoneCreate','uses'=>'ZoneController@create']);
		Route::post('/delete' , 'ZoneController@delete');
		Route::post('/update' , array( 'uses' =>'ZoneController@update', 'as' => 'formupdate'));

	});

	Route::group(['prefix' => '/storagelocationtype'], function () {
		Route::get('/' , ['as' => 'storagelocationtype' ,'uses' => 'StorageLocationTypeController@index']);
		Route::post('/create' ,  array( 'uses' =>'StorageLocationTypeController@create', 'as' => 'storagelocationtype.formcreate'));
		Route::post('/update' , array( 'uses' =>'StorageLocationTypeController@update', 'as' => 'storagelocationtype.formupdate'));
		Route::post('/delete' , array( 'uses' =>'StorageLocationTypeController@delete', 'as' => 'storagelocationtype.formupdelete'));

	});

	Route::group(['prefix' => '/unitloadtype'], function () {
		Route::get('/' , ['as' => 'unitloadtype' ,'uses' => 'UnitloadTypeController@index']);
		Route::post('/create' ,  array('uses' => 'UnitloadTypeController@create', 'as' => 'unitloadtype.formcreate'));
		Route::post('/update' , array( 'uses' =>'UnitloadTypeController@update', 'as' => 'unitloadtype.formupdate'));
		Route::post('/delete' , array( 'uses' =>'UnitloadTypeController@delete', 'as' => 'unitloadtype.formupdelete'));

	});

	Route::group(['prefix' => '/skutype'], function () {
		Route::get('/' , ['as' => 'skutype' ,'uses' => 'SKUTypeController@index']);
		Route::post('/create' ,  array( 'uses' =>'SKUTypeController@create', 'as' => 'skutype.formcreate'));
		Route::post('/delete' , array( 'uses' =>'SKUTypeController@delete', 'as' => 'skutype.formupdelete'));
		Route::post('/update' , array( 'uses' =>'SKUTypeController@update', 'as' => 'skutype.formupdate'));

	});


	Route::group(['prefix' => '/sku'], function () {
		Route::get('/' , ['as' => 'sku' ,'uses' => 'SKUController@index']);
		Route::post('/' , 'SKUController@getprop');
		Route::post('/create' , ['as'=>'skuCreate','uses'=>'SKUController@create']);
		Route::post('/delete' , 'SKUController@delete');
		Route::post('/update' , array( 'uses' =>'SKUController@update', 'as' => 'formupdate'));

	});


	Route::group(['prefix'=>'/unitload'],function(){
		Route::get('/',['as'=>'unitload','uses'=>'UnitloadController@index']);
		Route::post('/','UnitloadController@getprop');
		Route::post('/create',['as'=>'unitloadCreate','uses'=>'UnitloadController@create']);
		Route::post('/delete','UnitloadController@delete');
		Route::post('/update',['as'=>'forumupdate','uses'=>'UnitloadController@update']);
		Route::get('/barcodeimage',['as'=>'ULbarcodeimage','uses'=>'UnitloadController@barcodeImage']);
		Route::post('/getzones','UnitloadController@getZones');
		Route::post('/getracks','UnitloadController@getRacks');
		Route::post('/getlocations','UnitloadController@getStorageLocationsAjax');
	});


	Route::group(['prefix'=>'/storagelocation'],function(){
		Route::get('/',['as'=>'storagelocation','uses'=>'StorageLocationController@index']);
		Route::post('/','StorageLocationController@getprop');
		Route::post('/create',['as'=>'STLCreate','uses'=>'StorageLocationController@create']);
		Route::post('/delete','StorageLocationController@delete');
		Route::post('/update',['as'=>'forumupdate','uses'=>'StorageLocationController@update']);
		Route::post('/getzonebysitename','StorageLocationController@getZoneBySiteName');
		Route::post('/getrackbyzonename','StorageLocationController@getRackByZoneName');
		Route::get('/barcodeimage',['as'=>'STLbarcodeimage','uses'=>'StorageLocationController@barcodeImage']);
		Route::post('/storaglocationtable',['as'=>'storagelocationtableajax','uses'=>'StorageLocationController@storagLocationTable']);
	});


	Route::group(['prefix'=>'/customer'],function(){
		Route::get('/',['as'=>'customer','uses'=>'CustomerController@index']);
		Route::post('/','CustomerController@getprop');
		Route::post('/create',['as'=>'customerCreate','uses'=>'CustomerController@create']);
		Route::post('/delete','CustomerController@delete');
		Route::post('/update',['as'=>'forumupdate','uses'=>'CustomerController@update']);
	});


	Route::group(['prefix'=>'/supplier'],function(){
		Route::get('/',['as'=>'supplier','uses'=>'SupplierController@index']);
		Route::post('/','SupplierController@getprop');
		Route::post('/create',['as'=>'supplierCreate','uses'=>'SupplierController@create']);
		Route::post('/delete','SupplierController@delete');
		Route::post('/update',['as'=>'forumupdate','uses'=>'SupplierController@update']);
	});


	Route::group(['prefix'=>'/provider'],function(){
		Route::get('/',['as'=>'provider','uses'=>'ProviderController@index']);
		Route::post('/','ProviderController@getprop');
		Route::post('/create',['as'=>'providerCreate','uses'=>'ProviderController@create']);
		Route::post('/delete','ProviderController@delete');
		Route::post('/update',['as'=>'forumupdate','uses'=>'ProviderController@update']);
	});


	Route::group(['prefix'=>'/user'],function(){
		Route::get('/',['as'=>'user','uses'=>'UserController@index']);
		Route::post('/','UserController@getprop');
		Route::post('/create',['as'=>'userCreate','uses'=>'UserController@create']);
		Route::post('/delete','UserController@delete');
		Route::post('/update',['as'=>'forumupdate','uses'=>'UserController@update']);
	});


	Route::group(['prefix'=>'/strategy'],function(){
		Route::get('/',['as'=>'strategy','uses'=>'StrategyController@index']);
	});



	Route::group(['prefix' => '/site'], function () {
		Route::get('/' , ['as' => 'site' ,'uses' => 'SiteController@index']);
		Route::post('/' , 'SiteController@getprop');
		Route::post('/create' ,['as'=>'siteCreate', 'uses'=>'SiteController@create']);
		Route::post('/delete' , 'SiteController@delete');
		Route::post('/update' , array( 'uses' =>'SiteController@update', 'as' => 'formupdate'));

	});

	Route::group(['prefix' => '/capacitylocation'], function () {
		Route::get('/' , ['as' => 'capacitylocation' ,'uses' => 'CapacityLocationController@index']);
		Route::post('/create' ,  array( 'uses' =>'CapacityLocationController@create', 'as' => 'capacitylocation.formcreate'));
		Route::post('/delete' , array( 'uses' =>'CapacityLocationController@delete', 'as' => 'capacitylocation.formupdelete'));
		Route::post('/update' , array( 'uses' =>'CapacityLocationController@update', 'as' => 'capacitylocation.formupdate'));

	});

	Route::group(['prefix' => '/capacityunitload'], function () {
		Route::get('/' , ['as' => 'capacityunitload' ,'uses' => 'CapacityUnitloadController@index']);
		Route::post('/create' ,  array( 'uses' =>'CapacityUnitloadController@create', 'as' => 'capacityunitload.formcreate'));
		Route::post('/delete' , array( 'uses' =>'CapacityUnitloadController@delete', 'as' => 'capacityunitload.formupdelete'));
		Route::post('/update' , array( 'uses' =>'CapacityUnitloadController@update', 'as' => 'capacityunitload.formupdate'));

	});

	Route::group(['prefix' => '/mismatches'], function () {
		Route::get('/' , ['as' => 'mismatches' ,'uses' => 'MismatchesController@index']);
		Route::post('/create' ,  array( 'uses' =>'MismatchesController@create', 'as' => 'mismatches.formcreate'));
		Route::post('/delete' , array( 'uses' =>'MismatchesController@delete', 'as' => 'mismatches.formupdelete'));
		Route::post('/update' , array( 'uses' =>'MismatchesController@update', 'as' => 'mismatches.formupdate'));

	});

	Route::group(['prefix' => '/functionalarea'], function () {
		Route::get('/' , ['as' => 'functionalarea' ,'uses' => 'FunctionalAreaController@index']);
		Route::post('/create' ,  array( 'uses' =>'FunctionalAreaController@create', 'as' => 'functionalarea.formcreate'));
		Route::post('/delete' , array( 'uses' =>'FunctionalAreaController@delete', 'as' => 'functionalarea.formupdelete'));
		Route::post('/update' , array( 'uses' =>'FunctionalAreaController@update', 'as' => 'functionalarea.formupdate'));

	});

	Route::group(['prefix' => '/client'], function () {
		Route::get('/' , ['as' => 'client' ,'uses' => 'ClientController@index']);
		Route::post('/create' ,  array( 'uses' =>'ClientController@create', 'as' => 'client.formcreate'));
		Route::post('/delete' , array( 'uses' =>'ClientController@delete', 'as' => 'client.formupdelete'));
		Route::post('/update' , array( 'uses' =>'ClientController@update', 'as' => 'client.formupdate'));

	});

	Route::group(['prefix' => '/rack'], function () {
		Route::get('/' , ['as' => 'rack' ,'uses' => 'RackController@index']);
		Route::post('/' , 'RackController@getprop');
		Route::post('/getzones','RackController@getZones');
		Route::post('/create' ,  array( 'uses' =>'RackController@create', 'as' => 'rack.formcreate'));
		Route::post('/delete' , array( 'uses' =>'RackController@delete', 'as' => 'rack.formupdelete'));
		Route::post('/update' , array( 'uses' =>'RackController@update', 'as' => 'rack.formupdate'));

	});

	Route::group(['prefix'=>'/reports'],function(){
		Route::get('/',['as'=>'transactionreport','uses'=>'ReportsController@indexTransactionReport']);
		Route::get('/stockreport',['as'=>'stockreport','uses'=>'ReportsController@indexStockReport']);
		Route::get('/dailytransactionreport',['as'=>'dailytransactionreport','uses'=>'ReportsController@indexMinTransactionReport']);
		Route::get('/orderreport',['as'=>'orderreport','uses'=>'ReportsController@indexOrderReport']);
		Route::post('/gettransactionreport','ReportsController@getTransactionReport');
		Route::post('/getstockreport','ReportsController@getStockReport');
		Route::post('/getmintransactionreport','ReportsController@getMinTransactionReport');
		Route::post('/getorderreport','ReportsController@getOrderReport');
	});

	Route::group(['prefix'=>'/issueorder'],function(){
		Route::get('/',['as'=>'issueorder','uses'=>'IssueOrderController@index']);
		Route::get('/items',['as'=>'issueorder.items','uses'=>'IssueOrderController@itemindex']);
		Route::get('/issueorderwizard',['as'=>'issueorderwizard','uses'=>'IssueOrderController@createorderindex']);
		Route::post('/getentry' ,'IssueOrderController@getEntry');
		Route::post('/getcustomers' ,'IssueOrderController@getCustomers');
		Route::post('/save','IssueOrderController@save');
		Route::post('/saveitem','IssueOrderController@saveItem');
		Route::post('/getskus' ,'IssueOrderController@getSKUs');
		Route::post('/removeitem','IssueOrderController@removeItem');
		Route::get('/pdf',['as'=>'printPDF','uses'=>'IssueOrderController@pdf']);
		Route::post('/finishissueorder',['as'=>'finishissueorder','uses'=>'IssueOrderController@finishIssueOrder']);
	});
	Route::group(['prefix'=>'/purchaseorder'],function(){
		Route::get('/items',['uses'=>'PurchaseOrderController@itemindex','as'=>'purchaseorder.items']);
		Route::get('/purchaseorderwizard',['uses'=>'PurchaseOrderController@createorderindex','as'=>'purchaseorderwizard']);
		Route::post('/getentry' ,'PurchaseOrderController@getEntry');
		Route::post('/getproviders' ,'PurchaseOrderController@getProviders');
		Route::post('/save','PurchaseOrderController@save');
		Route::post('/saveitem','PurchaseOrderController@saveItem');
		Route::post('/getskus' ,'PurchaseOrderController@getSKUs');
		Route::post('/removeitem','PurchaseOrderController@removeItem');
		Route::get('/pdf',['as'=>'purchaseOrderPdf','uses'=>'PurchaseOrderController@pdf']);
		Route::get('/{PO_number?}',['as'=>'purchaseorder','uses' =>'PurchaseOrderController@index']);
		Route::post('/create',['as'=>'PurchaseOrderCreate','uses'=>'PurchaseOrderController@create']);
		Route::post('/update','PurchaseOrderController@update');
		Route::post('/delete','PurchaseOrderController@delete');
		Route::post('/finishpurchaseorder',['as'=>'finishpurchaseorder','uses'=>'PurchaseOrderController@finishPurchaseOrder']);
	});
	Route::group(['prefix'=>'/pickorder'],function(){
		Route::get('/',['uses'=>'PickOrderController@pickorderindex','as'=>'pickorder']);
		Route::get('/pickrequestindex',['as'=>'pickorder.pickrequestindex','uses'=>'PickOrderController@pickRequestIndex']);
		Route::get('/pickrequestitemsindex',['as'=>'pickorder.pickrequestitemsindex','uses'=>'PickOrderController@pickRequestItemIndex']);
		Route::post('/getitems','PickOrderController@getItems');
		Route::post('/getitemamount','PickOrderController@getItemAmount');
		Route::post('/getpickoutunitloads','PickOrderController@getStorageUnitloads');
		Route::post('/getpickinunitloads','PickOrderController@pickInUnitloads');
		Route::post('/createpickrequest','PickOrderController@createPickRequest');
		Route::post('/getpickinunitloadamount','PickOrderController@getPickInUnitloadAmount');
		Route::post('/getpickoutunitloadamount','PickOrderController@getPickOutUnitloadAmount');
		Route::post('/createpickjob',['as'=>'createPickJob','uses'=>'PickOrderController@createPickJob']);
		Route::post('/pickjobitems','PickOrderController@pickJobItems');
		Route::get('/pickjobindex',['as'=>'pickorder.pickjobindex','uses'=>'PickOrderController@pickJobIndex']);
		Route::post('/suggestpickout','PickOrderController@suggestPickOut');
		Route::get('/pdf',['as'=>'pickrequestpdf','uses'=>'PickOrderController@pdf']);
	});
	Route::get('/monitoring',['as'=>'monitoring','uses'=>'SpriteController@index']);
	Route::post('/getwarehousedata','SpriteController@getWarehouseData');
	Route::post('/getunitloadlocation','SpriteController@getUnitloadLocation');

});




/*
 * Goods in Operations Routes
 * Khaled M.Fathy
 */

/*
 * Purchase Order Routes
 */
//*************************************************************************************************

Route::group(['prefix'=>'/purchaseorderitem'],function(){
	Route::post('/create',['as'=>'PurchaseOrderItemCreate','uses'=>'PurchaseOrderItemController@create']);
	Route::get('/create','PurchaseOrderItemController@createIndex');
	Route::get('/{POI_number?}','PurchaseOrderItemController@index');
	Route::post('/update','PurchaseOrderItemController@update');
	Route::post('/delete','PurchaseOrderItemController@delete');
	Route::post('/',['uses'=>'PurchaseOrderItemController@getprop','as'=>'purchaseorderitem']);
});
//****************************************************************************************************



/*
 * Receiving Job Routes
 */
//*************************************************************************************************
Route::group(['prefix'=>'/receivingjob'],function(){
	Route::get('/create',['as'=>'createrecevingjob','uses'=>'ReceivingJobController@createIndex']);
	Route::get('/',['as'=>'receivingjob','uses'=>'ReceivingJobController@index']);
	Route::post('/pobyname','ReceivingJobController@getPurchaseOrderByName');
	Route::post('/poibyname','ReceivingJobController@getPurchaseOrderItemByName');
	Route::post('/matchedunitload','ReceivingJobController@getMatchedUnitloads');
	Route::get('/rcjnumber','ReceivingJobController@getRCJNumber');
	Route::post('/submit','ReceivingJobController@submitReceivingJob');

});

//****************************************************************************************************


/*
 * Storage Job Routes
 */
//*************************************************************************************************
Route::group(['prefix'=>'/storagejob'],function(){
	Route::get('/create',['as'=>'StorageJobCreate','uses'=>'StorageJobController@createIndex']);
	Route::get('/',['as'=>'storagejob','uses'=>'StorageJobController@index']);
	Route::get('/stjdata','StorageJobController@getDataForStorageJob');
	Route::post('/stjstrategy','StorageJobController@getStorageLocationsForStrategy');
	Route::get('/stjname','StorageJobController@generateStorageJobName');
	Route::post('/submit',['as'=>'SubmitSTJ','uses'=>'StorageJobController@submitStorageJob']);
});

//****************************************************************************************************



/*
 * Shipping Routes
 */
//*************************************************************************************************
Route::group(['prefix'=>'/shipping'],function(){
	Route::get('/create',['as'=>'ShippingCreate','uses'=>'ShippingController@createIndex']);
	Route::post('/submit',['as'=>'SubmitShip','uses'=>'ShippingController@submitShip']);
	Route::get('/issueorders','ShippingController@getAllIssueOrders');
	Route::post('/issueorderitems','ShippingController@getAllIssueOrderItems');
});

//****************************************************************************************************



///////////////////////

///Register Routes//
Route::get('/', ['as'=>'registerwarehouseview','uses'=>'RegisterController@index']);
Route::get('/supportcenter', ['as'=>'supportcenter','uses'=>'RegisterController@supportIndex']);

Route::post('/',['as'=>'registerwarehouse','uses'=>'RegisterController@register']);
////////////////////////

Route::post('/rfq', ['as'=>'rfq','uses'=>'RegisterController@RFQ']);
Route::post('/verifycode', ['as'=>'verifycode','uses'=>'RegisterController@verifyCode']);
Route::post('/invitation', ['as'=>'invitation','uses'=>'RegisterController@invitation']);