<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if($this->isHttpException($e))
        {
            switch ($e->getStatusCode()) 
                {
                // not found
                case 404:
                    ////get subdomain url
                    $link = url('');
                    // if the url contains localhost
                    if (strpos($link, 'localhost') !== false)
                    {
                        // echo 'die';
                        // die();
                        if(strpos($link, '.') !== false)
                        {
                            $pos = strrpos($link,'.');
                            $subdomain = substr($link,7,$pos-7);
                            $basedomain = substr($link,0,$pos+1);
                            $endwebsitenamepos = strripos($link,':');
                            $websitename= '';
                            if($endwebsitenamepos != 4)
                                $websitename = substr($link,$pos,$endwebsitenamepos-$pos);

                            else
                                $websitename = substr($link,$pos,strlen($link)-$pos);
                            $websitename = str_replace('.','',$websitename);
                        }
                        else
                        {
                            $subdomain = null;
                            $websitename = null;
                        }
                    }
                    //doesn't contain localhost
                    else
                    {
                        // echo 'die';
                        // die();
                        $substrings = explode('.',$link);
                        // me-wims.com
                        if(sizeof($substrings) == 2){
                            $subdomain = null ;
                            $websitename =null;
                        }
                        //www or a.me-wims.com
                        else if(sizeof($substrings) == 3){

                            $substrings2 = explode('://',$substrings[0]);
                            
                            $websitename  = $substrings[1].'.'.$substrings[2];
                            $subdomain = $substrings2[1];
                            if($subdomain == 'www')
                            {
                                $websitename = null;
                                $subdomain = null;
                            }
                        }
                    }
                    if($subdomain != null)
                    {
                        return response()->make(view('errors.Error404Internal'),400);
                    }
                    else
                    {
                        return response()->make(view('errors.Error404'), 404);
                    }
                break;

                default:
                    return $this->renderHttpException($e);
                break;
            }
        }
        else
        {
                return parent::render($request, $e);
        }
    }
}
