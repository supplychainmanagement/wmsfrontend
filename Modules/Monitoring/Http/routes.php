<?php

$websitename = null;
$subdomain = null;
Route::group(array('domain' => $subdomain.'.'.$websitename), function() {
	Route::group(['middleware' => 'web', 'prefix' => 'monitoring', 'namespace' => 'Modules\Monitoring\Http\Controllers'], function()
	{
	    Route::get('/',['as'=>'monitoring','uses'=>'MonitoringController@index']);
		Route::post('/getwarehousedata','MonitoringController@getWarehouseData');
		Route::post('/getunitloadlocation','MonitoringController@getUnitloadLocation');
	});

});
