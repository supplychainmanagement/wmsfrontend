function zoomOntoRack(location)
{
	var rackid = '.'+location.split('-')[0].replace(" ","-");
	
	$(rackid).click(); 
}



function rackRotate(zoneExpand,zoneid,rackid,height)
{
	var rotateRack  = new Snap.Matrix();
	//svgsnap.zpd();
	rotateRack.rotate(90,zoneExpand[zoneid]["zonerackcoordinates"][rackid]["centerx"],zoneExpand[zoneid]["zonerackcoordinates"][rackid]["centery"]);
	//rotateRack.translate(0,-200);
	// rotateRack.scale(3,3,zoneExpand[zoneid]["zonerackcoordinates"][rackid]["centerx"],zoneExpand[zoneid]["zonerackcoordinates"][rackid]["centery"]);
	// rotateRack.translate(0,300);
	if(zoneExpand[zoneid]["zonerackcoordinates"][rackid]["centerx"] <=height && zoneExpand[zoneid]["zonerackcoordinates"][rackid]["centery"] <=(height/2))
	{
		rotateRack.translate(((height/2)-zoneExpand[zoneid]["zonerackcoordinates"][rackid]["centery"]),-(height-zoneExpand[zoneid]["zonerackcoordinates"][rackid]["centerx"]));
	}
	else if(zoneExpand[zoneid]["zonerackcoordinates"][rackid]["centerx"] >height && zoneExpand[zoneid]["zonerackcoordinates"][rackid]["centery"] <=(height/2))
	{
		rotateRack.translate(((height/2)-zoneExpand[zoneid]["zonerackcoordinates"][rackid]["centery"]),(zoneExpand[zoneid]["zonerackcoordinates"][rackid]["centerx"]-height));
	}
	else if(zoneExpand[zoneid]["zonerackcoordinates"][rackid]["centerx"] <=height && zoneExpand[zoneid]["zonerackcoordinates"][rackid]["centery"] >(height/2))
	{
		rotateRack.translate(-(zoneExpand[zoneid]["zonerackcoordinates"][rackid]["centery"]-(height/2)),-(height-zoneExpand[zoneid]["zonerackcoordinates"][rackid]["centerx"]));
	}
	else if(zoneExpand[zoneid]["zonerackcoordinates"][rackid]["centerx"] >height && zoneExpand[zoneid]["zonerackcoordinates"][rackid]["centery"] >(height/2))
	{
		rotateRack.translate(-(zoneExpand[zoneid]["zonerackcoordinates"][rackid]["centery"]-(height/2)),(zoneExpand[zoneid]["zonerackcoordinates"][rackid]["centerx"]-height));
	}
	return rotateRack;
}

function drawRack(svgsnap,nolevels,nofields,rackname,width,height,rackstexture_frontfull,rackstexture_frontleft,response){
	var max =0;
	if(nolevels>nofields)
		max = nolevels;
	else
		max = nofields;
	// rack image attributtes from rackleft svg
	var rackimagewidthleft = 1208.5;
	var rackimagewidthfull = 1413.5;
	var rackimageheight = 222;
	var rackwidthofleft = 0;
	var rackwidthfull =0;
	var rackheight = 0;
	//30 deeh margin mn 3ala el yemmen
	rackwidthofleft = Math.floor((width-30)/(nofields+((rackimagewidthfull-rackimagewidthleft)/rackimagewidthleft)));
	rackwidthfull = (rackwidthofleft)*(rackimagewidthfull/rackimagewidthleft);
	rackheight = rackwidthofleft*(rackimageheight/rackimagewidthleft);
	if(height<rackheight*nolevels)
	{
		rackheight = height/nolevels;
		rackwidthofleft = rackheight*(rackimagewidthleft/rackimageheight);
		rackwidthfull =rackheight*(rackimagewidthfull/rackimageheight);
	}
	var rackmarginX = (width-(rackwidthfull+((nofields-1)*rackwidthofleft)))/2;
	var rackmarginY = (height-(rackheight*nolevels))/2;
	
	
	
	
	var palletsecondhit = rackheight*(106/rackimageheight);
	var pallerthirdhit = rackheight*(145/rackimageheight);
	var firstpalletdistance = rackwidthofleft*(222/rackimagewidthleft);
	var secondpalletdistance = rackwidthofleft*(559/rackimagewidthleft);
	var thirdpalletdistance = rackwidthofleft*(892/rackimagewidthleft);
	var palletcorner = rackwidthofleft*(31/rackimagewidthleft);
	var palletwidth = rackwidthofleft*(223/rackimagewidthleft);
	var unitwidth =palletwidth/4;

	// First Option
	// x:xshift,
	// y:yshift,
	// width:rackwidthfull,
	// height:rackheight,
	// x:xshift,
	// y:yshift,
	// width:rackwidthofleft,
	// height:rackheight,
	// End First Option

	// Second Option
	var rackPageMargin =0.05*width;
	var rackpagewidth = (width-2*parseInt(rackPageMargin))/(parseInt(nofields)+0.17);
	var rackpageheight = (height-2*parseInt(rackPageMargin))/(nolevels);	
	var totalwidth = (parseInt(nofields)+0.17)*rackpagewidth;
	var totalheight = (7/5)*height-2*rackPageMargin;
	var scalerackPagey = 1;
	if(totalwidth<totalheight)
	{
		
		scalerackPagey = 0.5*totalwidth/((222/(1208/rackpagewidth))*nolevels);
	}									
	else if(totalwidth>=totalheight)
	{
		
		scalerackPagey = totalheight/((222/(1208/rackpagewidth))*nolevels);
	}								
	

	// x:parseInt(rackPageMargin)+((nofields-1)*rackpagewidth),
	// //y:parseInt(rackPageMargin)*nofields+(rlevels*(222/(1208/rackpagewidth))),
	// y:parseInt(rackPageMargin)+(rlevels*(222/(1208/rackpagewidth))),
	// width:(rackpagewidth+(rackpagewidth*0.17)),
	// height:((222/(1208/rackpagewidth))),

	// x:rackPageMargin+(rfields*rackpagewidth),
	// //y:rackPageMargin*nofields+(rlevels*(222/(1208/rackpagewidth))),
	// y:rackPageMargin+(rlevels*(222/(1208/rackpagewidth))),
	// width:(rackpagewidth),
	// height:((222/(1208/rackpagewidth))),
	// End Second Option
	// array that holds all the data
	var rackdrawings = new Array();
	//loop that creates the racks
	var unitloadtexture = svgsnap.select('#unitload');
	for(var rlevels =0;rlevels<nolevels;rlevels++)
		{
			for(var rfields=0;rfields<nofields;rfields++)
			{
				//pallet name + x distance and y distance when the field chand and the level change
				var firstpallentname=rackname+"_"+(rfields+1)+"_"+(nolevels-rlevels)+'_1';
				var secondpalletname=rackname+"_"+(rfields+1)+"_"+(nolevels-rlevels)+'_2';
				var thirpalletname=rackname+"_"+(rfields+1)+"_"+(nolevels-rlevels)+'_3';
				var xshift=rfields*rackwidthofleft+rackmarginX;
				var yshift=rlevels*rackheight+rackmarginY;
				if(rfields ==(nofields-1))
				{
					var pagerackend = rackstexture_frontfull.clone();
				pagerackend.attr({

					viewBox:"0, 0, 1413.572, 222.007",
					visibility:"visible",
					x:xshift,
					y:yshift,
					width:rackwidthfull,
					height:rackheight,

				});
				//var scale_matrix = new Snap.Matrix();		
				//scale_matrix.scale(1,scalerackPagey,rackPageMargin,rackPageMargin);
				//pagerackend.animate({transform:scale_matrix}, animationduration, mina.easeinout);
				rackdrawings.push(pagerackend);
				}
				else
				{
					var pagerack = rackstexture_frontleft.clone();
					pagerack.attr({
				viewBox:"0, 0, 1208.512, 222.007",
				visibility:"visible",
					x:xshift,
					y:yshift,
					width:rackwidthofleft,
					height:rackheight,
				});
				//var scale_matrix = new Snap.Matrix();		
				//scale_matrix.scale(1,scalerackPagey,rackPageMargin,rackPageMargin);
				//pagerack.animate({transform:scale_matrix}, animationduration, mina.easeinout);	

				rackdrawings.push(pagerack);
				}

				var first = svgsnap.text(xshift+firstpalletdistance+(2*palletcorner),yshift+pallerthirdhit,firstpallentname);
				var second = svgsnap.text(xshift+secondpalletdistance+(2*palletcorner),yshift+pallerthirdhit,secondpalletname);
				var third = svgsnap.text(xshift+thirdpalletdistance+(2*palletcorner),yshift+pallerthirdhit,thirpalletname);
			first.attr({
				'font-size':((width/32)/max)+'px',
				'font-family': "Times New Roman"
			});
			
			second.attr({							
				'font-size':((width/32)/max)+'px',
				'font-family': "Times New Roman"
			});
			
			third.attr({
				'font-size':((width/32)/max)+'px',
				'font-family': "Times New Roman"
			});
			rackdrawings.push(first);
			rackdrawings.push(second);
			rackdrawings.push(third);
			var firstPalletIndex = -1;
			var secondPalletIndex = -1;
			var thirdPalletIndex = -1;
			for(var i=0;i<response.length;i++)
			{
				if(response[i].location.STL_name == firstpallentname)
					firstPalletIndex = i;
				if(response[i].location.STL_name == secondpalletname)
					secondPalletIndex = i;
				if(response[i].location.STL_name == thirpalletname)
					thirdPalletIndex = i;
			}

			
			if(firstPalletIndex != -1)
			{
				var unit =0;										
				for (var i=0;i<response[firstPalletIndex].unitloads.length;i++)
				{
					
					var xpositionunitload = ((unit%4)*unitwidth)+firstpalletdistance+palletcorner+xshift;
					var ypositionunitload = (-(Math.floor(unit/4))*unitwidth)+yshift+unitwidth;
					var unitloadshape = unitloadtexture.clone();
					unitloadshape.attr({
						id: response[firstPalletIndex].unitloads[i].UL_name.replace(" ","-"),
						viewBox:'0 0 96.024 90.19',
						x:xpositionunitload,
						y:ypositionunitload,
						width:unitwidth,
						height:unitwidth,
						visibility:'visible',
						class:".tooltip"
					});
					var unitloadpath = svgsnap.path(Snap.format("M{x},{y}h{dim.width}v{dim.height}h{dim['negative width']}z", {
					    x: xpositionunitload,
					    y: ypositionunitload,
					    dim: {
					        width: unitwidth,
					        height: unitwidth,
					        "negative width": -unitwidth
					    }
					}));
					unitloadpath.attr({fill:'none'});
					$('#'+response[firstPalletIndex].unitloads[i].UL_name.replace(" ","-")).tooltipster({
						theme:'tooltipster-punk',
						content: $('<span> <strong>'+response[firstPalletIndex].unitloads[i].UL_name+'</strong></span>')
							
					});

					// if(response[firstpallentname][key] == "invalid")
					// {
					// 	unitloadpath.attr({
					// 		stroke:"#FF0000",
					// 		"stroke-width":2
					// 	});
					// }
					rackdrawings.push(unitloadshape);
					rackdrawings.push(unitloadpath);
					unit++;
				}
			}
			if(secondPalletIndex != -1)
			{
				var unit =0;										
				for (var i=0;i<response[secondPalletIndex].unitloads;i++)
				{
					
					var xpositionunitload = ((unit%4)*unitwidth)+secondpalletdistance+palletcorner+xshift;
					var ypositionunitload = (-(Math.floor(unit/4))*unitwidth)+yshift+unitwidth;
					var unitloadshape = unitloadtexture.clone();
					unitloadshape.attr({
						id: response[secondPalletIndex].unitloads[i].UL_name.replace(" ","-"),
						viewBox:'0 0 96.024 90.19',
						x:xpositionunitload,
						y:ypositionunitload,
						width:unitwidth,
						height:unitwidth,
						visibility:'visible',
						class:".tooltip"
					});
					var unitloadpath = svgsnap.path(Snap.format("M{x},{y}h{dim.width}v{dim.height}h{dim['negative width']}z", {
					    x: xpositionunitload,
					    y: ypositionunitload,
					    dim: {
					        width: unitwidth,
					        height: unitwidth,
					        "negative width": -unitwidth
					    }
					}));
					unitloadpath.attr({fill:'none'});
					$('#'+response[secondPalletIndex].unitloads[i].UL_name.replace(" ","-")).tooltipster({
							theme:'tooltipster-punk',
							content: $('<span> <strong>'+response[secondPalletIndex].unitloads[i].UL_name+'</strong></span>')
					});

					// if(response[secondpalletname][key] == "invalid")
					// {
					// 	unitloadpath.attr({
					// 		stroke:"#FF0000",
					// 		"stroke-width":2
					// 	});
					// }
					rackdrawings.push(unitloadshape);
					rackdrawings.push(unitloadpath);
					unit++;
				}
			}
			if(thirdPalletIndex != -1)
			{
				var unit =0;										
				for (var i=0;i<response[thirdPalletIndex].unitloads;i++)
				{
					
					var xpositionunitload = ((unit%4)*unitwidth)+thirdpalletdistance+palletcorner+xshift;
					var ypositionunitload = (-(Math.floor(unit/4))*unitwidth)+yshift+unitwidth;
					var unitloadshape = unitloadtexture.clone();
					unitloadshape.attr({
						id: response[thirdPalletIndex].unitloads[i].UL_name.replace(" ","-"),
						viewBox:'0 0 96.024 90.19',
						x:xpositionunitload,
						y:ypositionunitload,
						width:unitwidth,
						height:unitwidth,
						visibility:'visible',
						class:".tooltip"
					});
					var unitloadpath = svgsnap.path(Snap.format("M{x},{y}h{dim.width}v{dim.height}h{dim['negative width']}z", {
					    x: xpositionunitload,
					    y: ypositionunitload,
					    dim: {
					        width: unitwidth,
					        height: unitwidth,
					        "negative width": -unitwidth
					    }
					}));
					unitloadpath.attr({fill:'none'});
					$('#'+response[thirdPalletIndex].unitloads[i].UL_name.replace(" ","-")).tooltipster({
							theme:'tooltipster-punk',
							content: $('<span> <strong>'+response[thirdPalletIndex].unitloads[i].UL_name+'</strong></span>')
					});

					// if(response[thirpalletname][key] == "invalid")
					// {
					// 	unitloadpath.attr({
					// 		stroke:"#FF0000",
					// 		"stroke-width":2
					// 	});
					// }
					rackdrawings.push(unitloadshape);
					rackdrawings.push(unitloadpath);
					unit++;
				}
			}
			}

		}
		return rackdrawings;
	}