var AvailableCars = function() {
    var arr = [];
    arr.push = function() {
        if(queuecarsAnimation.length>0)
        {
        	drawzonepath(queuecarsAnimation[0]['svgsnap']
        		,queuecarsAnimation[0]['getfromZone'],
        		queuecarsAnimation[0]['gotoZone'],arguments[0]);
        	queuecarsAnimation.splice(0,1);
        }
        else
        {
        	
        	return Array.prototype.push.apply(this, arguments);
        }
        
    }

    return arr;
};
var QueueCarsAnimation = function() {
    var arr = [];
    arr.push = function() {
        if(avaCars.length>0)
        {
        	
        	
        	drawzonepath(arguments[0]['svgsnap']
        		,arguments[0]['getfromZone'],
        		arguments[0]['gotoZone'],avaCars[0]);
        	avaCars.splice(0,1);
        }
        else
        {
        	
        	return Array.prototype.push.apply(this, arguments);
        }
    }

    return arr;
};
function pickCar(svgsnap,getfromZone,gotoZone)
{
    queuecarsAnimation.push({'svgsnap':svgsnap,'getfromZone':getfromZone,'gotoZone':gotoZone});
}



function animatealongarray(cargroup,car,arraypath,index,stoparray,carid,changeskin){
    if(index == (arraypath.length))
    {
        avaCars.push(carid);
        return;
    } 
    
    if($.inArray(index-1, stoparray) == -1){
        cargroup.drawAtPath(arraypath[index],180, arraypath[index].getTotalLength()*4.5,{'callback':animatealongarray.bind(null,cargroup,car,arraypath,++index,stoparray,carid,changeskin)});
        //recko.drawAtPath(arraypath[index],180, arraypath[index].getTotalLength()*4.5,{'callback':animatealongarray.bind(null,car,arraypath,++index,stoparray)});
    }
    else{
        if(changeskin == 0)
        {
            car.select('.nopallet').attr({visibility:'hidden'});
            car.select('.palletbox').attr({visibility:'visible'});
            changeskin++;
        }
        else if(changeskin ==1)
        {
            car.select('.nopallet').attr({visibility:'visible'});
            car.select('.palletbox').attr({visibility:'hidden'});
        }
        setTimeout(function() {
            
            cargroup.drawAtPath(arraypath[index],180, arraypath[index].getTotalLength()*4.5,{'callback':animatealongarray.bind(null,cargroup,car,arraypath,++index,stoparray,carid,changeskin)});
            //recko.drawAtPath(arraypath[index],180, arraypath[index].getTotalLength()*4.5,{'callback':animatealongarray.bind(null,car,arraypath,++index,stoparray)});
        }, 500);

    }
}