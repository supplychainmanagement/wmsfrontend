function notification(item,fromlocation,to,status,svgsnap)
{
	var gotoZone = to.substring(0,1);
	var palletname =to.split('-')[0];
	// 
	if(fromlocation == null)
	{
		
		if(status=="invalid")
		{
			noty({text: 'Item '+item+' arrived to location '+to+' but it is missplaced', layout: 'topRight', type: 'error',timeout:2000});
			$("#timelinediv").append(timelinehtmlnew(item,to,status));
			
		}
		else
		{
			noty({text: 'Item '+item+' arrived to location '+to, layout: 'topRight', type: 'success',timeout:2000});
			$("#timelinediv").append(timelinehtmlnew(item,to,status));
			
		}
		if(currentpage=='mainpage')
		{
			if(animationduration != 0)
				pickCar(svgsnap,gotoZone,gotoZone);
		}
	}
	else
	{
		var getfromZone = fromlocation.substring(0,1);
		if(status=="invalid")
		{
			noty({text: 'Item '+item+' transfared from location '+fromlocation+' to location '+to+' but it is missplaced', layout: 'topRight', type: 'error',timeout:2000});
			$("#timelinediv").append(timelinehtmltransfer(item,to,fromlocation,status));
			
		}
		else
		{
			noty({text: 'Item '+item+' transfared from location '+fromlocation+' to location '+to, layout: 'topRight', type: 'success',timeout:2000});
			$("#timelinediv").append(timelinehtmltransfer(item,to,fromlocation,status));
			
		}
		if(currentpage=='mainpage')
		{
			if(animationduration != 0)
				pickCar(svgsnap,getfromZone,gotoZone);
		}
	}
	var key = item+to;
	key=key.replace(" ","");
	$('#'+key).on('click', function() {zoomOntoRack(to);});

	for(var i = 0;i<zoneExpand.length;i++)
	{
		if(gotoZone == zoneExpand[i]['zonename'])
		{
			
			for(var j =0;j<zoneExpand[i]['zonerackcoordinates'].length;j++)
			{
				if(palletname == zoneExpand[i]['zonerackcoordinates'][j]['rackname'])
				{
					if(currentpage=='mainpage')
					{
						zoneExpand[i]['zonerackcoordinates'][j]['rackpath'].attr({visibility:'visible'});
					}
					else if(currentpage==gotoZone)
					{
						// 
						zoneExpand[i]['zonerackcoordinates'][j]['rackpath'].attr({visibility:'visible'});
						
					}
					zoneExpand[i]['zonerackcoordinates'][j]['rackpathstatus'] = 'on';
				}
			}
		}
	}

}