var animationduration = 1500; 
var zoneExpand = new Array();
var currentpage = "mainpage";
var mainpageinfo = new Array();
var zoominratio = 1;
var s = null;
var site = null;
$('#loadingdiv').hide();
function remove(s)
{
    for(var i =0 ;i<zoneExpand.length;i++)
    {
        zoneExpand[i].zone.remove();
        zoneExpand[i].zoneplus.remove();
        zoneExpand[i].zoneminus.remove();
        zoneExpand[i].zoneGroup.remove();
        zoneExpand[i].zonetext.remove();
        for(var j=0;j<zoneExpand[i].zoneracks.length;j++)
        {
            zoneExpand[i].zoneracks[j].remove();
            zoneExpand[i].zoneracksGroup[j].remove();
        }

    }
}
function getWarehouseData (height_width_ratio,sitename)
{
    remove();
    var zoneExpand = new Array();
    var currentpage = "mainpage";
    var mainpageinfo = new Array();
    s = Snap("#firstsvg");
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    $( "#loadingdiv" ).show();
    $.ajax({
              url: "getwarehousedata",
              type: "post",
              data:{'sitename':sitename},
              success: function (response) {
              $( "#loadingdiv" ).hide();
              var width = $("#drawdiv").width();
              var height = (5/(10*height_width_ratio))*$("#drawdiv").height();
              var shift = (1/(10*height_width_ratio))*$("#drawdiv").height();
              var shiftdown = (2/(10*height_width_ratio))*$("#drawdiv").height();
              if(response.data.zones.length>0)
              {
                
                drawZones(s,width,height,0.1,0.1,0,shiftdown,response.data);
              }
                
              else
              {
                document.getElementById('message').innerHTML=
              '<div class="alert alert-warning" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                  +'<strong>No Zones in this Site</strong>'
              +'</div>';
              }
               site = response.site.SITE_name;
               $('#sitename').val(site).selectpicker('refresh');
              },
              error: function(jqXHR, textStatus, errorThrown) {
                 
              }
    });
}
$(document).ready(function(){
    s = Snap("#firstsvg");
    //s.circle(860,430,2);
    var navigationwheel = Snap("#navigationwheel");
    navigationwheel.attr({visibility:'visible'});
    var height_width_ratio = 0.8;
    document.getElementById("drawdiv").style.height=(height_width_ratio*$("#drawdiv").width())+"px";
    getWarehouseData(height_width_ratio,'');
    $('#sitename').on('change', function() {
        
        $('#message').empty();
        getWarehouseData(height_width_ratio,$('#sitename').val());
    });
     $("#zonefinder").on("click",function(){
        var findzone = ".zone"+$("#zonepicker").val()+"-plus";
        
        $(findzone).click();
    });
     var intervalF;
            var clearIntervalF = function () {
                clearInterval(intervalF);
            }
    document.onkeydown = function (e) {
        switch(e.keyCode) {
            case 37: // left
                s.zpd({drag:true});
                s.panTo('-10');
                s.zpd({drag:false,zoom:false,pan:false});
                break;
            case 38: // up
                s.zpd({drag:true});
                s.panTo('+0', '-10');
                s.zpd({drag:false,zoom:false,pan:false});
                break;
            case 39: // right
                s.zpd({drag:true});
                s.panTo('+10');
                s.zpd({drag:false,zoom:false,pan:false});
                break;
            case 40: // down
                s.zpd({drag:true});
                s.panTo('+0', '+10');
                s.zpd({drag:false,zoom:false,pan:false});
                break;
        }
    };
    document.getElementById('leftMonitoring').onmouseup = clearIntervalF;
    document.getElementById('leftMonitoring').onmouseleave = clearIntervalF;
    document.getElementById('leftMonitoring').onmousedown = function () {
        s.zpd({drag:true});
        s.panTo('-40');
        s.zpd({drag:false,zoom:false,pan:false});
        intervalF = setInterval(function () {
            s.zpd({drag:true});
            s.panTo('-40');
            s.zpd({drag:false,zoom:false,pan:false});
        }, 100);
    };
    document.getElementById('rightMonitoring').onmouseup = clearIntervalF;
    document.getElementById('rightMonitoring').onmouseleave = clearIntervalF;
    document.getElementById('rightMonitoring').onmousedown = function () {
        s.zpd({drag:true});
        s.panTo('+40');
        s.zpd({drag:false,zoom:false,pan:false});
        intervalF = setInterval(function () {
            s.zpd({drag:true});
            s.panTo('+40');
            s.zpd({drag:false,zoom:false,pan:false});
        }, 100);
    };
    document.getElementById('upMonitoring').onmouseup = clearIntervalF;
    document.getElementById('upMonitoring').onmouseleave = clearIntervalF;
    document.getElementById('upMonitoring').onmousedown = function () {
        s.zpd({drag:true});
        s.panTo('+0', '-40');
        s.zpd({drag:false,zoom:false,pan:false});
        intervalF = setInterval(function () {
            s.zpd({drag:true});
            s.panTo('+0', '-40');
            s.zpd({drag:false,zoom:false,pan:false});
        }, 100);
    };
    document.getElementById('downMonitoring').onmouseup = clearIntervalF;
    document.getElementById('downMonitoring').onmouseleave = clearIntervalF;
    document.getElementById('downMonitoring').onmousedown = function () {
        s.zpd({drag:true});
        s.panTo('+0', '+40');
        s.zpd({drag:false,zoom:false,pan:false});
        intervalF = setInterval(function () {
            s.zpd({drag:true});
            s.panTo('+0', '+40');
            s.zpd({drag:false,zoom:false,pan:false});
        }, 100);
    };
    document.getElementById('zoomInMonitoring').onmouseup = clearIntervalF;
    document.getElementById('zoomInMonitoring').onmouseleave = clearIntervalF;
    document.getElementById('zoomInMonitoring').onmousedown = function () {
        zoominratio = zoominratio+0.1;
        s.zpd({drag:true});
        s.zoomTo(zoominratio, 400);
        s.zpd({drag:false,zoom:false,pan:false});
        intervalF = setInterval(function () {
            zoominratio = zoominratio+0.1;
            s.zpd({drag:true});
            s.zoomTo(zoominratio, 400);
            s.zpd({drag:false,zoom:false,pan:false});
        }, 100);
    };
    document.getElementById('zoomOutMonitoring').onmouseup = clearIntervalF;
    document.getElementById('zoomOutMonitoring').onmouseleave = clearIntervalF;
    document.getElementById('zoomOutMonitoring').onmousedown = function () {
        if(zoominratio>=1.1)
        {
            zoominratio = zoominratio-0.1;
        }
        s.zpd({drag:true});
        s.zoomTo(zoominratio, 400);
        s.zpd({drag:false,zoom:false,pan:false});
        intervalF = setInterval(function () {
            if(zoominratio>=1.1)
            {
                zoominratio = zoominratio-0.1;
            }
            s.zpd({drag:true});
            s.zoomTo(zoominratio, 400);
            s.zpd({drag:false,zoom:false,pan:false});
        }, 100);
    };
    $('#restoreMonitoring').on('click',function(){
        s.zpd('destroy');
    });
});
