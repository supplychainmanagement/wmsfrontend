
function drawcarpath(svgsnap,carid,paths)
{
	var startx = carpoints[carid].xpos;
	var starty = carpoints[carid].ypos;
	var endx = 0;
	var endy = 0;
	for(var i in zonepoints)
	{
		if(zonepoints[i].zonename == "start" && zonepoints[i].pathstart)
		{
			endx = zonepoints[i].zonepointx;
			endy = zonepoints[i].zonepointy;
		}
	}
	var diffx = Math.abs(endx-startx);
	var diffy = Math.abs(endy-starty);
	if(startx<= endx)
	{
		var path = svgsnap.path(Snap.format("M{x},{y}v{dim['negative height']}h{dim.width}", {
	    x: startx,
	    y: starty,
	    dim: {
	        width:diffx,
	        "negative height": -diffy
	    	}
		}));
	}
	else
	{
		var path = svgsnap.path(Snap.format("M{x},{y}v{dim['negative height']}h{dim['negative width']}", {
	    x: startx,
	    y: starty,
	    dim: {
	        "negative width": -diffx,
	        "negative height":-diffy
	    }
	}));
	}
	
	path.attr({
		fill:'none',
		visibility:'hidden',
		'stroke-width':2,
		'stroke':"#FF0000"
	}); 
	paths.push(path);
	//var rect = svgsnap.rect(0,0,70,40).attr({ fill: 'blue', opacity: 1 });	
	//carpoints[carid].cargroup.drawAtPath( path,180, 2000 );

}

function drawcarendpath(svgsnap,carid,paths)
{
	var endx = carpoints[carid].xpos;
	var endy = carpoints[carid].ypos;
	var startx = 0;
	var starty = 0;
	for(var i in zonepoints)
	{
		if(zonepoints[i].zonename == "start" && zonepoints[i].pathstart)
		{
			startx = zonepoints[i].zonepointx;
			starty = zonepoints[i].zonepointy;
		}
	}
	var diffx = Math.abs(endx-startx);
	var diffy = Math.abs(endy-starty);
	if(startx<= endx)
	{
		var path = svgsnap.path(Snap.format("M{x},{y}h{dim.width}v{dim.height}v{dim['negative height']}", {
	    x: startx,
	    y: starty,
	    dim: {
	        width:diffx,
	        height:diffy+200,
	        "negative height": -200
	    	}
		}));
	}
	else
	{
		var path = svgsnap.path(Snap.format("M{x},{y}h{dim['negative width']}v{dim.height}v{dim['negative height']}", {
	    x: startx,
	    y: starty,
	    dim: {
	        "negative width": -diffx,
	        height:diffy+200,
	        "negative height":-200
	    }
	}));
	}
	
	path.attr({
		fill:'none',
		visibility:'hidden',
		'stroke-width':2,
		'stroke':"#FF0000"
	}); 
	paths.push(path);
	//var rect = svgsnap.rect(0,0,70,40).attr({ fill: 'blue', opacity: 1 });	
	//carpoints[carid].cargroup.drawAtPath( path,180, 2000 );

}

function drawtwopointspath(svgsnap,car,firstx,firsty,secondx,secondy,stepx,stepy,margin,margindiff,direction,start,paths,endIndex)
{
	var firstx = firstx;
	var firsty = firsty;
	var secondx = secondx;
	var secondy = secondy;
	var diffx = Math.round(((secondx-firstx)-(margin*margindiff))/stepx);
	var diffxx = ((secondx-firstx)-(margin*margindiff))/stepx;
	if(diffxx <0)
	{
		var diffx = Math.ceil(((secondx-firstx)-(margin*margindiff))/stepx);
	}
	else
	{
		var diffx = Math.floor(((secondx-firstx)-(margin*margindiff))/stepx);
	}
	var diffy = Math.round((secondy-firsty)/stepy);	
	
	var margindiff = margindiff;
	//var paths = new Array();
	if(diffx != 0 || diffy != 0) // Possible Error
	{
		if(direction && !start && diffy != 0)
		{
			// var path = svgsnap.path(Snap.format("M{x},{y}h{dim.width}",{
			// 	x:firstx,
			// 	y:firsty,
			// 	dim:{
			// 		width: stepx
			// 	}
			// }));
			var path = svgsnap.path("M"+firstx+" "+firsty+"L"+(stepx+firstx)+" "+firsty);
			path.attr({
				id:'direction-path',
				fill:'none',
				visibility:'hidden',
				'stroke-width':2,
				'stroke':"#FF0000"

			});
			
			firstx = firstx + stepx;
			
			if(secondx>firstx)
				diffx--;// = Math.round(((secondx-firstx)-(margin*margindiff))/stepx);
			else
				diffx--;
			paths.push(path);
		}
		else if(!direction && !start && diffy != 0)
		{
			// var path = svgsnap.path(Snap.format("M{x},{y}h{dim['negative width']}",{
			// 	x:firstx,
			// 	y:firsty,
			// 	dim:{
			// 		"negative width": -(stepx)
			// 	}
			// }));
			var path = svgsnap.path("M"+firstx+" "+firsty+"L"+(-stepx+firstx)+" "+firsty);
			path.attr({
				fill:'none',
				visibility:'hidden',
				'stroke-width':2,
				'stroke':"#FF0000"

			});
			firstx = firstx - stepx;
			if(secondx<firstx)
				diffx++;// = Math.round(((secondx-firstx)-(margin*margindiff))/stepx);
			else
				diffx++;
			paths.push(path);
		}
	}
	
	if(diffy < 0)
	{
		for(var i = diffy;i<0;i++)
		{
			// var path = svgsnap.path(Snap.format("M{x},{y}v{dim['negative height']}",{
			// 	x:firstx,
			// 	y:firsty,
			// 	dim:{
			// 		"negative height": -(stepy)
			// 	}
			// }));
			var path = svgsnap.path("M"+firstx+" "+firsty+"L"+firstx+" "+(firsty-stepy));
			path.attr({
				fill:'none',
				visibility:'hidden',
				'stroke-width':2,
				'stroke':"#FF0000"

			});
			paths.push(path);
			firsty = firsty - stepy;
		}
	}
	if(diffy > 0)
	{
		for(var i = 0;i<diffy;i++)
		{
			// var path = svgsnap.path(Snap.format("M{x},{y}v{dim.height}",{
			// 	x:firstx,
			// 	y:firsty,
			// 	dim:{
			// 		height: stepy
			// 	}
			// }));
			var path = svgsnap.path("M"+firstx+" "+firsty+"L"+(firstx)+" "+(firsty+stepy));
			path.attr({
				fill:'none',
				visibility:'hidden',
				'stroke-width':2,
				'stroke':"#FF0000"

			});
			paths.push(path);
			firsty = firsty + stepy;
		}
	}
	if(diffx < 0)
	{
		
		for(var i = -1;i<0;i++)
		{
			var increasemargin = 0;
			if(margindiff>0)
			{
				increasemargin = margin;
				//margindiff--;
			}
			var path = svgsnap.path(Snap.format("M{x},{y}h{dim['negative width']}",{
				x:firstx,
				y:firsty,
				dim:{
					"negative width": -((Math.abs(diffx)*stepx+margindiff*increasemargin))
				}
			}));
			path.attr({
				id:'path'+i,
				fill:'none',
				visibility:'hidden',
				'stroke-width':2,
				'stroke':"#FF0000"

			});
			paths.push(path);
			endIndex.push((paths.length)-1);
			firstx = firstx -((Math.abs(diffx)*stepx+margindiff*increasemargin));
			
		}
	}
	if(diffx > 0)
	{
		for(var i = 0;i<diffx;i++)
		{
			var increasemargin = margin;
			if(margindiff == 0)
			{
				increasemargin = 0;
			}
			else
				margindiff--;
			var path = svgsnap.path(Snap.format("M{x},{y}h{dim.width}",{
				x:firstx,
				y:firsty,
				dim:{
					width: stepx+increasemargin
				}
			}));
			//var path = svgsnap.path("M"+firstx+" "+firsty+"L"+(stepx+increasemargin+firstx)+" "+firsty);
			path.attr({
				fill:'none',
				visibility:'hidden',
				'stroke-width':2,
				'stroke':"#FF0000"

			});
			paths.push(path);
			if(i == diffx-1)
				endIndex.push((paths.length)-1);
			
			firstx = firstx + (stepx+increasemargin);
			
		}
	}
	
	//
	var newpoints = {xpos:firstx,ypos:firsty,'paths':paths};
	//for(var i in paths)
	//{
		// car.drawAtPath(paths[0],0, 7000 );
	
	//}
	return newpoints;
	// var zonepath = svgsnap.path(Snap.format("M{x},{y}h{dim.width}v{dim.height}h{dim['negative width']}z", {
	//     x: (x+(marginX/2)),
	//     y: (y+(marginY/2)+shift),
	//     dim: {
	//         width: scaleX+marginX,
	//         height: scaleY+marginY,
	//         "negative width": -(scaleX+marginX)
	//     }
	// }));
	// zonepath.attr({
	// 	fill:'none',
	// 	visibility:'visible',
	// 	'stroke-width':2,
	// 	'stroke':"#FF0000"
	// }); 
	// var car = cartexture.clone();
	// car.attr({
	// 	id:"car"+carid,
	// 	viewBox:"0, 250, 1000, 500",
	// 	visibility:"visible",
	// });
	
	// var rect = svgsnap.rect(0,0,70,40).attr({ fill: 'blue', opacity: 1 });	
	// rect.drawAtPath( zonepath,180, 7000 );


}
function drawzonepath (svgsnap,firstzone,secondzone,carid)
{
	var startposx = 0;
	var startposy = 0;
	var firstzonex = 0;
	var firstzoney = 0;
	var secondzonex = 0;
	var secondzoney = 0;
	var pathstepx = 0;
	var pathstepy = 0;
	var pathmargin = 0;
	var car = carpoints[carid].cargroup;
	for(var i in zonepoints)
	{
		if(zonepoints[i].zonename == "start" && zonepoints[i].pathstart)
		{
			startposx = zonepoints[i].zonepointx;
			startposy = zonepoints[i].zonepointy;
			pathstepx = zonepoints[i].pathstepx;
			pathstepy = zonepoints[i].pathstepy;
			pathmargin = zonepoints[i].pathmargin;

		}
		if(zonepoints[i].zonename == firstzone)
		{
			firstzonex = zonepoints[i].zonepointx;
			firstzoney = zonepoints[i].zonepointy;
			firstzoneindex = zonepoints[i].zoneindex;
		}
		if(zonepoints[i].zonename == secondzone)
		{
			secondzonex = zonepoints[i].zonepointx;
			secondzoney = zonepoints[i].zonepointy;
			secondzoneindex = zonepoints[i].zoneindex;
		}
	}
	
		+" second "+secondzonex+" "+ secondzoney+" pathStepx "+pathstepx+" pathStepy "+pathstepy);
	var rect = svgsnap.rect(0,0,20,30).attr({ fill: '#A78955', opacity: 1 });
	var paths = new Array();
	drawcarpath(svgsnap,carid,paths);
	var endIndex = new Array();
	// var path = svgsnap.path(Snap.format("M{x},{y}h{dim.width}",{
	// 			x:startposx-(pathmargin/2),
	// 			y:startposy,
	// 			dim:{
	// 				width: (pathmargin/2)
	// 			}
	// 		}));
	// 		path.attr({
	// 			fill:'none',
	// 			visibility:'visible',
	// 			'stroke-width':2,
	// 			'stroke':"#FF0000"

	// 		});
	// 		paths.push(path);

	var newpoints = drawtwopointspath(svgsnap,rect,startposx,startposy,firstzonex,firstzoney,pathstepx,pathstepy,pathmargin,firstzoneindex,true,true,paths,endIndex);
	var margindiff = Math.abs(secondzoneindex - firstzoneindex);
	var newpoints2= drawtwopointspath(svgsnap,rect,newpoints.xpos,newpoints.ypos,secondzonex,secondzoney,pathstepx,pathstepy,pathmargin,margindiff,true,false,paths,endIndex);
	//
	 if(secondzonex > firstzonex)
	 {
	 	var blablabla=drawtwopointspath(svgsnap,rect,newpoints2.xpos,newpoints2.ypos,startposx,startposy,pathstepx,pathstepy,pathmargin,secondzoneindex,true,false,paths,endIndex);
	 	
	 		//animatealongarray(car,blablabla.paths,0,500);
	 }
	 	
	 else
	 	
	 	{
	 		var blablabla = drawtwopointspath(svgsnap,rect,newpoints2.xpos,newpoints2.ypos,startposx,startposy,pathstepx,pathstepy,pathmargin,secondzoneindex,false,false,paths,endIndex);
	 		
	 		
	 	}
	 	drawcarendpath(svgsnap,carid,paths);
	 	
	 	rect.attr({visibility:'hidden'});
	 	// var carGroup = svgsnap.group();
	 	// carGroup.append(car);
		animatealongarray(carpoints[carid].cargroup,carpoints[carid].car,paths,0,endIndex,carid,0);
		//carpoints[carid].cargroup.drawAtPath(paths[2],180, 2000 );
	 	

}