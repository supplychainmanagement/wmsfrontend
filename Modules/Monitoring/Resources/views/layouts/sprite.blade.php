@extends('main&templates.MainTemplateMasterdata')
 @section('content')
 <style>
 .loadingfont {
  font-family: sans-serif;
  color: #fea223;
  font-size: 20px;
  font-weight: 200;
    }
.owl-theme .owl-controls .owl-page span{
    background: #f8f8f8;
}
.loading {
  position: absolute;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
  text-align: center;
  border: 2px solid;
  padding: 1em 1.5em;
  border-radius: .5em;
  box-shadow: 0 0.125em #fea223;
}
.loading--svg {
  display: block;
  margin: 0 auto .25em;
}
.loading--path {
  fill: transparent;
  stroke: currentColor;
  stroke-width: 6;
  stroke-dasharray: 200;
  stroke-dashoffset: 150;
  -webkit-animation-duration: 3s;
          animation-duration: 3s;
  -webkit-animation-name: bounce;
          animation-name: bounce;
  -webkit-animation-iteration-count: infinite;
          animation-iteration-count: infinite;
}

@-webkit-keyframes bounce {
  0% {
    stroke-width: 12;
    stroke-dashoffset: 150;
  }
  15% {
    stroke-width: 2;
  }
  30% {
    stroke-width: 12;
  }
  50% {
    stroke-width: 12;
    stroke-dashoffset: -150;
  }
  65% {
    stroke-width: 2;
  }
  80% {
    stroke-width: 12;
  }
  100% {
    stroke-width: 12;
    stroke-dashoffset: 150;
  }
}

@keyframes bounce {
  0% {
    stroke-width: 12;
    stroke-dashoffset: 150;
  }
  15% {
    stroke-width: 2;
  }
  30% {
    stroke-width: 12;
  }
  50% {
    stroke-width: 12;
    stroke-dashoffset: -150;
  }
  65% {
    stroke-width: 2;
  }
  80% {
    stroke-width: 12;
  }
  100% {
    stroke-width: 12;
    stroke-dashoffset: 150;
  }
}
 </style>
 <div class="row" style="background-color:#f5f5f5;background-image:  url('../images/bg.png')">
    <div class ="col-md-9">
          <div class = row style="margin-top:10px">
          <div class="col-md-4 col-md-offset-4">
            <div class="form-group">
              <div class="row">
                <label class="col-md-2 control-label">Site</label>
                <div class="col-md-10">                                        
                  <select class="form-control selectpicker show-tick" name = "sitename" data-live-search="true" id = "sitename" required>
                    @foreach($sites as $site)
                      <option>{{$site['SITE_name']}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
          </div>
          </div>
      <div class= "row" style="margin-top:15px">
          <div class="col-md-4 col-md-offset-4" id="message">

          </div>
      </div>
      <div class ="row" >
        <div class="loading loadingfont" id ="loadingdiv" hidden>
          <svg class="loading--svg" width="80px" height="75px" viewBox="-6 0 146 75">
              <path class="loading--path" d="M3.44921875,65.0390638 C3.44921875,-14.6640591 134.007812,-19.9999982 134.007812,65.0390639" ></path>
          </svg>
          - Loading -
        </div>
        <div id ="drawdiv" style="width:100%;margin-top:10px;margin-left:20px;">
            <svg id="firstsvg" style="width: 100%; ">
                <defs>
                <filter id="filteroff" x="0" y="0">
                  <feGaussianBlur in="SourceGraphic" stdDeviation="30" />
                </filter>
              </defs>
              <defs>
                <filter id="filteron" x="0" y="0" width="200%" height="200%">
                  <feOffset result="offOut" in="SourceGraphic" dx="20" dy="20" />
                  <feColorMatrix result = "matrixOut" in = "offOut" type = "matrix" values = "0.2 0 0 0 0 0 0.2 0 0 0 0 0 0.2 0 0 0 0 0 1 0"/>
                  <feGaussianBlur result="blurOut" in="matrixOut" stdDeviation="10" />
                  <feBlend in="SourceGraphic" in2="blurOut" mode="normal" />
                </filter>
              </defs>
                @include('monitorsvg.zonetexture')  
                @include('monitorsvg.plussign') 
                @include('monitorsvg.rack-top-view')     
                @include('monitorsvg.goods-in-out-texture')         
                @include('monitorsvg.rack-frontleft')            
                @include('monitorsvg.rack-frontfull')
                @include('monitorsvg.car-texture')
                @include('monitorsvg.carwithpallet')
                @include('monitorsvg.parkingarea')
                @include('monitorsvg.unitload')
                
            </svg>
        </div>
      </div>
    </div>
    <div class="col-md-3">
        <div class="row">
       <!--      <div class="widget widget-primary widget-carousel" style ="margin-top:20px">
                <div class="owl-carousel" id="owl-example">
                    <div>                                    
                        <div class="widget-title">Total</div>                                                                        
                        <div class="widget-subtitle">Assets</div>
                        <div class="widget-int" id="total-assets">3,548</div>
                    </div>
                    <div>                                    
                        <div class="widget-title">Missplaced </div>
                        <div class="widget-subtitle">Assets</div>
                        <div class="widget-int" id ="missplaced-assets">1,695</div>
                    </div>
                    <div>                                    
                        <div class="widget-title">Missing</div>
                        <div class="widget-subtitle">Assets</div>
                        <div class="widget-int"id="missing-assets">1,977</div>
                    </div>
                </div>                            
                <div class="widget-controls">                                
                    <a href="#" class="widget-control-right"><span class="fa fa-times"></span></a>
                </div>                             
            </div>
        </div>
 -->
        <div class="row" style="margin-bottom: 15px">
            <div class="col-md-4 col-md-offset-4">
            @include('monitorsvg.navigation')
            </div>

        </div>    

        <div class="row">
            <div class="panel panel-default panel-toggled">
                <div class="panel-heading">
                    <h3 class="panel-title"><span class="glyphicon glyphicon-search"></span> Quick search panel</h3>                                
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>                                   
                </div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-4">
                                <input type="text" class="form-control" name = "zonepicker" id="zonepicker" />
                            </div>
                             <div class="col-md-3">
                                <button id="zonefinder"type="button" class="btn btn-success">Find</button>
                            </div>
                            <div class="col-md-5">
                                <span class="help-block">Enter Zone Name</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('Jsplugin')
<script type="text/javascript" src="{{asset('js/snap/snap.svg.js')}}"></script>
<script type="text/javascript" src="{{asset('js/snap/snap.svg.zpd.js')}}"></script>
<script type="text/javascript" src="{{asset('js/snap/animatepath.js')}}"></script>
<script type="text/javascript" src="{{asset('js/sprite/rack.js')}}"></script>
<script type="text/javascript" src="{{asset('js/sprite/zone.js')}}"></script>   
<script type="text/javascript" src="{{asset('js/sprite/sprite.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/plugins/owl/owl.carousel.min.js')}}"></script>   
@stop