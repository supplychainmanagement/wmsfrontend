<svg version="1.0" id="car_texture" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="100px" height="50px" viewBox="0 0 100 50" enable-background="new 0 0 100 50" xml:space="preserve" style="visibility:hidden;">
<g class = "carmaingroup">
<g class="nopallet">
	<polygon fill="#F68D4F" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" points="64.485,39.305 48.675,39.431 
		48.487,38.98 48.449,38.229 48.524,36.086 52.621,36.424 53.541,36.333 54.876,36.838 57.507,37.439 57.996,37.439 58.334,37.176 
		58.597,36.876 57.883,36.5 57.996,36.086 58.484,36.274 58.622,34.369 58.822,34.369 59.273,33.993 61.98,33.944 62.543,34.019 
		63.032,34.044 64.811,34.019 65.086,34.219 65.111,34.445 65.136,38.779 64.936,39.08 64.648,39.394 	"/>
	<polygon fill="#F68D4F" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" points="23.873,9.107 24.221,9.676 
		40.636,8.618 41.162,8.618 41.763,8.881 42.44,9.295 42.966,10.084 46.323,10.084 47.176,10.122 48.115,10.009 48.59,9.934 
		48.604,7.829 42.102,7.679 21.13,7.904 20.979,7.904 20.897,7.822 20.859,7.774 11.463,7.774 7.291,7.962 6.314,8.338 5.412,8.751 
		4.999,9.089 4.172,10.104 2.48,13.449 1.015,17.32 0.376,19.613 0,22.808 0.188,25.476 0.526,27.13 1.24,29.422 2.293,32.053 
		3.345,34.384 4.134,35.962 5.149,36.939 6.314,37.879 7.517,38.18 20.822,38.255 21.047,37.766 21.192,37.146 21.192,37.445 
		20.892,38.121 48.479,38.196 48.554,36.054 47.313,35.829 44.908,35.791 43.48,35.791 42.677,35.904 42.202,36.843 41.676,37.144 
		41.262,37.257 32.994,36.919 24.537,36 23.936,36.919 23.748,37.106 21.21,37.069 21.716,34.909 15.203,35.444 14.577,35.286 
		14.324,34.721 13.831,11.382 14.207,10.818 14.846,10.555 21.469,10.438 21.122,8.601 21.116,8.581 23.798,8.581 	"/>
	<polyline fill="#F68D4F" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" points="48.461,9.987 48.475,7.873 
		48.285,7.533 48.323,7.042 48.399,6.626 64.428,6.664 64.807,6.853 65.073,7.382 65.136,11.409 64.997,11.799 64.833,11.912 
		63.098,11.988 62.529,11.988 61.921,12.139 59.199,12.176 58.781,11.686 58.477,11.585 58.502,10.251 	"/>
	<path fill="#3D3D3C" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M61.579,33.995l-2.305,0.1l-0.451,0.376h-0.2
		l-0.025-0.476l-1.102,0.025l0.025-22.05l0.927,0.075l0.125-0.501h0.326v0.15l0.351,0.351l2.481,0.025l-0.476,0.351l-0.777,0.877
		L60.075,14.3c0,0-0.2,16.637-0.1,16.913c0.1,0.276,0.426,1.478,0.426,1.478L61.579,33.995z"/>
	<polygon fill="#1D1D1B" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" points="49.727,39.415 49.727,39.691 
		50.002,40.167 68.769,40.191 69.045,40.091 69.195,39.891 69.22,33.777 69.195,33.452 68.97,33.251 68.368,33.176 68.193,33.377 
		64.935,33.978 65.086,34.228 65.111,34.454 65.136,38.789 64.935,39.089 64.485,39.315 	"/>
	<path fill="#1D1D1B" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M65.036,6.985c0,0,0.15,0.551,0.276,0.1
		l0.125-0.451l-0.351-0.05l0.075,0.326v4.46l-0.3,0.501l3.182,0.702l0.326,0.201l0.676-0.1l0.15-0.326l0.05-6.014l-0.15-0.301
		l-0.25-0.226H50.203l-0.226,0.05l-0.201,0.276l-0.025,0.451l14.52,0.073l0.313,0.002"/>
	<polygon fill="#3D3D3C" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" points="20.524,35.549 20.41,33.782 
		18.32,33.82 18.357,32.843 18.13,32.467 17.635,32.128 13.563,32.162 13.796,34.759 14.063,35.286 14.595,35.624 14.823,35.661 	
		"/>
	<polygon fill="#3D3D3C" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" points="43.037,10.106 42.398,10.144 
		24.358,10.144 24.433,9.43 30.559,9.016 38.753,8.828 40.707,8.64 41.496,8.678 42.173,9.016 	"/>
	<polygon fill="#3D3D3C" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" points="42.962,36.037 41.759,35.887 
		35.671,35.924 24.508,36 24.32,36.714 29.62,36.827 32.964,37.052 38.151,37.165 40.744,37.315 41.647,37.278 42.173,36.977 	"/>
	<polygon fill="#847A73" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" points="57.681,11.956 58.608,12.031 
		58.759,9.676 58.12,9.864 57.368,9.976 56.842,10.089 55.528,10.314 55.528,9.49 49.358,10.673 49.358,36.007 55.528,36.007 
		55.528,35.634 56.428,35.872 57.025,36.131 57.518,36.06 58.646,36.361 58.721,34.143 57.656,34.005 	"/>
	<polygon fill="#847A73" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" points="21.166,8.48 21.127,9.866 
		21.485,10.366 20.031,10.385 20.031,10.385 14.255,10.348 13.577,10.616 13.377,10.999 13.377,32.153 17.175,32.344 17.699,32.689 
		17.941,33.072 18.087,33.916 20.531,34.089 20.531,33.628 20.801,33.149 21.48,33.187 21.52,13.03 21.477,13.01 21.672,12.951 
		22.186,12.866 21.886,11.152 21.437,10.255 22.236,11.254 22.48,10.46 24.535,10.104 23.861,8.48 	"/>
	<polygon fill="#847A73" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" points="21.426,35.511 22.14,34.609 
		22.215,33.143 21.689,32.955 21.238,32.843 20.674,33.444 20.41,33.782 20.524,35.549 	"/>
	<path fill="#1D1D1B" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M66.353,40.259
		C66.333,40.239,66.334,40.24,66.353,40.259"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="25.42" y1="9.284" x2="32.473" y2="8.995"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="68.247" y1="12.712" x2="68.247" y2="12.712"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="57.369" y1="8.544" x2="57.405" y2="8.526"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M49.341,11.792"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M49.287,11.737"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M49.63,11.593"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M49.485,11.539"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M49.63,11.737"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M49.63,11.593"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="40.324" y1="8.671" x2="32.495" y2="8.995"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="41.963" y1="7.655" x2="41.963" y2="8.887"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="42.089" y1="9.031" x2="41.872" y2="8.887"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="42.955" y1="10.114" x2="42.089" y2="9.01"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="41.872" y1="8.887" x2="41.21" y2="8.587"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="41.121" y1="8.62" x2="40.395" y2="8.66"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="33.068" y1="8.977" x2="33.05" y2="8.977"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="33.068" y1="8.977" x2="33.087" y2="8.977"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="65.127" y1="38.762" x2="65.127" y2="34.667"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="64.639" y1="34.107" x2="63.16" y2="34.107"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="62.024" y1="33.963" x2="61.897" y2="33.963"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="41.963" y1="37.878" x2="41.963" y2="37.139"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="13.224" y1="33.638" x2="13.224" y2="12.386"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="10.843" y1="16.969" x2="3.825" y2="16.969"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="21.162" y1="9.915" x2="14.938" y2="9.915"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="21.162" y1="9.861" x2="22.154" y2="11.413"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="22.317" y1="10.132" x2="21.433" y2="8.815"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="23.471" y1="8.797" x2="24.355" y2="10.131"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="24.403" y1="9.896" x2="24.403" y2="9.328"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="25.293" y1="9.284" x2="25.293" y2="8.706"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="22.154" y1="11.25" x2="22.173" y2="11.25"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="22.19" y1="10.979" x2="22.19" y2="10.979"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="22.19" y1="10.979" x2="22.19" y2="10.962"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="22.19" y1="10.835" x2="22.19" y2="10.727"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="11.42" y1="8.508" x2="11.42" y2="7.787"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="13.224" y1="12.261" x2="13.531" y2="12.243"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="13.531" y1="12.387" x2="13.224" y2="12.387"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="10.843" y1="29.074" x2="3.825" y2="29.074"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="20.386" y1="35.388" x2="18.42" y2="35.388"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="21.162" y1="36.11" x2="14.938" y2="36.11"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="21.396" y1="35.749" x2="21.396" y2="35.749"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="21.162" y1="36.164" x2="22.064" y2="34.793"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="13.873" y1="33.729" x2="13.53" y2="33.783"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="13.224" y1="33.783" x2="13.531" y2="33.783"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="33.05" y1="8.977" x2="33.015" y2="8.977"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="49.359" y1="23.915" x2="49.359" y2="23.915"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="49.359" y1="23.915" x2="49.359" y2="23.86"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="64.856" y1="11.918" x2="67.905" y2="12.495"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="67.904" y1="33.512" x2="64.856" y2="34.107"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="69.185" y1="36.796" x2="69.185" y2="33.783"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="68.716" y1="33.314" x2="68.23" y2="33.314"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="68.247" y1="12.712" x2="68.717" y2="12.712"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="18.42" y1="33.656" x2="18.42" y2="33.801"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="18.42" y1="33.656" x2="18.42" y2="33.169"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="18.42" y1="35.461" x2="21.595" y2="35.461"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="18.42" y1="35.461" x2="18.42" y2="35.388"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="18.42" y1="35.388" x2="18.42" y2="33.801"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="17.247" y1="32.015" x2="13.422" y2="32.128"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M14.234,32.015"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="14.992" y1="34.829" x2="14.848" y2="34.829"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="14.811" y1="33.097" x2="14.992" y2="33.097"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M68.247,12.712
		c-0.114-0.1-0.199-0.15-0.343-0.198"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M64.856,11.918
		c0.164-0.146,0.275-0.31,0.27-0.542"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="64.874" y1="11.918" x2="64.657" y2="11.9"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M65.127,34.667
		c-0.02-0.197-0.085-0.449-0.27-0.559"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M64.657,34.125
		c0.072-0.007,0.144-0.015,0.217-0.018"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M61.843,33.927
		c0.013,0.021,0.031,0.035,0.054,0.036"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="43.027" y1="35.93" x2="43.027" y2="35.93"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M14.811,33.097
		c-0.269,0.221-0.487,0.459-0.487,0.828c0,0.407,0.201,0.655,0.505,0.904"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M14.992,34.829
		c0.373,0.508,0.917,0.9,1.584,0.805c0.6-0.085,1.22-0.446,1.422-1.053c0.226-0.677,0.064-1.221-0.347-1.781
		c-0.351-0.479-1.177-0.606-1.698-0.432c-0.382,0.127-0.744,0.383-0.96,0.729"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M14.992,34.829
		c0.373,0.508,0.917,0.9,1.584,0.805c0.6-0.085,1.22-0.446,1.422-1.053c0.226-0.677,0.064-1.221-0.347-1.781
		c-0.351-0.479-1.177-0.606-1.698-0.432c-0.382,0.127-0.744,0.383-0.96,0.729"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M69.185,33.783
		c-0.054-0.22-0.19-0.495-0.469-0.451"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M68.716,12.712
		c0.22-0.028,0.52-0.195,0.469-0.469"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M18.42,33.169
		c-0.052-0.633-0.509-1.155-1.172-1.155"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="29.587" y1="34.721" x2="34.512" y2="34.721"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M34.512,11.304
		c-1.642,0-3.283,0.006-4.925,0.018"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M41.963,8.147
		c-2.763,0.09-5.527,0.18-8.29,0.269c-2.735,0.089-5.47,0.189-8.205,0.284c-0.058,0.002-0.117,0.004-0.175,0.006"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M57.712,8.544
		c-0.104-0.013-0.202-0.018-0.307-0.018"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M44.831,11.268
		c-2.772,0.01-5.544,0.02-8.316,0.031l-1.444,0.005"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M49.485,11.503"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M49.359,11.737"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M49.63,11.737"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M49.377,11.539"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M49.485,11.503"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="41.963" y1="8.075" x2="41.963" y2="8.147"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M41.963,8.941
		c-0.028-0.021-0.058-0.039-0.09-0.054"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="33.087" y1="8.977" x2="33.068" y2="8.977"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="33.068" y1="8.977" x2="33.05" y2="8.977"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="35.071" y1="11.304" x2="34.512" y2="11.304"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M61.951,33.963
		c-0.032,0.008-0.044-0.004-0.036-0.036"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="58.415" y1="36.344" x2="58.542" y2="36.344"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="34.512" y1="34.721" x2="35.071" y2="34.721"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M48.529,38.076
		c0.006,0.006,0.012,0.012,0.018,0.018"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="42.017" y1="38.112" x2="42.017" y2="38.094"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="42.017" y1="38.094" x2="41.998" y2="38.094"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="41.999" y1="38.094" x2="41.999" y2="38.076"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M41.999,38.076
		c0.003-0.016-0.004-0.028-0.018-0.036"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M41.98,38.04
		c-0.015-0.022-0.022-0.046-0.018-0.073"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="41.963" y1="37.968" x2="41.963" y2="37.878"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="42.017" y1="38.04" x2="41.98" y2="38.04"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M2.417,20.469
		c0.123-0.676,0.271-1.322,0.451-1.985"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M10.843,29.074
		c-0.244-2.62-0.327-5.243-0.258-7.873c0.037-1.415,0.125-2.823,0.258-4.232"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M5.412,11.737
		c0.219-0.438,0.47-0.913,0.837-1.248C6.668,10.108,7.085,9.8,7.577,9.519"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="22.19" y1="10.619" x2="22.19" y2="10.438"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M22.172,10.15c2.471,0,4.943,0.006,7.414,0.018"
		/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M25.42,9.284
		c-0.326,0.012-0.649,0.03-0.974,0.054"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M22.136,8.815c0.464,0,0.926-0.006,1.389-0.018"
		/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M25.293,8.706c-0.45,0-0.923,0.02-1.353,0.162"
		/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="21.162" y1="9.843" x2="21.162" y2="9.915"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="22.19" y1="10.438" x2="22.19" y2="10.294"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="21.27" y1="8.815" x2="22.136" y2="8.815"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M29.587,11.322
		c-2.472-0.012-4.943-0.018-7.414-0.018"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="22.19" y1="10.727" x2="22.19" y2="10.691"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="22.19" y1="10.691" x2="22.19" y2="10.618"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="22.19" y1="10.998" x2="22.19" y2="10.962"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="22.19" y1="10.962" x2="22.19" y2="10.962"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="22.19" y1="10.979" x2="22.19" y2="10.944"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="22.19" y1="10.944" x2="22.19" y2="10.907"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="22.19" y1="10.907" x2="22.19" y2="10.889"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="22.19" y1="10.889" x2="22.19" y2="10.835"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M14.938,9.915
		c-0.82,0.096-1.31,0.563-1.407,1.389"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M8.01,9.32c0.506-0.238,0.997-0.425,1.541-0.561
		c0.556-0.139,1.118-0.186,1.688-0.233"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M11.42,8.508
		c-0.058,0.016-0.12,0.022-0.18,0.018"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="11.24" y1="8.526" x2="11.601" y2="8.526"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M11.42,8.508
		c0.058,0.016,0.12,0.022,0.181,0.018"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M7.577,9.518C7.722,9.453,7.866,9.387,8.01,9.32
		"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M10.843,16.969
		c0.119-0.949,0.249-1.917,0.595-2.814c0.318-0.822,0.833-1.767,1.786-1.894"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="13.531" y1="12.243" x2="13.531" y2="12.387"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="13.224" y1="12.261" x2="13.224" y2="12.387"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="13.531" y1="11.304" x2="13.531" y2="12.243"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M3.41,16.843
		c0.627-1.722,1.278-3.422,2.002-5.105"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M2.869,18.484
		c0.167-0.526,0.341-1.048,0.523-1.569"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M3.392,16.915
		c0.011-0.022,0.017-0.048,0.018-0.072"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M7.577,36.507
		c-0.955-0.545-1.669-1.21-2.165-2.201"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M21.162,36.11
		c-0.012,0.343-0.024,0.685-0.036,1.028"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M29.587,35.875
		c-1.708,0-3.415,0.006-5.124,0.018"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="24.066" y1="34.721" x2="29.587" y2="34.721"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="29.587" y1="34.992" x2="21.958" y2="34.992"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="29.587" y1="35.875" x2="24.508" y2="36"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M22.226,34.739c0.614,0,1.226-0.006,1.84-0.018"
		/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="24.066" y1="34.991" x2="24.048" y2="34.991"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M20.44,35.388
		c0.024-0.435,0.036-0.864,0.036-1.299"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M20.476,34.09
		c-0.006-0.095,0.003-0.181,0.036-0.271"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M20.512,33.819
		c-0.004-0.026,0.003-0.051,0.018-0.073"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="20.53" y1="33.746" x2="20.53" y2="33.729"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="20.386" y1="35.461" x2="20.386" y2="35.425"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="20.386" y1="35.425" x2="20.386" y2="35.388"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M20.386,35.461
		c0.032,0.008,0.044-0.004,0.036-0.036"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M20.422,35.425
		c0.014-0.008,0.02-0.02,0.018-0.036"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="20.44" y1="35.388" x2="20.385" y2="35.388"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M13.224,33.783
		c-0.964-0.192-1.465-1.047-1.786-1.912c-0.334-0.901-0.473-1.847-0.595-2.796"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="13.531" y1="33.783" x2="13.531" y2="34.721"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="13.224" y1="33.783" x2="13.224" y2="33.638"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M13.531,34.721
		c0.089,0.805,0.585,1.34,1.407,1.389"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M21.126,37.571
		c-2.689,0-5.376-0.054-8.065-0.054H11.6"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M21.126,8.555c-2.689,0-5.376-0.054-8.065-0.054
		H11.6"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M21,38.131
		c-0.047,0.016-0.095,0.022-0.144,0.018"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M21.126,37.571
		c-0.058,0.139-0.118,0.277-0.181,0.415"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="21.126" y1="37.138" x2="21.126" y2="37.571"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="17.933" y1="38.239" x2="18.077" y2="38.239"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M11.24,37.499
		c-0.552-0.03-1.093-0.065-1.631-0.2c-0.557-0.139-1.074-0.344-1.598-0.575"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M11.24,37.499
		c0.058,0.017,0.12,0.023,0.18,0.018"/>
	<line fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" x1="11.601" y1="37.517" x2="11.42" y2="37.517"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M11.601,37.517
		c-0.122-0.012-0.238-0.018-0.361-0.018"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M8.01,36.723
		c-0.148-0.068-0.29-0.139-0.433-0.217"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M3.392,29.128
		c-0.178-0.522-0.352-1.045-0.523-1.57"/>
	
		<path fill="none" stroke="#1D1D1B" stroke-width="1.5246" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
		M2.417,25.556c-0.214-1.713-0.214-3.374,0-5.087"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M2.869,27.559
		c-0.183-0.669-0.329-1.32-0.451-2.003"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M5.412,34.306
		c-0.724-1.683-1.369-3.387-2.002-5.105"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M3.41,29.201
		c0.003-0.026-0.003-0.051-0.019-0.072"/>
	<path fill="none" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M3.825,29.074
		c-0.016-0.029-0.028-0.059-0.036-0.09"/>
	<polygon fill="#3D3D3C" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" points="69.621,28.357 69.628,29.056 
		69.621,33.555 68.017,33.555 67.566,33.519 64.61,34.12 62.831,34.145 61.327,33.694 60.451,32.967 59.874,31.74 59.849,31.038 
		59.874,14.3 60.275,13.299 60.801,12.772 61.503,12.246 62.58,12.02 64.66,11.845 67.692,12.571 67.917,12.597 68.247,12.612 
		69.844,12.176 69.671,14.125 69.631,26.477 	"/>
	<path fill="#3D3D3C" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M71.575,29.986l27.286-0.025l0.952,0.401
		l0.376,0.451c0,0,0.15,1.704,0.075,1.704c-0.075,0-0.501,0.576-0.501,0.576l-0.977,0.326l-27.236,0.05l0.05,4.56l-1.604,0.025
		l-0.05-0.376l-0.326,0.05l0.01-11.252l0.015-18.165h0.251l0.075-0.251L71.6,8.087l0.025,4.535h27.086l0.877,0.301l0.676,0.551
		l-0.05,1.754l-0.351,0.501l-0.777,0.326L71.6,16.005L71.575,29.986z"/>
	
		<rect x="20.437" y="10.781" fill="#847A73" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" width="28.91" height="24.247"/>
	<polyline fill="#847A73" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" points="58.597,9.311 58.559,8.898 
		58.559,8.598 58.219,8.334 57.728,8.146 56.67,8.334 55.575,8.56 54.366,8.823 52.741,9.048 51.872,9.349 50.777,9.349 
		48.661,9.575 47.792,9.65 46.394,9.725 44.845,9.763 22.178,9.688 22.14,11.04 32.076,10.965 34.834,10.965 41.596,10.777 
		47.188,10.815 49.908,10.74 53.573,10.176 55.726,9.8 57.199,9.612 58.446,9.349 	"/>
	<polyline fill="#847A73" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" points="58.503,36.813 58.615,37.001 
		58.352,37.302 57.864,37.452 57.525,37.565 55.872,37.227 55.082,36.964 53.805,36.738 52.339,36.625 50.911,36.4 49.67,36.137 
		48.317,36.099 47.077,36.024 45.311,35.949 43.694,35.949 42.191,35.911 39.71,35.986 37.606,35.836 34.975,35.799 24.526,36.024 
		23.737,37.264 21.332,37.339 21.144,36.024 22.159,34.633 48.806,34.746 49.971,35.009 52.527,35.197 54.444,35.46 55.834,35.686 
		56.285,35.911 58.014,36.212 57.901,36.625 58.39,36.663 	"/>
</g>
<g class = "palletbox" style="visibility:hidden">
	<g>
		<g>
			<polygon fill="#F68D4F" stroke="#010101" stroke-width="0.25" stroke-miterlimit="10" points="64.345,40.237 48.569,40.363 
				48.382,39.913 48.344,39.163 48.419,37.025 52.507,37.362 53.425,37.271 54.757,37.775 57.382,38.375 57.87,38.375 58.207,38.112 
				58.47,37.812 57.757,37.437 57.87,37.025 58.357,37.212 58.495,35.312 58.695,35.312 59.145,34.937 61.845,34.887 62.408,34.962 
				62.895,34.987 64.67,34.962 64.945,35.162 64.97,35.387 64.995,39.712 64.795,40.012 64.508,40.325 		"/>
			<polygon fill="#F68D4F" stroke="#010101" stroke-width="0.25" stroke-miterlimit="10" points="23.821,10.104 24.168,10.672 
				40.548,9.617 41.073,9.617 41.673,9.879 42.348,10.292 42.873,11.079 46.223,11.079 47.073,11.117 48.011,11.004 48.485,10.929 
				48.498,8.829 42.01,8.679 21.084,8.904 20.934,8.904 20.851,8.822 20.814,8.774 11.438,8.774 7.275,8.962 6.3,9.337 5.4,9.749 
				4.988,10.087 4.163,11.099 2.475,14.437 1.012,18.3 0.375,20.587 0,23.775 0.187,26.438 0.525,28.088 1.238,30.376 2.288,33.001 
				3.338,35.326 4.125,36.901 5.138,37.876 6.3,38.814 7.501,39.114 20.777,39.189 21.002,38.701 21.146,38.082 21.146,38.38 
				20.846,39.055 48.373,39.131 48.448,36.993 47.211,36.768 44.811,36.73 43.386,36.73 42.585,36.843 42.11,37.781 41.585,38.08 
				41.173,38.193 32.922,37.855 24.484,36.939 23.884,37.855 23.697,38.043 21.164,38.006 21.669,35.85 15.17,36.384 14.545,36.227 
				14.293,35.662 13.801,12.374 14.176,11.812 14.814,11.549 21.422,11.433 21.077,9.599 21.07,9.579 23.746,9.579 		"/>
			<polyline fill="#F68D4F" stroke="#010101" stroke-width="0.25" stroke-miterlimit="10" points="48.356,10.982 48.369,8.872 
				48.18,8.533 48.218,8.043 48.294,7.629 64.288,7.667 64.667,7.855 64.932,8.383 64.994,12.401 64.856,12.79 64.692,12.903 
				62.961,12.979 62.393,12.979 61.786,13.129 59.071,13.167 58.654,12.677 58.35,12.577 58.375,11.246 		"/>
			<path fill="#3D3D3C" d="M61.445,34.938l-2.3,0.1l-0.45,0.375h-0.2l-0.025-0.475l-1.1,0.025l0.025-22.002l0.925,0.075l0.125-0.5
				h0.325v0.15l0.35,0.35l2.475,0.025l-0.475,0.35l-0.775,0.875l-0.4,1c0,0-0.2,16.601-0.1,16.876c0.1,0.275,0.425,1.475,0.425,1.475
				L61.445,34.938z"/>
			<polygon fill="#1D1D1B" points="49.619,40.346 49.619,40.621 49.894,41.096 68.62,41.121 68.895,41.021 69.045,40.821 
				69.07,34.721 69.045,34.396 68.82,34.196 68.22,34.121 68.045,34.321 64.794,34.921 64.945,35.171 64.97,35.396 64.995,39.721 
				64.794,40.021 64.345,40.246 		"/>
			<path fill="#1D1D1B" d="M64.895,7.986c0,0,0.15,0.55,0.275,0.1l0.125-0.45l-0.35-0.05l0.075,0.325v4.45l-0.3,0.499l3.175,0.701
				l0.325,0.2l0.675-0.1l0.15-0.325l0.05-6.001l-0.15-0.3l-0.25-0.225H50.094l-0.225,0.05l-0.201,0.275l-0.025,0.45L64.132,7.66
				l0.313,0.002"/>
			<polygon fill="#3D3D3C" points="20.48,36.489 20.366,34.726 18.28,34.763 18.318,33.788 18.09,33.413 17.597,33.076 
				13.533,33.109 13.767,35.701 14.032,36.226 14.563,36.564 14.791,36.601 		"/>
			<polygon fill="#3D3D3C" points="42.944,11.101 42.306,11.139 24.305,11.139 24.38,10.426 30.493,10.014 38.668,9.826 
				40.619,9.639 41.406,9.676 42.081,10.014 		"/>
			<polygon fill="#3D3D3C" points="42.869,36.976 41.669,36.826 35.593,36.864 24.455,36.939 24.267,37.651 29.555,37.764 
				32.893,37.989 38.068,38.101 40.656,38.251 41.556,38.214 42.081,37.914 		"/>
			<polygon fill="#847A73" stroke="#030303" stroke-width="0.25" stroke-miterlimit="10" points="57.556,12.947 58.481,13.022 
				58.631,10.672 57.994,10.859 57.243,10.972 56.718,11.084 55.408,11.309 55.408,10.487 49.251,11.667 49.251,36.945 
				55.408,36.945 55.408,36.573 56.306,36.811 56.902,37.07 57.394,36.999 58.519,37.299 58.594,35.086 57.531,34.948 		"/>
			<polygon fill="#847A73" points="21.12,9.478 21.081,10.862 21.438,11.361 19.988,11.379 19.988,11.379 14.224,11.342 
				13.547,11.61 13.348,11.993 13.348,33.1 17.137,33.291 17.661,33.635 17.902,34.018 18.048,34.859 20.486,35.032 20.486,34.572 
				20.756,34.094 21.433,34.132 21.473,14.019 21.43,13.999 21.625,13.94 22.137,13.855 21.838,12.145 21.391,11.25 22.188,12.246 
				22.431,11.454 24.482,11.1 23.809,9.478 		"/>
			<polygon fill="#847A73" points="21.38,36.451 22.092,35.551 22.167,34.088 21.642,33.901 21.192,33.788 20.63,34.388 
				20.366,34.726 20.48,36.489 		"/>
			<path fill="#1D1D1B" d="M66.209,41.189C66.189,41.169,66.19,41.169,66.209,41.189"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="25.364" y1="10.281" x2="32.403" y2="9.993"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="68.099" y1="13.701" x2="68.099" y2="13.701"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="57.244" y1="9.543" x2="57.28" y2="9.525"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M49.234,12.783"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M49.18,12.729"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M49.522,12.585"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M49.378,12.531"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M49.522,12.729"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M49.522,12.585"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="40.237" y1="9.669" x2="32.424" y2="9.993"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="41.871" y1="8.655" x2="41.871" y2="9.885"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="41.997" y1="10.029" x2="41.782" y2="9.885"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="42.862" y1="11.109" x2="41.997" y2="10.007"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="41.782" y1="9.885" x2="41.12" y2="9.586"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="41.032" y1="9.619" x2="40.307" y2="9.659"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="32.997" y1="9.975" x2="32.979" y2="9.975"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="32.997" y1="9.975" x2="33.015" y2="9.975"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="64.985" y1="39.695" x2="64.985" y2="35.608"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="64.499" y1="35.05" x2="63.023" y2="35.05"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="61.889" y1="34.906" x2="61.763" y2="34.906"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="41.871" y1="38.813" x2="41.871" y2="38.075"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="13.195" y1="34.582" x2="13.195" y2="13.377"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="10.819" y1="17.949" x2="3.817" y2="17.949"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="21.116" y1="10.911" x2="14.905" y2="10.911"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="21.116" y1="10.857" x2="22.106" y2="12.405"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="22.268" y1="11.127" x2="21.386" y2="9.813"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="23.42" y1="9.795" x2="24.302" y2="11.126"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="24.35" y1="10.891" x2="24.35" y2="10.324"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="25.238" y1="10.281" x2="25.238" y2="9.705"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="22.106" y1="12.243" x2="22.124" y2="12.243"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="22.142" y1="11.973" x2="22.142" y2="11.973"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="22.142" y1="11.973" x2="22.142" y2="11.955"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="22.142" y1="11.829" x2="22.142" y2="11.721"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="11.395" y1="9.507" x2="11.395" y2="8.787"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="13.195" y1="13.251" x2="13.502" y2="13.233"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="13.501" y1="13.377" x2="13.195" y2="13.377"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="10.819" y1="30.028" x2="3.817" y2="30.028"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="20.341" y1="36.329" x2="18.38" y2="36.329"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="21.116" y1="37.049" x2="14.905" y2="37.049"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="21.35" y1="36.689" x2="21.35" y2="36.689"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="21.116" y1="37.102" x2="22.016" y2="35.734"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="13.843" y1="34.672" x2="13.501" y2="34.727"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="13.195" y1="34.726" x2="13.502" y2="34.726"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="32.979" y1="9.975" x2="32.943" y2="9.975"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="49.252" y1="24.88" x2="49.252" y2="24.88"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="49.252" y1="24.88" x2="49.252" y2="24.826"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="64.715" y1="12.909" x2="67.757" y2="13.485"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="67.757" y1="34.456" x2="64.715" y2="35.05"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="69.035" y1="37.733" x2="69.035" y2="34.726"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="68.567" y1="34.259" x2="68.081" y2="34.259"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="68.099" y1="13.701" x2="68.567" y2="13.701"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="18.38" y1="34.6" x2="18.38" y2="34.744"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="18.38" y1="34.6" x2="18.38" y2="34.114"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="18.38" y1="36.401" x2="21.548" y2="36.401"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="18.38" y1="36.401" x2="18.38" y2="36.329"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="18.38" y1="36.329" x2="18.38" y2="34.744"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="17.21" y1="32.962" x2="13.393" y2="33.076"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M14.203,32.962"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="14.959" y1="35.771" x2="14.816" y2="35.771"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="14.779" y1="34.042" x2="14.96" y2="34.042"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M68.099,13.701c-0.114-0.1-0.199-0.15-0.342-0.198"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M64.715,12.909c0.163-0.145,0.275-0.309,0.27-0.54"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="64.733" y1="12.909" x2="64.517" y2="12.891"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M64.985,35.608c-0.02-0.196-0.085-0.448-0.27-0.558"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M64.517,35.068c0.072-0.007,0.144-0.015,0.216-0.018"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M61.709,34.87c0.013,0.021,0.031,0.035,0.054,0.036"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="42.933" y1="36.869" x2="42.933" y2="36.869"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M14.779,34.042c-0.269,0.22-0.486,0.458-0.486,0.826c0,0.407,0.2,0.654,0.504,0.902"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M14.959,35.771c0.372,0.507,0.915,0.898,1.58,0.803c0.598-0.085,1.217-0.445,1.419-1.05c0.225-0.676,0.064-1.218-0.346-1.777
				c-0.35-0.478-1.174-0.604-1.695-0.431c-0.381,0.127-0.742,0.382-0.958,0.728"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M14.959,35.771c0.372,0.507,0.915,0.898,1.58,0.803c0.598-0.085,1.217-0.445,1.419-1.05c0.225-0.676,0.064-1.218-0.346-1.777
				c-0.35-0.478-1.174-0.604-1.695-0.431c-0.381,0.127-0.742,0.382-0.958,0.728"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M69.035,34.726c-0.054-0.219-0.189-0.494-0.468-0.45"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M68.567,13.701c0.22-0.028,0.519-0.195,0.468-0.468"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M18.38,34.114c-0.052-0.632-0.508-1.152-1.17-1.152"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="29.522" y1="35.662" x2="34.437" y2="35.662"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M34.437,12.297c-1.638,0-3.276,0.006-4.914,0.018"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M41.871,9.146c-2.757,0.09-5.515,0.179-8.272,0.269c-2.729,0.088-5.458,0.189-8.187,0.283c-0.058,0.002-0.116,0.004-0.174,0.006"
				/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M57.587,9.543c-0.104-0.013-0.201-0.018-0.306-0.018"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M44.734,12.261c-2.766,0.01-5.532,0.02-8.298,0.031l-1.441,0.005"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M49.378,12.495"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M49.252,12.729"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M49.522,12.729"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M49.27,12.531"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M49.378,12.495"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="41.871" y1="9.075" x2="41.871" y2="9.147"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M41.871,9.939c-0.028-0.021-0.058-0.039-0.09-0.054"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="33.015" y1="9.975" x2="32.997" y2="9.975"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="32.997" y1="9.975" x2="32.979" y2="9.975"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="34.995" y1="12.297" x2="34.437" y2="12.297"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M61.817,34.906c-0.032,0.008-0.044-0.004-0.036-0.036"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="58.289" y1="37.283" x2="58.415" y2="37.283"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="34.437" y1="35.662" x2="34.995" y2="35.662"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M48.424,39.011c0.006,0.006,0.012,0.012,0.018,0.018"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="41.925" y1="39.047" x2="41.925" y2="39.029"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="41.925" y1="39.029" x2="41.907" y2="39.029"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="41.907" y1="39.029" x2="41.907" y2="39.011"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M41.907,39.011c0.003-0.016-0.004-0.028-0.018-0.036"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M41.889,38.975c-0.015-0.022-0.021-0.046-0.018-0.072"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="41.871" y1="38.903" x2="41.871" y2="38.812"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="41.925" y1="38.975" x2="41.889" y2="38.975"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M2.412,21.442c0.123-0.674,0.27-1.319,0.45-1.98"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M10.819,30.028c-0.244-2.614-0.326-5.232-0.257-7.856c0.037-1.412,0.124-2.817,0.257-4.223"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M5.401,12.729c0.219-0.437,0.469-0.911,0.835-1.246c0.417-0.381,0.834-0.688,1.325-0.969"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="22.142" y1="11.613" x2="22.142" y2="11.432"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M22.124,11.145c2.466,0,4.932,0.006,7.398,0.018"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M25.364,10.281c-0.325,0.012-0.648,0.03-0.972,0.054"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M22.088,9.813c0.463,0,0.924-0.006,1.386-0.018"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M25.238,9.705c-0.449,0-0.921,0.02-1.35,0.162"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="21.116" y1="10.839" x2="21.116" y2="10.911"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="22.142" y1="11.433" x2="22.142" y2="11.289"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="21.224" y1="9.813" x2="22.088" y2="9.813"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M29.522,12.315c-2.466-0.012-4.932-0.018-7.398-0.018"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="22.142" y1="11.721" x2="22.142" y2="11.685"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="22.142" y1="11.685" x2="22.142" y2="11.612"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="22.142" y1="11.991" x2="22.142" y2="11.955"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="22.142" y1="11.955" x2="22.142" y2="11.955"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="22.142" y1="11.973" x2="22.142" y2="11.937"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="22.142" y1="11.937" x2="22.142" y2="11.901"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="22.142" y1="11.901" x2="22.142" y2="11.883"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="22.142" y1="11.883" x2="22.142" y2="11.829"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M14.905,10.911c-0.818,0.095-1.308,0.562-1.404,1.386"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M7.993,10.316c0.505-0.238,0.995-0.424,1.538-0.56c0.555-0.138,1.116-0.186,1.685-0.232"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M11.395,9.507c-0.058,0.016-0.119,0.022-0.18,0.018"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="11.215" y1="9.525" x2="11.575" y2="9.525"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M11.395,9.507c0.058,0.016,0.12,0.022,0.181,0.018"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M7.561,10.515c0.144-0.065,0.288-0.131,0.432-0.198"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M10.819,17.949c0.119-0.947,0.248-1.913,0.594-2.808c0.317-0.82,0.831-1.763,1.783-1.89"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="13.501" y1="13.233" x2="13.501" y2="13.377"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="13.195" y1="13.251" x2="13.195" y2="13.377"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="13.501" y1="12.297" x2="13.501" y2="13.233"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M3.403,17.823c0.626-1.718,1.275-3.414,1.998-5.094"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M2.863,19.461c0.167-0.525,0.341-1.046,0.522-1.566"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M3.384,17.895c0.011-0.022,0.017-0.048,0.018-0.072"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M7.561,37.445C6.608,36.9,5.895,36.237,5.4,35.249"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M21.116,37.049c-0.012,0.342-0.024,0.684-0.036,1.026"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M29.522,36.815c-1.705,0-3.408,0.006-5.113,0.018"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="24.014" y1="35.662" x2="29.522" y2="35.662"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="29.522" y1="35.933" x2="21.91" y2="35.933"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="29.522" y1="36.815" x2="24.455" y2="36.939"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M22.178,35.681c0.613,0,1.224-0.006,1.836-0.018"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="24.014" y1="35.933" x2="23.996" y2="35.933"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M20.396,36.329c0.024-0.434,0.036-0.862,0.036-1.296"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M20.432,35.033c-0.006-0.095,0.003-0.18,0.036-0.27"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M20.468,34.762c-0.004-0.026,0.003-0.05,0.018-0.072"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="20.486" y1="34.69" x2="20.486" y2="34.672"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="20.341" y1="36.401" x2="20.341" y2="36.365"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="20.341" y1="36.365" x2="20.341" y2="36.329"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M20.341,36.401c0.032,0.008,0.044-0.004,0.036-0.036"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M20.378,36.365c0.014-0.008,0.02-0.02,0.018-0.036"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="20.396" y1="36.329" x2="20.341" y2="36.329"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M13.195,34.726c-0.961-0.192-1.462-1.045-1.782-1.908c-0.334-0.899-0.472-1.843-0.594-2.79"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="13.501" y1="34.726" x2="13.501" y2="35.662"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="13.195" y1="34.726" x2="13.195" y2="34.582"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M13.501,35.662c0.088,0.803,0.583,1.337,1.404,1.386"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M21.08,38.507c-2.683,0-5.365-0.054-8.047-0.054h-1.457"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M21.08,9.553c-2.683,0-5.365-0.054-8.047-0.054h-1.457"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M20.954,39.065c-0.047,0.015-0.095,0.022-0.144,0.018"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M21.08,38.507c-0.058,0.139-0.118,0.277-0.18,0.414"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="21.08" y1="38.075" x2="21.08" y2="38.507"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="17.894" y1="39.173" x2="18.038" y2="39.173"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M11.215,38.435c-0.551-0.03-1.091-0.065-1.628-0.2c-0.556-0.139-1.071-0.343-1.595-0.574"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M11.215,38.435c0.058,0.017,0.12,0.023,0.18,0.018"/>
			
				<line fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="11.575" y1="38.453" x2="11.395" y2="38.453"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M11.575,38.453c-0.122-0.012-0.238-0.018-0.36-0.018"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M7.993,37.66c-0.147-0.068-0.29-0.138-0.432-0.216"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M3.384,30.082c-0.178-0.521-0.351-1.043-0.522-1.566"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M2.412,26.518c-0.213-1.71-0.213-3.367,0-5.076"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M2.863,28.516c-0.182-0.668-0.329-1.317-0.45-1.998"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M5.401,35.249c-0.722-1.68-1.366-3.379-1.998-5.094"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M3.403,30.154c0.003-0.026-0.003-0.05-0.019-0.072"/>
			
				<path fill="none" stroke="#1D1D1B" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
				M3.817,30.028c-0.016-0.029-0.028-0.059-0.036-0.09"/>
			<polygon fill="#3D3D3C" stroke="#010101" stroke-width="0.25" stroke-miterlimit="10" points="69.47,29.313 69.477,30.01 
				69.47,34.499 67.869,34.499 67.42,34.463 64.47,35.063 62.694,35.088 61.194,34.638 60.319,33.913 59.744,32.688 59.719,31.988 
				59.744,15.286 60.144,14.287 60.669,13.762 61.37,13.237 62.444,13.011 64.52,12.836 67.545,13.561 67.77,13.586 68.099,13.602 
				69.692,13.167 69.52,15.112 69.48,27.437 		"/>
			<path fill="#3D3D3C" stroke="#0E0E0E" stroke-width="0.25" stroke-miterlimit="10" d="M71.42,30.938l27.227-0.025l0.95,0.4
				l0.375,0.45c0,0,0.15,1.7,0.075,1.7c-0.075,0-0.5,0.575-0.5,0.575l-0.975,0.325l-27.177,0.05l0.05,4.551l-1.6,0.025l-0.05-0.375
				l-0.325,0.05l0.01-11.227l0.015-18.125h0.25l0.075-0.25l1.625,0.025l0.025,4.526h27.027l0.875,0.3l0.675,0.55l-0.05,1.75
				l-0.35,0.5l-0.775,0.325l-27.427-0.05L71.42,30.938z"/>
			
				<rect x="20.393" y="11.775" fill="#847A73" stroke="#030303" stroke-width="0.25" stroke-miterlimit="10" width="28.847" height="24.194"/>
			<polyline fill="#847A73" stroke="#030303" stroke-width="0.25" stroke-miterlimit="10" points="58.47,10.308 58.432,9.896 
				58.432,9.596 58.093,9.333 57.603,9.146 56.547,9.333 55.454,9.558 54.248,9.821 52.626,10.046 51.76,10.346 50.667,10.346 
				48.556,10.571 47.688,10.646 46.294,10.721 44.748,10.758 22.13,10.684 22.092,12.033 32.006,11.958 34.758,11.958 41.506,11.771 
				47.085,11.808 49.8,11.733 53.456,11.171 55.605,10.796 57.075,10.608 58.319,10.346 		"/>
			<polyline fill="#847A73" stroke="#030303" stroke-width="0.25" stroke-miterlimit="10" points="58.376,37.75 58.488,37.938 
				58.226,38.238 57.738,38.388 57.401,38.5 55.75,38.163 54.963,37.9 53.688,37.675 52.225,37.563 50.8,37.338 49.562,37.075 
				48.212,37.038 46.975,36.963 45.212,36.888 43.6,36.888 42.1,36.85 39.624,36.925 37.524,36.775 34.899,36.738 24.473,36.963 
				23.686,38.2 21.285,38.275 21.098,36.963 22.11,35.575 48.7,35.688 49.862,35.95 52.413,36.138 54.325,36.4 55.713,36.625 
				56.163,36.85 57.888,37.15 57.776,37.563 58.263,37.6 		"/>
		</g>
		<g>
			<rect x="77.048" y="9.354" fill="#F0BB83" width="3.42" height="29.32"/>
			<path fill="#92715A" d="M76.918,9.146h3.68v29.736h-3.68V9.146z M80.338,9.562h-3.16v28.904h3.16V9.562z"/>
			<rect x="80.396" y="9.354" fill="#F0BB83" width="3.42" height="29.32"/>
			<path fill="#92715A" d="M83.945,38.882h-3.68V9.146h3.68L83.945,38.882L83.945,38.882z M80.526,38.466h3.16V9.562h-3.16V38.466z"
				/>
			<rect x="83.802" y="9.354" fill="#F0BB83" width="3.42" height="29.32"/>
			<path fill="#92715A" d="M87.352,38.882h-3.68V9.146h3.68V38.882z M83.931,38.466h3.16V9.562h-3.16V38.466z"/>
			<rect x="87.236" y="9.354" fill="#F0BB83" width="3.42" height="29.32"/>
			<path fill="#92715A" d="M90.785,38.882h-3.68V9.146h3.68V38.882z M87.365,38.466h3.16V9.562h-3.16V38.466z"/>
			<rect x="90.642" y="9.354" fill="#F0BB83" width="3.42" height="29.32"/>
			<path fill="#92715A" d="M90.512,9.146h3.68v29.736h-3.68V9.146z M93.932,9.562h-3.16v28.904h3.16V9.562z"/>
			<rect x="76.399" y="7.613" fill="#F0BB83" width="18.312" height="3.42"/>
			<path fill="#92715A" d="M76.269,11.163v-3.68h18.572v3.68H76.269z M76.529,7.743v3.16h18.052v-3.16H76.529z"/>
			<circle fill="#916F56" cx="93.683" cy="10.427" r="0.357"/>
			<path fill="#FCD2AE" d="M93.488,10.427c0-0.169,0.118-0.31,0.276-0.347c-0.026-0.006-0.053-0.01-0.081-0.01
				c-0.197,0-0.357,0.16-0.357,0.357c0,0.197,0.16,0.357,0.357,0.357c0.028,0,0.055-0.004,0.081-0.01
				C93.606,10.737,93.488,10.596,93.488,10.427z"/>
			<circle fill="#916F56" cx="93.683" cy="8.392" r="0.357"/>
			<path fill="#FCD2AE" d="M93.488,8.392c0-0.169,0.118-0.31,0.276-0.347c-0.026-0.006-0.053-0.01-0.081-0.01
				c-0.197,0-0.357,0.16-0.357,0.357c0,0.197,0.16,0.357,0.357,0.357c0.028,0,0.055-0.004,0.081-0.01
				C93.606,8.702,93.488,8.561,93.488,8.392z"/>
			<circle fill="#916F56" cx="77.092" cy="10.427" r="0.357"/>
			<path fill="#FCD2AE" d="M76.897,10.427c0-0.169,0.118-0.31,0.276-0.347c-0.026-0.006-0.053-0.01-0.081-0.01
				c-0.197,0-0.357,0.16-0.357,0.357c0,0.197,0.16,0.357,0.357,0.357c0.028,0,0.055-0.004,0.081-0.01
				C77.015,10.737,76.897,10.596,76.897,10.427z"/>
			<circle fill="#916F56" cx="77.092" cy="8.392" r="0.357"/>
			<path fill="#FCD2AE" d="M76.897,8.392c0-0.169,0.118-0.31,0.276-0.347c-0.026-0.006-0.053-0.01-0.081-0.01
				c-0.197,0-0.357,0.16-0.357,0.357c0,0.197,0.16,0.357,0.357,0.357c0.028,0,0.055-0.004,0.081-0.01
				C77.015,8.702,76.897,8.561,76.897,8.392z"/>
			<circle fill="#916F56" cx="88.915" cy="9.323" r="0.357"/>
			<path fill="#FCD2AE" d="M88.721,9.323c0-0.169,0.118-0.31,0.276-0.347c-0.026-0.006-0.053-0.01-0.081-0.01
				c-0.197,0-0.357,0.16-0.357,0.357c0,0.197,0.16,0.357,0.357,0.357c0.028,0,0.055-0.004,0.081-0.01
				C88.838,9.633,88.721,9.492,88.721,9.323z"/>
			<circle fill="#916F56" cx="85.555" cy="9.323" r="0.357"/>
			<path fill="#FCD2AE" d="M85.36,9.323c0-0.169,0.118-0.31,0.276-0.347c-0.026-0.006-0.053-0.01-0.081-0.01
				c-0.197,0-0.357,0.16-0.357,0.357c0,0.197,0.16,0.357,0.357,0.357c0.028,0,0.055-0.004,0.081-0.01
				C85.478,9.633,85.36,9.492,85.36,9.323z"/>
			<circle fill="#916F56" cx="81.853" cy="9.323" r="0.357"/>
			<path fill="#FCD2AE" d="M81.659,9.323c0-0.169,0.118-0.31,0.276-0.347c-0.026-0.006-0.053-0.01-0.081-0.01
				c-0.197,0-0.357,0.16-0.357,0.357c0,0.197,0.16,0.357,0.357,0.357c0.028,0,0.055-0.004,0.081-0.01
				C81.777,9.633,81.659,9.492,81.659,9.323z"/>
			<rect x="76.399" y="36.891" fill="#F0BB83" width="18.312" height="3.42"/>
			<path fill="#92715A" d="M76.269,40.441v-3.68h18.572v3.68H76.269z M76.529,37.021v3.16h18.052v-3.16H76.529z"/>
			<circle fill="#916F56" cx="93.683" cy="39.705" r="0.357"/>
			<path fill="#FCD2AE" d="M93.488,39.705c0-0.169,0.118-0.31,0.276-0.347c-0.026-0.006-0.053-0.01-0.081-0.01
				c-0.197,0-0.357,0.16-0.357,0.357c0,0.197,0.16,0.357,0.357,0.357c0.028,0,0.055-0.004,0.081-0.01
				C93.606,40.015,93.488,39.875,93.488,39.705z"/>
			<circle fill="#916F56" cx="93.683" cy="37.671" r="0.357"/>
			<path fill="#FCD2AE" d="M93.488,37.671c0-0.169,0.118-0.31,0.276-0.347c-0.026-0.006-0.053-0.01-0.081-0.01
				c-0.197,0-0.357,0.16-0.357,0.357c0,0.197,0.16,0.357,0.357,0.357c0.028,0,0.055-0.004,0.081-0.01
				C93.606,37.981,93.488,37.84,93.488,37.671z"/>
			<circle fill="#916F56" cx="77.092" cy="39.705" r="0.357"/>
			<path fill="#FCD2AE" d="M76.897,39.705c0-0.169,0.118-0.31,0.276-0.347c-0.026-0.006-0.053-0.01-0.081-0.01
				c-0.197,0-0.357,0.16-0.357,0.357c0,0.197,0.16,0.357,0.357,0.357c0.028,0,0.055-0.004,0.081-0.01
				C77.015,40.015,76.897,39.875,76.897,39.705z"/>
			<circle fill="#916F56" cx="77.092" cy="37.671" r="0.357"/>
			<path fill="#FCD2AE" d="M76.897,37.671c0-0.169,0.118-0.31,0.276-0.347c-0.026-0.006-0.053-0.01-0.081-0.01
				c-0.197,0-0.357,0.16-0.357,0.357c0,0.197,0.16,0.357,0.357,0.357c0.028,0,0.055-0.004,0.081-0.01
				C77.015,37.981,76.897,37.84,76.897,37.671z"/>
			<circle fill="#916F56" cx="88.915" cy="38.601" r="0.357"/>
			<path fill="#FCD2AE" d="M88.721,38.601c0-0.169,0.118-0.31,0.276-0.347c-0.026-0.006-0.053-0.01-0.081-0.01
				c-0.197,0-0.357,0.16-0.357,0.357c0,0.197,0.16,0.357,0.357,0.357c0.028,0,0.055-0.004,0.081-0.01
				C88.838,38.912,88.721,38.771,88.721,38.601z"/>
			<circle fill="#916F56" cx="85.555" cy="38.601" r="0.357"/>
			<path fill="#FCD2AE" d="M85.36,38.601c0-0.169,0.118-0.31,0.276-0.347c-0.026-0.006-0.053-0.01-0.081-0.01
				c-0.197,0-0.357,0.16-0.357,0.357c0,0.197,0.16,0.357,0.357,0.357c0.028,0,0.055-0.004,0.081-0.01
				C85.478,38.912,85.36,38.771,85.36,38.601z"/>
			<circle fill="#916F56" cx="81.853" cy="38.601" r="0.357"/>
			<path fill="#FCD2AE" d="M81.659,38.601c0-0.169,0.118-0.31,0.276-0.347c-0.026-0.006-0.053-0.01-0.081-0.01
				c-0.197,0-0.357,0.16-0.357,0.357c0,0.197,0.16,0.357,0.357,0.357c0.028,0,0.055-0.004,0.081-0.01
				C81.777,38.912,81.659,38.771,81.659,38.601z"/>
		</g>
	</g>
	<g>
		<g>
			<defs>
				<rect id="SVGID_1_" x="77.207" y="11.75" width="16.724" height="12.812"/>
			</defs>
			<clipPath id="SVGID_2_">
				<use xlink:href="#SVGID_1_"  overflow="visible"/>
			</clipPath>
			<path clip-path="url(#SVGID_2_)" fill="#B0794D" d="M77.2,14.498c-0.005-0.728,0.018-1.46-0.024-2.182
				c-0.04-0.693,0.096-0.899,0.556-0.897c4.363,0.026,8.726,0.013,13.089,0.012c0.919,0,1.839,0.029,2.758-0.015
				c0.383-0.018,0.487,0.19,0.465,0.748c-0.03,0.774-0.018,1.552-0.024,2.328c-0.647,0.002-1.294,0.004-1.941,0.006
				c-0.255-0.002-0.509-0.003-0.763-0.005c-0.18,0.012-0.359,0.025-0.539,0.037c-1.137-0.09-2.273-0.057-3.41-0.016
				c-0.207,0.004-0.413,0.008-0.62,0.012c-1.136-0.077-2.273-0.055-3.409-0.011c-0.206,0.005-0.413,0.01-0.619,0.014
				c-1.164-0.011-2.328-0.022-3.493-0.033C78.55,14.498,77.875,14.498,77.2,14.498z"/>
			<path clip-path="url(#SVGID_2_)" fill="#B0794D" d="M77.208,34.218c-0.008-1.477-0.021-2.955-0.022-4.432
				c-0.003-4.653-0.002-9.306,0-13.959c0-0.314,0.018-0.628,0.027-0.942c0.643,0.004,1.285,0.007,1.928,0.011
				c0.011,1.72,0.023,3.441,0.034,5.161c-0.007,0.278-0.014,0.555-0.021,0.833c0.01,1.814-0.062,3.629,0.021,5.443
				c-0.007,0.278-0.015,0.556-0.022,0.834c0.007,1.814,0.014,3.628,0.021,5.441c-0.007,0.278-0.014,0.556-0.021,0.835
				c-0.004,0.258-0.008,0.516-0.012,0.774C78.495,34.216,77.852,34.217,77.208,34.218z"/>
			<path clip-path="url(#SVGID_2_)" fill="#B0794D" d="M94.03,31.266c-0.644,0.27-1.299,0.092-1.949,0.131
				c-0.012-1.706-0.025-3.412-0.037-5.118c0.007-0.281,0.014-0.562,0.021-0.843c-0.012-1.813,0.053-3.628-0.035-5.439
				c0.064-0.319,0.036-0.645,0.028-0.97c0.004-1.379,0.009-2.758,0.013-4.137c0.622-0.003,1.244-0.006,1.866-0.009
				c0.026,0.021,0.054,0.032,0.083,0.034c0.007,3.682,0.015,7.364,0.019,11.047C94.041,27.729,94.034,29.498,94.03,31.266z"/>
			<path clip-path="url(#SVGID_2_)" fill="#DE9961" d="M81.763,34.63c0,1.013,0,2.026,0.001,3.039
				c-1.413,0.006-2.825,0.005-4.238,0.024c-0.262,0.004-0.358-0.139-0.349-0.547c0.019-0.845,0.016-1.692,0.021-2.538
				c0.645-0.001,1.289-0.001,1.934-0.002C80.009,34.614,80.886,34.622,81.763,34.63z"/>
			<path clip-path="url(#SVGID_2_)" fill="#8D401F" d="M79.14,14.896c-0.643-0.004-1.285-0.007-1.928-0.011
				c-0.004-0.128-0.008-0.257-0.013-0.386c0.675,0,1.35-0.001,2.025-0.001C79.196,14.63,79.168,14.763,79.14,14.896z"/>
			<path clip-path="url(#SVGID_2_)" fill="#8D401F" d="M79.132,34.606c-0.645,0-1.289,0.001-1.934,0.002
				c0.003-0.13,0.006-0.26,0.009-0.39c0.644-0.001,1.288-0.002,1.932-0.003C79.137,34.346,79.134,34.476,79.132,34.606z"/>
			<path clip-path="url(#SVGID_2_)" fill="#8D401F" d="M93.937,14.881c-0.622,0.003-1.244,0.006-1.866,0.009
				c0.002-0.13,0.005-0.26,0.007-0.391c0.647-0.002,1.294-0.004,1.941-0.006c-0.001,0.095-0.002,0.19-0.003,0.285
				C93.99,14.812,93.963,14.846,93.937,14.881z"/>
			<path clip-path="url(#SVGID_2_)" fill="#B86B3B" d="M93.937,14.881c0.027-0.034,0.053-0.069,0.08-0.103
				c0.017,0.044,0.018,0.09,0.004,0.136C93.991,14.912,93.963,14.901,93.937,14.881z"/>
			<path clip-path="url(#SVGID_2_)" fill="#B86B3B" d="M92.078,14.499c-0.002,0.13-0.005,0.261-0.007,0.391
				c-0.004,1.379-0.009,2.758-0.013,4.137c-0.129,0.172-0.265,0.333-0.385,0.519c-2.402,3.737-4.801,7.477-7.203,11.213
				c-0.12,0.186-0.258,0.343-0.387,0.513c-0.593,0.003-1.186,0.002-1.778,0.011c-0.363,0.006-0.573,0.196-0.543,0.869
				c0.037,0.822,0.004,1.651,0.002,2.477c-0.877-0.008-1.754-0.016-2.631-0.024c0.002-0.13,0.005-0.261,0.007-0.391
				c0.004-0.258,0.008-0.516,0.012-0.773c0.202,0.003,0.291-0.258,0.408-0.44c3.826-5.954,7.651-11.911,11.473-17.871
				c0.117-0.182,0.283-0.323,0.282-0.637C91.569,14.496,91.824,14.497,92.078,14.499z"/>
			<path clip-path="url(#SVGID_2_)" fill="#B86B3B" d="M92.03,19.997c0.088,1.812,0.024,3.626,0.035,5.439
				c-0.045,0.013-0.106,0.001-0.133,0.042c-1.202,1.852-2.405,3.702-3.596,5.572c-0.233,0.366,0.063,0.327,0.172,0.416
				c-0.045,0.388-0.276,0.546-0.418,0.814c-0.104,0.196-0.265,0.338-0.231,0.65c-0.152,0.12-0.415,0.027-0.385,0.492
				c-0.053-0.002-0.106-0.004-0.159-0.006c-0.008-0.123-0.016-0.246-0.024-0.369c0.046-0.699,0.467-0.988,0.748-1.654
				c-1.167,0-2.266,0-3.366,0c0.047-0.095,0.089-0.199,0.143-0.284c2.293-3.575,4.588-7.149,6.883-10.721
				C91.797,20.236,91.886,20.058,92.03,19.997z"/>
			<path clip-path="url(#SVGID_2_)" fill="#B86B3B" d="M92.044,26.279c0.012,1.706,0.025,3.412,0.037,5.118
				c-1.124,0.001-2.248,0.002-3.373,0.002c0.99-1.564,1.979-3.129,2.971-4.691C91.786,26.539,91.879,26.332,92.044,26.279z"/>
			<path clip-path="url(#SVGID_2_)" fill="#8D401F" d="M92.03,19.997c-0.144,0.062-0.232,0.239-0.331,0.392
				c-2.295,3.573-4.59,7.146-6.883,10.721c-0.054,0.084-0.095,0.189-0.143,0.284c-0.065,0.023-0.131,0.047-0.196,0.07
				c-0.132-0.012-0.264-0.024-0.396-0.036c0-0.052,0.001-0.104,0.001-0.156c0.129-0.17,0.268-0.327,0.387-0.513
				c2.402-3.736,4.801-7.476,7.203-11.213c0.12-0.186,0.256-0.347,0.385-0.519C92.066,19.351,92.093,19.677,92.03,19.997z"/>
			<path clip-path="url(#SVGID_2_)" fill="#8D401F" d="M92.044,26.279c-0.165,0.053-0.257,0.26-0.365,0.429
				c-0.991,1.562-1.981,3.127-2.971,4.691c-0.067,0.022-0.133,0.044-0.2,0.066c-0.11-0.089-0.406-0.05-0.172-0.416
				c1.191-1.87,2.394-3.719,3.596-5.572c0.027-0.042,0.088-0.029,0.133-0.042C92.058,25.717,92.051,25.998,92.044,26.279z"/>
			<path clip-path="url(#SVGID_2_)" fill="#B86B3B" d="M79.173,32.608c-0.007-1.814-0.014-3.628-0.021-5.441
				c0.219-0.004,0.314-0.292,0.441-0.49c2.49-3.87,4.978-7.746,7.464-11.622c0.109-0.17,0.206-0.359,0.309-0.539
				c1.137-0.041,2.274-0.075,3.41,0.016c-0.041,0.283-0.193,0.435-0.312,0.621c-3.618,5.64-7.238,11.279-10.859,16.915
				C79.474,32.27,79.368,32.527,79.173,32.608z"/>
			<path clip-path="url(#SVGID_2_)" fill="#B86B3B" d="M79.174,26.332c-0.083-1.814-0.011-3.629-0.021-5.443
				c0.07-0.043,0.158-0.058,0.207-0.134c1.328-2.076,2.652-4.158,3.976-6.239c1.136-0.045,2.273-0.067,3.409,0.011
				c-0.035,0.258-0.168,0.401-0.277,0.57c-2.31,3.6-4.62,7.199-6.931,10.797C79.429,26.063,79.336,26.267,79.174,26.332z"/>
			<path clip-path="url(#SVGID_2_)" fill="#B86B3B" d="M79.14,14.896c0.028-0.133,0.056-0.265,0.085-0.398
				c1.164,0.011,2.328,0.022,3.493,0.032c-0.047,0.277-0.194,0.432-0.313,0.618c-0.833,1.303-1.665,2.609-2.507,3.899
				c-0.23,0.353-0.416,0.789-0.723,1.009C79.163,18.336,79.151,16.616,79.14,14.896z"/>
			<path clip-path="url(#SVGID_2_)" fill="#8D401F" d="M79.173,32.608c0.195-0.081,0.301-0.338,0.431-0.541
				c3.621-5.636,7.241-11.275,10.859-16.915c0.119-0.186,0.271-0.338,0.312-0.621c0.18-0.012,0.359-0.024,0.539-0.037
				c0.001,0.314-0.165,0.455-0.282,0.637c-3.823,5.96-7.647,11.917-11.473,17.871c-0.117,0.182-0.206,0.443-0.408,0.44
				C79.158,33.164,79.166,32.886,79.173,32.608z"/>
			<path clip-path="url(#SVGID_2_)" fill="#8D401F" d="M79.174,26.332c0.162-0.065,0.255-0.27,0.364-0.439
				c2.311-3.598,4.622-7.197,6.931-10.797c0.108-0.169,0.242-0.312,0.277-0.57c0.207-0.004,0.413-0.008,0.62-0.011
				c-0.103,0.18-0.2,0.369-0.309,0.54c-2.487,3.876-4.974,7.752-7.464,11.622c-0.127,0.197-0.221,0.486-0.441,0.49
				C79.159,26.888,79.167,26.61,79.174,26.332z"/>
			<path clip-path="url(#SVGID_2_)" fill="#8D401F" d="M79.174,20.056c0.307-0.22,0.493-0.656,0.723-1.009
				c0.842-1.29,1.674-2.596,2.507-3.899c0.119-0.186,0.266-0.342,0.313-0.618c0.207-0.005,0.413-0.009,0.62-0.014
				c-1.325,2.081-2.649,4.163-3.977,6.239c-0.049,0.076-0.137,0.091-0.207,0.134C79.16,20.612,79.167,20.334,79.174,20.056z"/>
		</g>
		<g>
			<defs>
				<rect id="SVGID_3_" x="77.207" y="24.562" width="16.724" height="11.667"/>
			</defs>
			<clipPath id="SVGID_4_">
				<use xlink:href="#SVGID_3_"  overflow="visible"/>
			</clipPath>
			<path clip-path="url(#SVGID_4_)" fill="#DE9961" d="M89.376,14.494c0-1.013,0-2.026-0.001-3.039
				c1.413-0.006,2.825-0.005,4.238-0.024c0.262-0.004,0.358,0.139,0.349,0.547c-0.019,0.845-0.016,1.692-0.021,2.538
				c-0.645,0.001-1.289,0.001-1.934,0.002C91.13,14.51,90.253,14.502,89.376,14.494z"/>
			<path clip-path="url(#SVGID_4_)" fill="#8D401F" d="M91.999,34.229c0.643,0.004,1.285,0.007,1.928,0.011
				c0.004,0.128,0.008,0.257,0.013,0.386c-0.675,0-1.35,0.001-2.025,0.001C91.943,34.494,91.971,34.361,91.999,34.229z"/>
			<path clip-path="url(#SVGID_4_)" fill="#8D401F" d="M92.007,14.518c0.645,0,1.289-0.001,1.934-0.002
				c-0.003,0.13-0.006,0.26-0.009,0.39c-0.644,0.001-1.288,0.002-1.932,0.003C92.002,14.779,92.005,14.648,92.007,14.518z"/>
			<path clip-path="url(#SVGID_4_)" fill="#8D401F" d="M77.202,34.244c0.622-0.003,1.244-0.006,1.866-0.009
				c-0.002,0.13-0.005,0.26-0.007,0.391c-0.647,0.002-1.294,0.004-1.941,0.006c0.001-0.095,0.002-0.19,0.003-0.285
				C77.149,34.312,77.176,34.278,77.202,34.244z"/>
			<path clip-path="url(#SVGID_4_)" fill="#B86B3B" d="M77.202,34.244c-0.027,0.034-0.053,0.069-0.08,0.103
				c-0.017-0.044-0.018-0.09-0.004-0.136C77.148,34.212,77.176,34.223,77.202,34.244z"/>
			<path clip-path="url(#SVGID_4_)" fill="#B86B3B" d="M78.288,35.811c0.003-0.156-0.261,0.028-0.258-0.128
				c0.005-1.652,0.062-2.327,0.067-3.979c0.16-0.207,0.329-0.399,0.478-0.622c2.983-4.476,5.964-8.957,8.947-13.433
				c0.149-0.223,0.32-0.411,0.481-0.615c0.736-0.004,1.473-0.002,2.209-0.013c0.451-0.007,0.711-0.235,0.674-1.041
				C90.84,14.994,90.881,14,90.884,13.01c1.089,0.009,2.179,0.019,3.268,0.029c-0.003,0.156-0.006,0.312-0.009,0.468
				c-0.005,0.309-0.01,0.618-0.015,0.926c-0.251-0.003-0.362,0.309-0.507,0.527c-4.753,7.133-9.453,13.786-14.201,20.926
				c-0.145,0.218-0.362-0.361-0.36,0.015C78.745,35.9,78.604,35.813,78.288,35.811z"/>
			<path clip-path="url(#SVGID_4_)" fill="#B86B3B" d="M79.116,30.247c-0.101-1.963-0.027-3.929-0.04-5.893
				c0.052-0.014,0.122-0.001,0.153-0.046c1.374-2.007,2.75-4.011,4.111-6.036c0.267-0.397-0.072-0.354-0.197-0.451
				c0.051-0.421,0.315-0.591,0.477-0.881c0.119-0.213,0.303-0.366,0.264-0.704c0.173-0.129,0.475-0.029,0.44-0.533
				c0.061,0.002,0.121,0.004,0.182,0.006c0.009,0.133,0.019,0.266,0.028,0.4c-0.053,0.758-0.534,1.071-0.855,1.791
				c1.334,0,2.591,0,3.848,0c-0.054,0.103-0.101,0.216-0.163,0.307c-2.622,3.873-5.246,7.745-7.87,11.615
				C79.382,29.988,79.281,30.18,79.116,30.247z"/>
			<path clip-path="url(#SVGID_4_)" fill="#B86B3B" d="M79.095,22.845c-0.012-1.706-0.025-3.412-0.037-5.118
				c1.124-0.001,2.248-0.002,3.373-0.002c-0.99,1.564-1.979,3.129-2.971,4.691C79.353,22.585,79.261,22.792,79.095,22.845z"/>
			<path clip-path="url(#SVGID_4_)" fill="#8D401F" d="M77.376,29.128c0.144-0.062,0.232-0.239,0.331-0.392
				c2.295-3.573,4.59-7.146,6.883-10.721c0.054-0.084,0.095-0.189,0.143-0.284c0.065-0.023,0.131-0.047,0.196-0.07
				c0.132,0.012,0.264,0.024,0.396,0.036c0,0.052-0.001,0.104-0.001,0.156c-0.129,0.17-0.268,0.327-0.387,0.513
				c-2.402,3.736-4.801,7.476-7.203,11.213c-0.12,0.186-0.256,0.347-0.385,0.519C77.34,29.773,77.312,29.447,77.376,29.128z"/>
			<path clip-path="url(#SVGID_4_)" fill="#8D401F" d="M79.095,22.845c0.165-0.053,0.257-0.26,0.365-0.429
				c0.991-1.562,1.981-3.127,2.971-4.691c0.067-0.022,0.133-0.044,0.2-0.066c0.11,0.089,0.406,0.05,0.172,0.416
				c-1.191,1.87-2.394,3.719-3.596,5.572c-0.027,0.042-0.088,0.029-0.133,0.042C79.081,23.407,79.088,23.126,79.095,22.845z"/>
			<path clip-path="url(#SVGID_4_)" fill="#B86B3B" d="M93.288,14.447c0.009,2.228,0.017,4.456,0.026,6.683
				c-0.27,0.004-0.385,0.359-0.541,0.602c-3.059,4.754-6.114,9.514-9.168,14.275c-0.134,0.209-0.53-0.198-0.656,0.023
				c-1.396,0.051-2.481-0.364-3.877-0.474c0.051-0.348,0.201,0.56,0.348,0.332c4.444-6.928,8.89-13.853,13.338-20.776
				C92.918,14.862,93.049,14.547,93.288,14.447z"/>
			<path clip-path="url(#SVGID_4_)" fill="#B86B3B" d="M92.798,21.48c0.101,2.215,0.013,4.432,0.025,6.647
				c-0.085,0.053-0.193,0.071-0.252,0.164c-1.622,2.535-3.239,5.078-4.856,7.62c-1.388,0.054-2.776,0.082-4.163-0.013
				c0.042-0.315,0.206-0.489,0.338-0.696c2.82-4.397,5.642-8.792,8.465-13.186C92.487,21.81,92.601,21.56,92.798,21.48z"/>
			<path clip-path="url(#SVGID_4_)" fill="#B86B3B" d="M91.999,34.229c-0.028,0.133-0.056,0.265-0.085,0.398
				c-1.164-0.011-2.328-0.022-3.493-0.032c0.047-0.277,0.194-0.432,0.313-0.618c0.833-1.303,1.665-2.609,2.507-3.899
				c0.23-0.353,0.416-0.789,0.723-1.009C91.976,30.788,91.988,32.508,91.999,34.229z"/>
			<path clip-path="url(#SVGID_4_)" fill="#8D401F" d="M90.234,16.517c-0.195,0.081-0.301,0.338-0.431,0.541
				c-3.621,5.636-7.241,11.275-10.859,16.915c-0.119,0.186-0.271,0.338-0.312,0.621c-0.18,0.012-0.359,0.024-0.539,0.037
				c-0.001-0.314,0.165-0.455,0.282-0.637c3.823-5.96,7.647-11.917,11.473-17.871c0.117-0.182,0.206-0.443,0.408-0.44
				C90.248,15.96,90.241,16.239,90.234,16.517z"/>
			<path clip-path="url(#SVGID_4_)" fill="#8D401F" d="M90.225,22.798c-0.162,0.065-0.255,0.27-0.364,0.439
				c-2.311,3.598-4.622,7.197-6.931,10.797c-0.109,0.169-0.242,0.312-0.277,0.57c-0.207,0.004-0.413,0.008-0.62,0.011
				c0.103-0.18,0.2-0.369,0.309-0.54c2.487-3.876,4.974-7.752,7.464-11.622c0.127-0.197,0.221-0.486,0.441-0.49
				C90.24,22.242,90.233,22.52,90.225,22.798z"/>
			<path clip-path="url(#SVGID_4_)" fill="#8D401F" d="M92.79,24.221c-0.162,0.065-0.255,0.27-0.364,0.439
				c-2.311,3.598-4.622,7.197-6.931,10.797c-0.109,0.169-0.242,0.312-0.277,0.57c-0.207,0.004-0.413,0.008-0.62,0.011
				c0.103-0.18,0.2-0.369,0.309-0.54c2.487-3.876,4.974-7.752,7.464-11.622c0.127-0.197,0.221-0.486,0.441-0.49
				C92.805,23.665,92.798,23.943,92.79,24.221z"/>
			<path clip-path="url(#SVGID_4_)" fill="#8D401F" d="M92.697,30.208c-0.307,0.22-0.493,0.656-0.723,1.009
				c-0.842,1.29-1.674,2.596-2.507,3.899c-0.119,0.186-0.266,0.342-0.313,0.618c-0.207,0.005-0.413,0.009-0.62,0.014
				c1.325-2.081,2.649-4.163,3.977-6.239c0.049-0.076,0.137-0.091,0.207-0.134C92.711,29.653,92.704,29.931,92.697,30.208z"/>
			<path clip-path="url(#SVGID_4_)" fill="#B0794D" d="M77.109,17.858c0.659-0.27,1.33-0.092,1.995-0.131
				c0.013,1.706,0.025,3.412,0.038,5.118c-0.007,0.281-0.014,0.562-0.021,0.843c0.012,1.813-0.054,3.628,0.036,5.439
				c-0.065,0.319-0.037,0.645-0.029,0.97c-0.004,1.379-0.009,2.758-0.013,4.137c-0.637,0.003-1.273,0.006-1.91,0.009
				c-0.027-0.021-0.055-0.032-0.085-0.034c-0.007-3.682-0.016-7.364-0.02-11.047C77.098,21.395,77.106,19.626,77.109,17.858z"/>
			<path clip-path="url(#SVGID_4_)" fill="#B0794D" d="M93.939,33.166c0.005,0.728-0.018,1.46,0.024,2.182
				c0.04,0.693-0.096,0.899-0.556,0.897c-4.363-0.026-8.726-0.013-13.089-0.012c-0.919,0-1.839-0.029-2.758,0.015
				c-0.383,0.018-0.487-0.19-0.465-0.748c0.03-0.774,0.018-1.552,0.024-2.328c0.647-0.002,1.294-0.004,1.941-0.006
				c0.255,0.002,0.509,0.003,0.763,0.005c0.18-0.012,0.359-0.025,0.539-0.037c1.137,0.09,2.273,0.057,3.41,0.016
				c0.207-0.004,0.413-0.008,0.62-0.012c1.136,0.077,2.273,0.055,3.409,0.011c0.206-0.005,0.413-0.01,0.619-0.014
				c1.164,0.011,2.328,0.022,3.493,0.033C92.589,33.167,93.264,33.167,93.939,33.166z"/>
			<path clip-path="url(#SVGID_4_)" fill="#B0794D" d="M93.931,14.906c0.008,1.477,0.021,2.955,0.022,4.432
				c0.003,4.653,0.002,9.306,0,13.959c0,0.314-0.018,0.628-0.027,0.942c-0.643-0.004-1.285-0.007-1.928-0.011
				c-0.011-1.72-0.023-3.441-0.034-5.161c0.007-0.278,0.014-0.555,0.021-0.833c-0.01-1.814,0.062-3.629-0.021-5.443
				c0.007-0.278,0.015-0.556,0.022-0.834c-0.007-1.814-0.014-3.628-0.021-5.441c0.007-0.278,0.014-0.556,0.021-0.835
				c0.004-0.258,0.008-0.516,0.012-0.774C92.644,14.908,93.288,14.907,93.931,14.906z"/>
		</g>
		<rect x="77.2" y="14.498" fill="#8D401F" width="1.94" height="0.397"/>
		<rect x="77.2" y="32.742" fill="#8D401F" width="1.94" height="0.397"/>
		<rect x="91.992" y="32.742" fill="#8D401F" width="1.94" height="0.397"/>
	</g>
</g>
</g>
</svg>