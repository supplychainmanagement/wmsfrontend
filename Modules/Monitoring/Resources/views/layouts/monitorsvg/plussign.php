<svg id ="plus-sign"version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"width="32px" height="31.999px" viewBox="0 0 32 31.999" style="enable-background:new 0 0 32 31.999;visibility: hidden" xml:space="preserve" >
                <g class = "plusmaingroup">
                <g id="Elipse_7_">
                    <g>
                        <circle style="fill:#88C057;" cx="16" cy="16" r="16"/>
                    </g>
                </g>
                <g id="Plus_1_">
                    <g>
                        <path style="fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;" d="M21,14.999h-4v-4c0-0.553-0.447-1-1-1s-1,0.447-1,1v4h-4
                            c-0.553,0-1,0.447-1,1c0,0.552,0.447,1,1,1h4v4c0,0.552,0.447,1,1,1s1-0.448,1-1v-4h4c0.553,0,1-0.448,1-1
                            C22,15.446,21.553,14.999,21,14.999z"/>
                    </g>
                </svg>
                <svg id = "minus-sign"version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         width="32px" height="32px" viewBox="0 0 32 32" style="enable-background:new 0 0 32 32;visibility:hidden" xml:space="preserve">
                    <g id="Elipse_44_">
                        <g>
                            <path style="fill-rule:evenodd;clip-rule:evenodd;fill:#CCCCCC;" d="M16,0C7.163,0,0,7.163,0,16c0,8.836,7.163,16,16,16
                                s16-7.164,16-16C32,7.163,24.837,0,16,0z M16,30C8.268,30,2,23.732,2,16C2,8.269,8.268,2,16,2s14,6.269,14,14
                                C30,23.732,23.732,30,16,30z"/>
                        </g>
                    </g>
                    <g id="Minus_1_">
                        <g>
                            <path style="fill:#ED7161;" d="M20,15h-8c-0.552,0-1,0.447-1,1c0,0.552,0.448,1,1,1h8c0.553,0,1.001-0.448,1.001-1
                                C21.001,15.447,20.553,15,20,15z"/>
                        </g>
                    </g>
                </svg>