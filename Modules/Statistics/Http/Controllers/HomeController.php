<?php

namespace Modules\Statistics\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class HomeController extends Controller
{
     public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function index()
    {   
        $url = $this->url.'/gethomedataindex';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if ($status_code == 404) return redirect('/')->with('message', 'This URI is Not Found')->
        with('messagetype', 'danger');
        if ($status_code == 500) return redirect('/')->with('message', $body['errors'][0])->with('messagetype', 'danger');
        if ($status_code == 200)
        {
            return View('Home.home')->with('skus',$body['sku'])->with('clients',$body['noClients']);
            //return $body['sku'];
        }
    }
    public function homeData()
    {
        $url = $this->url.'/gethomedata';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        //return $response['body'];
        if ($status_code == 404)
        {
            $return_data = array(
                'error'=>true,
                'message'=>'Not Found'
                );
            return $return_data;
        }
        if ($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>'Internal Server Error'
                );
            return $return_data;
        }
        if ($status_code == 200)
        {
            $return_data = array(
                'error'=>false,
                'message'=>'Data Pulled',
                'data'=>$body
                );
            return $return_data;
        }
    }
    public function getSKUs()
    {
        $url = $this->url.'/randomskus';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        //return $response['body'];
        if ($status_code == 404)
        {
            $return_data = array(
                'error'=>true,
                'message'=>'Not Found'
                );
            return $return_data;
        }
        if ($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>'Internal Server Error'
                );
            return $return_data;
        }
        if ($status_code == 200)
        {
            $return_data = array(
                'error'=>false,
                'message'=>'Data Pulled',
                'data'=>$body
                );
            return $return_data;
        }
    }
}
}
