<?php
$subdomain = null;
$websitename = null;
Route::group(array('domain' => $subdomain.'.'.$websitename), function() {
	Route::group(['middleware' => 'web', 'prefix' => 'statistics', 'namespace' => 'Modules\Statistics\Http\Controllers'], function()
	{
		Route::group(['prefix' => '/home'], function () {
	    	Route::get('/' , ['as' => 'home' ,'uses' => 'HomeController@index']);
			Route::get('/getskus' , ['as' => 'home.getSkus' ,'uses' => 'HomeController@getSKUs']);
			Route::get('/homedata','HomeController@homeData');
		});
	});
});
