var interval = 5000;
function addSKUHtml(sku,number,amount){
	return 
}
function attachSKUS(skus)
{
	//var appenedHtml ='';
    $('#skusWidget').data('owlCarousel').removeItem();
    $('#skusWidget').data('owlCarousel').removeItem();
    $('#skusWidget').data('owlCarousel').removeItem();
	for(var i=0;i<skus.length;i++)
	{
		appenedHtml = '<div><div class="widget-title">'+skus[i].sku+'</div>'
                        +'<div class="widget-subtitle">Item Number '+skus[i].number+'</div>'
                        +'<div class="widget-int">'+skus[i].amount+'</div></div>';
        $('#skusWidget').data('owlCarousel').addItem(appenedHtml);
	}
	//console.log(appenedHtml);
	//$('#skusWidget').html(appenedHtml).trigger('refresh.owl.carousel');

}
function doAjax() {
    $.ajax({
            type: 'GET',
            url: '/home/getskus',
            success: function (data) {
            	if(data.error == false)
                	attachSKUS(data.data);
            },
            complete: function (data) {
                setTimeout(doAjax, interval);
            }
    });
}
$(document).ready(function(){
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

	setTimeout(doAjax, interval);
    $.ajax({
        url:"/homedata",
        type: "GET",
        success:function(response)
        {
        	$addedHtml = '';
        	for(var x in response.data.amountSKU)
        	{
        		$addedHtml += addSKUHtml(response.data.amountSKU[x].sku,response.data.amountSKU[x].number,response.data.amountSKU[x].amount);
        	}
        	$( "#sku" ).append($addedHtml);
            var morrisCharts = function() {
			    // Morris.Bar({
			    //     element: 'morris-bar-example',
			    //     data: [
			    //         { y: '2006', a: 100, b: 90 },
			    //         { y: '2007', a: 75,  b: 65 },
			    //         { y: '2008', a: 50,  b: 40 },
			    //         { y: '2009', a: 75,  b: 65 },
			    //         { y: '2010', a: 50,  b: 40 },
			    //         { y: '2011', a: 75,  b: 65 },
			    //         { y: '2012', a: 100, b: 90 }
			    //     ],
			    //     xkey: 'y',
			    //     ykeys: ['a', 'b'],
			    //     labels: ['Series A', 'Series B'],
			    //     barColors: ['#B64645', '#33414E']
			    // });


			    Morris.Donut({
			        element: 'empty-zones',
			        data: [
			            {label: "Empty Zones", value: response.data.getEmptyZones.noOfEmptyZones},
			            {label: "NonEmptyZones", value: response.data.getEmptyZones.noOfNonEmptyZones},
			        ],
			        colors: ['#3FBAE4', '#B64645']
			    });
			    Morris.Donut({
			        element: 'issueOrderCategories',
			        data: [
			            {label: "New Issue Orders", value: response.data.issueOrderCategories.noNew},
			            {label: "Treated Issue Orders", value: response.data.issueOrderCategories.noTreated},
			            {label: " Partially Treated Issue Orders", value: response.data.issueOrderCategories.noPartiallyTreated}
			        ],
			        colors: ['#3FBAE4', '#B64645','#FEA223']
			    });
			    Morris.Donut({
			        element: 'issueOrderCategories2',
			        data: [
			            {label: "Shipped Issue Orders", value: response.data.issueOrderCategories2.noShipped},
			            {label: "Partially Shipped Issue Orders", value: response.data.issueOrderCategories2.noPartiallyShipped},
			            {label: "Picked Issue Orders", value: response.data.issueOrderCategories2.noPicked},
			            {label: "Partially Picked Issue Orders", value: response.data.issueOrderCategories2.noPartiallyPicked}
			        ],
			        colors: ['#B64645', '#551a8b','#3FBAE4','#FEA223']
			    });
			    Morris.Donut({
			        element: 'purchaseOrderCategories',
			        data: [
			            {label: "New Purchase Orders", value: response.data.purchaseOrderCategories.noNew},
			            {label: "Received Purchase Orders", value: response.data.purchaseOrderCategories.noRecieved},
			            {label: "Partially Recieved Purchase Orders", value: response.data.purchaseOrderCategories.noPartiallyRecieved}
			        ],
			        colors: ['#FF0000', '#B64645','#3FBAE4']
			    });
			    var rj_sj = new Array();
			    var counter = 0;
			    for(var x in response.data.recevingJobsShippingJobsDate.receivingJobs)
			    {
			    	rj_sj.push({'y':response.data.recevingJobsShippingJobsDate.receivingJobs[x].year,
			    		'a':response.data.recevingJobsShippingJobsDate.receivingJobs[x].amount,
			    		'b':response.data.recevingJobsShippingJobsDate.shippingJobs[x].amount
			    	});
			    }
			    
			    Morris.Bar({
			      element: 'RJ_SJ',
			      data: rj_sj,
			      xkey: 'y',
			      ykeys: ['a', 'b'],
			      labels: ['Recieved', 'Issued'],
			      resize: true,
			      lineColors: ['#00ff00', '#ff0000']
			    });

		}();
        }
    });
    
	
});