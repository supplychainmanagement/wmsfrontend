<?php
$subdomain = null;
$websitename = null;
Route::group(array('domain' => $subdomain.'.'.$websitename), function() {
	Route::group(['middleware' => 'web', 'prefix' => 'reports', 'namespace' => 'Modules\Reports\Http\Controllers'], function()
	{
	    Route::get('/',['as'=>'transactionreport','uses'=>'ReportsController@indexTransactionReport']);
		Route::get('/stockreport',['as'=>'stockreport','uses'=>'ReportsController@indexStockReport']);
		Route::get('/dailytransactionreport',['as'=>'dailytransactionreport','uses'=>'ReportsController@indexMinTransactionReport']);
		Route::get('/orderreport',['as'=>'orderreport','uses'=>'ReportsController@indexOrderReport']);
		Route::post('/gettransactionreport','ReportsController@getTransactionReport');
		Route::post('/getstockreport','ReportsController@getStockReport');
		Route::post('/getmintransactionreport','ReportsController@getMinTransactionReport');
		Route::post('/getorderreport','ReportsController@getOrderReport');
	});
});
