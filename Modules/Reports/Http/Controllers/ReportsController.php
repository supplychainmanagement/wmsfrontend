<?php

namespace Modules\Reports\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class ReportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function indexTransactionReport()
    {
        $url = $this->url.'/reports/getalldata';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        //return $res
        if ($status_code == 404) return redirect('/home')->with('message', 'This URI is Not Found')->
        with('messagetype', 'danger');
        if ($status_code == 500) return redirect('/home')->with('message', $body['errors'][0])->with('messagetype', 'danger');
        if ($status_code == 200)
        {
            return View('Reports.transactions')->with('clients',$body['client_names'])->with('date',date("Y-m-d"));
        }
        if($status_code == 401)
        {
            return redirect('/home')->with('message',$body[0])->with('messagetype','danger');
        }

    }

    public function indexStockReport()
    {
        $url = $this->url.'/reports/getalldata';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        //return $response;
        if ($status_code == 404) return redirect('/home')->with('message', 'This URI is Not Found')->
        with('messagetype', 'danger');
        if ($status_code == 500) return redirect('/home')->with('message', $body['errors'][0])->with('messagetype', 'danger');
        if ($status_code == 200)
        {
            return View('Reports.stocks')->with('clients',$body['client_names'])->with('sites',$body['site_names'])->with('date',date("Y-m-d"));
        }
        if($status_code == 401)
        {
            return redirect('/home')->with('message',$body[0])->with('messagetype','danger');
        }

    }

    public function indexMinTransactionReport()
    {
        $url = $this->url.'/reports/getalldata';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if ($status_code == 404) return redirect('/home')->with('message', 'This URI is Not Found')->
        with('messagetype', 'danger');
        if ($status_code == 500) return redirect('/home')->with('message', $body['errors'][0])->with('messagetype', 'danger');
        if ($status_code == 200)
        {
            return View('Reports.mintransactions')->with('clients',$body['client_names'])->with('sites',$body['site_names'])->with('skus',$body['sku_names'])->with('date',date("Y-m-d"));
        }
            if($status_code == 401)
            {
                return redirect('/home')->with('message',$body[0])->with('messagetype','danger');
            }


    }

    public function indexOrderReport()
    {
        $url = $this->url.'/reports/getalldata';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if ($status_code == 404) return redirect('/home')->with('message', 'This URI is Not Found')->
        with('messagetype', 'danger');
        if ($status_code == 500) return redirect('/home')->with('message', $body['errors'][0])->with('messagetype', 'danger');
        if ($status_code == 200)
        {
            return View('Reports.orders')->with('clients',$body['client_names'])->with('skus',$body['sku_names'])->with('date',date("Y-m-d"));
        }
        if($status_code == 401)
        {
            return redirect('/home')->with('message',$body[0])->with('messagetype','danger');
        }

    }

    public function getTransactionReport(Request $request)
    {
        $url = $this->url.'/reports/gettransactionreport';
        $fields = $request->all();
        //return $fields;
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>'Missing Inputs',
                'body'=>$body
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>'Server Encountered an error .. '.$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>'Items Pulled',
            'transactions'=>$body
            );
            return $return_data;
        }

    }

    public function getStockReport(Request $request)
    {
        $url = $this->url.'/reports/getstockreport';
        $fields = $request->all();
        //return $fields;
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        //return $body;
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>'Missing Inputs',
                'body'=>$body
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>'Server Encountered an error .. '.$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>'Items Pulled',
            'stocks'=>$body
            );
            return $return_data;
        }

    }

    public function getMinTransactionReport(Request $request)
    {
        $url = $this->url.'/reports/getmintransactionreport';
        $fields = $request->all();
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>'Missing Inputs',
                'body'=>$body
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>'Server Encountered an error .. '.$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>'Items Pulled',
            'mintransactions'=>$body
            );
            return $return_data;
        }

    }

    public function getOrderReport(Request $request)
    {
        $url = $this->url.'/reports/getorderreport';
        $fields = $request->all();
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>'Missing Inputs',
                'body'=>$body
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>'Server Encountered an error .. '.$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>'Items Pulled',
            'orders'=>$body
            );
            return $return_data;
        }

    }
}