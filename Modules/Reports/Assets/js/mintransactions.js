var numofCols =8;
function addDataToTable(mintransactions)
{
    var reportsTable = $('#report-table').DataTable();
     $('#report-table').dataTable().fnClearTable();
    for(var i=0;i<numofCols;i++)
    {
        $('#select'+i).empty().selectpicker('refresh');
    }
    for(var i=0;i<mintransactions.length;i++)
    {
        var counter =0;
         var rowData = new Array();

        rowData.push(mintransactions[i]['date']);
        $('#select'+counter).append( '<option value="'+mintransactions[i]['date']+'">'+mintransactions[i]['date']+'</option>').selectpicker('refresh');
        counter ++;

        rowData.push(mintransactions[i]['purchaseorders']);
        $('#select'+counter).append( '<option value="'+mintransactions[i]['purchaseorders']+'">'+mintransactions[i]['purchaseorders']+'</option>').selectpicker('refresh');
        counter++;
        
        rowData.push(mintransactions[i]['receivingjobs']);
        $('#select'+counter).append( '<option value="'+mintransactions[i]['receivingjobs']+'">'+mintransactions[i]['receivingjobs']+'</option>').selectpicker('refresh');
        counter++;

        rowData.push(mintransactions[i]['storagejobs']);
        $('#select'+counter).append( '<option value="'+mintransactions[i]['storagejobs']+'">'+mintransactions[i]['storagejobs']+'</option>').selectpicker('refresh');
        counter++;

        rowData.push(mintransactions[i]['issueorders']);
        $('#select'+counter).append( '<option value="'+mintransactions[i]['issueorders']+'">'+mintransactions[i]['issueorders']+'</option>').selectpicker('refresh');
        counter++;

        rowData.push(mintransactions[i]['pickrequests']);
        $('#select'+counter).append( '<option value="'+mintransactions[i]['pickrequests']+'">'+mintransactions[i]['pickrequests']+'</option>').selectpicker('refresh');
        counter++;

        rowData.push(mintransactions[i]['pickjobs']);
        $('#select'+counter).append( '<option value="'+mintransactions[i]['pickjobs']+'">'+mintransactions[i]['pickjobs']+'</option>').selectpicker('refresh');
        counter++;

        rowData.push(mintransactions[i]['shippings']);
        $('#select'+counter).append( '<option value="'+mintransactions[i]['shippings']+'">'+mintransactions[i]['shippings']+'</option>').selectpicker('refresh');
        counter++;

         reportsTable.row.add(rowData).draw();
        
        
    }
    // $('#report-table').dataTable().fnSettings().oScroll.sX = true;
    $('#report-panel').show();
    
}

$(document).ready(function() {
  var utc = new Date().toJSON().slice(0,10);
    $('#report-table').DataTable( {
         dom: 'Bfrtip',
        buttons: [
            {
                extend: 'pdfHtml5',
                orientation: 'portrait',
                pageSize: 'LEGAL',
                title:'Daily Transactions Report ['+utc+']',
                message:'Powered By Micro Engineering roboVics'
            }
        ],
        initComplete: function () {
            var counter =0;

            this.api().columns().every( function () {
                var column = this;
                var select = $('<select class="form-control selectpicker show-tick" title="Pick Something Please!" data-live-search="true"id = "select'+counter+'"></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
                counter++;
            } );
        }//,
        //"scrollX": true
        });    
    //alert('hi');
    $("#mintransactiongenerate-form").submit(function(e) {
        //alert('hi');
         $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
         var sentdata = $("#mintransactiongenerate-form").serialize();
         if($('#mintransactiongenerate-form').valid())
         {
           $.ajax({
                url: "getmintransactionreport",
                type: "post",
                data: sentdata ,
                success: function (response) {
                   if(response.error==true){
                    
                   }
                   else
                   {
                     var mintransactions = response.mintransactions;
                     addDataToTable(mintransactions);
                   }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                   
                }


            });
        }
      e.preventDefault();
  });
    
});