var numofCols =0;
function addDataToTable(stocks)
{
    var reportsTable = $('#report-table').DataTable();
     $('#report-table').dataTable().fnClearTable();
    for(var i=0;i<numofCols;i++)
    {
        $('#select'+i).empty().selectpicker('refresh');
    }
    for(var i=0;i<stocks.length;i++)
    {
        var counter =0;
         var rowData = new Array();
        rowData.push(stocks[i]['SKU_name']);
        $('#select'+counter).append( '<option value="'+stocks[i]['SKU_name']+'">'+stocks[i]['SKU_name']+'</option>').selectpicker('refresh');
        counter ++;
        rowData.push(stocks[i]['CLI_name']);
        $('#select'+counter).append( '<option value="'+stocks[i]['CLI_name']+'">'+stocks[i]['CLI_name']+'</option>').selectpicker('refresh');
        counter++;
        for(var j=0;j<stocks[i]['site'].length;j++)
        {
            rowData.push(stocks[i]['site'][j]['amount']);
            $('#select'+counter).append( '<option value="'+stocks[i]['site'][j]['amount']+'">'+stocks[i]['site'][j]['amount']+'</option>').selectpicker('refresh');
            counter++;
        }
         reportsTable.row.add(rowData).draw();

        
    }
    
    // if(dataset.length>0)
    // {
    //     $('#report-table').DataTable( {
    //     data: dataset,
    //     columns:sitecolumns,
    //     });

    //     // $('#report-table').DataTable( {
    //     // initComplete: function () {
    //     //     this.api().columns().every( function () {
    //     //         var column = this;
    //     //         var select = $('<select><option value=""></option></select>')
    //     //             .appendTo( $(column.footer()).empty() )
    //     //             .on( 'change', function () {
    //     //                 var val = $.fn.dataTable.util.escapeRegex(
    //     //                     $(this).val()
    //     //                 );
 
    //     //                 column
    //     //                     .search( val ? '^'+val+'$' : '', true, false )
    //     //                     .draw();
    //     //             } );
 
    //     //         column.data().unique().sort().each( function ( d, j ) {
    //     //             select.append( '<option value="'+d+'">'+d+'</option>' )
    //     //         } );
    //     //     } );
    //     // }
    //     // });
        $('#report-panel').show();

        
        //$('')
    // }
    // else
    // {
    //     
    // }
    
}

$(document).ready(function() {
    var utc = new Date().toJSON().slice(0,10);
    $('#report-table').DataTable( {
        dom: 'Bfrtip',
        buttons: [
           {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                title:'Stocks Report ['+utc+']',
                message:'Powered By Micro Engineering roboVics'
            }
        ],
        initComplete: function () {
            var counter =0;

            this.api().columns().every( function () {
                numofCols++;
                var column = this;
                var select = $('<select class="form-control selectpicker show-tick" title="Pick Something Please!" data-live-search="true"id = "select'+counter+'"></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
                counter++;
            } );
        }
        });    
    //alert('hi');
    $("#stockgenerate-form").submit(function(e) {
        //alert('hi');
         $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
         var sentdata = $("#stockgenerate-form").serialize();
         if($('#stockgenerate-form').valid())
         {
           $.ajax({
                url: "getstockreport",
                type: "post",
                data: sentdata ,
                success: function (response) {
                   if(response.error==true){
                    
                   }
                   else
                   {
                     var stocks = response.stocks;
                     addDataToTable(stocks);
                   }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                   
                }


            });
        }
      e.preventDefault();
  });
    
});