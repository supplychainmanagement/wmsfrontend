var numofCols =4;
function addDataToTable(orders)
{
    var reportsTable = $('#report-table').DataTable();
     $('#report-table').dataTable().fnClearTable();
    for(var i=0;i<numofCols;i++)
    {
        $('#select'+i).empty().selectpicker('refresh');
    }
    for(var i=0;i<orders.length;i++)
    {
        var counter =0;
         var rowData = new Array();

        rowData.push(orders[i]['order']);
        $('#select'+counter).append( '<option value="'+orders[i]['order']+'">'+orders[i]['order']+'</option>').selectpicker('refresh');
        counter ++;

        rowData.push(orders[i]['client']);
        $('#select'+counter).append( '<option value="'+orders[i]['client']+'">'+orders[i]['client']+'</option>').selectpicker('refresh');
        counter++;
        
        rowData.push(orders[i]['site']);
        $('#select'+counter).append( '<option value="'+orders[i]['site']+'">'+orders[i]['site']+'</option>').selectpicker('refresh');
        counter++;

        rowData.push(orders[i]['type']);
        $('#select'+counter).append( '<option value="'+orders[i]['type']+'">'+orders[i]['type']+'</option>').selectpicker('refresh');
        counter++;


         reportsTable.row.add(rowData).draw();
        
        
    }
    // $('#report-table').dataTable().fnSettings().oScroll.sX = true;
    $('#report-panel').show();
    
}

$(document).ready(function() {
    var utc = new Date().toJSON().slice(0,10);
    $('#report-table').DataTable( {
         dom: 'Bfrtip',
        buttons: [
            {
                extend: 'pdfHtml5',
                orientation: 'portrait',
                title:'Non Finished Orders Report ['+utc+']',
                message:'Powered By Micro Engineering roboVics',
                pageSize: 'LEGAL',
                 exportOptions: {
                     columns: [0, 1, 2, 3]
                  },
                customize : function(doc) {
                 doc.styles['td:nth-child(2)'] = { 
                   width: '500px',
                   'max-width': '500px'
                 }
              }
            }
        ],
        initComplete: function () {
            var counter =0;

            this.api().columns().every( function () {
                var column = this;
                var select = $('<select class="form-control selectpicker show-tick" title="Pick Something Please!" data-live-search="true"id = "select'+counter+'"></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
                counter++;
            } );
        }//,
        //"scrollX": true
        });    
    //alert('hi');
    $("#ordergenerate-form").submit(function(e) {
        //alert('hi');
         $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
         var sentdata = $("#ordergenerate-form").serialize();
         if($('#ordergenerate-form').valid())
         {
           $.ajax({
                url: "/reports/getorderreport",
                type: "post",
                data: sentdata ,
                success: function (response) {
                   if(response.error==true){
                    
                   }
                   else
                   {
                     var orders = response.orders;
                     addDataToTable(orders);
                   }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                   
                }


            });
        }
      e.preventDefault();
  });
    
});