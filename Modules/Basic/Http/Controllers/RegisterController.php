<?php

namespace Modules\Basic\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class RegisterController extends Controller
{
    class RegisterController extends Controller
{
    public function __construct()
    {
      parent::__construct();
    }
    public function index()
    {
      
    }
    /**
     * generate support center view.
     * @return support view
     */
    public function supportIndex()
    {
      
    }
    /**
     * Register warehouse.
     *
     * @param  Array containing data for registeration
     * @return error or redirect to subdomain
     */
    public function register(Request $request)
    {
       
    }
    /**
     * verifies invitationCode.
     * @param  invitationCode attributes
     * @return success or failure of verification
     */
    public function verifyCode(Request $request)
    {
       
    }
    /**
     * create RFQ.
     * @param  RFQ attributes
     * @return success or failure of creation
     */
    public function RFQ(Request $request)
    {
      
    }
}
    
