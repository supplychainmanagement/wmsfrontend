<?php

Route::group(['middleware' => 'web', 'prefix' => 'basic', 'namespace' => 'Modules\Basic\Http\Controllers'], function()
{
    Route::get('/', ['as'=>'registerwarehouseview','uses'=>'RegisterController@index']);
	Route::get('/supportcenter', ['as'=>'supportcenter','uses'=>'RegisterController@supportIndex']);

	Route::post('/',['as'=>'registerwarehouse','uses'=>'RegisterController@register']);
	Route::post('/rfq', ['as'=>'rfq','uses'=>'RegisterController@RFQ']);
});
