$(document).ready(function(){
	 $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
	 $("#verifycode").submit(function(e) {

     $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
     var sentdata = $("#verifycode").serialize();
     if($('#verifycode').valid())
     {
       var code = $('#code').val();
       $.ajax({
            url: "verifycode",
            type: "post",
            data: sentdata ,
            success: function (response) {
               if(response.error == true)
              {
                 var list = "<ul>";
	              for(var x=0 ;x<response.message.length;x++)
	              {
	                list += '<li>'+response.message[x]+'</li>';
	              }
	              list += "</ul>";
	              document.getElementById('messagecode').innerHTML=
	              '<div class="alert alert-warning" role="alert">'+
	                  '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
	                  +'<strong>'+list+'</strong>'
	              +'</div>';
	              }
               else
               {
               		$('#codehidden').val(code);
                 $('#verifycode').hide();
                 $('#join').show();
                 var list = "<ul>";
	              for(var x=0 ;x<response.data.length;x++)
	              {
	                list += '<li>'+response.data[x]+'</li>';
	              }
	              list += "</ul>";
	              document.getElementById('messagecode').innerHTML=
	              '<div class="alert alert-success" role="alert">'+
	                  '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
	                  +'<strong>'+list+'</strong>'
	              +'</div>';
	              
               }
            },
            error: function(jqXHR, textStatus, errorThrown) {
               
            }


        });
    }
      e.preventDefault();
  	});


	 $("#join").submit(function(e) {

     $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
     var sentdata = $("#join").serialize();
     if($('#join').valid())
     {
       
       $.ajax({
            url: "invitation",
            type: "post",
            data: sentdata ,
            success: function (response) {
               if(response.error == true)
              {
                 var list = "<ul>";
	              for(var x=0 ;x<response.message.length;x++)
	              {
	                list += '<li>'+response.message[x]+'</li>';
	              }
	              list += "</ul>";
	              document.getElementById('messagecode').innerHTML=
	              '<div class="alert alert-warning" role="alert">'+
	                  '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
	                  +'<strong>'+list+'</strong>'
	              +'</div>';
	              }
               else
               {

                 var list = "<ul>";
	              for(var x=0 ;x<response.data.length;x++)
	              {
	                list += '<li>'+response.data[x]+'</li>';
	              }
	              list += "</ul>";
	              document.getElementById('messagecode').innerHTML=
	              '<div class="alert alert-success" role="alert">'+
	                  '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
	                  +'<strong>'+list+'</strong>'
	              +'</div>';
	              
               }
            },
            error: function(jqXHR, textStatus, errorThrown) {
               
            }


        });
    }
      e.preventDefault();
  	});

	 $("#rfqform").submit(function(e) {

     $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
     var sentdata = $("#rfqform").serialize();
     if($('#rfqform').valid())
     {
       
       $.ajax({
            url: "rfq",
            type: "post",
            data: sentdata ,
            success: function (response) {
               if(response.error == true)
              {
                 var list = "<ul>";
	              for(var x=0 ;x<response.message.length;x++)
	              {
	                list += '<li>'+response.message[x]+'</li>';
	              }
	              list += "</ul>";
	              document.getElementById('messagerfq').innerHTML=
	              '<div class="alert alert-warning" role="alert">'+
	                  '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
	                  +'<strong>'+list+'</strong>'
	              +'</div>';
	              }
               else
               {
                 var list = "<ul>";
	              for(var x=0 ;x<response.data.length;x++)
	              {
	                list += '<li>'+response.data[x]+'</li>';
	              }
	              list += "</ul>";
	              document.getElementById('messagerfq').innerHTML=
	              '<div class="alert alert-success" role="alert">'+
	                  '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
	                  +'<strong>'+list+'</strong>'
	              +'</div>';
	              
               }
            },
            error: function(jqXHR, textStatus, errorThrown) {
               
            }


        });
    }
      e.preventDefault();
  	});
});