<?php

Route::group(['middleware' => 'web', 'prefix' => 'receiving', 'namespace' => 'Modules\Receiving\Http\Controllers'], function()
{
    Route::group(['prefix'=>'/purchaseorder'],function(){
		Route::get('/items',['uses'=>'PurchaseOrderController@itemindex','as'=>'purchaseorder.items']);
		Route::get('/purchaseorderwizard',['uses'=>'PurchaseOrderController@createorderindex','as'=>'purchaseorderwizard']);


		Route::post('/getentry' ,'PurchaseOrderController@getEntry');
		Route::post('/getproviders' ,'PurchaseOrderController@getProviders');
		Route::post('/save','PurchaseOrderController@save');
		Route::post('/saveitem','PurchaseOrderController@saveItem');
		Route::post('/getskus' ,'PurchaseOrderController@getSKUs');
		Route::post('/removeitem','PurchaseOrderController@removeItem');
		Route::get('/pdf',['as'=>'purchaseOrderPdf','uses'=>'PurchaseOrderController@pdf']);
		Route::get('/{PO_number?}',['as'=>'purchaseorder','uses' =>'PurchaseOrderController@index']);
		Route::post('/create',['as'=>'PurchaseOrderCreate','uses'=>'PurchaseOrderController@create']);
		Route::post('/update','PurchaseOrderController@update');
		Route::post('/delete','PurchaseOrderController@delete');
		Route::post('/finishpurchaseorder',['as'=>'finishpurchaseorder','uses'=>'PurchaseOrderController@finishPurchaseOrder']);
	});

	Route::group(['prefix'=>'/purchaseorderitem'],function(){
		Route::post('/create',['as'=>'PurchaseOrderItemCreate','uses'=>'PurchaseOrderItemController@create']);
		Route::get('/create','PurchaseOrderItemController@createIndex');
		Route::get('/{POI_number?}','PurchaseOrderItemController@index');
		Route::post('/update','PurchaseOrderItemController@update');
		Route::post('/delete','PurchaseOrderItemController@delete');
		Route::post('/',['uses'=>'PurchaseOrderItemController@getprop','as'=>'purchaseorderitem']);
	});

	Route::group(['prefix'=>'/receivingjob'],function(){
		Route::get('/create',['as'=>'createrecevingjob','uses'=>'ReceivingJobController@createIndex']);
		Route::get('/',['as'=>'receivingjob','uses'=>'ReceivingJobController@index']);
		Route::post('/pobyname','ReceivingJobController@getPurchaseOrderByName');
		Route::post('/poibyname','ReceivingJobController@getPurchaseOrderItemByName');
		Route::post('/matchedunitload','ReceivingJobController@getMatchedUnitloads');
		Route::get('/rcjnumber','ReceivingJobController@getRCJNumber');
		Route::post('/submit','ReceivingJobController@submitReceivingJob');
	});

	Route::group(['prefix'=>'/storagejob'],function(){
		Route::get('/create',['as'=>'StorageJobCreate','uses'=>'StorageJobController@createIndex']);
		Route::get('/',['as'=>'storagejob','uses'=>'StorageJobController@index']);
		Route::get('/stjdata','StorageJobController@getDataForStorageJob');
		Route::post('/stjstrategy','StorageJobController@getStorageLocationsForStrategy');
		Route::get('/stjname','StorageJobController@generateStorageJobName');
		Route::post('/submit',['as'=>'SubmitSTJ','uses'=>'StorageJobController@submitStorageJob']);
	});
});
