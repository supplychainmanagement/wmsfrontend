/**
 * Created by Khaled on 8/11/2016.
 */
function message(response,status)
{
    var list = "<ul>";
      for(var x=0 ;x<response.message.length;x++)
      {
        list += '<li>'+response.message[x]+'</li>';
      }
      list += "</ul>";
      document.getElementById('message').innerHTML=
      '<div class="alert alert-'+status+'" role="alert">'+
          '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
          +'<strong>'+list+'</strong>'
      +'</div>';
}


$(document).ready(function(){
    var RCJ_number = 0;
    var POI_number = 0;
    var RCJ_amount = 0;
    var Unitloads_amount_array = new Array();
    var counter = 0;
    var counter2 = 0;
    $.ajax({
        url: "rcjnumber",
        type: "GET",
        success: function(response)
        {
             if(response.error == true)
            {
                message(response,'warning');
            }
            else
            {
               message(response,'success');
                RCJ_number = response.data;
            }
            
        }
    });
    $('#PO_name').on('change', function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var value = this.value;
        $.ajax({
            url: "pobyname",
            type: "post",
            data: {'PO_number':value} ,
            success: function (response) {
                if(response.error == true)
                {
                  message(response,'warning');
                }
                else
                {
                    message(response,'success');
                    $('#POI_name').empty().selectpicker('refresh');
                    response.data.forEach(function(x)
                    {
                        $('#POI_name').append($('<option>', {
                            value: x.name,
                            text: (x.name + "  | "+x.sku+" => "+x.amount+" |")
                        }));
                    });
                    $('#POI_name').selectpicker('refresh');
                }
                
            },
            error: function(jqXHR, textStatus, errorThrown) {
                

            }
        });
    });
    $('#POI_name').on('change', function() {
        POI_number = this.value;
        $.ajax({
            url: "poibyname",
            type: "post",
            data: {'POI_number':POI_number} ,
            success: function (response) {
                if(response.error == true)
                {
                  message(response,'warning');
                }
                else
                {
                    message(response,'success');
                    document.getElementById('client').innerHTML= response.data.POI_client;
                    document.getElementById('provider').innerHTML= response.data.PRV_name.PRV_name;
                    document.getElementById('productiondate').innerHTML= response.data.POI_productiondate;
                    document.getElementById('sku').innerHTML= response.data.SKU_name;
                    document.getElementById('amount').innerHTML= response.data.POI_amount;
                    document.getElementById('received').innerHTML= response.data.POI_received;
                    //document.getElementsByName('requiredKnob').value = response.POI_amount;
                    $('#requiredKnob').trigger('configure', {
                        max: response.data.POI_amount
                       });
                     $("#requiredKnob").val(response.POI_amount);
                     $("#requiredKnob").trigger('change');
                    RCJ_amount = response.data.POI_amount;

                    document.getElementById('RCJ_amount').value = 0;
                    $('#POI_info').removeClass('show').addClass('hide');
                    $('#amount-info').removeClass('show').addClass('hide');
                    $('#knob-col').removeClass('show').addClass('hide');
                    $('#availableUnitloadsDiv').addClass('collapse');
                    $('#buttonsDiv').removeClass('show').addClass('hide');
                }
                
            },
            error: function(jqXHR, textStatus, errorThrown) {
                
            }
        });
    });

    $('#RCJ_amount').keyup(function(){
            $('#myForm').validate({
                rules:{
                    RCJ_amount:{
                        required:true,
                        number:true,
                        min:1,
                        range:[1,document.getElementById('amount').innerHTML]
                    }
                }
            });
            if($('#RCJ_amount').valid())
            {
                $('#RCJ_add').removeClass('disabled');
            }else{
                $('#RCJ_add').addClass('disabled');
            }
    });

    $('#RCJ_add').on('click',function(e){
        $('#myForm').validate({
            rules:{
                RCJ_amount:{
                    required:true,
                    number:true,
                    min:1
                }
            }
        });
        var now = new Date();
        var prodate = new Date(document.getElementById('productiondatefrom').value);
        if(prodate.getTime() > now.getTime())
        {
            document.getElementById('message').innerHTML=
              '<div class="alert alert-warning" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                  +'<strong>Production Date Must be before Today</strong>'
              +'</div>';
        }
        else
        {
            if(!$('#RCJ_amount').valid())
            {
                $('#RCJ_add').addClass('disabled');
            }else{
                $('#RCJ_add').addClass('disabled');
                $('#requiredKnob').show();
                $('#POI_info').removeClass('hide').addClass('show');
                $('#amount-info').removeClass('hide').addClass('show');
                $('#knob-col').removeClass('hide').addClass('show');
                $('#availableUnitloadsDiv').removeClass('collapse');
                RCJ_amount = document.getElementById('RCJ_amount').value;

                $("#requiredKnob").val(RCJ_amount);
                $("#requiredKnob").trigger('change');

                var dataTable = $('#available_unitloads').DataTable();
                $('#available_unitloads').dataTable().fnClearTable();

                $.ajax({
                    url:'matchedunitload',
                    type:'post',
                    data: {'POI_number':POI_number},
                    success: function (unitloads)
                    {
                        if(unitloads.error == true)
                        {
                          message(unitloads,'warning');
                        }
                        else
                        {
                            message(unitloads,'success');

                            unitloads.data.forEach(function(unitload){
                            dataTable.row.add
                                ([
                                    unitload.unitload.UL_name,
                                    unitload.route,
                                    '<form id="UL_status">' +
                                    '<input type="text" class="form-control" name = "UL_status[]" id="UL_status'+counter+'" required/>'+
                                    '</form>',
                                    unitload.unitload.storage_location.STL_name,
                                    '<form id="UL_form">' +
                                    '<input type="text" class="form-control" name = "UL_amount[]" id="UL_amount'+counter+'" required/>'+
                                    '</form>',
                                    '<button type="button" class="btn btn-warning addUnitload" id="addUnitload'+counter+'">Add</button>'
                                ]).draw();
                            counter++;
                            });
                        }
                        
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        
                    }
                });
            }            
        }

        
    });

        $(document).on("click",'.addUnitload',function(e)
        {
            var tr = $(this).closest('tr');
            cell4 = tr.find("td:nth-child(4)");
            cell1 = tr.find("td:nth-child(1)");
            var oTableApi = $('#available_unitloads').dataTable().api();
            var id = '#UL_amount'+tr[0]._DT_RowIndex;
            var UL_status_id = '#UL_status'+tr[0]._DT_RowIndex;
            if (isNaN($(id).val()) || $(id).val()< 1) {
                document.getElementById('message').innerHTML=
                      '<div class="alert alert-warning" role="alert">'+
                          '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                          +'<strong>Please Enter a Postive integer</strong>'
                      +'</div>';
            }
            else if(!$(id).val() == '')
            {
                //$('#addUnitload'+tr[0]._DT_RowIndex).addClass('disabled');
                if($('#requiredKnob').val() < $(id).val())
                {
                    document.getElementById('message').innerHTML=
                      '<div class="alert alert-warning" role="alert">'+
                          '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                          +'<strong>Amount Specified is more than the amount in the receiving Job</strong>'
                      +'</div>';
                }else{
                    //$('#requiredKnob').val($('#requiredKnob').val()-$(id).val());
                    var knobNewAmount =parseInt($("#requiredKnob").val())-parseInt($(id).val())-1; 
                    $({animatedVal: parseInt($("#requiredKnob").val())}).animate({animatedVal: knobNewAmount}, {
                     duration: 500,
                     easing: "swing", 
                     step: function() { 
                         $("#requiredKnob").val(Math.ceil(this.animatedVal)).trigger("change"); 
                     }
                    });
                    Unitloads_amount_array.push({
                        "unitload_name":oTableApi.cell(cell1).data(),
                        "unitload_amount":$(id).val(),
                        "unitload_status":$(UL_status_id).val()
                    });
                    counter2++;
                    document.getElementById('message').innerHTML=
                      '<div class="alert alert-success" role="alert">'+
                          '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                          +'<strong>Amount Added You May Proceed on Adding New ULs</strong>'
                      +'</div>';

                    if(knobNewAmount <= 0)
                    {
                        //alert('hi   '+ $('#requiredKnob').val());
                        $('.addUnitload').addClass('disabled');
                        $('#buttonsDiv').removeClass('hide').addClass('show');
                         document.getElementById('message').innerHTML=
                          '<div class="alert alert-success" role="alert">'+
                              '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                              +'<strong>Recieving job amount fullfilled Please Dont forget to Submit the Job </strong>'
                          +'</div>';
                    }

                }
            }
    });


    $('#submit').on('click',function(){
        $.ajax({
            url:'submit',
            type:'post',
            data:{
                'RCJ_number':RCJ_number[0],
                'POI_number':POI_number,
                'RCJ_amount':$('#RCJ_amount').val(),
                'Unitloads_amount_array':Unitloads_amount_array,
                'POI_productiondate':$('#productiondatefrom').val()
            },
            success: function(response){
                if(response.error == true)
                {
                  message(response,'warning');
                }
                else
                {
                   $('.message-box-success').addClass('open');
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                
            }
        });
    //      setTimeout(function () {
    //     location.reload()
    // }, 3000);
    });

    $('#clear').on('click',function(){
        location.reload();
    });
});

// var jvalidate = $("#UL_form").validate({
//     ignore: [],
//     rules: {                                            
//         UL_amount:
//         {
//             required: true,
            
//         }
//     }                                        
// });   