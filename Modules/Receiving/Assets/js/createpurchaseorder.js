function getClientProviders(client)
{
  $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
     //alert(client);
       $.ajax({
          url: "getproviders",
          type: "post",
          async: false,
          data: {'value':client} ,
          success: function (response) {
            if(response.error == true)
            {
              var list = "<ul>";
              for(var x=0 ;x<response.message.length;x++)
              {
                list += '<li>'+response.message[x]+'</li>';
              }
              list += "</ul>";
              document.getElementById('message').innerHTML=
              '<div class="alert alert-warning" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                  +'<strong>'+list+'</strong>'
              +'</div>';
            }
            else
            {
              $('#provider').empty();
              $('#provider').attr('disabled',false);
              $selecthtml ='';
              for(var x in response.providers)
              {
                var provider = response.providers[x];
                //alert(customer);
                str = provider.replace(/\s+/g, '');
                $selecthtml = $selecthtml+'<option id="'+str+'">'+provider+'</option>';
              }
              $('#provider').html($selecthtml).selectpicker('refresh');
            }
          }
      });
}
function getClientSKUs(client)
{
   $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
     //alert(client);
       $.ajax({
          url: "getskus",
          type: "post",
          async: false,
          data: {'value':client} ,
          success: function (response) {
            if(response.error == true)
            {
              var list = "<ul>";
              for(var x=0 ;x<response.message.length;x++)
              {
                list += '<li>'+response.message[x]+'</li>';
              }
              list += "</ul>";
              document.getElementById('message').innerHTML=
              '<div class="alert alert-warning" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                  +'<strong>'+list+'</strong>'
              +'</div>';
            }
            else
            {
              $('#sku').empty();
              $('#sku').attr('disabled',false);
              $selecthtml ='';
              for(var x in response.skus)
              {
                var sku = response.skus[x];
                //alert(customer);
                str = sku.replace(/\s+/g, '');
                $selecthtml = $selecthtml+'<option id="'+str+'">'+sku+'</option>';
              }
              $('#sku').html($selecthtml).selectpicker('refresh');
            }
          }
      });
}
function getPurchaseOrderData(value)
{
  if(value == 'New Purchase Order')
    {
      $('#client').selectpicker('val', 'Pick Something Please!');
      $('#site').selectpicker('val', 'Pick Something Please!');
      $('#provider').selectpicker('val', 'Pick Something Please!');
      $('#assigned-purchaseorderitems-table').dataTable().fnClearTable();
      $("#assigned-purchaseorderitems-title").text('No Purchase Order Picked Yet');
    }
    else
    {
       $("#assigned-purchaseorderitems-title").text(value+' Items');
       document.getElementById('message').innerHTML=
          '<div class="alert alert-warning" role="alert">'+
              '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
              +'<strong>Loading...</strong>'
          +'</div>';
       $.ajax({
          url: "getentry",
          type: "post",
          data: {'value':value} ,
          success: function (response) {
            if(response.error == true)
            {
              var list = "<ul>";
              for(var x=0 ;x<response.message.length;x++)
              {
                list += '<li>'+response.message[x]+'</li>';
              }
              list += "</ul>";
              document.getElementById('message').innerHTML=
              '<div class="alert alert-warning" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                  +'<strong>'+list+'</strong>'
              +'</div>';
            }
            else
            {
              document.getElementById('message').innerHTML=
              '<div class="alert alert-success" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                  +'<strong>Finished Loading</strong>'
              +'</div>';
              $('#client').selectpicker('val', response.purchaseorder.CLI_name);
              $('#site').selectpicker('val', response.purchaseorder.SITE_name);
              $('#dateofdelivery').val(response.purchaseorder.dateofdelivery);
              getClientProviders(response.purchaseorder.CLI_name);
              getClientSKUs(response.purchaseorder.CLI_name);

             // update purchase Order Items
             var purchaseOrderItems = $('#assigned-purchaseorderitems-table').DataTable();
             $('#assigned-purchaseorderitems-table').dataTable().fnClearTable();
             for(var orderItem in response.purchaseorder.purchaseorderitems_data)
             {
                purchaseOrderItems.row.add
              ([
                response.purchaseorder.purchaseorderitems_data[orderItem].purchaseitem.POI_number,
                response.purchaseorder.purchaseorderitems_data[orderItem].SKU_name,
                response.purchaseorder.purchaseorderitems_data[orderItem].purchaseitem.POI_amount,
                response.purchaseorder.purchaseorderitems_data[orderItem].PRV_name,
                '<button type="button" class="btn btn-danger remove" disabled = true>Remove</button>'
              ]).draw();
             }
             purchaseOrderItems.draw();
             $('#addpurchaseorderitem').attr('disabled',false);
             $('#finish').attr('disabled',false);
             $('#purchaseordername').val(value);
             $('#purchaseordernamex').val(value);
            }                 
             
              ///////////////////////////////////////////////////////////////
             
          },
          error: function(jqXHR, textStatus, errorThrown) {
             

          }


      });
    }
}

$(document).ready(function(){
  $('#addpurchaseorderitem').attr('disabled',true);
  $('#finish').attr('disabled',true);
  //get customers
  $('#client').on('change', function(){
    var client = $(this).find("option:selected").val();
    getClientProviders(client);
    getClientSKUs(client); 
  });
  //get purchase order data
  $('#purchaseorder').on('change', function() {
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
    var value = this.value;
    if(value == 'New Purchase Order')
    {
      $('#addpurchaseorderitem').attr('disabled',true);
      $('#finish').attr('disabled',true);
    }
    getPurchaseOrderData(value);

  });
  $("#savepurchaseorder").submit(function(e) {

     $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
     var sentdata = $("#savepurchaseorder").serialize();
     if($('#savepurchaseorder').valid())
     {
       var purchaseordervalue = $('#purchaseorder').val();
       $.ajax({
            url: "save",
            type: "post",
            data: sentdata ,
            success: function (response) {
               if(response.error == true)
              {
                var list = "<ul>";
                for(var x=0 ;x<response.message.length;x++)
                {
                  list += '<li>'+response.message[x]+'</li>';
                }
                list += "</ul>";
                document.getElementById('message').innerHTML=
                '<div class="alert alert-warning" role="alert">'+
                    '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                    +'<strong>'+list+'</strong>'
                +'</div>';
              }
               else
               {
                document.getElementById('message').innerHTML=
                        '<div class="alert alert-success" role="alert">'+
                            '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                            +'<strong>Well done!</strong>"'+response.message+'.'
                        +'</div>';
                  if(response.state == 'new')
                  {
                      $('#purchaseorder').append($('<option>', {
                          value: response.purchaseorder,
                          text: response.purchaseorder
                      }));
                      $('#purchaseorder').selectpicker('refresh');              
                      $('#purchaseorder').selectpicker('val',response.purchaseorder);
                  }
                  else if(response.state == 'old')
                  {
                    $('#purchaseorder').selectpicker('val',purchaseordervalue);
                  }
                  $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                if(response.state == 'new')
                  getPurchaseOrderData(response.purchaseorder);
                else if(response.state == 'old')
                  getPurchaseOrderData(purchaseordervalue);
               }
            },
            error: function(jqXHR, textStatus, errorThrown) {
               
            }


        });
    }
      e.preventDefault();
  });

  $("#savepurchaseorderitem").submit(function(e) {

     $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
     var sentdata = $("#savepurchaseorderitem").serialize();
     if($('#savepurchaseorderitem').valid())
     {
       $.ajax({
            url: "saveitem",
            type: "post",
            data: sentdata ,
            success: function (response) {
               if(response.error == true)
            {
              var list = "<ul>";
              for(var x=0 ;x<response.message.length;x++)
              {
                list += '<li>'+response.message[x]+'</li>';
              }
              list += "</ul>";
              document.getElementById('message-step-2').innerHTML=
              '<div class="alert alert-warning" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                  +'<strong>'+list+'</strong>'
              +'</div>';
            }
               else
               {
                document.getElementById('message-step-2').innerHTML=
                        '<div class="alert alert-success" role="alert">'+
                            '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                            +'<strong>Well done!</strong>"'+response.message+'.'
                        +'</div>';
                var purchaseorderitems = $('#assigned-purchaseorderitems-table').DataTable();
                purchaseorderitems.row.add
                ([
                  response.purchaseitem[0].POI_number,
                  $('#sku').val(),
                  $('#amount').val(),
                  $('#provider').val(),
                  '<button type="button" class="btn btn-danger remove" disabled= true>Remove</button>'
                ]).draw();
               }
            },
            error: function(jqXHR, textStatus, errorThrown) {
               
            }


        });
    }
      e.preventDefault();
  });
  $(document).on("click",".remove",function(e)
    {
      var purchaseorderitems = $('#assigned-purchaseorderitems-table').DataTable();
      var oTableApi = $('#assigned-purchaseorderitems-table').dataTable().api();
      var tr = $(this).closest('tr');
      cell1 = tr.find("td:first");
      var purchaseItem = oTableApi.cell(cell1).data();
      $.ajax({
          url: "removeitem",
          type: "post",
          data: {'purchaseorder':$("#purchaseordername").val(),
                 'purchaseorderitem':purchaseItem
                } ,
          success: function (response) {
              purchaseorderitems.row(tr).remove().draw();
          },
          error: function(jqXHR, textStatus, errorThrown) {
             

          }
      });
      
    });
    

});