<form role="form" class="form-horizontal" id="savepurchaseorderitem" method="post">
    {!! csrf_field() !!}
    <input type="hidden" name="purchaseordername" id="purchaseordername"></input>
    <div class="row" style="margin-bottom: 20px">
      <div class="col-md-6">
        <div class="form-group">
          <label class="col-md-5 control-label">SKU</label>
          <div class="col-md-7">                                        
            <select class="form-control selectpicker show-tick" name = "sku" title="Pick Something Please!" id = "sku" data-live-search="true" required>
            <!-- SKU FOR EACH -->
            </select>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
            <label class="col-md-5 control-label">Amount</label>
              <div class="col-md-7">
                  <input type="number" class="form-control" name = "amount" id="amount" required/>
              </div>
        </div>
      </div>
      </div>
      <div class="row" style="margin-bottom: 20px">
        <div class="col-md-6">
        <div class="form-group">
          <label class="col-md-5 control-label">Provider</label>
          <div class="col-md-7">                                        
            <select class="form-control selectpicker show-tick" name = "provider" title="Pick Something Please!" id = "provider" data-live-search="true">
            <!-- Provider FOR EACH -->
            </select>
          </div>
        </div>
      </div>
    </div>

    <div class="row" style="margin-bottom:10px;margin-top:10px;">
      <div class="col-md-5" id="message">

      </div>
      <div class="col-md-offset-6 col-md-1">
      <button id="addpurchaseorderitem" type="submit" value= "Submit" class="btn btn-success pull-right">Add</button>
      </div>
    </div>
</form>

<div class = 'row'>
  <div class="col-md-8" id="message-step-2">

    </div>
</div>

<div class="row" id="assigned-purchaseorderitems">
  <div class="col-md-12">
            <div class="panel panel-warning" id= "assigned-purchaseorderitems-panel">
                 <div class="panel-heading">
                      <h3 class="panel-title" id = "assigned-purchaseorderitems-title">No Purchase Order Selected</h3>                               
                      <ul class="panel-controls">
                          <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                           <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                       </ul>                                               
                   </div>
                    <div class="panel-body " >
                    <table class="table  datatable maintable" id = "assigned-purchaseorderitems-table">
                            <thead>
                                <tr>
                                  <th>Purchase Order Item</th>
                                  <th>SKU</th>
                                  <th>Amount</th>
                                  <th>Provider</th>
                                  <th>Remove</th>
                                </tr>                
                             </thead>
                             <div class = "scroll">
                               <tbody>             
                               </tbody>
                               </div>
                         </table>                                        
                     </div>
             </div>
    </div>
  </div>
  <form role="form" class="form-horizontal" id="finishpurchaseorder" method="post" action="{{route('finishpurchaseorder')}}">
  {!! csrf_field() !!}
  <div class="row" style="margin-bottom:10px;margin-top:10px;" >
    <input type="hidden" name="purchaseordernamex" id="purchaseordernamex"></input>
      <button  type="submit" value= "Submit" class="btn btn-success pull-right" id="finish">Finish</button>
  </div>
  </form>