@extends('main&templates.MainTemplateMasterdata')

@section('content')
     <div class="panel panel-default" id= "data-panel">
    <div class="panel-heading">
        <h3 class="panel-title" id = "table-title">Storage Job {{$STR_number}}
            <button class = "pulse-button popover-dismiss"  data-content="Assin Locations To Each Unitload" data-placement="bottom"></button>
        </h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>
    </div>

    <!-- Creation Body -->
    <!-- Khaled M.Fathy-->

    <div class="panel-body">
        <button onclick="bootstro.start('.bootstro');" class="btn btn-danger">Start Tour</button>
        <form role="form" class="form-horizontal"  id="storagejob-form">
            {!! csrf_field() !!}
            <br/>
            <div class="row">

                <div class="col-md-4">
                    <div class="form-group">
                        <div class="bootstro" data-bootstro-title="Strategy"
                             data-bootstro-content="Choose Storage Strategy That Will Specify The Unitloads and
                             Their Available Storage Locations"
                             data-bootstro-placement="right" data-bootstro-width="400px">
                        <label class="col-md-4 control-label">Strategy</label>
                        <div class="col-md-10">
                            <select name="STRAT_name" id="strategy_name" class="form-control selectpicker show-tick"
                                    title="Choose Strategy" form="storagejob-form">
                            </select>
                        </div>
                            </div>
                    </div>
                </div>

            </div>
            <br/>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="bootstro" data-bootstro-title="Unitload"
                             data-bootstro-content="Choose Unitload You Want To Store"
                             data-bootstro-placement="right" data-bootstro-width="400px">
                        <label class="col-md-4 control-label">Unitload</label>
                        <div class="col-md-10">
                            <select class="form-control selectpicker show-tick" name="UL_name" id="UL_name"
                                    title="Choose Unitload" form="storagejob-form">
                            </select>
                        </div>
                    </div>
                        </div>
                </div>
            </div>
            <br/>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="bootstro" data-bootstro-title="Site"
                             data-bootstro-content="Choose Site You Want To Store the Unitload In"
                             data-bootstro-placement="right" data-bootstro-width="400px">
                        <label class="col-md-4 control-label">Site</label>
                        <div class="col-md-10">
                            <select class="form-control selectpicker show-tick" name="SITE_name" id="SITE_name"
                                    title="Choose Site" form="storagejob-form">
                            </select>
                        </div>
                    </div>
                        </div>
                </div>
            </div>
            <br/>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="bootstro" data-bootstro-title="Zone"
                             data-bootstro-content="Choose Zone You Want To Store the Unitload In"
                             data-bootstro-placement="right" data-bootstro-width="400px">
                        <label class="col-md-4 control-label">Zone</label>
                        <div class="col-md-10">
                            <select class="form-control selectpicker show-tick" name="ZONE_name" id="ZONE_name"
                                    title="Choose Zone" form="storagejob-form">
                            </select>
                        </div>
                    </div>
                        </div>
                </div>
            </div>
            <br/>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="bootstro" data-bootstro-title=""
                             data-bootstro-content="Choose Rack You Want To Store the Unitload in"
                             data-bootstro-placement="right" data-bootstro-width="400px">
                        <label class="col-md-4 control-label">Rack</label>
                        <div class="col-md-10">
                            <select class="form-control selectpicker show-tick" name="RACK_name" id="RACK_name"
                                    title="Choose Rack" form="storagejob-form">
                            </select>
                        </div>
                    </div>
                        </div>
                </div>
            </div>
            <br/>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="bootstro" data-bootstro-title="Storage Location"
                             data-bootstro-content="Choose Storage Location Where You'll Store The Selected Unitload"
                             data-bootstro-placement="right" data-bootstro-width="400px">
                        <label class="col-md-4 control-label">Location</label>
                        <div class="col-md-10">
                            <select class="form-control selectpicker show-tick" name="STL_name" id="STL_name"
                                    title="Choose Location" form="storagejob-form">
                            </select>
                        </div>
                    </div>
                        </div>
                </div>

                <input type="text" style="display:none;" name="STJ_name" id="STJ_name" form="storagejob-form">

            </div>
             <div class="row" style="margin-bottom:10px;margin-top:10px;">
                <div class="col-md-5" id="message">
                </div>
            </div>

            <div class="panel-footer">
                <button type="submit" value= "Submit" class="btn btn-primary pull-right">Submit</button>
            </div>
        </form>
    </div>
    <div class="message-box message-box-success animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-check"></span>Success</div>
                    <div class="mb-content">
                        <h4 style="color:white;">Storage job Successfully submitted In-Bound operation done</h4>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>
    </div>
    <script type="text/javascript" src={{("../js/GoodsIn/storagejob.js")}}></script>

    <script type="text/javascript" src="{{asset('js/plugins/tour/bootstrap-tour.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/tour/bootstro.min.js')}}"></script>
    

@stop