@extends('main&templates.tables_layout')
@section('actionOfUpdate')
    /purchaseorder/update
@stop
@section('actionOfDelete')
    /purchaseorder/delete
@stop
@section('table-title')
    Create Purchase Order
@stop
@section('info-button')
    <button class = "pulse-button popover-dismiss"  data-content="Order To Move Products From Warehouse To Customer" data-placement="bottom"></button>
@stop


@section('message-body')
    @if(session()->has('message'))

        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span>{{session('messagetype')}}</div>
                    <div class="mb-content">
                        <p>{{session('message')}}</p>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif

    @if (count($errors) > 0)
        <div class="alert alert-danger  alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@stop
@section('table-body')


        <div class="panel-heading">
            <h3 class="panel-title" id = "table-title">Create New Purchase Order</h3>
            <ul class="panel-controls">
                <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
            </ul>
        </div>

        <!-- Creation Body -->
        <!-- Khaled M.Fathy-->

        <div class="panel-body">
            <form role="form" class="form-horizontal"  action="{{route('PurchaseOrderCreate')}}" method="post" id="purchaseorder-form">
                {!! csrf_field() !!}
            <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="col-md-4 control-label">Date of Delivery:</label>
                <div class="col-md-6">
                    <div class="input-group date" id="dp-2" data-date="{{$date}}" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control datepicker" value="{{$date}}" id="from" name="PO_dateOfDelivery"/>
                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label class="col-md-2 control-label">Client</label>
                <div class="col-md-10">
                    <select name="CLI_name" class="form-control selectpicker show-tick" form="purchaseorder-form">
                        @foreach ($client_names as $client)
                            <option>{{$client}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>


        <div class="col-md-4">
            <div class="form-group">
                <label class="col-md-2 control-label">Site</label>
                <div class="col-md-10">
                    <select class="form-control selectpicker show-tick" name="SITE_name" form="purchaseorder-form">
                        @foreach ($site_names as $site)
                            <option>{{$site}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

            </div>

        {{--<button type="submit" value= "Submit" class="btn btn-primary pull-right" style="margin-right:20px;--}}
                    {{--margin-top:20px;">Submit</button>--}}
        <div class="panel-footer">
            <button type="submit" value= "Submit" class="btn btn-primary pull-right">Submit</button>
        </div>
    </form>
        </div>

@stop


