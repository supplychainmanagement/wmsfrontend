@extends('main&templates.tables_layout')
@section('actionOfUpdate')
    purchaseorder/update
@stop

@section('actionOfDelete')
    purchaseorder/delete
@stop

@section('table-title')
    Purchase Orders
@stop

@section('table_id')
"purchaseordertable"
@stop 

@section('table_data_val'){{$PO_number}}@stop


@section('table-head')
    <tr>
        <th style="display:none";>Purchase Order ID</th>
        <th>Purchase Order Number</th>
        <th>Client</th>
        <th>Site</th>
        <th>Date of Delivery</th>
        <th>Print</th>
    </tr>
@stop

@section('table-body')

    @foreach($purchaseOrders as $purchaseOrder)
        <tr>
            <td style="display:none;">{{$purchaseOrder['purchaseorder']['id']}}</td>
            <td><a href="{{route('purchaseorderitem',array('search'=>$purchaseOrder['purchaseorder']['PO_number']))}}">{{$purchaseOrder['purchaseorder']['PO_number']}}</a></td>
            <td><a href="{{route('client',array('search'=>$purchaseOrder['client_name']))}}">{{$purchaseOrder['client_name']}}</a></td>
            <td><a href="{{route('site',array('search'=>$purchaseOrder['site_name']))}}">{{$purchaseOrder['site_name']}}</a></td>
            <td>{{$purchaseOrder['purchaseorder']['PO_dateOfDelivery']}}</td>
            <td><a class ="btn btn-success" href="{{route('purchaseOrderPdf',array('PO_number'=>$purchaseOrder['purchaseorder']['PO_number']))}}"
                   role="button">Print</a></td>
        </tr>
    @endforeach

@stop
@section('message-body')
    @if(session()->has('message'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                        <p>{{session('message')}}</p>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif
@stop



@section('Jsplugin')
    <script>
        $('.maintable').dataTable();
        $('#goodsinlink').addClass('active');
        $('#purchaseorderlink').addClass('active');
        // $(document).ready(function(){
        //     var pathname = window.location.href;
        //     var position = pathname.indexOf("?");
        //     if(position != -1)
        //     {
        //         var getvalues = pathname.substring(position+1,pathname.length);
        //         var splited = getvalues.split('=');
        //         console.log(splited[0]);
        //         if(splited.length == 2)
        //         {
        //             if(splited[0] == 'search')
        //             {
        //                  $('.maintable').DataTable().search(splited[1]).draw();
        //                  console.log('i am here');
        //             }
        //         } 
        //     }
        // });
        </script> 
        @if($PO_number != null)
        <script>
            $(document).ready(function(){
                var temp = $('#purchaseordertable').DataTable();
                var data_search= $('#purchaseordertable').data('myval');
                temp.search(data_search).draw();
            });
        </script>
    @endif
@stop
