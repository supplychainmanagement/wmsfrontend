@extends('main&templates.tables_layout')
@section('actionOfUpdate')
    
@stop

@section('actionOfDelete')
    
@stop

@section('table-title')
    Recieving Jobs
@stop


@section('table-head')
    <tr>
        <th style="display:none";>Recieving Job Id</th>
        <th>Recieving Job Number</th>
        <th>Receiving Job Amount</th>
        <th>Purchase Order item</th>
        <th>Site</th>
    </tr>
@stop

@section('table-body')

    @foreach($receivingJobs as $receivingJob)
        <tr>
            <td style="display:none;">{{$receivingJob['receivingjob']['id']}}</td>
            <td>{{$receivingJob['receivingjob']['RCJ_number']}}</td>
            <td>{{$receivingJob['receivingjob']['RCJ_amount']}}</td>
            <td><a href="{{route('purchaseorderitem',array('search'=>$receivingJob['POI_name']))}}">{{$receivingJob['POI_name']}}</a></td>
            <td><a href="{{route('site',array('search'=>$receivingJob['SITE_name']))}}">{{$receivingJob['SITE_name']}}</a></td>
        </tr>
    @endforeach

@stop

@section('message-body')
    @if(session()->has('message'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                        <p>{{session('message')}}</p>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif
@stop



@section('Jsplugin')
<script>
$('.maintable').dataTable();
        $('#goodsinlink').addClass('active');
        $('#receivingjoblink').addClass('active');$(document).ready(function(){
            var pathname = window.location.href;
            var position = pathname.indexOf("?");
            if(position != -1)
            {
                var getvalues = pathname.substring(position+1,pathname.length);
                var splited = getvalues.split('=');
                console.log(splited[0]);
                if(splited.length == 2)
                {
                    if(splited[0] == 'search')
                    {
                         $('.maintable').DataTable().search(splited[1]).draw();
                         console.log('i am here');
                    }
                } 
            }
        }); 
    </script>
@stop