<?php

namespace Modules\Strategies\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class StrategiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function index()
    {
        $url = $this->url.'/strategy';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        if($status_code == 200||$status_code==204)
        {
            return View('Strategy.strategy')->with('strategies',$body);
        }else{
            return redirect('/login');
        }
    }
}
