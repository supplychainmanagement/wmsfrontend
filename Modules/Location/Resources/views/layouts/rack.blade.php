@extends('main&templates.tables_layout')
@section('actionOfUpdate')
rack/update
@stop

@section('actionOfDelete')
rack/delete
@stop


@section('table-head')
    <tr>
        <th style="display:none";>Rack ID</th>
        <th>Rack Name</th>
        <th>Zone Name</th>
        <th>No.Rows</th>
        <th>No.Columns</th>
        <th>Depth</th>
    </tr>
@stop

@section('table-body')

    @foreach($racks as $rack)
        <tr>
            <td style="display:none;">{{$rack['id']}}</td>
            <td>{{$rack['rack_name']}}</td>
            <td>{{$rack['zone_name']}}</td>
            <td>{{$rack['row']}}</td>
            <td>{{$rack['column']}}</td>
            <td>{{$rack['depth']}}</td>
        </tr>
    @endforeach

@stop

@section('prop-head')

    <th>Property</th>
    <th>Value</th>
@stop

@section('message-body')
    @if(session()->has('message'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                        <ul>
                            @foreach(session('message') as $mess)
                            <li>{{$mess}}</li>
                            @endforeach
                        </ul>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif
@stop
@if(session('system_client') == true)
@if(session('user_role') == 'system_admin'  || session('user_role') == 'site_admin')
@section('create-panel')

  <div id="data"class="col-md-12">

            <div class="panel panel-warning" id= "data-panel">
              <form role="form" class="form-horizontal"  action="{{route('rack.formcreate')}}" method="post" id="wmszone-form">
              {!! csrf_field() !!}
                 <div class="panel-heading">                                
                    <h3 class="panel-title" id = "table-title">Create New Rack</h3>
                      <ul class="panel-controls">
                          <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                          <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                           <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                       </ul>                                               
                </div>
                <div class="panel-body"> 
                      
                      <div class="row">

                      <div class="col-md-4">
                          <div class="form-group">
                            <label class="col-md-5 control-label">Name</label>
                            <div class="col-md-7">                                        
                              <input type="text" class="form-control" name = "rack_name" required/>
                            </div>
                          </div>
                      </div>
                      @if(session('user_role') == 'system_admin')
                      <div class="col-md-4">
                          <div class="form-group">
                            <label class="col-md-5 control-label">Site</label>
                            <div class="col-md-7">                                        
                              <select class="form-control selectpicker show-tick" title = "please pick something" name = "SITE_name" id ="site" required>

                                       @foreach ($sites as $site)
                                          <option>{{$site}}</option>
                                        @endforeach
                                    </select>
                            </div>
                          </div>
                      </div>
                      
                      @endif
                      <div class="col-md-4">
                          <div class="form-group">
                            <label class="col-md-5 control-label">Zone Name</label>
                            <div class="col-md-7">                                        
                              <select class="form-control selectpicker show-tick"  title = "please pick something" name = "zone_name" id = "zone" required>
                                        @if(session('user_role') == 'site_admin')
                                          @foreach ($zones as $zone)
                                            <option>{{$zone}}</option>
                                          @endforeach
                                        @endif
                                    </select>
                            </div>
                          </div>
                      </div>
                      
                    
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <label class="col-md-5 control-label">No.Rows</label>
                            <div class="col-md-7">                                        
                              <input type="text" class="form-control" name = "row" required/>
                            </div>
                          </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group">
                            <label class="col-md-5 control-label">No.Columns</label>
                            <div class="col-md-7">                                        
                              <input type="text" class="form-control" name = "column" required/>
                            </div>
                          </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group">
                            <label class="col-md-5 control-label">Depth</label>
                            <div class="col-md-7">                                        
                              <input type="text" class="form-control" name = "depth" required/>
                            </div>
                          </div>
                      </div>
                      
                    
                    </div>
                    </br>
                      <div class="row">

                      <div class="col-md-4">
                          <div class="form-group">
                            <label class="col-md-5 control-label">Functional Area Name</label>
                            <div class="col-md-7">                                        
                              <select class="form-control selectpicker show-tick" name = "functionalarea_name" required>
                                        @foreach ($functionalareas as $functionalarea)
                                          <option>{{$functionalarea}}</option>
                                        @endforeach
                                    </select>
                            </div>
                          </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group">
                            <label class="col-md-5 control-label">Storage Location Type Name</label>
                            <div class="col-md-7">                                        
                              <select class="form-control selectpicker show-tick" name = "storagelocationtype_name" required>
                                        @foreach ($storagelocationtypes as $storagelocationtype)
                                          <option>{{$storagelocationtype}}</option>
                                        @endforeach
                                    </select>
                            </div>
                          </div>
                      </div>
                    
                    </div>
                    </div>                                
                     <div class="panel-footer">
                        <button type="submit" value= "Submit" class="btn btn-primary pull-right">Submit</button>
                     </div>
                     </form>
              </div>
           
    </div>

    @stop
    
  @endif
  @endif


@section('Jsplugin')
    <script type="text/javascript" src="js/masterdata/rack.js"></script>
@stop                                      

