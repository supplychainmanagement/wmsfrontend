@extends('main&templates.tables_layout')
@section('actionOfUpdate')
    site/update
@stop

@section('actionOfDelete')
    site/delete
@stop

@section('table-title')
    Sites
@stop


@section('table-head')
    <tr>
        <th style="display:none";>Site ID</th>
        <th>Site Name</th>
    </tr>
@stop

@section('table-body')

    @foreach($sites as $site)
        <tr>
            <td style="display:none;">{{$site['id']}}</td>
            <td>{{$site['SITE_name']}}</td>

        </tr>
    @endforeach

@stop

@section('prop-head')

    <th>Property</th>
    <th>Value</th>
@stop


@section('prop-body')


@stop
@section('message-body')
    @if(session()->has('messages'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                        <ul>
                            @foreach(session('messages') as $message)
                            <li>{{$message}}</li>
                            @endforeach
                        </ul>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif
@stop

@if(session('system_client') == true)
    @if(session('user_role') == 'system_admin')
    @section('create-panel')

    <div id="data"class="col-md-12">

        <div class="panel panel-warning" id= "data-panel">
            <form role="form" class="form-horizontal"  action="{{route('siteCreate')}}" method="post" id="site-form">
                {!! csrf_field() !!}
                <div class="panel-heading">
                    <h3 class="panel-title" id = "table-title">Create New Site</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">

                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Name</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name = "SITE_name" required/>
                                </div>
                            </div>
                        </div>

                        @if($systemClient == true)
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Client</label>
                                    <div class="col-md-10">

                                            @foreach ($clients as $client)
                                                <input type="checkbox" name="SITE_client[]" value="{{$client['CLI_name']}}">
                                                {{$client['CLI_name']}}<br>
                                            @endforeach

                                    </div>
                                </div>
                            </div>
                        @endif


                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" value= "Submit" class="btn btn-primary pull-right">Submit</button>
                </div>
            </form>
        </div>

    </div>

@stop

@endif
@endif


@section('Jsplugin')
    <script type="text/javascript" src="js/masterdata/site.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
            var pathname = window.location.href;
            var position = pathname.indexOf("?");
            if(position != -1)
            {
                var getvalues = pathname.substring(position+1,pathname.length);
                var splited = getvalues.split('=');
                console.log(splited[0]);
                if(splited.length == 2)
                {
                    if(splited[0] == 'search')
                    {
                         $('.maintable').DataTable().search(splited[1]).draw();
                         console.log('i am here');
                    }
                } 
            }
        }); 
    </script>
@stop                                      

