$(document).ready(function(){
    $('#locationlink').addClass('active');
    $('#sitelink').addClass('active');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    oTable = $('.maintable').dataTable();
    OTable =$('.maintable').DataTable();
    oTable.$('tr').click( function () {
        $("#data").removeClass('col-md-12');
        $("#data").addClass('col-md-8');
        OTable.rows( '.selected' ).nodes().to$() .removeClass( 'selected' );
        $(this).addClass('selected');
        $("#prop").removeClass("hidden").addClass("show");
        var title=$(this).find("td:nth-child(2)").html();
        $("#prop-title").text(title+' '+'properties');
        var title=$(this).find("td:nth-child(1)").html();
        $.post("site",{id:title}, function(data, status){


            var table = document.getElementById("prop-table");
            var element2 = document.createElement("input");
            element2.type = "hidden";
            element2.name = "id";
            element2.value = data['site']['id'];
            table.appendChild(element2);
            $("#prop-table").find("tr:gt(0)").remove();



            var row = table.insertRow(1);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Name";
            var element1 = document.createElement("input");
            element1.setAttribute('class','form-control');
            element1.type = "text";
            element1.name = "SITE_name";
            element1.value=data['site']['SITE_name'];
            cell2.appendChild(element1);
            if(!data['admin'] && !data['site'])
            {
                element1.readOnly = true;
            }

           

            var row = table.insertRow(2);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Client";
            if(data['admin'] || data['site'])
            {
                //var element1 = document.createElement("select");
                //element1.name = "clients";
                //element1.setAttribute('class','form-control selectpicker show-tick');
                //cell2.appendChild(element1);
                var arrtypes = data['clients'];
                arrtypes.forEach(function(ind){
                    var option = document.createElement("input");
                    option.type = 'checkbox';
                    option.value = ind;
                    option.name = 'CLI_name[]';
                    var attached = data['attached'];
                    attached.forEach(function(client){
                        
                        if(option.value == client)option.checked=true;
                    });
                    cell2.appendChild(option);
                    cell2.appendChild(document.createTextNode(ind));
                    cell2.appendChild(document.createElement('br'));

                });
            }
            else
            {
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "site_client";
                element1.readOnly = true;
                element1.value=data['clientArray'][0];
                cell2.appendChild(element1);
            }



            document.getElementById("prop").style.maxHeight = document.getElementById("data").style.height
            if(!data['admin'] && !data['site'])
            {
                $("#zonesave").removeClass("show").addClass("hidden");
                $("#zonedelete").removeClass("show").addClass("hidden");
            }


        });


    });

    $('.prop-remove').click(function(){
        $("#data").removeClass('col-md-8').addClass('col-md-12');
        $("#prop").removeClass("show").addClass("hidden");
        $("tr").removeClass('selected');
    });

    $('#create').click(function(){
        $("#create-panel").removeClass("hidden").addClass("show");
    });

});

// var jvalidate = $("#wmszone-form").validate({
//     ignore: [],
//     rules: {                                            
//         name:
//         {
//             required: true
//         }
//     }                                        
// });   


