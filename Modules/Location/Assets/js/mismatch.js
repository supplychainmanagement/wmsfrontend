
$(document).ready(function(){
    $('#unitloadslink').addClass('active');
    $('#mismatchlink').addClass('active');
   $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
   oTable = $('.maintable').dataTable();
OTable =$('.maintable').DataTable();
  oTable.$('tr').click( function () {
        $("#data").removeClass('col-md-12');
        $("#data").addClass('col-md-8');
        OTable.rows( '.selected' ).nodes().to$() .removeClass( 'selected' );
        $(this).addClass('selected');
        $("#prop").removeClass("hidden").addClass("show");
        var title=$(this).find("td:nth-child(2)").html();
        $("#prop-title").text(title+' '+'properties');
        var title=$(this).find("td:nth-child(1)").html();

        var firstunitloadtypeName = $(this).find("td:nth-child(2)").html();
        var secondunitloadtypeName = $(this).find("td:nth-child(3)").html(); 
        var storagelocationtypeName = $(this).find("td:nth-child(4)").html();  
        var created_at = $(this).find("td:nth-child(5)").html(); 
        var updated_at = $(this).find("td:nth-child(6)").html();  

        $.get("getrole", function(data, status){


            var table = document.getElementById("prop-table");
            var element2 = document.createElement("input");
            element2.type = "hidden";
            element2.name = "id";
           element2.value = title;
            table.appendChild(element2);
            $("#prop-table").find("tr:gt(0)").remove();


            var row = table.insertRow(1);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "First Unitload Type Name";
            var element1 = document.createElement("input"); 
            element1.setAttribute('class','form-control');           
            element1.type = "text";
            element1.name = "first_unitloadtype_name";
            element1.value=firstunitloadtypeName;
            cell2.appendChild(element1);
            element1.readOnly = true;

            var row = table.insertRow(2);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Second Unitload Type Name";
            var element1 = document.createElement("input"); 
            element1.setAttribute('class','form-control');           
            element1.type = "text";
            element1.name = "second_unitloadtype_name";
            element1.value=secondunitloadtypeName;
            cell2.appendChild(element1);
            element1.readOnly = true;  

            var row = table.insertRow(3);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Storage Location Type Name";
            var element1 = document.createElement("input"); 
            element1.setAttribute('class','form-control');           
            element1.type = "text";
            element1.name = "storagelocationtype_name";
            element1.value=storagelocationtypeName;
            cell2.appendChild(element1);
            element1.readOnly = true; 
          


            document.getElementById("prop").style.maxHeight = document.getElementById("data").style.height
            if(data['role'] == 'labor')
            {
                $("#zonesave").removeClass("show").addClass("hidden");
                $("#zonedelete").removeClass("show").addClass("hidden");
            }
            
            
            });


 });

       $('.prop-remove').click(function(){
        $("#data").removeClass('col-md-8').addClass('col-md-12');
        $("#prop").removeClass("show").addClass("hidden");
        $("tr").removeClass('selected');
       });

       $('#create').click(function(){
        $("#create-panel").removeClass("hidden").addClass("show");
       });

});

// var jvalidate = $("#wmszone-form").validate({
//     ignore: [],
//     rules: {                                            
//         name:
//         {
//             required: true
//         }
//     }                                        
// });   


