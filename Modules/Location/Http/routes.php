<?php
$websitename = null;
$subdomain = null;
Route::group(array('domain' => $subdomain.'.'.$websitename), function() {
	Route::group(['middleware' => 'web', 'prefix' => 'location', 'namespace' => 'Modules\Location\Http\Controllers'], function()
	{
	    Route::group(['prefix' => '/site'], function () {
			Route::get('/' , ['as' => 'site' ,'uses' => 'SiteController@index']);

			Route::post('/' , 'SiteController@getprop');
			Route::post('/create' ,['as'=>'siteCreate', 'uses'=>'SiteController@create']);
			Route::post('/delete' , 'SiteController@delete');
			Route::post('/update' , array( 'uses' =>'SiteController@update', 'as' => 'formupdate'));
		});

		Route::group(['prefix' => '/zone'], function () {
			Route::get('/' , ['as' => 'zone' ,'uses' => 'ZoneController@index']);

			Route::post('/' , 'ZoneController@getprop');
			Route::post('/create',['as'=>'zoneCreate','uses'=>'ZoneController@create']);
			Route::post('/delete' , 'ZoneController@delete');
			Route::post('/update' , array( 'uses' =>'ZoneController@update', 'as' => 'formupdate'));
		});

		Route::group(['prefix' => '/rack'], function () {
			Route::get('/' , ['as' => 'rack' ,'uses' => 'RackController@index']);

			Route::post('/' , 'RackController@getprop');
			Route::post('/getzones','RackController@getZones');
			Route::post('/create' ,  array( 'uses' =>'RackController@create', 'as' => 'rack.formcreate'));
			Route::post('/delete' , array( 'uses' =>'RackController@delete', 'as' => 'rack.formupdelete'));
			Route::post('/update' , array( 'uses' =>'RackController@update', 'as' => 'rack.formupdate'));

		});

		Route::group(['prefix'=>'/storagelocation'],function(){
			Route::get('/',['as'=>'storagelocation','uses'=>'StorageLocationController@index']);

			Route::post('/','StorageLocationController@getprop');
			Route::post('/create',['as'=>'STLCreate','uses'=>'StorageLocationController@create']);
			Route::post('/delete','StorageLocationController@delete');
			Route::post('/update',['as'=>'forumupdate','uses'=>'StorageLocationController@update']);
			Route::post('/getzonebysitename','StorageLocationController@getZoneBySiteName');
			Route::post('/getrackbyzonename','StorageLocationController@getRackByZoneName');
			Route::get('/barcodeimage',['as'=>'STLbarcodeimage','uses'=>'StorageLocationController@barcodeImage']);
			Route::post('/storaglocationtable',['as'=>'storagelocationtableajax','uses'=>'StorageLocationController@storagLocationTable']);
		});

		Route::group(['prefix' => '/storagelocationtype'], function () {
			Route::get('/' , ['as' => 'storagelocationtype' ,'uses' => 'StorageLocationTypeController@index']);

			Route::post('/create' ,  array( 'uses' =>'StorageLocationTypeController@create', 'as' => 'storagelocationtype.formcreate'));
			Route::post('/update' , array( 'uses' =>'StorageLocationTypeController@update', 'as' => 'storagelocationtype.formupdate'));
			Route::post('/delete' , array( 'uses' =>'StorageLocationTypeController@delete', 'as' => 'storagelocationtype.formupdelete'));
		});

		Route::group(['prefix' => '/functionalarea'], function () {
			Route::get('/' , ['as' => 'functionalarea' ,'uses' => 'FunctionalAreaController@index']);
			Route::post('/create' ,  array( 'uses' =>'FunctionalAreaController@create', 'as' => 'functionalarea.formcreate'));
			Route::post('/delete' , array( 'uses' =>'FunctionalAreaController@delete', 'as' => 'functionalarea.formupdelete'));
			Route::post('/update' , array( 'uses' =>'FunctionalAreaController@update', 'as' => 'functionalarea.formupdate'));

		});

		Route::group(['prefix' => '/mismatch'], function () {
		Route::get('/' , ['as' => 'mismatch' ,'uses' => 'MismatchController@index']);
		Route::post('/create' ,  array( 'uses' =>'MismatchController@create', 'as' => 'mismatch.formcreate'));
		Route::post('/delete' , array( 'uses' =>'MismatchController@delete', 'as' => 'mismatch.formupdelete'));
		Route::post('/update' , array( 'uses' =>'MismatchController@update', 'as' => 'mismatch.formupdate'));

		});
		
		Route::group(['prefix' => '/capacitylocation'], function () {
			Route::get('/' , ['as' => 'capacitylocation' ,'uses' => 'CapacityLocationController@index']);
			Route::post('/create' ,  array( 'uses' =>'CapacityLocationController@create', 'as' => 'capacitylocation.formcreate'));
			Route::post('/delete' , array( 'uses' =>'CapacityLocationController@delete', 'as' => 'capacitylocation.formupdelete'));
			Route::post('/update' , array( 'uses' =>'CapacityLocationController@update', 'as' => 'capacitylocation.formupdate'));

		});
	});
});
