<?php

namespace Modules\Location\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class MismatchController extends Controller
{
  public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function index()
    {
        $url = $this->url . '/mismatches';
        $return_code = $this->return_code;
       $response = HttpRequest::get($url);
       $status_code = $response['info']['http_code'];
       if($status_code != $return_code['ok'] && $status_code != $return_code['no_content'])
       {
            return Redirect::route('login');
       }
       elseif($status_code == $return_code['no_content'])
       {
        return view('capacities.mismatches')->with('message','No Content')
        ->with('mismatches',array())->with('unitloadtypes',array())
        ->with('storagelocationtypes',array());
       }
       else
       {
        $mismatches = array();
        $body = $response['body'];
        $backendArray = json_decode($body, true);
        $counter = 0;
        foreach ($backendArray['Missmatch'] as $key => $value) {
            $mismatches[$counter]['id'] = $value['missmatch']['id'];
            $mismatches[$counter]['first_unitloadtype_name'] = $value["MISSMATCH_ULT_Type_first"];
            $mismatches[$counter]['second_unitloadtype_name'] =$value["MISSMATCH_ULT_Type_second"];
            $mismatches[$counter]['storagelocationtype_name'] = $value["MISSMATCH_STL_Type"];
            $mismatches[$counter]['created_at'] = $value['missmatch']['created_at'];
            $mismatches[$counter]['updated_at'] = $value['missmatch']['updated_at'];
            $counter++;

        }

        $unitloadtypes = array();
        $counter = 0;
        foreach ($backendArray['ULT'] as $key => $value) {
            $unitloadtypes[$counter] = $value['ULT_Type_name'];
            $counter++;
        }

        $storagelocationtypes = array();
        $counter = 0;
        foreach ($backendArray['STLT'] as $key => $value) {
            $storagelocationtypes[$counter] = $value['STL_Type_name'];
            $counter++;
        }



        return view('capacities.mismatches')->with('mismatches',$mismatches)
        ->with('unitloadtypes',$unitloadtypes)
        ->with('storagelocationtypes',$storagelocationtypes);
       }
    }

    public function create(Request $request)
    {
        $fields = array();
        $fields['MISSMATCH_ULT_Type_name_first'] = $request->first_unitloadtype_name;
        $fields['MISSMATCH_ULT_Type_name_second'] = $request->second_unitloadtype_name;
        $fields['MISSMATCH_STL_Type_name'] = $request->storagelocationtype_name;

        if($request->first_unitloadtype_name == $request->second_unitloadtype_name)
        {
            return redirect('/mismatches') 
                    -> with('message', 'The Two Selected Unitloads Must not be the Same')->with('messagetype','danger');
        }

        $url = $this->url . '/mismatches/create'; 
        $return_code = $this->return_code;
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        if($statusCode == $return_code['bad_request'])
        {
            return redirect('/mismatches') 
                    -> with('message', $body['errors'])->with('messagetype','danger');
        }
        elseif($statusCode == $return_code['internal_server_error'])
        {
            return redirect('/mismatches') 
                    -> with('message', $body)->with('messagetype','danger');
        }  
        elseif($statusCode == $return_code['created'])
        {
            return redirect('/mismatches') 
                    -> with('message', $body)->with('messagetype','success');
        }  
    }

    public function update(Request $request)
    {
        $fields = array();
        $fields['id'] = $request->id;
        $fields['ULT_Type_name_first'] = $request->first_unitloadtype_name;
        $fields['ULT_Type_name_second'] = $request->second_unitloadtype_name;
        $fields['STL_Type_name'] = $request->storagelocationtype_name;
    
        $url = $this->url . '/mismatches/update'; 
        $return_code = $this->return_code;  
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        if($statusCode == $return_code['bad_request'])
        {
            return redirect('/mismatches') 
                    -> with('message', $body['errors'])->with('messagetype','danger');
        }
        elseif($statusCode == $return_code['internal_server_error'])
        {
            return redirect('/mismatches') 
                    -> with('message', $body)->with('messagetype','danger');
        }  
        elseif($statusCode == $return_code['ok'])
        {
            return redirect('/mismatches') 
                    -> with('message', $body)->with('messagetype','success');
        }           
    }

    public function delete(Request $request)
    {
        $fields =array(
          'id'=>$request->id);
        $url = $this->url . '/mismatches/delete';  
        $return_code = $this->return_code; 
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        if($statusCode == $return_code['bad_request'])
        {
            return redirect('/mismatches') 
                    -> with('message', $body['errors'])->with('messagetype','danger');
        }
        elseif($statusCode == $return_code['internal_server_error'])
        {
            return redirect('/mismatches') 
                    -> with('message', $body)->with('messagetype','danger');
        }  
        elseif($statusCode == $return_code['ok'])
        {
            return redirect('/mismatches') 
                    -> with('message', $body)->with('messagetype','success');
        }       
    }
}
