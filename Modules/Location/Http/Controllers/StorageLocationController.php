<?php

namespace Modules\Location\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class StorageLocationController extends Controller{


    public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }

    public function index()
    {

        $url = $this->url .'/storagelocation';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        //return $response['body'];
        //$locations = $this->getLocations($response);
        $types = $this->getTypes($response);
        $sites = $this->getSites($response);
        $FAs = $this->getFunctionalArea($response);
        $locations= array();
        if($status_code == 200 || $status_code == 204)
        {
            return View('Locations.storagelocation')->with('locations',$locations)->with('types',$types)
                ->with('sites',$sites)->with('FAs',$FAs);
        }else{
            return redirect()->route('home');
        }
    }

    public function getLocations($response)
    {
        $locations = array();
        if($response['body']!=null)
        {
            $body = json_decode($response['body'],true);
            if($body['Locations']!=null)
            {
                foreach($body['Locations'] as $key => $location)
                {
                    $locations[$key] = $location;
                }
            }
        }
        return $locations;
    }
    public function getTypes($response)
    {
        $types = array();
        if($response['body']!=null)
        {
            $body = json_decode($response['body'],true);
            if($body['Types']!=null) {
                foreach ($body['Types'] as $key => $type) {
                    $types[$key] = $type;
                }
            }
        }
        return $types;
    }
    public function getSites($response)
    {
        $sites = array();
        if($response['body']!=null)
        {
            $body = json_decode($response['body'],true);
            if($body['Sites']!=null) {
                foreach ($body['Sites'] as $key => $site) {
                    $sites[$key] = $site;
                }
            }
        }
        return $sites;
    }
    public function getFunctionalArea($response)
    {
        $FAs = array();
        if($response['body']!=null)
        {
            $body = json_decode($response['body'],true);
            if($body['FunctionalAreas']!=null) {
                foreach ($body['FunctionalAreas'] as $key => $FA) {
                    $FAs[$key] = $FA;
                }
            }
        }
        return $FAs;
    }
    public function getZoneBySiteName(Request $request)
    {
        $url = $this->url.'/services/getzonebysitename';
        $field = $request->all();
        $response = HttpRequest::post($url,$field);
        $body = json_decode($response['body'],true);
        return $body;
    }

    public function getRackByZoneName(Request $request)
    {
        $url = $this->url.'/services/getrackbyzonename';
        $fields = $request->all();
        $response = HttpRequest::post($url,$fields);
        $body = json_decode($response['body'],true);
        return $body;
    }


    public function getprop(Request $request)
    {
        $url = $this->url.'/storagelocation/prop';
        $fields = array('id'=>$request->id);
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);

        if($status_code == 200)
        {
            return response($body);
        }else if($status_code == 204)
        {
            $dataFrontEnd = array();
            return $dataFrontEnd;
        }else{
            return redirect('/login');
        }
    }

    public function update(Request $request)
    {
        $url = $this->url .'/storagelocation/update';
        $fields = array(
            'id' => $request->id,
            'STL_name'=> $request->STL_name,
            'STL_Status' => $request->STL_Status,
            'STL_Rack' => $request->STL_Rack,
            'STL_FA' => $request->STL_FA,
            'STL_Type' => $request->STL_Type
        );
        //return $fields;
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        if($status_code == 200)return redirect('/storagelocation')->with('message',$body)
            ->with('messagetype','success');
        if($status_code == 400)return redirect('/storagelocation')->with('message',$body['errors'])->with('messagetype','danger');
        if($status_code == 500)return redirect('/storagelocation')->with('message',$body)->with('messagetype','danger');
    }

    public function create(Request $request)
    {
        $url = $this->url.'/storagelocation/create';
        $fields = array(
            'STL_name'=>$request->STL_name,
            'STL_FA'=>$request->STL_FA,
            'STL_RACK'=>$request->STL_RACK,
            'STL_Type'=>$request->STL_Type,
            'STL_SITE'=>$request->STL_SITE
        );
        $response = HttpRequest::post($url,$fields);
        $body = json_decode($response['body'],true);
        $status_code = $response['info']['http_code'];
        if($status_code == 400) return redirect('/storagelocation')->with('message',$body['errors'])->with('messagetype','danger');
        if($status_code == 201) return redirect('/storagelocation')->with('message',$body)
            ->with('messagetype','success');
        if($status_code == 500) return redirect('/storagelocation')->with('message',$body)
            ->with('messagetype','danger');
    }

    public function delete(Request $request)
    {
        $url = $this->url.'/storagelocation/delete';
        $fields = array('id'=>$request->id);
        $response = HttpRequest::post($url,$fields);
        $body = json_decode($response['body'],true);
        $status_code = $response['info']['http_code'];
        if($status_code == 200)return redirect('/storagelocation')->with('message',$body)->with('messagetype','success');;
        if($status_code == 400)return redirect('/storagelocation')->with('message',$body['errors'])->with('messagetype','danger');
        if($status_code == 500)return redirect('/storagelocation')->with('message',$body)->with('messagetype','danger');
    }
    public function barcodeImage(Request $request)
    {
        $this->CreatePDF($request);
        
    }
    public function CreatePDF($request)
    {
        PDF::setAuthor('me-wims');
        PDF::SetTitle($request['STL_name']);
        PDF::SetSubject('Storagelocation');
        PDF::SetMargins(15,10,10,true);
        $pagelayout = array(100,60);
        PDF::AddPage('L',$pagelayout,false,false);
        $style = array(
            'position' => 'C',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => false,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );
        PDF::write1DBarcode($request['STL_name'].$request['id'], 'C128', '', '', '', 18, 0.4, $style, '');
        PDF::writeHTML('<br/><br/><br/><br/><br/>',true,false,false,false,'R');
        PDF::writeHtml('<h3>'.$request['STL_name'].'</h3>',true,false,false,false,'C');
        PDF::Output($request['STL_name'].'.pdf');
    }
    public function storagLocationTable(Request $request)
    {
        //return $request;
        $url = $this->url.'/storagelocationtable';
        $fields = array(
            'limit'=>$request->iDisplayLength,
            'startfrom'=>$request->iDisplayStart,
            'search'=>$request->sSearch
        );
        $response = HttpRequest::post($url,$fields);
        $body = json_decode($response['body'],true);
        $status_code = $response['info']['http_code'];
        //return $response['body'];
        $output = array(
        "sEcho" => intval($request->sEcho),
        "iTotalRecords" => $body['paginator']['total_count'],
        "iTotalDisplayRecords" => $body['paginator']['total_count'],
        "aaData" => array()
        );
        foreach ($body['data']['Locations'] as $key => $location) {
            $row = array();
            $row[] = $location['id'];
            $row[] = $location['STL_name'];
            $row[] = $location['STL_FA'];
            $row[] = $location['STL_allocation'];
            $row[] = $location['STL_type'];
            $row[] = $location['STL_stocktakingDate'];
            $row[] = $location['STL_RACK'];
            //$row[] = $location['STL_serialNumber'];
            $output['aaData'][] = $row;
        }
        //return $output;
        echo json_encode($output);
    }

}