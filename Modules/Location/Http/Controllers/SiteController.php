<?php

namespace Modules\Location\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class SiteController extends Controller{
    public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function index()
    {
        $url = $this->url.'/site';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        //return $response['body'];
        $sites = $this->getSiteArray($response);
        $clients = $this->getClients($response);
        $permission = $this->getPermission($response);
        $role = $this->role($response);
        $siteBased = $this->siteBased($response);
        $systemClient = $this->systemClient($response);

        if($status_code == 200)
        {
            return view('Locations.site')->with('sites',$sites)
                ->with('clients',$clients)->with('permission',$permission)
                ->with('role',$role)->with('siteBased',$siteBased)
                ->with('systemClient',$systemClient);
        }else if($status_code == 204)
        {
            return view('Locations.site')->with('message','No Content')
                ->with('sites',$sites)->with('clients',$clients)
                ->with('permission',$permission)->with('role',$role)
                ->with('siteBased',$siteBased)->with('systemClient',$systemClient);
        }else {
            return Redirect::route('login');
        }
    }


    public function create(Request $request)
    {
        $site = array();
        $site['SITE_name'] = $request['SITE_name'];
        if($request->has('SITE_client'))
        {
            for($i = 0;$i<sizeof($request['SITE_client']);$i++)
            {
                $site['SITE_client'][$i] = $request['SITE_client'][$i];
            }
        }
        $url = $this->url.'/site/create';
        $response = Httprequest::post($url,$site);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if($status_code == 400)
        {
            return redirect('/site')
                ->with('messages',$body['errors'])->with('messagetype','danger');
        }elseif($status_code == 201)
        {
            return redirect('/site')->with('messages',$body)->with('messagetype','success');
        }else
        {
            return redirect('/site')->with('messages',$body)->with('messagetype','danger');
        }

    }

    public function getprop(Request $request)
    {
        $url = $this->url.'/siteprop';
        $id = $request['id'];
        $fields = array('id'=>$id);
        $response = HttpRequest::post($url,$fields);
        $status_code=$response['info']['http_code'];
        if($status_code == 200)
        {
            $body = $response['body'];
            $dataArray = json_decode($body,true);
            $clientArray = array();
            $counter = 0;
            foreach($dataArray['clients'] as $key=>$value)
            {
                $clientArray[$counter] = $value['CLI_name'];
                $counter++;
            }
            $dataFrontEnd = ['site' => $dataArray['sites'],'siteBased'=>$dataArray['site'],'admin'=>$dataArray['admin']
            ,'clients'=>$clientArray,'attached'=>$dataArray['attached']];
            return response($dataFrontEnd);
        }elseif($status_code == 204)
        {
            $body = array();
            return response($body);
        }else{
            redirect::route('login');
        }
    }


    public function delete(Request $request)
    {
        $fields = array('id'=>$request->id);
        $url = $this->url.'/site/delete';
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        //return $body;
        if($status_code == 400)return redirect('/site')->with('messages',$body['errors'])->with('messagetype','danger');
            elseif($status_code == 401)return redirect('/site')->with('messages',$body)->with('messagetype','danger');
        elseif($status_code == 200)return redirect('/site')->with('messages',$body)->with('messagetype','success');
        else return redirect('/site')->with('messages',$body)->with('messagetype','danger');
    }

    public function update(Request $request)
    {
        $fields = array(
            'id' => $request->id,
            'SITE_name' => $request->SITE_name,
            'CLI_name' => $request->CLI_name,
        );

        $url = $this->url.'/site/update';
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if($status_code == 400)return redirect('/site')->with('message',$body['errors'])->with('messagetype','danger');
            elseif($status_code == 401)return redirect('/site')->with('messages',$body)->with('messagetype','danger');
        elseif($status_code == 200)return redirect('/site')->with('messages',$body)->with('messagetype','success');
        else return redirect('/site')->with('messages',$body)->with('messagetype','danger');
    }

    public function getSiteArray($response)
    {
        $siteArray = array();
        if($response['body']!=null)
        {
            $body = $response['body'];
            $someArray = json_decode($body,true);
            $counter =0;
            foreach($someArray['sites'] as $key => $value)
            {
                $siteArray[$counter]['id'] = $value['id'];
                $siteArray[$counter]['SITE_name'] = $value['SITE_name'];
                $counter++;
            }
        }
        return $siteArray;
    }

    public function getClients($response)
    {
        $clientArray = array();
        if($response['body']!=null)
        {
            $body = $response['body'];
            $someArray = json_decode($body,true);
            $counter =0;
            foreach($someArray['clients'] as $client)
            {
                $clientArray[$counter]['id'] = $client['id'];
                $clientArray[$counter]['CLI_name'] = $client['CLI_name'];
                $clientArray[$counter]['CLI_phone'] = $client['CLI_phone'];
                $clientArray[$counter]['CLI_address'] = $client['CLI_address'];
                $clientArray[$counter]['CLI_email'] = $client['CLI_email'];
                $counter++;
            }
        }
        return $clientArray;
    }

    public function getPermission($response)
    {
        if($response['body']!=null)
        {
            $body = $response['body'];
            $body = json_decode($body,true);
            return $body['permission'] ? true : false;
        }
        return false;
    }

    public function role($response)
    {
        if($response['body']!=null)
        {
            $body = $response['body'];
            $body = json_decode($body,true);
            return $body['role'];
        }
    }

    public function siteBased($response)
    {
        if($response['body']!=null)
        {
            $body = $response['body'];
            $body = json_decode($body,true);
            return $body['siteBased'] ? true : false;
        }
    }

    public function systemClient($response)
    {
        if($response['body']!=null)
        {
            $body = $response['body'];
            $body = json_decode($body,true);
            return $body['systemClient'] ? true : false;
        }
    }
}