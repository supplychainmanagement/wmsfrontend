<?php

namespace Modules\Item\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class UnitloadController extends Controller{
    public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function index()
    {
        $url = $this->url .'/unitload';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $unitloads = $this->getUnitload($response);
        $clients = $this->getClients($response);
        $sites = $this->getSites($response);
        $skus = $this->getSKUs($response);
        $types = $this->getTypes($response);

        if($status_code == 200 || $status_code == 204)
        {
            return View('Unitload.unitload')->with('unitloads',$unitloads)->with('clients',$clients)
                ->with('sites',$sites)->with('skus',$skus)->with('types',$types);
        }else{
            return redirect('/');
        }
    }

    public function getUnitload($response)
    {
        $unitloads = array();
        if($response['body']!=null)
        {
            $body = json_decode($response['body'],true);
            if($body['unitloads']!=null)
            {
                foreach($body['unitloads'] as $key => $unitload)
                {
                    $unitloads[$key] = $unitload;
                }
            }
        }
        return $unitloads;
    }
    public function getClients($response)
    {
        $clients = array();
        if($response['body']!=null)
        {
            $body = json_decode($response['body'],true);
            if($body['all_clients']!=null) {
                foreach ($body['all_clients'] as $key => $client) {
                    $clients[$key] = $client;
                }
            }
        }
        return $clients;
    }
    public function getSites($response)
    {
        $sites = array();
        if($response['body']!=null)
        {
            $body = json_decode($response['body'],true);
            if($body['all_sites']!=null) {
                foreach ($body['all_sites'] as $key => $site) {
                    $sites[$key] = $site;
                }
            }
        }
        return $sites;
    }
    public function getStorageLocations($response)
    {
        $locations = array();
        if($response['body']!=null)
        {
            $body = json_decode($response['body'],true);
            if($body['all_storagelocations']) {
                foreach ($body['all_storagelocations'] as $key => $location) {
                    $locations[$key] = $location;
                }
            }
        }
        return $locations;
    }
    public function getSKUs($response)
    {
        $skus = array();
        if($response['body']!=null)
        {
            $body = json_decode($response['body'],true);
            if($body['all_skus']!=null) {
                foreach ($body['all_skus'] as $key => $sku) {
                    $skus[$key] = $sku;
                }
            }
        }
        return $skus;
    }
    public function getTypes($response)
    {
        $types = array();
        if($response['body']!=null)
        {
            $body = json_decode($response['body'],true);
            if($body['all_unitload_types']!=null) {
                foreach ($body['all_unitload_types'] as $key => $type) {
                    $types[$key] = $type;
                }
            }
        }
        return $types;
    }

    public function getprop(Request $request)
    {
        $url = $this->url.'/unitload/prop';
        $fields = array('id'=>$request->id);
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        $clients = $body['all_clients'];
        $locations = $body['all_storagelocations'];
        $skus = $body['all_skus'];
        $types = $body['all_unitload_types'];

        $unitload = $body['unitload'];
        $unitload_client = $body['unitload_client'];
        $unitload_location = $body['unitload_storagelocation'];
        $unitload_sku = $body['unitload_sku'];
        $unitload_type = $body['unitload_unitloadType'];

        if($status_code == 200)
        {
            $dataFrontEnd = ['clients'=>$clients,'locations'=>$locations,'skus'=>$skus,'types'=>$types,
            'unitload'=>$unitload,'unitload_client'=>$unitload_client,'unitload_location'=>$unitload_location,
            'unitload_sku'=>$unitload_sku,'unitload_type'=>$unitload_type];
            return response($dataFrontEnd);
        }else if($status_code == 204)
        {
            $dataFrontEnd = array();
            return $dataFrontEnd;
        }else{
            return redirect('/login');
        }
    }

    public function update(Request $request)
    {
        // $url = $this->url .'/unitload/update';
        // $fields = array(
        //     'id'=>$request->id,
        //     'UL_name'=>$request->UL_name,
        //     'UL_CLI'=>$request->UL_CLI,
        //     'UL_ULT_Type'=>$request->UL_ULT_Type,
        //     'UL_SKU'=>$request->UL_SKU,
        //     'UL_allocation'=>$request->UL_allocation,
        //     'UL_amount'=>$request->UL_amount,
        //     'UL_STL'=>$request->UL_STL,
        //     'UL_status'=>$request->UL_status,
        //     'UL_serialNumber'=>$request->UL_serialNumber,
        //     'UL_reserved'=>$request->UL_reserved,
        //     'UL_expiryDate'=>$request->UL_expiryDate,
        //     'UL_stocktakingDate'=>$request->UL_stocktakingDate
        // );
        // $response = HttpRequest::post($url,$fields);
        // $status_code = $response['info']['http_code'];
        // $body = json_decode($response['body'],true);
        // return $response['body'];
        // if($status_code == 200)return redirect('/unitload')->with('message','Unitload has Been Updated Successfully')
        //     ->with('messagetype','success');
        // if($status_code == 400)return redirect('/unitload')->with('message',$body['errors'][0])->with('messagetype','danger');
        // if($status_code == 500)return redirect('/unitload')->with('message','Unitload could not Be Updated')->with('messagetype','danger');
    }

    public function create(Request $request)
    {
        $url = $this->url.'/unitload/create';
        $fields = array();
        $fields ['UL_name'] = $request->UL_name;
        $fields['UL_STL'] = $request->UL_STL;
        $fields['UL_CLI'] = $request->UL_CLI;
        //$fields['UL_amount'] = $request->UL_amount;
        $fields['UL_SKU'] = $request->UL_SKU;
        $fields['UL_ULT_Type'] = $request->UL_ULT_Type;
        $response = HttpRequest::post($url,$fields);
        $body = json_decode($response['body'],true);
        //return $response['body'];
        $status_code = $response['info']['http_code'];
        if($status_code == 400) return redirect('/unitload')->with('message',$body['errors'][0])->with('messagetype','danger');
        if($status_code == 201) return redirect('/unitload')->with('message','Unitload Has Been Successfully Created')
            ->with('messagetype','success');
        if($status_code == 500) return redirect('/unitload')->with('message','Unitload has Failed to Be Created')
            ->with('messagetype','danger');
    }

    public function delete(Request $request)
    {
        $url = $this->url.'/unitload/delete';
        $fields = array('id'=>$request->id);
        $response = HttpRequest::post($url,$fields);
        $body = json_decode($response['body'],true);
        $status_code = $response['info']['http_code'];
        if($status_code == 200)return redirect('/unitload')->with('message','Unitload is Deleted Successfully')->with('messagetype','success');;
        if($status_code == 400)return redirect('/unitload')->with('message',$body['errors'][0])->with('messagetype','danger');
        if($status_code == 500)return redirect('/unitload')->with('message','Unitload has Failed to Be Deleted')->with('messagetype','danger');
    }

    public function barcodeImage(Request $request)
    {
        $this->CreatePDF($request);
        
    }
    public function CreatePDF($request)
    {
        PDF::setAuthor('me-wims');
        PDF::SetTitle($request['UL_name']);
        PDF::SetSubject('Unitload');
        PDF::SetMargins(15,10,10,true);
        $pagelayout = array(100,60);
        PDF::AddPage('L',$pagelayout,false,false);
        $style = array(
            'position' => 'C',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => false,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );
        PDF::write1DBarcode($request['UL_name'].$request['id'], 'C128', '', '', '', 18, 0.4, $style, '');
        PDF::writeHTML('<br/><br/><br/><br/><br/>',true,false,false,false,'R');
        PDF::writeHtml('<h3>'.$request['UL_name'].'</h3>',true,false,false,false,'C');
        PDF::Output($request['UL_name'].'.pdf');
    }

    public function getZones(Request $request)
    {
        $url = $this->url.'/unitload/getzones';
        $fields = array(
            'SITE_name'=>$request->SITE_name
        );
        //return $fields;
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        //return $response['body'];
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>'Missing Inputs',
                'body'=>$body
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>'Server Encountered an error .. '.$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>'Items Pulled',
            'data'=>$body
            );
            return $return_data;
        }
    }
    public function getRacks(Request $request)
    {
        $url = $this->url.'/unitload/getracks';
        $fields = array(
            'SITE_name'=>$request->SITE_name,
            'ZONE_name'=>$request->ZONE_name
        );
        //return $fields;
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>'Missing Inputs',
                'body'=>$body
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>'Server Encountered an error .. '.$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>'Items Pulled',
            'data'=>$body
            );
            return $return_data;
        }
    }
    public function getStorageLocationsAjax(Request $request)
    {
        $url = $this->url.'/unitload/getlocations';
        $fields = array(
            'SITE_name'=>$request->SITE_name,
            'ZONE_name'=>$request->ZONE_name,
            'RACK_name'=>$request->RACK_name
        );
        //return $fields;
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>'Missing Inputs',
                'body'=>$body
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>'Server Encountered an error .. '.$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>'Items Pulled',
            'data'=>$body
            );
            return $return_data;
        }
    }
}