<?php

namespace Modules\Item\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class CapacityUnitloadController extends Controller
{
  public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function index()
    {
        $url = $this->url . '/capacityunitload';
        $return_code = $this->return_code;
       $response = HttpRequest::get($url);
       $status_code = $response['info']['http_code'];
       if($status_code != $return_code['ok'] && $status_code != $return_code['no_content'])
       {
            return Redirect::route('login');
       }
       elseif($status_code == $return_code['no_content'])
       {
        return view('capacities.capacityunitload')->with('message','No Content')
        ->with('capacityunitloads',array())->with('unitloadtypes',array())
        ->with('skutypes',array());
       }
       else
       {
        $capacityunitloads = array();
        $body = $response['body'];
        $backendArray = json_decode($body, true);
        $counter = 0;
        foreach ($backendArray['CapacityUnitload'] as $key => $value) {
            $capacityunitloads[$counter]['id'] = $value['capacity']['id'];
            $capacityunitloads[$counter]['unitloadtype_name'] = $value["CU_ULT_Type_name"];
            $capacityunitloads[$counter]['skutype_name'] = $value["CU_SKU_Type_name"];
            $capacityunitloads[$counter]['allocation'] = $value['capacity']["CU_allocation"];
            $capacityunitloads[$counter]['created_at'] = $value['capacity']['created_at'];
            $capacityunitloads[$counter]['updated_at'] = $value['capacity']['updated_at'];
            $counter++;

        }

        $unitloadtypes = array();
        $counter = 0;
        foreach ($backendArray['ULT'] as $key => $value) {
            $unitloadtypes[$counter] = $value['ULT_Type_name'];
            $counter++;
        }

        $skutypes = array();
        $counter = 0;
        foreach ($backendArray['SKUT'] as $key => $value) {
            $skutypes[$counter] = $value['SKU_Type_name'];
            $counter++;
        }



        return view('capacities.capacityunitload')->with('capacityunitloads',$capacityunitloads)
        ->with('unitloadtypes',$unitloadtypes)
        ->with('skutypes',$skutypes);
       }
    }

    public function create(Request $request)
    {
        $fields = array();
        $fields['CU_ULT_Type_name'] = $request->unitloadtype_name;
        $fields['CU_SKU_Type_name'] = $request->skutype_name;
        $fields['CU_allocation'] = $request->allocation;

        $url = $this->url . '/capacityunitload/create'; 
        $return_code = $this->return_code;
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];

        if($statusCode == $return_code['bad_request'])
        {
            return redirect('/capacityunitload') 
                    -> with('message', $response['body'])->with('messagetype','danger');
        }
        elseif($statusCode == $return_code['internal_server_error'])
        {
            return redirect('/capacityunitload') 
                    -> with('message', 'Internal Server Error')->with('messagetype','danger');
        }  
        elseif($statusCode == $return_code['created'])
        {
            return redirect('/capacityunitload') 
                    -> with('message', 'Capacity of Unitload Rule Successfully Created')->with('messagetype','success');
        }  
    }

    public function update(Request $request)
    {
        $fields = array();
        $fields['id'] = $request->id;
        $fields['ULT_Type_name'] = $request->unitloadtype_name;
        $fields['SKU_Type_name'] = $request->skutype_name;
        $fields['allocation'] = $request->allocation;

    
        $url = $this->url . '/capacityunitload/update'; 
        $return_code = $this->return_code;  
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];

        if($statusCode == $return_code['bad_request'])
        {
            return redirect('/capacityunitload') 
                    -> with('message', $response['body'])->with('messagetype','danger');
        }
        elseif($statusCode == $return_code['internal_server_error'])
        {
            return redirect('/capacityunitload') 
                    -> with('message', 'Internal Server Error')->with('messagetype','danger');
        }  
        elseif($statusCode == $return_code['ok'])
        {
            return redirect('/capacityunitload') 
                    -> with('message', 'Capacity of Unitload Rule Successfully Updated')->with('messagetype','success');
        }       
    }

    public function delete(Request $request)
    {
        $fields =array(
          'id'=>$request->id);
        $url = $this->url . '/capacityunitload/delete';  
        $return_code = $this->return_code; 
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];

        if($statusCode == $return_code['bad_request'])
        {
            return redirect('/capacityunitload') 
                    -> with('message', $response['body'])->with('messagetype','danger');
        }
        elseif($statusCode == $return_code['internal_server_error'])
        {
            return redirect('/capacityunitload') 
                    -> with('message', 'Internal Server Error')->with('messagetype','danger');
        }  
        elseif($statusCode == $return_code['ok'])
        {
            return redirect('/capacityunitload') 
                    -> with('message', 'Capacity of Unitload Rule Successfully Deleted')->with('messagetype','success');
        } 
    }
}
