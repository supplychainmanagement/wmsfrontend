<?php

namespace Modules\Item\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class SKUTypeController extends Controller
{
  public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function index()
    {
        $url = $this->url . '/skutype';
        $return_code = $this->return_code;
       $response = HttpRequest::get($url);
       $status_code = $response['info']['http_code'];
       if($status_code != $return_code['ok'] && $status_code != $return_code['no_content'])
       {
            return Redirect::route('login');
       }
       elseif($status_code == $return_code['no_content'])
       {
        return view('types.skutype')->with('message','No Content')
        ->with('skutypes',array());
       }
       else
       {
        $skutypes = array();
        $body = $response['body'];
        $backendArray = json_decode($body, true);
        $counter = 0;
        foreach ($backendArray['SKUType'] as $key => $value) {
            $skutypes[$counter]['id'] = $value['id'];
            $skutypes[$counter]['type_name'] = $value["SKU_Type_name"];
            $skutypes[$counter]['created_at'] = $value['created_at'];
            $skutypes[$counter]['updated_at'] = $value['updated_at'];
            $counter++;

        }


        return view('types.skutype')
        ->with('skutypes',$skutypes);
       }
    }

    public function create(Request $request)
    {
        $fields = array();
        $fields['SKU_Type_name'] = $request->type_name;

        $url = $this->url . '/skutype/create'; 
        $return_code = $this->return_code;
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if($statusCode == $return_code['bad_request'])
        {
            return redirect('/skutype') 
                    -> with('message', $body['errors'])->with('messagetype','danger');
        }
        elseif($statusCode == $return_code['internal_server_error'])
        {
            return redirect('/skutype') 
                    -> with('message', $body)->with('messagetype','danger');
        }  
        elseif($statusCode == $return_code['created'])
        {
            return redirect('/skutype') 
                    -> with('message', $body)->with('messagetype','success');
        }       
    }


    public function update(Request $request)
    {
        $fields =array(
          'id'=>$request->id,
          'SKU_Type_name'=>$request->type_name,
          );
        $url = $this->url . '/skutype/update'; 
        $return_code = $this->return_code;  
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if($statusCode == $return_code['bad_request'])
        {
            return redirect('/skutype') 
                    -> with('message', $body['errors'])->with('messagetype','danger');
        }
        elseif($statusCode == $return_code['internal_server_error'])
        {
            return redirect('/skutype') 
                    -> with('message', $body)->with('messagetype','danger');
        } 
        elseif($statusCode == $return_code['ok'])
        {
            return redirect('/skutype') 
                    -> with('message', $body)->with('messagetype','success');
        }    
    }

    public function delete(Request $request)
    {
        $fields =array(
          'id'=>$request->id);
        $url = $this->url . '/skutype/delete';  
        $return_code = $this->return_code; 
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if($statusCode == $return_code['bad_request'])
        {
            return redirect('/skutype') 
                    -> with('message', $body['errors'])->with('messagetype','danger');
        }
        
        elseif($statusCode == $return_code['internal_server_error'])
        {
            return redirect('/skutype') 
                    -> with('message', $body)->with('messagetype','danger');
        }
        elseif($statusCode == $return_code['ok'])
        {
            return redirect('/skutype') 
                    -> with('message', $body)->with('messagetype','success');
        }    
    }
}
