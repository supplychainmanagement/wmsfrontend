<?php

namespace Modules\Item\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class SupplierController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function index()
    {
        $url = $this->url.'/supplier';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        $suppliers = $body['suppliers'];
        $skus = $body['skus'];
        if($status_code == 200 || $status_code == 204)return View('Supplier.supplier')->with('suppliers',$suppliers)
            ->with('skus',$skus);
        else{
            return redirect('/login');
        }

    }
    public function create(Request $request)
    {
        $url = $this->url.'/supplier/create';
        $fields = array(
            'SUP_name'=>$request->SUP_name,
            'SUP_phone'=>$request->SUP_phone,
            'SUP_email'=>$request->SUP_email,
            'SUP_address'=>$request->SUP_address,
            'SUP_skus'=>$request->SUP_SKU

        );
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        if($status_code == 201)return redirect('/supplier')->with('message','Supplier Has Been Created Successfully')
            ->with('messagetype','success');
        if($status_code == 401)return redirect('/login');
        if($status_code == 400)return redirect('/supplier')->with('message',$body['errors'][0])->with('messagetype','danger');
        if($status_code == 500)return redirect('/supplier')->with('message','Supplier Has Failed To Be Created')
            ->with('messagetype','danger');
    }

    public function update(Request $request)
    {
        $url = $this->url.'/supplier/update';
        $fields = array(
            'id'=>$request->id,
            'SUP_name'=>$request->SUP_name,
            'SUP_phone'=>$request->SUP_phone,
            'SUP_email'=>$request->SUP_email,
            'SUP_address'=>$request->SUP_address,
            'SUP_skus'=>$request->SUP_SKU
        );
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        if($status_code == 200)return redirect('/supplier')->with('message','Supplier Has Been Updated Successfully')
            ->with('messagetype','success');
        if($status_code == 400)return redirect('/supplier')->with('message',$body['errors'][0])
            ->with('messagetype','danger');
        if($status_code == 500)return redirect('/supplier')->with('message','Supplier Has Been Failed To Be Updated')
            ->with('messagetype','danger');
    }

    public function delete(Request $request)
    {
        $url = $this->url.'/supplier/delete';
        $fields = ['id'=>$request->id];
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        if($status_code == 200)return redirect('/supplier')->with('message','Supplier Has Been Deleted Successfully')
            ->with('messagetype','success');
        if($status_code == 400)return redirect('/supplier')->with('message','Invalid Index')->with('messagetype','danger');
        if($status_code == 500)return redirect('/supplier')->with('message','Supplier Has Failed To Be Deleted')
            ->with('messagetype','danger');
    }

    public function getprop(Request $request)
    {
        $url = $this->url.'/supplier/supplierprop';
        $fields = array(
            'id'=>$request->id,
        );
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        $supplier = $body['supplier'];
        $skus = $body['skus'];
        $supplier_skus = $body['supplier_skus'];
        if($status_code == 200)
        {
            $dataFrontEnd = ['id'=>$supplier['id'],'supplier'=>$supplier,'skus'=>$skus,'supplier_skus'=>$supplier_skus];
            return response($dataFrontEnd);
        }elseif($status_code == 204)
        {
            $dataFrontEnd = array();
            return $dataFrontEnd;
        }else{
            return redirect('/login');
        }
    }
}