<?php
$websitename = null;
$subdomain = null;
Route::group(array('domain' => $subdomain.'.'.$websitename), function() {
	Route::group(['middleware' => 'web', 'prefix' => 'item', 'namespace' => 'Modules\Item\Http\Controllers'], function()
	{
	    Route::group(['prefix' => '/sku'], function () {
			Route::get('/' , ['as' => 'sku' ,'uses' => 'SKUController@index']);

			Route::post('/' , 'SKUController@getprop');
			Route::post('/create' , ['as'=>'skuCreate','uses'=>'SKUController@create']);
			Route::post('/delete' , 'SKUController@delete');
			Route::post('/update' , array( 'uses' =>'SKUController@update', 'as' => 'formupdate'));

		});

		Route::group(['prefix' => '/skutype'], function () {
			Route::get('/' , ['as' => 'skutype' ,'uses' => 'SKUTypeController@index']);

			Route::post('/create' ,  array( 'uses' =>'SKUTypeController@create', 'as' => 'skutype.formcreate'));
			Route::post('/delete' , array( 'uses' =>'SKUTypeController@delete', 'as' => 'skutype.formupdelete'));
			Route::post('/update' , array( 'uses' =>'SKUTypeController@update', 'as' => 'skutype.formupdate'));

		});

		Route::group(['prefix'=>'/unitload'],function(){
			Route::get('/',['as'=>'unitload','uses'=>'UnitloadController@index']);
			Route::get('/barcodeimage',['as'=>'ULbarcodeimage','uses'=>'UnitloadController@barcodeImage']);

			Route::post('/','UnitloadController@getprop');
			Route::post('/create',['as'=>'unitloadCreate','uses'=>'UnitloadController@create']);
			Route::post('/delete','UnitloadController@delete');
			Route::post('/update',['as'=>'forumupdate','uses'=>'UnitloadController@update']);
			Route::post('/getzones','UnitloadController@getZones');
			Route::post('/getracks','UnitloadController@getRacks');
			Route::post('/getlocations','UnitloadController@getStorageLocationsAjax');
		});

		Route::group(['prefix' => '/unitloadtype'], function () {
			Route::get('/' , ['as' => 'unitloadtype' ,'uses' => 'UnitloadTypeController@index']);

			Route::post('/create' ,  array('uses' => 'UnitloadTypeController@create', 'as' => 'unitloadtype.formcreate'));
			Route::post('/update' , array( 'uses' =>'UnitloadTypeController@update', 'as' => 'unitloadtype.formupdate'));
			Route::post('/delete' , array( 'uses' =>'UnitloadTypeController@delete', 'as' => 'unitloadtype.formupdelete'));

		});

		Route::group(['prefix' => '/capacityunitload'], function () {
			Route::get('/' , ['as' => 'capacityunitload' ,'uses' => 'CapacityUnitloadController@index']);

			Route::post('/create' ,  array( 'uses' =>'CapacityUnitloadController@create', 'as' => 'capacityunitload.formcreate'));
			Route::post('/delete' , array( 'uses' =>'CapacityUnitloadController@delete', 'as' => 'capacityunitload.formupdelete'));
			Route::post('/update' , array( 'uses' =>'CapacityUnitloadController@update', 'as' => 'capacityunitload.formupdate'));
		});

		Route::group(['prefix'=>'/supplier'],function(){
			Route::get('/',['as'=>'supplier','uses'=>'SupplierController@index']);
			
			Route::post('/','SupplierController@getprop');
			Route::post('/create',['as'=>'supplierCreate','uses'=>'SupplierController@create']);
			Route::post('/delete','SupplierController@delete');
			Route::post('/update',['as'=>'forumupdate','uses'=>'SupplierController@update']);
		});
	});
});
