@extends('main&templates.tables_layout')
@section('actionOfUpdate')
    sku/update
@stop

@section('actionOfDelete')
    sku/delete
@stop

@section('table-title')
    SKU
@stop


@section('table-head')
    <tr>
        <th style="display:none";>SKU ID</th>
        <th>Item Number</th>
        <th>Name</th>
        <th>Type</th>
        <th>Client</th>
        <th>Life Time</th>
    </tr>
@stop

@section('table-body')

    @foreach($skus as $sku)
        <tr>
            <td style="display:none;">{{$sku['sku']['id']}}</td>
            <td>{{$sku['sku']['SKU_item_no']}}</td>
            <td>{{$sku['sku']['SKU_name']}}</td>
            <td>{{$sku['skuType_name']}}</td>
            <td>{{$sku['client_name']}}</td>
            <td>{{$sku['sku']['SKU_lifeTime']}}</td>

        </tr>
    @endforeach

@stop

@section('prop-head')

    <th>Property</th>
    <th>Value</th>
@stop


@section('prop-body')


@stop
@section('message-body')
    @if(session()->has('message'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                         @foreach(session('message') as $mess)
                            <li>{{$mess}}</li>
                        @endforeach
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif
@stop
@if(session('system_client') == true)
    @if(session('user_role') == 'system_admin')
@section('create-panel')

    <div id="data"class="col-md-12">

        <div class="panel panel-warning" id= "data-panel">
            <form role="form" class="form-horizontal"  action="{{route('skuCreate')}}" method="post" id="sku-form">
                {!! csrf_field() !!}
                <div class="panel-heading">
                    <h3 class="panel-title" id = "table-title">Create New SKU</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">

                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Name</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name = "SKU_name" required/>
                                </div>
                            </div>
                        </div>

                        @if($systemClient == true)
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Client</label>
                                    <div class="col-md-10">
                                        <select class="form-control selectpicker show-tick" name = "SKU_CLI_name" required>
                                            @foreach ($clients as $client)
                                                <option>{{$client['CLI_name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        @endif


                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Type</label>
                                <div class="col-md-10">
                                    <select class="form-control selectpicker show-tick" name = "SKU_SKU_Type_name" required>
                                        @foreach ($skuType as $type)
                                            <option>{{$type['SKU_Type_name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>

                   </br>

                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Life Time</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name = "SKU_lifeTime"/>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Item Number</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name = "SKU_item_no" required/>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Stacking Number</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name = "SKU_stackingNumber"/>
                                </div>
                            </div>
                        </div>

                        <br/>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Suppliers</label>
                                <div class="col-md-10">

                                    @foreach ($suppliers as $supplier)
                                        <input type="checkbox" name="SKU_SUP_name[]" value="{{$supplier['SUP_name']}}">
                                        {{$supplier['SUP_name']}}<br>
                                    @endforeach

                                </div>
                            </div>
                        </div>


                    </div>


                </div>
                <div class="panel-footer">
                    <button type="submit" value= "Submit" class="btn btn-primary pull-right">Submit</button>
                </div>
            </form>
        </div>

    </div>

@stop

@endif
@endif


@section('Jsplugin')
    <script type="text/javascript" src="js/masterdata/sku.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var pathname = window.location.href;
            var position = pathname.indexOf("?");
            if(position != -1)
            {
                var getvalues = pathname.substring(position+1,pathname.length);
                var splited = getvalues.split('=');
                console.log(splited[0]);
                if(splited.length == 2)
                {
                    if(splited[0] == 'search')
                    {
                         $('.maintable').DataTable().search(splited[1]).draw();
                         console.log('i am here');
                    }
                } 
            }
        }); 
    </script>
    </script>
@stop                                      

