@extends('main&templates.tables_layout')
@section('actionOfUpdate')
    unitload/update
@stop

@section('actionOfDelete')
    unitload/delete
@stop

@section('table-title')
    Unitloads
@stop


@section('table-head')
    <tr>
        <th style="display:none";>Unitload ID</th>
        <th>Name</th>
        <th>Type</th>
        <th>Client</th>
        <th>Status</th>
        <th>Location</th>
        <th>Reserved</th>
        <th>SKU</th>
        <th>barcode</th>
    </tr>
@stop

@section('table-body')

    @foreach($unitloads as $unitload)
        <tr>
            <td style="display:none;">{{$unitload['unitload']['id']}}</td>
            <td>{{$unitload['unitload']['UL_name']}}</td>
            <td>{{$unitload['unitloadType_name']}}</td>
            <td>{{$unitload['client_name']}}</td>
            <td>{{$unitload['unitload']['UL_status']}}</td>
            <td>{{$unitload['storagelocation_name']}}</td>
            <td>{{$unitload['unitload']['UL_reserved']}}</td>
            <td>{{$unitload['sku_name']}}</td>
            <td><a class ="btn btn-success" href="{{route('ULbarcodeimage',array('UL_name'=>$unitload['unitload']['UL_name'],'id'=>$unitload['unitload']['id']))}}"
                   role="button">Show</a></td>

        </tr>
    @endforeach

@stop

@section('prop-head')

    <th>Property</th>
    <th>Value</th>
@stop


@section('prop-body')


@stop
@section('message-body')
    @if(session()->has('message'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                        <p>{{session('message')}}</p>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif
@stop
@if(session('system_client') == true)
    @if(session('user_role') == 'system_admin'||session('user_role')=='site_admin')
@section('create-panel')

    <div id="data"class="col-md-12">

        <div class="panel panel-warning" id= "data-panel">
            <form role="form" class="form-horizontal"  action="{{route('unitloadCreate')}}" method="post" id="unitload-form">
                {!! csrf_field() !!}
                <div class="panel-heading">
                    <h3 class="panel-title" id = "table-title">Create New Unitload</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">

                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Name</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name = "UL_name" required/>
                                </div>
                            </div>
                        </div>

                        @if(session('system_client') == true)
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Client</label>
                                    <div class="col-md-10">
                                        <select class="form-control selectpicker show-tick" name = "UL_CLI" required>
                                            @foreach ($clients as $client)
                                                <option>{{$client['CLI_name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        @endif


                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Type</label>
                                <div class="col-md-10">
                                    <select class="form-control selectpicker show-tick" name = "UL_ULT_Type" required>
                                        @foreach ($types as $type)
                                            <option>{{$type['ULT_Type_name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>

                    </br>
                    <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Site</label>
                            <div class="col-md-10">
                                <select class="form-control selectpicker show-tick" title="pick Something Please" name = "UL_SITE" required id="UL_SITE">
                                    @foreach ($sites as $site)
                                        <option>{{$site['SITE_name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Zone</label>
                            <div class="col-md-10">
                                <select class="form-control selectpicker show-tick" title="pick Something Please" name = "UL_ZONE" required id="UL_ZONE">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Rack</label>
                            <div class="col-md-10">
                                <select class="form-control selectpicker show-tick" title="pick Something Please" name = "UL_RACK" required id="UL_RACK">
                                </select>
                            </div>
                        </div>
                    </div>

                    </div>
                    </br>
                    <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Storage Location</label>
                            <div class="col-md-10">
                                <select class="form-control selectpicker show-tick" title="pick Something Please" name = "UL_STL" required id="UL_STL">
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-2 control-label">SKU</label>
                            <div class="col-md-10">
                                <select class="form-control selectpicker show-tick" name = "UL_SKU" required>
                                    @foreach ($skus as $sku)
                                        <option>{{$sku['SKU_name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    </div>
                        <br/>

                    </div>


                </div>
                <div class="panel-footer">
                    <button type="submit" value= "Submit" class="btn btn-primary pull-right">Submit</button>
                </div>
            </form>
        </div>

    </div>

@stop

@endif
@endif


@section('Jsplugin')
    <script type="text/javascript" src="js/masterdata/unitload.js"></script>
@stop
