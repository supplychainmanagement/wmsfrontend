@extends('main&templates.tables_layout')
@section('actionOfUpdate')
skutype/update
@stop

@section('actionOfDelete')
skutype/delete
@stop


@section('table-title')
    SKU Types
@stop


@section('table-head')
    <tr>
        <th style="display:none";>SKU Type ID</th>
        <th>SKU Type Name</th>
        <th>Created at</th>
        <th>Last Updated at</th>
    </tr>
@stop

@section('table-body')

    @foreach($skutypes as $skutype)
        <tr>
            <td style="display:none;">{{$skutype['id']}}</td>
            <td>{{$skutype['type_name']}}</td>
            <td>{{$skutype['created_at']}}</td>
            <td>{{$skutype['updated_at']}}</td>
        </tr>
    @endforeach

@stop

@section('prop-head')

    <th>Property</th>
    <th>Value</th>
@stop

@section('message-body')
    @if(session()->has('message'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                         <ul>
                            @foreach(session('message') as $mess)
                            <li>{{$mess}}</li>
                            @endforeach
                        </ul>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif
@stop
@if(session('system_client') == true)
@if(session('user_role') == 'system_admin' || session('user_role') == 'site_admin')
@section('create-panel')

  <div id="data"class="col-md-12">

            <div class="panel panel-warning" id= "data-panel">
              <form role="form" class="form-horizontal"  action="{{route('skutype.formcreate')}}" method="post" id="wmszone-form">
              {!! csrf_field() !!}
                 <div class="panel-heading">                                
                    <h3 class="panel-title" id = "table-title">Create New SKU Type</h3>
                      <ul class="panel-controls">
                          <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                          <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                           <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                       </ul>                                               
                </div>
                <div class="panel-body"> 
                      
                      <div class="row">

                      <div class="col-md-4">
                          <div class="form-group">
                            <label class="col-md-2 control-label">Name</label>
                            <div class="col-md-10">                                        
                              <input type="text" class="form-control" name = "type_name" required/>
                            </div>
                          </div>
                      </div>
                    
                    </div>
                    </div>                                
                     <div class="panel-footer">
                        <button type="submit" value= "Submit" class="btn btn-primary pull-right">Submit</button>
                     </div>
                     </form>
              </div>
           
    </div>

    @stop
    
  @endif
  @endif


@section('Jsplugin')
    <script type="text/javascript" src="js/masterdata/skutype.js"></script>
@stop                                      

