$(document).ready(function(){
    $('#inventorylink').addClass('active');
    $('#skulink').addClass('active');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    oTable = $('.maintable').dataTable();
    OTable =$('.maintable').DataTable();
    oTable.$('tr').click( function () {
        $("#data").removeClass('col-md-12');
        $("#data").addClass('col-md-8');
        OTable.rows( '.selected' ).nodes().to$() .removeClass( 'selected' );
        $(this).addClass('selected');
        $("#prop").removeClass("hidden").addClass("show");
        var title=$(this).find("td:nth-child(3)").html();
        $("#prop-title").text(title+' '+'properties');
        var title=$(this).find("td:nth-child(1)").html();
        $.post("sku",{id:title}, function(data, status){

            
            var table = document.getElementById("prop-table");
            var element2 = document.createElement("input");
            element2.type = "hidden";
            element2.name = "id";
            element2.value = data['sku']['id'];
            table.appendChild(element2);
            $("#prop-table").find("tr:gt(0)").remove();



            var row = table.insertRow(1);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Name";
            var element1 = document.createElement("input");
            element1.setAttribute('class','form-control');
            element1.type = "text";
            element1.name = "SKU_name";
            element1.value=data['sku']['SKU_name'];
            cell2.appendChild(element1);
            if(!data['permission'])
            {
                element1.readOnly = true;
            }

            var row = table.insertRow(2);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Created";
            cell2.innerHTML = data['sku'].created_at;

            var row = table.insertRow(3);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Modified";
            cell2.innerHTML = data['sku'].updated_at

            var row = table.insertRow(4);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Client";
            if(data['permission'])
            {
                var element1 = document.createElement("select");
                element1.name = "SKU_CLI_name";
                element1.setAttribute('class','form-control selectpicker show-tick');
                cell2.appendChild(element1);
                var arrtypes = data.all_clients;
                arrtypes.forEach(function(ind){
                    var option = document.createElement("option");
                    option.value = ind['CLI_name'];
                    option.text = ind['CLI_name'];
                    element1.appendChild(option);
                });
                element1.value = data.sku_client;
                element1.text = data.sku_client;
            }
            else
            {
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "SKU_CLI_name";
                element1.readOnly = true;
                element1.value=data.sku_client;
            }


            var row = table.insertRow(5);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Type";
            if(data['permission'])
            {
                var element1 = document.createElement("select");
                element1.name = "SKU_SKU_Type";
                element1.setAttribute('class','form-control selectpicker show-tick');
                cell2.appendChild(element1);
                var arrtypes = data.all_sku_types;
                arrtypes.forEach(function(ind){
                    var option = document.createElement("option");
                    option.value = ind['SKU_Type_name'];
                    option.text = ind['SKU_Type_name'];
                    element1.appendChild(option);
                });
                element1.value = data.sku_sku_type;
                element1.text = data.sku_sku_type;
            }
            else
            {
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "SKU_SKU_Type";
                element1.readOnly = true;
                element1.value=data.sku_sku_type;
            }


            var row = table.insertRow(6);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Life Time";
            if(data['permission'])
            {
                var element1 = document.createElement("input");
                element1.type = "text";
                element1.name = "SKU_lifeTime";
                element1.value = data['sku']['SKU_lifeTime'];
                element1.setAttribute('class','form-control');
                cell2.appendChild(element1);

            }
            else
            {
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "SKU_lifeTime";
                element1.readOnly = true;
                element1.value=data['sku']['SKU_lifeTime'];
            }


            var row = table.insertRow(7);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Supplier";
            if(data['permission'])
            {
                //var element1 = document.createElement("select");
                //element1.name = "clients";
                //element1.setAttribute('class','form-control selectpicker show-tick');
                //cell2.appendChild(element1);
                var arrtypes = data.all_suppliers;
                arrtypes.forEach(function(ind){
                    var option = document.createElement("input");
                    option.type = 'checkbox';
                    option.value = ind['SUP_name'];
                    option.name = 'SUP_name[]';
                    var attached = data['attached'];
                    attached.forEach(function(supplier){
                        if(option.value == supplier)option.checked=true;
                    });
                    cell2.appendChild(option);
                    cell2.appendChild(document.createTextNode(ind['SUP_name']));
                    cell2.appendChild(document.createElement('br'));

                });
            }
            else
            {
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "checkbox";
                element1.name = "SUP_name[]";
                element1.checkable = false;
                var arrtypes = data['attached'];
                arrtypes.forEach(function(ind)
                {
                    var option = document.createElement("input");
                    option.type = 'checkbox';
                    option.value = ind;
                    option.name = 'SUP_name[]';
                    option.checkable = false;
                    option.checked = true;
                    cell2.appendChild(option);
                    cell2.appendChild(document.createTextNode(ind));
                    cell2.appendChild(document.createElement('br'));
                });

            }




            var row = table.insertRow(8);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Stacking Number";
            if(data['permission'])
            {
                var element1 = document.createElement("input");
                element1.type = "text";
                element1.name = "SKU_stackingNumber";
                element1.value = data['sku']['SKU_stackingNumber'];
                element1.setAttribute('class','form-control');
                cell2.appendChild(element1);

            }
            else
            {
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "SKU_stackingNumber";
                element1.readOnly = true;
                element1.value=data['sku']['SKU_stackingNumber'];
            }



            var row = table.insertRow(9);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Item Number";
            if(data['permission'])
            {
                var element1 = document.createElement("input");
                element1.type = "text";
                element1.name = "SKU_item_no";
                element1.value = data['sku']['SKU_item_no'];
                element1.setAttribute('class','form-control');
                cell2.appendChild(element1);

            }
            else
            {
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "SKU_item_no";
                element1.readOnly = true;
                element1.value=data['sku']['SKU_item_no'];
            }



            document.getElementById("prop").style.maxHeight = document.getElementById("data").style.height
            if(!data['permission'])
            {
                $("#zonesave").removeClass("show").addClass("hidden");
                $("#zonedelete").removeClass("show").addClass("hidden");
            }


        });


    });

    $('.prop-remove').click(function(){
        $("#data").removeClass('col-md-8').addClass('col-md-12');
        $("#prop").removeClass("show").addClass("hidden");
        $("tr").removeClass('selected');
    });

    $('#create').click(function(){
        $("#create-panel").removeClass("hidden").addClass("show");
    });

});

// var jvalidate = $("#wmszone-form").validate({
//     ignore: [],
//     rules: {                                            
//         name:
//         {
//             required: true
//         }
//     }                                        
// });   


