<?php

namespace Modules\Issuing\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class IssueOrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }

    public function pdf(Request $request)
    {
        $url = $this->url.'/issueorderbyname';
        $fields = ['ISO_number'=>$request->ISO_number,'pdf'=>'true'];
        $response = HttpRequest::post($url,$fields);
        $body = json_decode($response['body'],true);

        PDF::SetTitle('Issue Order');
        PDF::SetMargins(15,10,10,true);
        PDF::setFooterCallback(function($pdf) {
             $pdf->SetY(-15);
             $pdf->writeHTML('<h5>Printed at '.date("Y-m-d").'</h5>',true,false,false,false,'R');
             $pdf->writeHTML('<h5>Powered By Micro Engineering roboVics<h5>',true,false,false,false,'R');
        });
        PDF::AddPage();
        PDF::writeHTML('<h1>Micro-Engineering Egypt</h1><br/>',true,false,false,false,'C');
        PDF::writeHTML('<h3>Floor 11th, Building #1</h3>',true,false,false,false,'C');
        PDF::writeHTML('<h4>82, Abdel-Aziz Fahmy St, Saint-Fatima</h4>',true,false,false,false,'C');
        PDF::writeHTML('<h5>Cairo, Egypt</h5><br/>',true,false,false,false,'C');

        PDF::writeHTML('<br/><h3><strong>Issue Order</strong></h3>',true,false,false,false,'C');
        $style = array(
            'position' => 'L',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        PDF::writeHTML('Issue Order: '.$request->ISO_number,false,false,false,false,'L');
        PDF::writeHTML('Client Name: '.$body['CLI_name'],false,false,false,false,'R');
        PDF::writeHTML('<br/>',true,false,false,false,'R');
        PDF::write1DBarcode($request->ISO_number.$body['issueorder']['id'],'C128A', '', '', '', 18, 0.4, $style, 'N');
        PDF::writeHTML('<br/><br/><hr/>',true,false,false,false,'C');
        PDF::writeHTML('
                        <table>
                            <tr>
                                <td>Customer:</td>
                                <td>'.$body['CST_name'].'<br/></td>
                                <td></td>
                            </tr>

                            <tr>
                                <td>Branch:</td>
                                <td>'.$body['Customer']['CST_branch'].'<br/></td>
                                <td></td>
                            </tr>

                            <tr>
                                <td>Address:</td>
                                <td>'.$body['Customer']['CST_address'].'<br/></td>
                                <td></td>
                            </tr>

                            <tr>
                                <td>Phone:</td>
                                <td>'.$body['Customer']['CST_phone'].'<br/></td>
                                <td></td>
                            </tr>

                            <tr>
                                <td>Email:</td>
                                <td>'.$body['Customer']['CST_email'].'</td>
                                <td></td>
                            </tr>
                        </table>
                        ',false,false,false,false,'L');


        PDF::writeHTML('<br/><br/><hr/><br/>',true,false,false,false,'L');


        $tablehtml = '<table><tr><th><strong>Order ID</strong></th>
                                <th><strong>Item Order Number</strong></th>
                                <th><strong>SKU</strong></th>
                                <th><strong>SKU Number</strong></th>
                                <th><strong>Amount</strong></th>
                                <th><strong>Shipping Date</strong></th></tr><br/>';
        foreach($body['issueorderitems_data'] as $issueorder)
        {
            $tablehtml .= '<tr>
                                <td>'.$issueorder['issueitem']['id'].'</td>
                                <td>'.$issueorder['issueitem']['ISI_number'].'</td>
                                <td>'.$issueorder['issueitem']['s_k_u']['SKU_name'].'</td>
                                <td>'.$issueorder['issueitem']['s_k_u']['SKU_item_no'].'</td>
                                <td>'.$issueorder['issueitem']['ISI_amount'].'</td>
                                <td>'.$issueorder['issueitem']['ISI_shippingDate'].'</td>
                           </tr><br/>';
        }
        $tablehtml .='</table>';
        PDF::writeHTML($tablehtml,false,false,false,false,'L');
        PDF::Output($request->ISO_number.'.pdf');
    }

    public function index()
    {
        $url = $this->url . '/issueorder';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if ($status_code == 404) return redirect('/home')->with('message', 'This URI is Not Found')->
        with('messagetype', 'danger');
        if ($status_code == 500) return redirect('/home')->with('message', $body['errors'][0])->with('messagetype', 'danger');
        if ($status_code == 200)
        {
            return View('GoodsOut.IssueOrder.index')->with('date',date("Y-m-d"))->
                with('issueOrders',$body);
        }
        if($status_code == 201)
        {
            return View('GoodsOut.IssueOrder.index')->with('date',date("Y-m-d"))->with('issueOrders',[]);
        }

    }
    public function itemindex()
    {
        $url = $this->url.'/issueorderitem';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        if ($status_code == 404) return redirect('/home')->with('message', 'This URI is Not Found')->
        with('messagetype', 'danger');
        if ($status_code == 500) return redirect('/home')->with('message', $body['errors'][0])->with('messagetype', 'danger');
        if ($status_code == 200)
        {
            return View('GoodsOut.IssueOrderItem.index')->with('date',date("Y-m-d"))->
                with('issueItems',$body);
        }
    }
    public function createorderindex()
    {
        //get site client and strategys
        $url = $this->url.'/issueorderdata';
        //return $url;
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        $issuOrderData_status_code = $response['info']['http_code'];
        $issueOrderDataBody = json_decode($response['body'], true);
        //get issue order names
        $url = $this->url.'/issueorders';
        $response = HttpRequest::get($url);
        $issuOrderNames_status_code = $response['info']['http_code'];
        $issueOrderNamesBody = json_decode($response['body'], true);


        if ($issuOrderData_status_code == 404 || $issuOrderNames_status_code == 404) return redirect('/home')->with('message', 'This URI is Not Found')->
        with('messagetype', 'danger');
        if ($issuOrderData_status_code == 500 || $issuOrderNames_status_code == 500) return redirect('/home')->with('message', $body['errors'][0])->with('messagetype', 'danger');
        if ($issuOrderData_status_code == 200 && $issuOrderNames_status_code == 200)
        {
            return View('GoodsOut.IssueOrderWizard.wizard')->with('client_names',$issueOrderDataBody['client_names'])
            ->with('site_names',$issueOrderDataBody['site_names'])->with('strategy_names',$issueOrderDataBody['strategy_names'])->with('issueorder_names',$issueOrderNamesBody)->with('date',date("Y-m-d"));
        }
    }
    public function getEntry(Request $request)
    {
        $url = $this->url.'/issueorderbyname';
        $fields = array(
            'ISO_number'=> $request->value
        );
        //return $url;
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        $return_data = array();
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors']
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>['Issue Order Data Pulled'],
            'data'=>$body
            );
            return $return_data;
        }
    }
    public function getCustomers(Request $request)
    {
        $url = $this->url.'/clientcustomers';
        $fields = array(
            'CLI_name'=>$request->value
        );
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        $return_data = array();
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors']
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            if(sizeof($body)>0){
                $return_data = array(
                'error'=>false,
                'message'=>['Customers for this Client have Been Pulled'],
                'data'=>$body
                );
                return $return_data;
            }
            else
            {
                $return_data = array(
                'error'=>true,
                'message'=>['No Customers For this client']
                );
                return $return_data;
            }
            
        }
    }
    public function save(Request $request)
    {
        $issueorder = $request->issueorder;

        if($issueorder == 'New Issue Order')
        {
            $url = $this->url.'/createissueorder';
            $fields = array(
                'SITE_name'=>$request->site,
                'CLI_name'=>$request->client,
                'CST_name'=>$request->customer
            );
            $response = HttpRequest::post($url,$fields);
            $status_code = $response['info']['http_code'];
            $body = json_decode($response['body'], true);
            $return_data = array();
            if($status_code == 400)
            {   
                $return_data = array(
                    'error'=>true,
                    'message'=>$body['errors']
                    );
                return $return_data;
            }
            elseif($status_code == 500)
            {
                $return_data = array(
                    'error'=>true,
                    'message'=>$body
                    );
                return $return_data;
            }
            else
            {
                $return_data = array(
                'error'=>false,
                'message'=>['Save Successfull You May Proceed to Add items to this issue order'],
                'data'=>$body,
                'state'=>'new'
                );
                return $return_data;
            }
        }
        else
        {
            $url = $this->url.'/issueorder/update';
            $fields = array(
                'SITE_name'=>$request->site,
                'CLI_name'=>$request->client,
                'CST_name'=>$request->customer,
                'ISO_number'=>$request->issueorder
            );
            
            $response = HttpRequest::post($url,$fields);
            $status_code = $response['info']['http_code'];
            $body = json_decode($response['body'], true);
            $return_data = array();
            if($status_code == 400)
            {   
                $return_data = array(
                    'error'=>true,
                    'message'=>$body['errors']
                    );
                return $return_data;
            }
            elseif($status_code == 500)
            {
                $return_data = array(
                    'error'=>true,
                    'message'=>$body
                    );
                return $return_data;
            }
            else
            {
                $return_data = array(
                'error'=>false,
                'message'=>['Update Successfull You May Proceed to Add items to this issue order'],
                'data'=>$body,
                'state'=>'old'
                );
                return $return_data;
            }

        } 
    }
    public function getSKUs(Request $request)
    {
        $url = $this->url.'/clientskus';
        $fields = array(
            'CLI_name'=>$request->value
        );
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        $return_data = array();
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors']
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            if(sizeof($body)>0){
                $return_data = array(
                'error'=>false,
                'message'=>['Customers for this Client have Been Pulled'],
                'data'=>$body
                );
                return $return_data;
            }
            else
            {
                $return_data = array(
                'error'=>true,
                'message'=>['No Customers For this client']
                );
                return $return_data;
            }
            
        }
    }
    public function saveItem(Request $request)
    {
        $url = $this->url.'/createissueorderitem';
        $fields = array(
            'ISO_number'=>$request->issueordername,
            'ISI_amount'=>$request->amount,
            'SKU_name'=>$request->sku,
            'ISI_shippingDate'=>$request->shippingdate,
            'STRAT_name'=>$request->orderstrategy
        );
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        //return $response['body'];
        $return_data = array();
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors']
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            
                $return_data = array(
                'error'=>false,
                'message'=>['Issue Item Added'],
                'data'=>$body
                );
                return $return_data;    
        }
    }
    function finishIssueOrder(Request $request)
    {
        return redirect()->route('issueorder',array('search'=>$request->issueordernamex))->with('message','Issue Order Created Successfully')->with('messagetype','success');
    }

}
