<?php

namespace Modules\Issuing\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class ShippingController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }

    public function createIndex()
    {
        $url = $this->url.'/shipping/generateshipname';
        $response = HttpRequest::get($url);
        $body = json_decode($response['body'],true);
        $shipjob = $body[0];
        return View('GoodsOut.Shipping.create')->with('date',date("Y-m-d"))->with('SHIP_number',$shipjob);
    }

    public function getAllIssueOrders()
    {
        $url = $this->url.'/issueorders';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        $return_data = array();
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors']
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>['Issue Order Data Pulled'],
            'data'=>$body
            );
            return $return_data;
        }
    }

    public function getAllIssueOrderItems(Request $request)
    {
        $url = $this->url.'/issueorderbyname';
        $fields = array(
            'type'=>'shipping',
            'ISO_number'=>$request->ISO_number
        );
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'], true);
        $return_data = array();
        if($status_code == 400)
        {   
            $return_data = array(
                'error'=>true,
                'message'=>$body['errors']
                );
            return $return_data;
        }
        elseif($status_code == 500)
        {
            $return_data = array(
                'error'=>true,
                'message'=>$body
                );
            return $return_data;
        }
        else
        {
            $return_data = array(
            'error'=>false,
            'message'=>['Issue Order Items Data Pulled'],
            'data'=>$body
            );
            return $return_data;
        }
    }

    public function submitShip(Request $request)
    {
        $url = $this->url.'/ship';
        $fields = $request->all();
        $response = HttpRequest::post($url,$fields);
        $body = json_decode($response['body'],true);
        $status_code = $response['info']['http_code'];
        if($status_code != 201)
        {
            return redirect()->route('ShippingCreate')->with('message',$body['errors'])
            ->with('messagetype','danger');
        };
        return redirect()->route('ShippingCreate')->with('message',['Shipping Job Has Been Submitted Successfully'])
            ->with('messagetype','success');
        // $status_code = $response['info']['http_code'];
        // $body = json_decode($response['body'], true);
        // $return_data = array();
        // if($status_code == 400)
        // {   
        //     $return_data = array(
        //         'error'=>true,
        //         'message'=>$body['errors']
        //         );
        //     return $return_data;
        // }
        // elseif($status_code == 500)
        // {
        //     $return_data = array(
        //         'error'=>true,
        //         'message'=>$body
        //         );
        //     return $return_data;
        // }
        // else
        // {
        //     $return_data = array(
        //     'error'=>false,
        //     'message'=>['Ship Job Successfully Submitted'],
        //     'data'=>$body
        //     );
        //     return $return_data;
        // }
    }
}