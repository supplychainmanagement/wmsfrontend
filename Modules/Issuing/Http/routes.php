<?php
$websitename = null;
$subdomain = null;

Route::group(array('domain' => $subdomain.'.'.$websitename), function() {
	Route::group(['middleware' => 'web', 'prefix' => 'issuing', 'namespace' => 'Modules\Issuing\Http\Controllers'], function()
	{
	    Route::group(['prefix'=>'/issueorder'],function(){
			Route::get('/',['as'=>'issueorder','uses'=>'IssueOrderController@index']);
			Route::get('/items',['as'=>'issueorder.items','uses'=>'IssueOrderController@itemindex']);
			Route::get('/issueorderwizard',['as'=>'issueorderwizard','uses'=>'IssueOrderController@createorderindex']);
			Route::post('/getentry' ,'IssueOrderController@getEntry');
			Route::post('/getcustomers' ,'IssueOrderController@getCustomers');
			Route::post('/save','IssueOrderController@save');
			Route::post('/saveitem','IssueOrderController@saveItem');
			Route::post('/getskus' ,'IssueOrderController@getSKUs');
			Route::post('/removeitem','IssueOrderController@removeItem');
			Route::get('/pdf',['as'=>'printPDF','uses'=>'IssueOrderController@pdf']);
			Route::post('/finishissueorder',['as'=>'finishissueorder','uses'=>'IssueOrderController@finishIssueOrder']);
		});

		Route::group(['prefix'=>'/pickorder'],function(){
			Route::get('/',['uses'=>'PickOrderController@pickorderindex','as'=>'pickorder']);
			Route::get('/pickrequestindex',['as'=>'pickorder.pickrequestindex','uses'=>'PickOrderController@pickRequestIndex']);
			Route::get('/pickrequestitemsindex',['as'=>'pickorder.pickrequestitemsindex','uses'=>'PickOrderController@pickRequestItemIndex']);
			Route::post('/getitems','PickOrderController@getItems');
			Route::post('/getitemamount','PickOrderController@getItemAmount');
			Route::post('/getpickoutunitloads','PickOrderController@getStorageUnitloads');
			Route::post('/getpickinunitloads','PickOrderController@pickInUnitloads');
			Route::post('/createpickrequest','PickOrderController@createPickRequest');
			Route::post('/getpickinunitloadamount','PickOrderController@getPickInUnitloadAmount');
			Route::post('/getpickoutunitloadamount','PickOrderController@getPickOutUnitloadAmount');
			Route::post('/createpickjob',['as'=>'createPickJob','uses'=>'PickOrderController@createPickJob']);
			Route::post('/pickjobitems','PickOrderController@pickJobItems');
			Route::get('/pickjobindex',['as'=>'pickorder.pickjobindex','uses'=>'PickOrderController@pickJobIndex']);
			Route::post('/suggestpickout','PickOrderController@suggestPickOut');
			Route::get('/pdf',['as'=>'pickrequestpdf','uses'=>'PickOrderController@pdf']);
		});

		Route::group(['prefix'=>'/shipping'],function(){
			Route::get('/create',['as'=>'ShippingCreate','uses'=>'ShippingController@createIndex']);
			Route::post('/submit',['as'=>'SubmitShip','uses'=>'ShippingController@submitShip']);
			Route::get('/issueorders','ShippingController@getAllIssueOrders');
			Route::post('/issueorderitems','ShippingController@getAllIssueOrderItems');
		});
	});

});
