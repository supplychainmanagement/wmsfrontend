@extends('main&templates.tables_layout')
@section('actionOfUpdate')
    issueorder/update
@stop

@section('actionOfDelete')
    issueorder/delete
@stop

@section('table-title')
    Issue Order
@stop


@section('table-head')
    <tr>
        <th style="display:none";>Issue Order ID</th>
        <th>Issue Order Number</th>
        <th>status</th>
        <th>Client</th>
        <th>Customer</th>
        <th>Site</th>
        <th>Print</th>
    </tr>
@stop

@section('table-body')

    @foreach($issueOrders as $issueOrder)
        <tr>
            <td style="display:none;">{{$issueOrder['issueorder']['id']}}</td>
            <td><a href="{{route('issueorder.items',array('search'=>$issueOrder['issueorder']['ISO_number']))}}">{{$issueOrder['issueorder']['ISO_number']}}</a></td>
            <td>{{$issueOrder['issueorder']['ISO_status']}}</td>
            <td><a href="{{route('client',array('search'=>$issueOrder['client']))}}">{{$issueOrder['client']}}</a></td>
            <td><a href="{{route('customer',array('search'=>$issueOrder['customer']))}}">{{$issueOrder['customer']}}</a></td>
            <td><a href="{{route('site',array('search'=>$issueOrder['site']))}}">{{$issueOrder['site']}}</a></td>
            <td><a class ="btn btn-success" href="{{route('printPDF',array('ISO_number'=>$issueOrder['issueorder']['ISO_number']))}}"
                   role="button">Print</a></td>
        </tr>
    @endforeach

@stop

@section('message-body')
    @if(session()->has('message'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                        <p>{{session('message')}}</p>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif
@stop

@section('Jsplugin')

<script>
$('.maintable').dataTable();
        $('#goodsoutlink').addClass('active');
        $('#issueorderlink').addClass('active');
        $(document).ready(function(){
            var pathname = window.location.href;
            var position = pathname.indexOf("?");
            if(position != -1)
            {
                var getvalues = pathname.substring(position+1,pathname.length);
                var splited = getvalues.split('=');
                console.log(splited[0]);
                if(splited.length == 2)
                {
                    if(splited[0] == 'search')
                    {
                         $('.maintable').DataTable().search(splited[1]).draw();
                         console.log('i am here');
                    }
                } 
            }
        }); 
    </script>
@stop
