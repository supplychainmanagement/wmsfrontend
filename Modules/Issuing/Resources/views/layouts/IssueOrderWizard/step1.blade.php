
<button onclick="bootstro.start('.bootstro');" class="btn btn-danger">Start Tour</button>
 <form role="form" class="form-horizontal" id="saveissueorder" method="post">
            {!! csrf_field() !!}
                      <div class="row" style="margin-top: 10px">
                        <div class="col-md-6">
                        <div class="form-group">
                            <div class="bootstro" data-bootstro-title="Issue Order"
                                 data-bootstro-content="Choose New Issue Order If You Want to Create New One
                                 Or Choose an Existing One To Update"
                                 data-bootstro-placement="right" data-bootstro-width="400px">
                          <label class="col-md-5 control-label">Issue Order</label>
                          <div class="col-md-7">                                        
                            <select class="form-control selectpicker show-tick " name = "issueorder" title="Pick Something Please!" id ="issueorder" onchange="" data-live-search="true" required>
                             <option>New Issue Order</option>
                             <option data-divider="true"></option>
                               @foreach ($issueorder_names as $issueorder)
                                <option>{{$issueorder}}</option>
                               @endforeach
                            </select>
                          </div>
                                </div>
                        </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                              <div class="bootstro" data-bootstro-title="Client"
                                   data-bootstro-content="Choose The Client That Will Do The Issue Operation"
                                   data-bootstro-placement="top" data-bootstro-width="400px">
                            <label class="col-md-5 control-label">Client</label>
                            <div class="col-md-7">                                        
                              <select class="form-control selectpicker show-tick" name = "client" title="Pick Something Please!" data-live-search="true" id = "client" required>
                              @foreach ($client_names as $client)
                                <option>{{$client}}</option>
                              @endforeach
                              </select>
                            </div>
                          </div>
                              </div>
                        </div>
                      </div>
                      <div class="row" style="margin-top: 10px">
                      @if(session('system_client') == true && session('user_role') == 'system_admin')
                         <div class="col-md-6">
                            <div class="form-group">
                                <div class="bootstro" data-bootstro-title="Site"
                                     data-bootstro-content="Choose The Site Where The Goods Will Be Issued"
                                     data-bootstro-placement="right" data-bootstro-width="400px">
                              <label class="col-md-5 control-label">Site</label>
                              <div class="col-md-7">                                        
                                <select class="form-control selectpicker show-tick" name = "site" title="Pick Something Please!" id = "site" data-live-search="true" required>
                                @foreach ($site_names as $site)
                                <option>{{$site}}</option>
                                @endforeach
                                </select>
                              </div>
                                    </div>
                            </div>
                          </div>
                          @endif
                          <div class="col-md-6">
                        <div class="form-group">
                            <div class="bootstro" data-bootstro-title="Customer"
                                 data-bootstro-content="Choose The Customer That Will Receive The Issued Goods"
                                 data-bootstro-placement="top" data-bootstro-width="400px">
                          <label class="col-md-5 control-label">Customer</label>
                          <div class="col-md-7">                                        
                            <select class="form-control selectpicker show-tick " name = "customer" title="Pick Something Please!" id ="customer" onchange="" data-live-search="true" disabled = "true" required>
                              <!-- Customer for each -->
                            </select>
                          </div>
                        </div>
                            </div>
                        </div>
                       
                      </div>
                      <div class="row" style="margin-bottom:10px;margin-top:10px;">
                          <div class="col-md-5" id="message">

                          </div>
                        <div class="col-md-offset-6 col-md-1">
                            <div class="bootstro" data-bootstro-title="Save"
                                 data-bootstro-content="Don't Forget To Click Save Then Next"
                                 data-bootstro-placement="top" data-bootstro-width="400px">
                          <button  type="submit" value= "Submit" class="btn btn-success pull-right">Save</button>
                                </div>
                        </div>
                      </div>
</form>