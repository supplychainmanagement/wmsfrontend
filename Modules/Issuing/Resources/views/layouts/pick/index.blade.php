@extends('main&templates.tables_layout')


@section('table-title')
    Pick Requests
@stop


@section('table-head')
    <tr>
        <th style="display:none";>Pick Request ID</th>
        <th>Pick Request Number</th>
        <th>State</th>
        <th>Date</th>
        <th>Issue Order Item</th>
        <th>Site</th>
        <th>Print</th>
    </tr>
@stop

@section('table-body')

    @foreach($pickRequests as $pickRequest)
        <tr>
            <td style="display:none;">{{$pickRequest['pickrequest']['id']}}</td>
            <td><a href="{{route('pickorder.pickrequestitemsindex',array('search'=>$pickRequest['pickrequest']['PR_number']))}}">{{$pickRequest['pickrequest']['PR_number']}}</a></td>
            <td>{{$pickRequest['pickrequest']['PR_state']}}</td>
            <td>{{$pickRequest['pickrequest']['PR_date']}}</td>
            <td><a href="{{route('issueorder.items',array('search'=>$pickRequest['issueitem']))}}">{{$pickRequest['issueitem']}}</a></td>
            <td><a href="{{route('site',array('search'=>$pickRequest['site']))}}">{{$pickRequest['site']}}</a></td>
            <td><a class ="btn btn-success" href="{{route('pickrequestpdf',array('PR_number'=>$pickRequest['pickrequest']['PR_number']))}}"
                   role="button">Print</a></td>
        </tr>
    @endforeach

@stop

@section('message-body')
    @if(session()->has('message'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                        <p>{{session('message')}}</p>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif
@stop

@section('Jsplugin')
<script>
$('.maintable').dataTable();
        $('#goodsoutlink').addClass('active');
        $('#pickrequestlink').addClass('active');
        $(document).ready(function(){
            var pathname = window.location.href;
            var position = pathname.indexOf("?");
            if(position != -1)
            {
                var getvalues = pathname.substring(position+1,pathname.length);
                var splited = getvalues.split('=');
                console.log(splited[0]);
                if(splited.length == 2)
                {
                    if(splited[0] == 'search')
                    {
                         $('.maintable').DataTable().search(splited[1]).draw();
                         console.log('i am here');
                    }
                } 
            }
        }); 
    </script>
@stop