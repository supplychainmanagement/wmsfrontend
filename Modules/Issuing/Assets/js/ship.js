/**
 * Created by Khaled on 8/14/2016.
 */
 function message(response,status)
{
    var list = "<ul>";
      for(var x=0 ;x<response.message.length;x++)
      {
        list += '<li>'+response.message[x]+'</li>';
      }
      list += "</ul>";
      document.getElementById('message').innerHTML=
      '<div class="alert alert-'+status+'" role="alert">'+
          '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
          +'<strong>'+list+'</strong>'
      +'</div>';
}
$(document).ready(function(){
    var IssueOrder;
    var IssueOrderItem;
    var Location;
    var STJ_name;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $.ajax({
        url: "issueorders",
        type: "GET",
        success: function(response)
        {
            if(response.error == true)
            {
                message(response.message,'danger');
            }
            else
            {
                response.data.forEach(function(ISO_name){
                    $('#ISO_name').append($('<option>',{
                        value:ISO_name,
                        text: ISO_name
                    }));
                    IssueOrder = ISO_name;
                    $('#ISO_name').selectpicker('refresh');
                });
            }
            
        }
    });


    $('#ISO_name').on('change',function(){
        IssueOrder = this.value;
        $('#ISI_number').empty();
        $.ajax({
            url: "issueorderitems",
            type: "POST",
            data: {ISO_number:IssueOrder},
            success: function(response)
            {
                if(response.error == true)
                {
                    message(response.message,'danger');
                }
                else
                {
                    response.data.issueorderitems_data.forEach(function(ISI){
                        $('#ISI_number').append($('<option>',{
                            value: ISI.issueitem['ISI_number'],
                            text: ISI.issueitem['ISI_number'] + "  | "+ISI.SKU_name+" => "+ISI.issueitem['ISI_amount']+" |"
                        }));
                        IssueOrderItem = ISI.issueitem['ISI_number'];
                        $('#ISI_number').selectpicker('refresh');
                    });
                }
                
            }
        });
    });


    $('#ship-from').on('submit',function(){
        alert('submitted');
    });


});