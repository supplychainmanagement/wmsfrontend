$(document).ready(function(){

	$('#submitJob').attr('disabled',true);
	$('#pickrequest').on('change', function() {
		
		$('#pickrequestitem').find('option:not(:first)').remove().selectpicker('refresh');
		$.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
		$.ajax({
          url: "pickjobitems",
          type: "post",
          data: {'value':$('#pickrequest').val()} ,
          success: function (response) {
          		if(response.error == false)
          		{

          			response.data.pickrequestitems_data.forEach(function(x)
	                {
	                    $('#pickrequestitem').append($('<option>', {
	                        value: x.pickrequestitem.PRI_number,
	                        text: (x.pickrequestitem.PRI_number + "  | "+x.UL_name+" => "+x.SKU_name+" => "+x.pickrequestitem.PRI_amount+" |")
	                    }));
	                });
	                $('#pickrequestitem').selectpicker('refresh');
	          		// $selecthtml ='';
	            // 	for(var x in response.pickrequestdata.pickrequestitems_data)
	            // 	{
	            // 	  var pickrequestitem = response.pickrequestdata.pickrequestitems_data[x].pickrequestitem.PRI_number;
	            // 	  str = pickrequestitem.replace(/\s+/g, '');
	            //       $selecthtml = $selecthtml+'<option id="'+str+'">'+pickrequestitem+'</option>';
	            // 	}
	            // 	$('#pickrequestitem').html($selecthtml).selectpicker('refresh');
	            	$('#submitJob').attr('disabled',false);
	            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
             

          }
      });
	});
});