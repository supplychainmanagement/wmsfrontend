<?php
$subdomain = null;
$websitename = null;
Route::group(array('domain' => $subdomain.'.'.$websitename), function() {
	Route::group(['middleware' => 'web', 'prefix' => 'system', 'namespace' => 'Modules\System\Http\Controllers'], function()
	{
	     Route::group(['prefix'=>'login'],function(){
	     	Route::get('/', 'LoginController@index');
	     	Route::get('/logout',['as'=>'logout','uses'=>'LoginController@logout']);

			Route::post('/',['as'=>'login','uses'=>'LoginController@login']);
	     });

	     Route::group(['prefix'=>'/user'],function(){
			Route::get('/',['as'=>'user','uses'=>'UserController@index']);
			Route::post('/','UserController@getprop');
			Route::post('/create',['as'=>'userCreate','uses'=>'UserController@create']);
			Route::post('/delete','UserController@delete');
			Route::post('/update',['as'=>'forumupdate','uses'=>'UserController@update']);
		 });

	     Route::group(['prefix' => '/client'], function () {
			Route::get('/' , ['as' => 'client' ,'uses' => 'ClientController@index']);
			Route::post('/create' ,  array( 'uses' =>'ClientController@create', 'as' => 'client.formcreate'));
			Route::post('/delete' , array( 'uses' =>'ClientController@delete', 'as' => 'client.formupdelete'));
			Route::post('/update' , array( 'uses' =>'ClientController@update', 'as' => 'client.formupdate'));

		 });

	     Route::group(['prefix'=>'/customer'],function(){
			Route::get('/',['as'=>'customer','uses'=>'CustomerController@index']);
			Route::post('/','CustomerController@getprop');
			Route::post('/create',['as'=>'customerCreate','uses'=>'CustomerController@create']);
			Route::post('/delete','CustomerController@delete');
			Route::post('/update',['as'=>'forumupdate','uses'=>'CustomerController@update']);
		 });

	     Route::group(['prefix'=>'/provider'],function(){
			Route::get('/',['as'=>'provider','uses'=>'ProviderController@index']);
			Route::post('/','ProviderController@getprop');
			Route::post('/create',['as'=>'providerCreate','uses'=>'ProviderController@create']);
			Route::post('/delete','ProviderController@delete');
			Route::post('/update',['as'=>'forumupdate','uses'=>'ProviderController@update']);
		 });
	});
});
