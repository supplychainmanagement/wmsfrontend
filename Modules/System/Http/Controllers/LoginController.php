<?php

namespace Modules\System\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('checksubdomain', ['only' => [
            'index'
        ]]);
        parent::__construct();
    }
    public function index(){

        if(Session::has('Authorization'))
            return redirect()->route('home');
        return view('main&templates.login');
    }
    public function login(Request $request)
    {
        $url = $this->url .'/login';
        $warehousename = $this->getWarehouseName();
        $fields = array(
           'password' => $request->password,
           'username'=>$request->username,
           'warehousename'=>$warehousename
        );


        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body =  $response['body'];
        //return $body;
        return $this->handleResponseCode($status_code , $body);
        
    }
    public function handleResponseCode($status_code , $body)
    {
        $data = json_decode($body , true);
        if($status_code == 200)
        {
            $auth_header = json_decode($body , true)['Authorization'];
            $role = json_decode($body , true)['user_role'];
            $system_client = json_decode($body , true)['system_client'];
            $warehouseName = json_decode($body,true)['warehouseName'];
            $enable = json_decode($body,true)['warehouseEnable'];
            $name = json_decode($body,true)['name'];
            Session::put('Authorization', $auth_header);
            Session::put('user_role',$role);
            Session::put('system_client',$system_client);
            Session::put('warehouseName',$warehouseName);
            Session::put('warehouseEnable',$enable);
            Session::put('name',$name);
            return redirect()->intended('/home');
        }
        else if($status_code == 401)
        {
            
            return redirect('/')->with('message',$data)->with('messagetype','danger');

        }
        else if($status_code == 400)
        {
            return redirect('/')->with('message',$data['errors'])->with('messagetype','danger');
        }
        // return $status_code;
    }
    public function logout()
    {
        //Auth::logout();
        Session::flush();
        return redirect('/');
    }
    public function getWarehouseName()
    {
        $link = url('');
        // if the url contains localhost
        if (strpos($link, 'localhost') !== false)
        {
            // echo 'die';
            // die();
            if(strpos($link, '.') !== false)
            {
                $pos = strrpos($link,'.');
                $subdomain = substr($link,7,$pos-7);
                $basedomain = substr($link,0,$pos+1);
                $endwebsitenamepos = strripos($link,':');
                $websitename= '';
                if($endwebsitenamepos != 4)
                    $websitename = substr($link,$pos,$endwebsitenamepos-$pos);

                else
                    $websitename = substr($link,$pos,strlen($link)-$pos);
                $websitename = str_replace('.','',$websitename);
            }
            else
            {
                $subdomain = null;
                $websitename = null;
            }
        }
        //doesn't contain localhost
        else
        {
            // echo 'die';
            // die();
            $substrings = explode('.',$link);
            // me-wims.com
            if(sizeof($substrings) == 2){
                $subdomain = null ;
                $websitename =null;
            }
            //www or a.me-wims.com
            else if(sizeof($substrings) == 3){

                $substrings2 = explode('://',$substrings[0]);
                
                $websitename  = $substrings[1].'.'.$substrings[2];
                $subdomain = $substrings2[1];
                if($subdomain == 'www')
                {
                    $websitename = null;
                    $subdomain = null;
                }
            }
        }
        return $subdomain;
    }
}
