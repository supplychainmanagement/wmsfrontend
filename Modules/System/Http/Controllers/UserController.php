<?php

namespace Modules\System\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function index()
    {
        $url = $this->url.'/user';
        $response = HttpRequest::get($url);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        //return $response['body'];
        $users = $body['users'];
        $user_role = $body['role'];
        $user_client = $body['client'];
        $user_site = $body['site'];
        $sites = $body['sites'];
        //return $sites;
        $roles = $body['roles'];
        $clients = $body['clients'];
        if($status_code == 200 || $status_code == 204)return View('User.user')->with('users',$users)
            ->with('user_role',$user_role)->with('roles',$roles)->with('clients',$clients)
            ->with('user_client',$user_client)->with('user_site',$user_site)->with('sites',$sites);
        else{
            return redirect('/home');
        }

    }
    public function create(Request $request)
    {
        $url = $this->url.'/user/create';
        $fields = array(
            'USR_name'=>$request->USR_name,
            'password'=>$request->password,
            'USR_email'=>$request->USR_email,
            'USR_SITE'=>$request->USR_SITE,
            'USR_ROLE'=>$request->USR_ROLE

        );
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        if($status_code == 201)return redirect('/user')->with('message','User Has Been Created Successfully')
            ->with('messagetype','success');
        if($status_code == 401)return redirect('/login');
        if($status_code == 400)return redirect('/user')->with('message',$body['errors'][0])->with('messagetype','danger');
        if($status_code == 500)return redirect('/user')->with('message','User Creation Failed')
            ->with('messagetype','danger');
    }

    public function update(Request $request)
    {
        $url = $this->url.'/user/update';
        $fields = array(
            'id'=>$request->id,
            'USR_name'=>$request->USR_name,
            'USR_email'=>$request->USR_email,
            'USR_CLI'=>$request->USR_CLI,
            'USR_SITE'=>$request->USR_SITE,
            'USR_ROLE'=>$request->USR_ROLE,
        );
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        if($status_code == 200)return redirect('/user')->with('message','User Updated Successfully')
            ->with('messagetype','success');
        if($status_code == 400)return redirect('/user')->with('message',$body['errors'][0])
            ->with('messagetype','danger');
        if($status_code == 500)return redirect('/user')->with('message','User Update Failed')
            ->with('messagetype','danger');
    }

    public function delete(Request $request)
    {
        $url = $this->url.'/user/delete';
        $fields = ['id'=>$request->id];
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        if($status_code == 200)return redirect('/user')->with('message','User Has Been Deleted Successfully')
            ->with('messagetype','success');
        if($status_code == 400)return redirect('/user')->with('message','Invalid Index')->with('messagetype','danger');
        if($status_code == 500)return redirect('/user')->with('message','User Has Failed To Be Deleted')
            ->with('messagetype','danger');
    }

    public function getprop(Request $request)
    {
        $url = $this->url.'/user/userprop';
        $fields = array(
            'id'=>$request->id,
        );
        $response = HttpRequest::post($url,$fields);
        $status_code = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        $user = $body['user'];
        $user_role = $body['user_role'];
        $user_client = $body['user_client'];
        $user_site = $body['user_site'];
        $clients = $body['clients'];
        $roles = $body['roles'];
        $sites = $body['sites'];
        if($status_code == 200)
        {
            $dataFrontEnd = ['id'=>$user['id'],'user'=>$user,'user_role'=>$user_role,'user_site'=>$user_site,
            'user_client'=>$user_client,'clients'=>$clients,'roles'=>$roles,'sites'=>$sites];
            return response($dataFrontEnd);
        }elseif($status_code == 204)
        {
            $dataFrontEnd = array();
            return $dataFrontEnd;
        }else{
            return redirect('/login');
        }
    }
    public function getRole()
    {
        $role = 'none';
        $system_client = false;
        if (Session::has('user_role')) {
            $role = Session::get('user_role');
        }
        if (Session::has('system_client')) {
            $system_client = Session::get('system_client');
        }
        $data = array('role' =>$role , 'system_client' =>$system_client);
        return response($data);
    }
}