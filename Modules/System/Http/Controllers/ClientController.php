<?php

namespace Modules\System\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class ClientController extends Controller
{
  public function __construct()
    {
        $this->middleware('checkauth');
        $this->middleware('checkowner');
        parent::__construct();
    }
    public function index()
    {       
        $url = $this->url . '/client';
        $return_code = $this->return_code;
       $response = HttpRequest::get($url);
       $status_code = $response['info']['http_code'];
       //return $response['body'];
       if($status_code != $return_code['ok'] && $status_code != $return_code['no_content'])
       {

            return Redirect::route('home');
       }
       elseif($status_code == $return_code['no_content'])
       {
        return view('system.client')->with('message','No Content')
        ->with('clients',array());
       }
       else
       {
        $clients = array();
        $body = $response['body'];
        $backendArray = json_decode($body, true);
        $counter = 0;
        foreach ($backendArray['clients'] as $key => $value) {
            $clients[$counter]['id'] = $value['id'];
            $clients[$counter]['client_name'] = $value["CLI_name"];
            $clients[$counter]['system_client'] = $value["CLI_systemClient"];
            $clients[$counter]['phone'] = $value["CLI_phone"];
            $clients[$counter]['address'] = $value["CLI_address"];
            $clients[$counter]['email'] = $value["CLI_email"];
            $clients[$counter]['created_at'] = $value['created_at'];
            $clients[$counter]['updated_at'] = $value['updated_at'];
            $counter++;

        }


        return view('system.client')
        ->with('clients',$clients);
       }
    }

    public function create(Request $request)
    {
        $fields = array();
        $fields['CLI_name'] = $request->client_name;
        $fields['CLI_phone'] = $request->phone;
        $fields['CLI_address'] = $request->address;
        $fields['CLI_email'] = $request->email;

        $url = $this->url . '/client/create'; 
        $return_code = $this->return_code;
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];
        return $response['body'];
        if($statusCode == $return_code['bad_request'])
            {
                return redirect('/client') 
                        -> with('message', $response['body'])->with('messagetype','danger');
            }
            elseif($statusCode == $return_code['internal_server_error'])
            {
                return redirect('/client') 
                        -> with('message', 'Internal Server Error')->with('messagetype','danger');
            }  
            elseif($statusCode == $return_code['created'])
            {
                return redirect('/client') 
                        -> with('message', 'Client Successfully Created')->with('messagetype','success');
            }       
    }

    public function update(Request $request)
    {
        $fields =array();
        $fields['id'] = $request->id;
        $fields['CLI_name'] = $request->client_name;
        $fields['CLI_phone'] = $request->phone;
        $fields['CLI_address'] = $request->address;
        $fields['CLI_email'] = $request->email;

       
        $url = $this->url . '/client/update'; 
        $return_code = $this->return_code;  
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];
        if($statusCode == $return_code['bad_request'])
            {
                return redirect('/client') 
                        -> with('message', $response['body'])->with('messagetype','danger');
            }
            elseif($statusCode == $return_code['internal_server_error'])
            {
                return redirect('/client') 
                        -> with('message', 'Internal Server Error')->with('messagetype','danger');
            } 
            elseif($statusCode == $return_code['ok'])
            {
                return redirect('/client') 
                        -> with('message', 'Client Successfully Updated')->with('messagetype','success');
            }    

    }

    public function delete(Request $request)
    {

        $fields =array(
          'id'=>$request->id);
        $url = $this->url . '/client/delete';  
        $return_code = $this->return_code; 
        $response = HttpRequest::post($url ,$fields);
        $statusCode = $response['info']['http_code'];
        $body = json_decode($response['body'],true);
        //return $response['body'];

            if($statusCode == $return_code['bad_request'])
            {
                return redirect('/client') 
                        -> with('message', $response['body'])->with('messagetype','danger');
            }
            
            elseif($statusCode == $return_code['internal_server_error'])
            {
                return redirect('/client') 
                        -> with('message', 'Internal Server Error')->with('messagetype','danger');
            }
            elseif($statusCode == $return_code['ok'])
            {
                return redirect('/client') 
                        -> with('message', 'Client Successfully Deleted')->with('messagetype','success');
            }    
            
    }
}
