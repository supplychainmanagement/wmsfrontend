<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>WMS++</title>
    <link rel="shortcut icon" href="{{URL::asset('images/favicon.png')}}">

    <link rel="stylesheet" type="text/css" id="theme" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{URL::asset('css/main.css')}}"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{URL::asset('css/responsive.css')}}"/>

    <style>
        .alert-danger {
            background-color: #b64645;
            color: #FFF;
            border-color: #af4342;
        }
        .to-top {
            width: 40px;
            height: 40px;
            background: #f5f5f5;
            line-height: 35px;
            text-align: center;
            border: 1px solid #FFF;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            font-size: 23px;
            color: #CCC;
            -webkit-transition: all 200ms ease;
            -moz-transition: all 200ms ease;
            -ms-transition: all 200ms ease;
            -o-transition: all 200ms ease;
            transition: all 200ms ease;
        }
        .to-top:hover {
            border-color: #33414e;
            color: #333;
        }

    </style>
    <script src="{{URL::asset('js/jquery-min.js')}}"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<!-- Nav Menu Section -->
<div class="logo-menu">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header col-md-3">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- <a class="navbar-brand" href="index.html"><i class="fa fa-fa fa-cubes"></i> AppBox</a> -->
                <img src="{{URL::asset('images/logo-01.png')}}">
            </div>

            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav pull-right">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li >
                        <a class="page-scroll" href="#use">How to use?</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#mobile">Mobile App</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">About</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<!-- Nav Menu Section End -->

<!-- Hero Area Section -->

<section id="hero-area">
    <div class="hero-inner">
        <div class="container" style="margin-top:70px;">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title-big">Wms++</h1>
                    <h2 class="subtitle-big">Warehouse managment system that facilitates your business</h2>
                </div>
                <div class="col-md-12 text-center">
                    <video autoplay loop>
                        <source src="{{URL::asset('videos/video.mp4')}}" type="video/mp4">
                        <source src="{{URL::asset('videos/video.webm')}}" type="video/webm">
                    </video>
                    <form role="form" class="sign-up-form"  action="{{route('login')}}" method="post">
                        {!! csrf_field() !!}
                        @if(session()->has('message'))
                        <div class="form-group col-md-offset-4 col-md-4 col-md-offset-4 wow fadeInDown">
                            <div class="alert alert-danger" role="alert">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <ul>
                                    <li><strong>Login Unsuccessful!</strong></li>
                                    @foreach(session('message') as $mess)
                                    <li>{{$mess}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @endif
                        <div class="form-group col-md-offset-4 col-md-4 col-md-offset-4 wow fadeInDown">

                            <input type="text"  name="username" required class="form-control" id="username1" placeholder="{{$username or 'Enter Username'}}">

                        </div>
                        <div class="form-group col-md-offset-4 col-md-4 col-md-offset-4 wow fadeInDown" data-wow-delay=".7s">
                            <input type="password" name="password" required class="form-control" id="username2" placeholder="{{$password or 'Enter Password'}}">
                            <button type="submit" class="btn btn-default" id="login">Login</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Hero Area Section End-->



<!-- feature Section -->
<section id="use">
    <section id="key-features" style="height:650px;">
        <div class="container text-center">
            <div class="row" style="margin-top: 150px">
                <h1 class="title-small wow tada">How to use?</h1>
                <h2 class="subtitle-small wow fadeInLeft" data-wow-delay=".5s">it's very easy</h2>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 wow fadeInUp" data-wow-delay=".5s">
                    <div class="feature-item">
                        <i class="fa fa-truck fa-4x"></i>
                        <h3>First</h3>
                        <p>Put your items in warehouse</p>
                    </div>
                </div>


                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 wow fadeInUp" data-wow-delay=".8s">
                    <div class="feature-item">
                        <i class="fa fa-check fa-4x"></i>
                        <h3>Second</h3>
                        <p>Create transactions through advices or orders</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 wow fadeInUp" data-wow-delay="1.2s">
                    <div class="feature-item">
                        <i class="fa fa-desktop fa-4x"></i>
                        <h3>Third</h3>
                        <p>Now you can Monitor your warehouse</p>
                    </div>
                </div>

            </div>
        </div>
    </section>
</section>
<!-- feature Section End -->

<!-- feature Section Start -->
<section id="mobile">
    <section id="features" style="height:650px;">
        <div class="container">
            <div class="row">
                <h1 class="title-small wow tada">Mobile App</h1>
                <div class="col-md-12 text-center">
                    <a class="btn btn-border btn-lg wow fadeInLeft animated" data-wow-offset="10"><i class="fa fa-android"></i> Android</a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="content-left text-right wow fadeInLeft animated" data-wow-offset="10">
                        <div class="box-item left">
              <span class="icon" style="background-color:#3D3F67;">
                <i class="fa fa-bell" style="font-size: 2em; color:white;"></i>
              </span>
                            <div class="text">
                                <h4>Notifications</h4>
                                <p>Notifications about items missing, missplaced items and items shortage in the warehouse zones.</p>
                            </div>
                        </div>
                        <div class="box-item left">
              <span class="icon" style="background-color:#3D3F67;">
                <i class="fa fa-map-marker" style="font-size: 2.5em; color:white;"></i>
              </span>
                            <div class="text">
                                <h4>Locations</h4>
                                <p>Items locations in the warehouse, </br>and show zones assigned to the users.</p>
                            </div>
                        </div>
                        <div class="box-item left">
              <span class="icon" style="background-color:#3D3F67;">
                <i class="fa fa-book" style="font-size: 2em; color:white;"></i>
              </span>
                            <div class="text">
                                <h4>Forklift Tutorial</h4>
                                <p>Help the workers know how to use the forklift.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="show-box wow fadeInDown animated" data-wow-offset="10">
                        <img src="{{URL::asset('images/features/android.png')}}" style=" height: 440px;margin-left:55px;"alt="">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="content-right text-left wow fadeInRight animated" data-wow-offset="10">
                        <div class="box-item right">
              <span class="icon" style="background-color:#3D3F67;">
                <i class="fa fa-file-text-o" style="font-size: 2em; color:white;"></i>
              </span>
                            <div class="text">
                                <h4>Report</h4>
                                <p>Generating reports about the status of the items. These reports are sent to the owner as Notifications.</p>
                            </div>
                        </div>
                        <div class="box-item right">
              <span class="icon" style="background-color:#3D3F67;">
                <i class="fa fa-barcode" style="font-size: 2em; color:white;"></i>
              </span>
                            <div class="text">
                                <h4>Scan</h4>
                                <p>Using RFID to scan unitloads at goods-in to locate the items inside the zones.</p>
                            </div>
                        </div>
                        <div class="box-item right">
              <span class="icon" style="background-color:#3D3F67;">
                <i class="fa fa-book" style="font-size: 2em; color:white;"></i>
              </span>
                            <div class="text">
                                <h4>Picking Tutorial</h4>
                                <p>Help the workers know the steps of picking the items.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
<!-- Features Section Start -->

<!-- feature-list Section -->
<section id="about">
    <section id="main-features">
        <div id="feature-list-item-1" style="height:467px;">
            <div class="container">
                <div class="row" style="margin-top: 100px;">
                    <div class="col-md-6">
                        <h1 class="title-small wow fadeInLeft" data-wow-offset="10">What is WMS ?</h1>
                        <h2 class="subtitle-small wow fadeInUp" data-wow-delay=".8s" data-wow-offset="10">WMS++ is a warehouse managment system as a service project and is a modular framework for the creation of Warehouse Management Systems. {{-- <br> <a class="btn btn-default btn-lg" href="#">Learn More</a> --}} </h2>
                    </div>
                    <div class="col-md-6 wow fadeInRight" data-wow-delay=".5s" data-wow-offset="10">
                        <img class="pull-right" src="{{URL::asset('images/features/graph.png')}}">
                    </div>
                </div>
            </div>
        </div>

        <!--     <div id="feature-list-item-2">
              <div class="container">
                <div class="row">
                  <div class="col-md-6 wow fadeInLeft" data-wow-delay=".5s" data-wow-offset="10">
                    <img src="assets/img/features/graph.png">
                  </div>
                  <div class="col-md-6">
                    <h1 class="title-small wow fadeInRight" data-wow-offset="10">Lorem Ipsum is simply</h1>
                    <h2 class="subtitle-small wow fadeInUp" data-wow-delay=".8s" data-wow-offset="10">Lorem Ipsum is simply dummy text of the printing and typesetting industry. <br> <a class="btn btn-default btn-lg" href="#">Learn More</a> </h2>
                  </div>
                </div>
              </div>
            </div> -->

        <!--     <div id="feature-list-item-3">
              <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <h1 class="title-small wow fadeInLeft" data-wow-offset="10">Lorem Ipsum is simply</h1>
                    <h2 class="subtitle-small wow fadeInUp" data-wow-delay=".8s" data-wow-offset="10">Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br><a class="btn btn-default btn-lg" href="#">Learn More</a> </h2>
                  </div>
                  <div class="col-md-6 wow fadeInRight" data-wow-delay=".5s" data-wow-offset="10">
                    <img class="pull-right" src="assets/img/features/graph.png">
                  </div>
                </div>
              </div>
            </div> -->
        <!-- <div id="feature-list-item-4">
          <div class="container">
            <div class="row">
              <div class="col-md-6 wow fadeInLeft" data-wow-delay=".5s" data-wow-offset="10">
                <img src="assets/img/features/graph.png">
              </div>
              <div class="col-md-6">
                <h1 class="title-small wow fadeInRight" data-wow-offset="10">Lorem Ipsum is simply</h1>
                <h2 class="subtitle-small wow fadeInUp" data-wow-delay=".8s" data-wow-offset="10">Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br><a class="btn btn-default btn-lg" href="#">Learn More</a> </h2>
              </div>
            </div>
          </div>
        </div> -->
    </section>
</section>
<!-- feature-list Section End -->

<section id="bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12">
                <h3>Products</h3>
                {{-- <ul>
                  <li><a href="#">Paypal</a>
                  </li>
                  <li><a href="#">BitCoin</a>
                  </li>
                  <li><a href="#">Skrill</a>
                  </li>
                  <li><a href="#">Alertpay</a>
                  </li>
                  <li><a href="#">Payoneer</a>
                  </li>
                </ul> --}}
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <h3>FAQs</h3>
                {{-- <ul>
                  <li><a href="#">Why choose us?</a>
                  </li>
                  <li><a href="#">Where we are?</a>
                  </li>
                  <li><a href="#">Fees</a>
                  </li>
                  <li><a href="#">Guarantee</a>
                  </li>
                  <li><a href="#">Discount</a>
                  </li>
                </ul> --}}
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <h3>About</h3>
                {{-- <ul>
                  <li><a href="#">Career</a>
                  </li>
                  <li><a href="#">Partners</a>
                  </li>
                  <li><a href="#">Team</a>
                  </li>
                  <li><a href="#">Clients</a>
                  </li>
                  <li><a href="#">Contact</a>
                  </li>
                </ul> --}}
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <h3>Find us on</h3>
                <a class="social" href="#" target="_blank"><i class="fa fa-facebook fa-2x"></i></a>
                <a class="social" href="#" target="_blank"><i class="fa fa-twitter fa-2x"></i></a>
                <a class="social" href="#" target="_blank"><i class="fa fa-google-plus fa-2x"></i></a>
                <a class="social" href="#" target="_blank"><i class="fa fa-linkedin fa-2x"></i></a>
            </div>
        </div>
    </div>
</section>
<!-- Bootstrap JS -->

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- WOW JS plugin for animation -->

<script src="{{URL::asset('js/wow.js')}}"></script>
<!-- All JS plugin Triggers -->

<script src="{{URL::asset('js/main.js')}}"></script>


<script src="{{URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>

<script src="js/scrollnavigation/jquery.easing.min.js"></script>
<script src="js/scrollnavigation/scrolling-nav.js"></script>

<script>
    function showless() {
        //alert('i am here');
        document.getElementById("message-box-success").style.display="none";
    }
</script>





</body>
</html>