var numofCols =14;
function addDataToTable(transactions)
{
    var reportsTable = $('#report-table').DataTable();
     $('#report-table').dataTable().fnClearTable();
    for(var i=0;i<numofCols;i++)
    {
        $('#select'+i).empty().selectpicker('refresh');
    }
    for(var i=0;i<transactions.length;i++)
    {
        var counter =0;
         var rowData = new Array();

        rowData.push(transactions[i]['item']);
        $('#select'+counter).append( '<option value="'+transactions[i]['item']+'">'+transactions[i]['item']+'</option>').selectpicker('refresh');
        counter ++;

        rowData.push(transactions[i]['sku']);
        $('#select'+counter).append( '<option value="'+transactions[i]['sku']+'">'+transactions[i]['sku']+'</option>').selectpicker('refresh');
        counter++;
        
        rowData.push(transactions[i]['amount']);
        $('#select'+counter).append( '<option value="'+transactions[i]['amount']+'">'+transactions[i]['amount']+'</option>').selectpicker('refresh');
        counter++;

        rowData.push(transactions[i]['order']);
        $('#select'+counter).append( '<option value="'+transactions[i]['order']+'">'+transactions[i]['order']+'</option>').selectpicker('refresh');
        counter++;

        rowData.push(transactions[i]['type']);
        $('#select'+counter).append( '<option value="'+transactions[i]['type']+'">'+transactions[i]['type']+'</option>').selectpicker('refresh');
        counter++;

        rowData.push(transactions[i]['client']);
        $('#select'+counter).append( '<option value="'+transactions[i]['client']+'">'+transactions[i]['client']+'</option>').selectpicker('refresh');
        counter++;

        rowData.push(transactions[i]['site']);
        $('#select'+counter).append( '<option value="'+transactions[i]['site']+'">'+transactions[i]['site']+'</option>').selectpicker('refresh');
        counter++;

        rowData.push(transactions[i]['receiving_start']);
        $('#select'+counter).append( '<option value="'+transactions[i]['receiving_start']+'">'+transactions[i]['receiving_start']+'</option>').selectpicker('refresh');
        counter++;

        rowData.push(transactions[i]['receiving_end']);
        $('#select'+counter).append( '<option value="'+transactions[i]['receiving_end']+'">'+transactions[i]['receiving_end']+'</option>').selectpicker('refresh');
        counter++;

        rowData.push(transactions[i]['treated']);
        $('#select'+counter).append( '<option value="'+transactions[i]['treated']+'">'+transactions[i]['treated']+'</option>').selectpicker('refresh');
        counter++;

        rowData.push(transactions[i]['picking_start']);
        $('#select'+counter).append( '<option value="'+transactions[i]['picking_start']+'">'+transactions[i]['picking_start']+'</option>').selectpicker('refresh');
        counter++;

        rowData.push(transactions[i]['picking_end']);
        $('#select'+counter).append( '<option value="'+transactions[i]['picking_end']+'">'+transactions[i]['picking_end']+'</option>').selectpicker('refresh');
        counter++;

        rowData.push(transactions[i]['shipping']);
        $('#select'+counter).append( '<option value="'+transactions[i]['shipping']+'">'+transactions[i]['shipping']+'</option>').selectpicker('refresh');
        counter++;

        rowData.push(transactions[i]['completed']);
        $('#select'+counter).append( '<option value="'+transactions[i]['completed']+'">'+transactions[i]['completed']+'</option>').selectpicker('refresh');
        counter++;
         reportsTable.row.add(rowData).draw();
        
        
    }
    // $('#report-table').dataTable().fnSettings().oScroll.sX = true;
    $('#report-panel').show();
    
}

$(document).ready(function() {
    var utc = new Date().toJSON().slice(0,10);
    $('#report-table').DataTable( {
         dom: 'Bfrtip',
        buttons: [
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                title:'Transactions Report ['+utc+']',
                message:'Powered By Micro Engineering roboVics'
            }
        ],
        initComplete: function () {
            var counter =0;

            this.api().columns().every( function () {
                var column = this;
                var select = $('<select class="form-control selectpicker show-tick" title="Pick Something Please!" data-live-search="true"id = "select'+counter+'"></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
                counter++;
            } );
        }//,
        //"scrollX": true
        });    
    //alert('hi');
    $("#transactiongenerate-form").submit(function(e) {
        //alert('hi');
         $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
         var sentdata = $("#transactiongenerate-form").serialize();
         if($('#transactiongenerate-form').valid())
         {
           $.ajax({
                url: "reports/gettransactionreport",
                type: "post",
                data: sentdata ,
                success: function (response) {
                   if(response.error==true){
                    
                   }
                   else
                   {
                     var transactions = response.transactions;
                     addDataToTable(transactions);
                   }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                   
                }


            });
        }
      e.preventDefault();
  });
    
});