
$(document).ready(function(){

	// Validation Rules For Register Warehouse Form
	var jvalidate = $("#register-form").validate({
	    ignore: [],
	    rules: {                                            
	        firstname:
	        {
	            required: true
	        },
	        lastname:
	        {
	            required: true
	        },
	        username:
	        {
	            required: true
	        },
	        password:
	        {
	            required: true
	        },
	        email:
	        {
	            required: true,
	            email:true
	        },
	        warehouseName:
	        {
	            required: true
	        }

	    }                                        
	});

});