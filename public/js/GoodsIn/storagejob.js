/**
 * Created by Khaled on 8/14/2016.
 */
function message(response,status)
{
    var list = "<ul>";
      for(var x=0 ;x<response.message.length;x++)
      {
        list += '<li>'+response.message[x]+'</li>';
      }
      list += "</ul>";
      document.getElementById('message').innerHTML=
      '<div class="alert alert-'+status+'" role="alert">'+
          '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
          +'<strong>'+list+'</strong>'
      +'</div>';
}

$(document).ready(function(){
    var Strategy;
    var Unitload;
    var Location;
    var STJ_name;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#SITE_name').on('change', function(){
        $('#ZONE_name').empty().selectpicker('refresh');
        $('#RACK_name').empty().selectpicker('refresh');
        $.ajax({
          url: "../unitload/getzones",
          type: "post",
          data: {'SITE_name':$("#SITE_name").val()} ,
          success: function (response) {
              if(response.error == false)
              {
                $selecthtml ='';
                  for(var x in response.data)
                  {
                    var zone = response.data[x];
                    //alert(customer);
                    str = zone.replace(/\s+/g, '');
                    $selecthtml = $selecthtml+'<option id="'+str+'">'+zone+'</option>';
                  }
                  $('#ZONE_name').html($selecthtml).selectpicker('refresh');
              }
          },
          error: function(jqXHR, textStatus, errorThrown) {
          }
        });

    });
     $('#ZONE_name').on('change', function(){
        $('#RACK_name').empty().selectpicker('refresh');
        $.ajax({
          url: "../unitload/getracks",
          type: "post",
          data: {'SITE_name':$("#SITE_name").val(),
                'ZONE_name':$('#ZONE_name').val()} ,
          success: function (response) {
              if(response.error == false)
              {
                $selecthtml ='';
                  for(var x in response.data)
                  {
                    var zone = response.data[x];
                    //alert(customer);
                    str = zone.replace(/\s+/g, '');
                    $selecthtml = $selecthtml+'<option id="'+str+'">'+zone+'</option>';
                  }
                  $('#RACK_name').html($selecthtml).selectpicker('refresh');
              }
          },
          error: function(jqXHR, textStatus, errorThrown) {
          }
        });

    });
    $('#RACK_name').on('change', function(){
        if($('#strategy_name').val() != null && $('#strategy_name').val() != '')
        {
            $.ajax({
                url:"stjstrategy",
                type:"POST",
                data: {
                    UL_name:$('#UL_name').val(),
                    STRAT_name: $('#strategy_name').val(),
                    RACK_name:$('#RACK_name').val(),
                    ZONE_name:$('#ZONE_name').val(),
                    SITE_name:$('#SITE_name').val()
                    },
                success:function(response){
                        if(response.error == true)
                    {
                        message(response,'danger');
                    }
                    else
                    {
                        response.data.forEach(function(location){
                            $('#STL_name').append($('<option>',{
                                value: location.name,
                                text: location.name
                            }));
                            Location = location;
                            $('#STL_name').selectpicker('refresh');

                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    
                    }
                });
        }
        else
        {
            $.ajax({
              url: "../unitload/getlocations",
              type: "post",
              data: {'SITE_name':$("#SITE_name").val(),
                    'ZONE_name':$('#ZONE_name').val(),
                    'RACK_name':$('#RACK_name').val(),
                    'type':'storage'},
              success: function (response) {
                  if(response.error == false)
                  {
                    $selecthtml ='';
                      for(var x in response.data)
                      {
                        var zone = response.data[x];
                        //alert(customer);
                        str = zone.replace(/\s+/g, '');
                        $selecthtml = $selecthtml+'<option id="'+str+'">'+zone+'</option>';
                      }
                      $('#STL_name').html($selecthtml).selectpicker('refresh');
                  }
              },
              error: function(jqXHR, textStatus, errorThrown) {
              }
            });
        }
        
    });
    $.ajax({
        url:"stjname",
        type: "GET",
        success:function(response)
        {
            STJ_name = response[0];
        }
    });
    $.ajax({
        url: "stjdata",
        type: "GET",
        success: function(response)
        {
            response.strategy_names.forEach(function(strategy){
                $('#strategy_name').append($('<option>',{
                    value: strategy,
                    text: strategy
                }));
                Strategy = strategy;
                $('#strategy_name').selectpicker('refresh');
            });

            response.goodsinUnitload_names.forEach(function(unitload){
                $('#UL_name').append($('<option>',{
                    value: unitload.name,
                    text: unitload.name+' | '+unitload.route+' |'
                }));
                Unitload = unitload;
                $('#UL_name').selectpicker('refresh');
            });

            response.site_names.forEach(function(site){
                $('#SITE_name').append($('<option>',{
                    value: site,
                    text: site
                }));
                Location = location;
                $('#SITE_name').selectpicker('refresh');
            });
        }
    });




    $('#strategy_name').on('change',function(){
        // Strategy = this.value;
        // if($('#UL_name').val() != null && $('#UL_name').val() != '')
        // {
        //     $('#STL_name').empty();
        //     $.ajax({
        //         url:"stjstrategy",
        //         type:"POST",
        //         data: {
        //             UL_name:$('#UL_name').val(),
        //             STRAT_name: $('#strategy_name').val()},
        //         success:function(response){
        //                 response.forEach(function(location){
        //                 $('#STL_name').append($('<option>',{
        //                     value: location.name,
        //                     text: location.name+' | '+location.route+' |'
        //                 }));
        //                 Location = location;
        //                 $('#STL_name').selectpicker('refresh');
        //             });
        //         },
        //         error: function(jqXHR, textStatus, errorThrown) {
                    
        //         }
        //     });
        $('#SITE_name').empty().selectpicker('refresh');
        $('#ZONE_name').empty().selectpicker('refresh');
        $('#RACK_name').empty().selectpicker('refresh');
        $('#STL_name').empty().selectpicker('refresh');
        $.ajax({
        url: "stjdata",
        type: "GET",
        success: function(response)
        {
            response.site_names.forEach(function(site){
                $('#SITE_name').append($('<option>',{
                    value: site,
                    text: site
                }));
                Location = location;
                $('#SITE_name').selectpicker('refresh');
            });
        }
        });
            // $('#UL_name').empty();
        // $.ajax({
        //     url: "stjdata",
        //     type: "GET",
        //     success: function(response)
        //     {
        //         response.goodsinUnitload_names.forEach(function(unitload){
        //             $('#UL_name').append($('<option>',{
        //                 value: unitload,
        //                 text: unitload
        //             }));
        //             Unitload = unitload;
        //             $('#UL_name').selectpicker('refresh');
        //         });
        //     }
        // });
    });


    $('#UL_name').on('change',function(){
        Unitload = this.value;
        if($('#strategy_name').val() != null && $('#strategy_name').val() != '')
        {
            $('#STL_name').empty();
            $.ajax({
                url:"stjstrategy",
                type:"POST",
                data: {
                    UL_name:Unitload,
                    STRAT_name: $('#strategy_name').val()},
                success:function(response){
                    if(response.error == true)
                    {
                        message(response,'danger');
                    }
                    else
                    {
                        response.data.forEach(function(location){
                            $('#STL_name').append($('<option>',{
                                value: location.name,
                                text: location.name
                            }));
                            Location = location;
                            $('#STL_name').selectpicker('refresh');

                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    
                }
            });
        }
    });


        $("#storagejob-form").submit(function(e)
        {
            $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
              });
            $('#STJ_name').val(STJ_name);
             var sentdata = $("#storagejob-form").serialize();
             if($('#storagejob-form').valid())
             {
                //alert('hi');
               $.ajax({
                    url: "submit",
                    type: "post",
                    data: sentdata ,
                    success: function (response) {
                       if(response.error == true)
                        {
                            message(response,'warning');
                        }
                        else
                        {
                          $('.message-box-success').addClass('open');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                       
                    }


                });
            }
            //alert('hi');
              e.preventDefault();
        });
});