function timelinehtmlnew(item,to,status)
{
    var key = item+to;
    key = key.replace(" ","");
    if(status=='invalid')
    {
        return  '<tr>'
                    +'<td><strong><a href=\'#\' id ="'+key+'">'+item+'</a></strong></td>'
                    +'<td><span class="label label-warning">New Missplaced item</span></td>'
                    +'<td><strong>'+to+'</strong></td>'
                    +'<td><strong>'+new Date().toLocaleString()+'</strong></td>'
                +'</tr>'
    }
    else
    {
        return  '<tr>'
                    +'<td><strong><a href=\'#\'id ="'+key+'">'+item+'</a></strong></td>'
                    +'<td><span class="label label-success">New item</span></td>'
                    +'<td><strong>'+to+'</strong></td>'
                    +'<td><strong>'+new Date().toLocaleString()+'</strong></td>'
                +'</tr>'
    }
     
}
function timelinehtmltransfer(item,to,fromlocation,status)
{
    var key = item+to;
    key = key.replace(" ","");
	 if(status=='invalid')
    {
        return  '<tr>'
                    +'<td><strong><a href=\'#\'id ="'+key+'">'+item+'</a></strong></td>'
                    +'<td><span class="label label-warning">Transfered Missplaced item</span></td>'
                    +'<td><strong>'+to+'</strong></td>'
                    +'<td><strong>'+new Date().toLocaleString()+'</strong></td>'
                +'</tr>'
    }
    else
    {
        return  '<tr>'
                    +'<td><strong><a href=\'#\'id ="'+key+'">'+item+'</a></strong></td>'
                    +'<td><span class="label label-success">Transfer Item</span></td>'
                    +'<td><strong>'+to+'</strong></td>'
                    +'<td><strong>'+new Date().toLocaleString()+'</strong></td>'
                +'</tr>'
    }
}
function mainTimelineHtml()
{
    return '<div class="panel panel-default">'
                        +'<div class="panel-heading">'
                            +'<div class="panel-title-box">'
                                +'<h3>Timeline</h3>'
                                +'<span>Timeline of Assets Activity</span>'
                            +'</div>'                                    
                            +'<ul class="panel-controls" style="margin-top: 2px;">'
                                +'<li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>'
                                +'<li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>'
                                +'<li class="dropdown">'
                                    +'<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>'                                        
                                +'<ul class="dropdown-menu">'
                                +'<li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>'
                                +'<li><a href="#" class="panel-remove"><span class="fa fa-times"></span> Remove</a></li>'
                            +'</ul>'                                        
                        +'</div>'
                        +'<div class="panel-body panel-body-table">'
                            +'<div class="table-responsive">'
                                +'<table class="table table-bordered table-striped">'
                                    +'<thead>'
                                        +'<tr>'
                                            +'<th width="20%">Item Name</th>'
                                            +'<th width="20%">Status</th>'
                                            +'<th width="30%">location</th>'
                                            +'<th width="30%">Time</th>'
                                        +'</tr>'
                                    +'</thead>'
                                    +'<tbody id ="timelinediv">'
                                        // +'<tr>'
                                        //     +'<td><strong><a href=\'#\'>item1</a></strong></td>'
                                        //     +'<td><span class="label label-warning">Missplaced</span></td>'
                                        //     +'<td><strong>A1-011-2</strong></td>'
                                        //     +'<td><strong>'+new Date().toLocaleString()+'</strong></td>'
                                        // +'</tr>'
                                        // +'<tr>'
                                        //     +'<td><strong><a href=\'#\'>item2</a></strong></td>'
                                        //     +'<td><span class="label label-success">New</span></td>'
                                        //     +'<td><strong>A1-021-2</strong></td>'
                                        //     +'<td><strong>2016-06-06</strong></td>'
                                        // +'</tr>'
                                        // +'<tr>'
                                        //     +'<td><strong><a href=\'#\'>item1</a></strong></td>'
                                        //     +'<td><span class="label label-success">Transfered</span></td>'
                                        //     +'<td><strong>A1-011-2</strong></td>'
                                        //     +'<td><strong>2016-06-06</strong></td>'
                                        // +'</tr>'
                                        // +'<tr>'
                                        //     +'<td><strong><a href=\'#\'>item1</a></strong></td>'
                                        //     +'<td><span class="label label-danger">Missing</span></td>'
                                        //     +'<td><strong>A1-011-2</strong></td>'
                                        //     +'<td><strong>2016-06-06</strong></td>'
                                        // +'</tr>'
                                    +'</tbody>'
                                +'</table>'
                            +'</div>'
                        +'</div>'
                    +'</div>';
}