function drawZones(svgsnap,width,height,margin,marginRack,shift,shiftdown,data)
{
	var zoneSingleDirection = Math.ceil(Math.sqrt(data.zones.length));
	site =data.site.SITE_name;

	var stepX = Math.floor(width/zoneSingleDirection);
	var stepY = Math.floor(height/zoneSingleDirection);
	var noZones =data.zones.length;
	var marginX= Math.floor(margin*stepX);
	var marginY= Math.floor(margin*stepY);
	var scaleX = Math.floor(stepX*(1-(2*margin)));
	var scaleY = Math.floor(stepY*(1-(2*margin)));
	
	mainpageinfo.push({
		stepX:stepX,
		stepY:stepY,
		zoneSingleDirection:zoneSingleDirection,
		marginX:marginX,
		marginY:marginY,
		scaleX:scaleX,
		scaleY:scaleY,
		margin:margin,
		width:width,
		shift:shift
	});
	var flagy = 0;
	if(noZones%zoneSingleDirection != 0)
		flagy = 1;
	var zonesInY = (Math.floor(noZones/zoneSingleDirection) + flagy)*stepY;
	
	var goodsinouttexture = svgsnap.select('#goods-in-out-texture');
	var plusSign = svgsnap.select("#plus-sign");
	var minusSign = svgsnap.select("#minus-sign");
	var q = 0;
	var carid= 0;
	var zonetexture = svgsnap.select("#zonetexture");
	var rackstexture_frontleft = svgsnap.select("#rackstexture_frontleft");
	var rackstexture_frontfull = svgsnap.select("#rackstexture_frontfull");
	for(var y=0;y<(height-scaleY-marginY+1) && noZones>0;y+=stepY)
	{
		var index = 0;
		for(var x=0;x<(width-scaleX-marginX+1) && noZones>0;x+=stepX)
		{
			var rectangle = zonetexture.clone();
			rectangle.attr({
				viewBox:"0, 0, 1720, 860",
				visibility:"visible",
				width:scaleX,
				height:scaleY,
				x:(x+marginX),
				y:(y+marginY+shift),
			});
			var zonename = svgsnap.text(x+(scaleX/2),y+marginY+shift+scaleY+(0.2*scaleY),data.zones[q].ZONE_name);
			zonename.attr({
				fill:'#FF8300',
				'font-size':(height/(zoneSingleDirection*6))+'px',
				'font-family': "Times New Roman"
			});
			

			
			
			rectangle.attr({id:"zone"+q});
			
			var zonePlusSign = plusSign.clone();
			zonePlusSign.attr({
				id:"expandZone"+q,
				viewBox:"0, 0, 32, 32",
				visibility:"visible",
				width:Math.floor((16*8)/zoneSingleDirection),
				x:x+marginX+scaleX,
				y:y+marginY+shift-3,
				class:"zone"+data.zones[q].ZONE_name+"-plus"
			});
			var zoneMinusSign = plusSign.clone();
			zoneMinusSign.attr({
				id:"contractZone"+q,
				viewBox:"0, 0, 32, 32",
				visibility:"hidden",
				width:32,
				x:width-marginX,
				y:marginY+shift

			});

			var noRacks = data.zones[q]["plus_rack"].length;
			var rackwidthtoheightratio  = 42/200;
			var removeborder = (4*(6*(scaleX/496)));
			
			var scalerackX = Math.floor((scaleX-removeborder)/(noRacks+((noRacks-1)*marginRack)));
			var scalerackY = scalerackX*(1/rackwidthtoheightratio);
			if(scalerackY>(scaleY-(removeborder)))
			{
				scalerackY = (scaleY-(2*marginRack*scaleY));
				scalerackX = (scalerackY*rackwidthtoheightratio);
			}
			var marginrackX = (scaleX-removeborder-(noRacks*scalerackX))/(noRacks+1);
			var marginrackY = (scaleY-scalerackY)/2;
			var steprackX=(scalerackX+marginrackX);
			// var marginrackX = steprackX*marginRack; 
			// var marginrackY = 0.1*scaleY;
			// var scalerackX = (1-(2*marginRack))*steprackX;
			// var scalerackY =(1-(2*marginRack))*scaleY;
			var zoneracks = new Array();
			var zoneracksGroup = new Array();
			var zonerackcoordinates = new Array();
			racktoptexture = svgsnap.select("#rack-top-view");
			for(var i=0;i<noRacks;i++)
			{
				var rack = racktoptexture.clone();
				rack.attr({
					id: "rack"+i+"-"+q,
					viewBox:"0 0 236.958 1296.13",
					x:(x+marginX+marginrackX+(i*(steprackX))+(removeborder/2)),
					y:(y+marginY+marginrackY+shift),
					width:scalerackX,
					height:scalerackY,
					visibility:"visible",
					title:"rack"+i,
					class:".tooltip "+data.zones[q]["plus_rack"][i].RACK_name.replace(" ","-")
				});


				// $('#rack'+i+"-"+q).tooltipster({
				// 		theme:'tooltipster-punk',
				// 		content: $('<strong>'+data.zones[q]["plus_rack"][i].RACK_name+'</strong><br>.No of assets:'
				// 			+data.zones[q]["plus_rack"][i].noOfAssets
				// 			+'<br>.No of locations:'+data.zones[q]["plus_rack"][i].noLocations
				// 			+'<br>'),
				// 		hideOnClick:true,
				// 		interactive:true,
				// 		animation:'grow'
				// 	});
				var xcoordinate =(x+marginX+marginrackX+(i*(steprackX))+(removeborder/2));
				var rackpath = svgsnap.path(Snap.format("M{x},{y}h{dim.width}v{dim.height}h{dim['negative width']}z", {
				    x: xcoordinate,
				    y: (y+marginY+marginrackY+shift),
				    dim: {
				        width: scalerackX,
				        height: scalerackY,
				        "negative width": -scalerackX
				    }
				}));
				rackpath.attr({
					fill:'none',
					visibility:'hidden',
					'stroke-width':2,
					'stroke':"#00FF00"
				});
				var rackcoordinates =({
					"rackname":data.zones[q]["plus_rack"][i].RACK_name,
					"rackpath":rackpath,
					"rackpathstatus":"off",
					"x":(x+marginX+marginrackX+(i*(steprackX))+(removeborder/2)),
					"y":(y+marginY+marginrackY+shift),
					"width":scalerackX,
					"height":scalerackY,
					"centerx":(x+marginX+marginrackX+(i*(steprackX))+(removeborder/2))+(scalerackX/2),
					"centery":(y+marginY+marginrackY+shift)+(scalerackY/2)
				});
				var rackGroup = svgsnap.group();
				rackGroup.append(rack);
				zoneracks.push(rack);
				zonerackcoordinates.push(rackcoordinates);
				zoneracksGroup.push(rackGroup);
			}
			var zoneGroup = svgsnap.group();
			zoneGroup.append(rectangle);
			zoneExpand.push({
				"zone":rectangle,
				"zoneGroup":zoneGroup,
				"zonename":data.zones[q].ZONE_name,
				"zoneplus":zonePlusSign,
				"zoneminus":zoneMinusSign,
				"zonetext":zonename,
				"zoneracks":zoneracks,
				"zoneracksGroup":zoneracksGroup,
				"zonerackcoordinates":zonerackcoordinates
			});
			noZones--;
			q++;
			index++;
		}

	}
	for(var i=0;i<zoneExpand.length;i++)
	{
		// zoneExpand[i].click(function(i){
		// 	  return function() { alert(i); }
		// })(zoneExpand[i]);
		for(var j=0;j<zoneExpand[i]["zoneracks"].length;j++)
		{

			$("#rack"+j+"-"+i).click(function(d){
				$('#sitename').attr('disabled',true);
				svgsnap.zpd('destroy');
				 //set_settings(theme_settings,false,svgsnap);
				 
				var id = this.id;
				var zoneid = parseInt(id.replace("rack","").split("-")[1]);
				var rackid = parseInt(id.replace("rack","").split("-")[0]);
				currentpage=zoneExpand[zoneid]["zonerackcoordinates"][rackid]['rackname'];
				// $("#restore-origin").click();
				 $.ajax({
          url: "getunitloadlocation",
          type: "post",
          data:{'zonename':zoneExpand[zoneid]['zonename'],
          'rackname':zoneExpand[zoneid]["zonerackcoordinates"][rackid]['rackname']
          ,'sitename':site},
          success: function (response) {
          
				for(var disappearzone=0;disappearzone<zoneExpand.length;disappearzone++)
				{
					if(zoneid != disappearzone)
					{
						zoneExpand[disappearzone]["zone"].attr(
						{
							visibility:"hidden"
						});
						zoneExpand[disappearzone]["zoneplus"].attr(
						{
							visibility:"hidden"
						});
						zoneExpand[disappearzone]["zoneminus"].attr(
						{
							visibility:"hidden"
						});
						zoneExpand[disappearzone]["zonetext"].attr(
						{
							visibility:"hidden"
						});
						for(var disappearrack=0;disappearrack<zoneExpand[disappearzone]["zoneracks"].length;disappearrack++)
						{
							zoneExpand[disappearzone]["zoneracks"][disappearrack].attr(
							{
								visibility:"hidden"
							});
							zoneExpand[disappearzone]["zonerackcoordinates"][disappearrack]['rackpath'].attr(
							{
								visibility:"hidden"
							});
						}
					}
					else
					{
						zoneExpand[zoneid]["zoneplus"].attr(
						{
							visibility:"hidden"
						});
						zoneExpand[zoneid]["zoneminus"].attr(
						{
							visibility:"hidden"
						});
						var carStartMatrix = new Snap.Matrix();
						var scalex= (1/(1-(2*margin)))*zoneSingleDirection*(1-((2*marginX)/width));
						carStartMatrix.scale(scalex,scalex,marginX,marginY+shift);
						carStartMatrix.translate(-((parseInt(zoneid)%zoneSingleDirection)*stepX),-((Math.floor(parseInt(zoneid)/zoneSingleDirection))*stepY));
						zoneExpand[disappearzone]["zoneGroup"].animate({transform:carStartMatrix}, animationduration, mina.easeinout,function()
						{
							zoneExpand[zoneid]["zone"].attr(
							{
								visibility:"hidden"
							});
							
							zoneExpand[zoneid]["zonetext"].attr(
							{
								visibility:"hidden"
							});
							for(var disappearrackcounter=0;disappearrackcounter<zoneExpand[zoneid]["zoneracks"].length;disappearrackcounter++)
							{
								if(disappearrackcounter != rackid)
								{
									zoneExpand[zoneid]["zoneracks"][disappearrackcounter].attr(
									{
										visibility:"hidden"
									});
									zoneExpand[zoneid]["zonerackcoordinates"][disappearrackcounter]['rackpath'].attr(
									{
										visibility:"hidden"
									});
								}
								else
								{
									zoneExpand[zoneid]["zonerackcoordinates"][disappearrackcounter]['rackpathstatus'] = 'off';
								}
							}
							//
							zoneExpand[zoneid]["zonerackcoordinates"][rackid]['rackpath'].animate({transform:rackRotate(zoneExpand,zoneid,rackid,height)}, animationduration, mina.easeinout,function(){
								zoneExpand[zoneid]["zonerackcoordinates"][rackid]['rackpath'].attr({
									visibility:'hidden'
								});
							});
							zoneExpand[zoneid]["zoneracksGroup"][rackid].animate({transform:rackRotate(zoneExpand,zoneid,rackid,height)}, animationduration, mina.easeinout,function(){
								zoneExpand[zoneid]["zoneracks"][rackid].attr({
									visibility:'hidden'
								});

								//rack info from DB
								var nofields = parseInt(data.zones[zoneid]["plus_rack"][rackid].RACK_column);
							 	var nolevels = parseInt(data.zones[zoneid]["plus_rack"][rackid].RACK_row);
							 	var rackname = data.zones[zoneid]["plus_rack"][rackid].RACK_name.replace("Pallet ","");
							 	var rackdrawing =drawRack(svgsnap,nolevels,nofields,rackname,width,height,rackstexture_frontfull,rackstexture_frontleft,response.data);
							 	var rackdeletebutton = plusSign.clone();
								rackdeletebutton.attr({
									id:"rackdelete",
									viewBox:"0, 0, 32, 32",
									visibility:"visible",
									width:32,
									x:width-48,
									y:0

								});
								$("#rackdelete").click(function(){
									svgsnap.zpd('destroy');
									// set_settings(theme_settings,false,svgsnap);
									 // enableCars();
									for(var rackelements=0;rackelements<rackdrawing.length;rackelements++)
									{
										
										rackdrawing[rackelements].remove();
									}
									rackdeletebutton.remove();
									zoneExpand[zoneid]["zoneracks"][rackid].attr({
									visibility:'visible'
									});

									
									
									var rackbiganimation = new Snap.Matrix();
									var scalex= (1/(1-(2*margin)))*zoneSingleDirection*(1-((2*marginX)/width));
									rackbiganimation.scale(scalex,scalex,marginX,marginY+shift);
									rackbiganimation.translate(-((parseInt(zoneid)%zoneSingleDirection)*stepX),-((Math.floor(parseInt(zoneid)/zoneSingleDirection))*stepY));
									var returnrackmatrix = new Snap.Matrix();
									returnrackmatrix.translate(0,0);
									returnrackmatrix.rotate(0);
									zoneExpand[zoneid]["zonerackcoordinates"][rackid]['rackpath'].animate({transform:rackbiganimation}, animationduration, mina.easeinout);
									zoneExpand[zoneid]["zoneracksGroup"][rackid].animate({transform:rackbiganimation}, animationduration, mina.easeinout,function(){
										for(var otherracks=0;otherracks<zoneExpand[zoneid]["zoneracks"].length;otherracks++)
										{
												if(zoneExpand[zoneid]["zonerackcoordinates"][otherracks]['rackpathstatus'] == 'on')
												{
													zoneExpand[zoneid]["zonerackcoordinates"][otherracks]['rackpath'].attr({
													visibility:'visible'
													});
												}
												zoneExpand[zoneid]["zoneracks"][otherracks].attr({visibility:'visible'});
												zoneExpand[zoneid]["zoneracksGroup"][otherracks].animate({transform:returnrackmatrix}, animationduration, mina.easeinout,function(){
												});
												zoneExpand[zoneid]["zonerackcoordinates"][otherracks]['rackpath'].animate({transform:returnrackmatrix}, animationduration, mina.easeinout,function(){
												});
										}
										zoneExpand[zoneid]["zone"].attr({visibility:'visible'});
										zoneExpand[zoneid]["zonetext"].attr({visibility:'visible'});
										zoneExpand[zoneid]["zoneGroup"].animate({transform:returnrackmatrix}, animationduration, mina.easeinout);
										zoneExpand[zoneid]["zonetext"].animate({transform:returnrackmatrix}, animationduration, mina.easeinout,function(){
											currentpage="mainpage";
											zoneExpand[zoneid]["zoneplus"].attr({visibility:'visible'});
											for(var otherzones=0;otherzones<zoneExpand.length;otherzones++)
											{
												if(otherzones!= zoneid)
												{
													zoneExpand[otherzones]["zone"].attr({visibility:'visible'});
													zoneExpand[otherzones]["zonetext"].attr({visibility:'visible'});
													zoneExpand[otherzones]["zoneplus"].attr({visibility:'visible'});
													for(var otherracks=0;otherracks<zoneExpand[otherzones]["zoneracks"].length;otherracks++)
													{
														zoneExpand[otherzones]["zoneracks"][otherracks].attr({visibility:'visible'});
														if(zoneExpand[otherzones]["zonerackcoordinates"][otherracks]['rackpathstatus'] == 'on')
														zoneExpand[otherzones]["zonerackcoordinates"][otherracks]['rackpath'].attr({visibility:'visible'});
													}
												}
												
											}
										});
									});
									$('#sitename').attr('disabled',false);
								});
								
							});

						});
						zoneExpand[disappearzone]["zonetext"].animate({transform:carStartMatrix}, animationduration, mina.easeinout);
						for(var disappearrack=0;disappearrack<zoneExpand[disappearzone]["zoneracks"].length;disappearrack++)
						{
							zoneExpand[disappearzone]["zoneracksGroup"][disappearrack].animate({transform:carStartMatrix}, animationduration, mina.easeinout);
							zoneExpand[disappearzone]["zonerackcoordinates"][disappearrack]['rackpath'].animate({transform:carStartMatrix}, animationduration, mina.easeinout);

						}
					}
					
				}
          },
          error: function(jqXHR, textStatus, errorThrown) {
             
          }


      		});
			});
		}
		$("#expandZone"+i).click(function(i){

			
			 svgsnap.zpd('destroy');
			 //set_settings(theme_settings,false,svgsnap);
			 
			var id = this.id;
			var zoneid =parseInt(id.replace("expandZone",""));
			currentpage = zoneExpand[zoneid]["zonename"];
			
			for(var z=0;z<zoneExpand.length;z++)
			{
				
				
				if(z != zoneid)
				{

					zoneExpand[z]["zone"].attr({
						visibility:"hidden"
					});
					zoneExpand[z]["zonetext"].attr({
						visibility:"hidden"
					});
					for(var j=0;j<zoneExpand[z]["zoneracks"].length;j++)
					{
						zoneExpand[z]["zoneracks"][j].attr({
							visibility:"hidden"
						});
						zoneExpand[z]["zonerackcoordinates"][j]['rackpath'].attr({
							visibility:"hidden"
						});
					}

				}
				else{
					var carStartMatrix = new Snap.Matrix();
					var scalex= (1/(1-(2*margin)))*zoneSingleDirection*(1-((2*marginX)/width));
					carStartMatrix.scale(scalex,scalex,marginX,marginY+shift);
					carStartMatrix.translate(-((parseInt(zoneid)%zoneSingleDirection)*stepX),-((Math.floor(parseInt(zoneid)/zoneSingleDirection))*stepY));
					zoneExpand[z]["zoneGroup"].animate({transform:carStartMatrix}, animationduration, mina.easeinout);
					zoneExpand[z]["zonetext"].animate({transform:carStartMatrix}, animationduration, mina.easeinout);
					for(var j=0;j<zoneExpand[z]["zoneracks"].length;j++)
					{
						zoneExpand[z]["zoneracksGroup"][j].animate({transform:carStartMatrix}, animationduration, mina.easeinout);
						zoneExpand[z]["zonerackcoordinates"][j]['rackpath'].animate({transform:carStartMatrix}, animationduration, mina.easeinout);
					}
					zoneExpand[z]["zoneminus"].attr({
						visibility:"visible"
					});
					
				}	
				zoneExpand[z]["zoneplus"].attr({
						visibility:"hidden"
					});

			}
			
		});
		

	}
	for(var d=0;d<zoneExpand.length;d++)
	{
		
		$("#contractZone"+d).click(function(d){
			 svgsnap.zpd('destroy');
			  //set_settings(theme_settings,false,svgsnap);
			 // enableCars();
			// alert("hi")
			var id = this.id;
			var zoneid =parseInt(id.replace("contractZone",""));
			
			
			for(var z=0;z<zoneExpand.length;z++)
			{
				
				
				if(z == zoneid){
					var carStartMatrix = new Snap.Matrix();
					var scalex= (1/(1-(2*margin)))*zoneSingleDirection*(1-((2*marginX)/width));
					carStartMatrix.scale(1,1,marginX,marginY+shift);
					carStartMatrix.translate(0,0);
					zoneExpand[z]["zoneGroup"].animate({transform:carStartMatrix}, animationduration, mina.easeinout,function(){
						currentpage="mainpage";
						for(var k =0 ;k<zoneExpand.length;k++ )
						{
							if(k != zoneid)
							{

								zoneExpand[k]["zone"].attr({
									visibility:"visible"
								});
								
								for(var g=0;g<zoneExpand[k]["zoneracks"].length;g++)
								{
									zoneExpand[k]["zoneracks"][g].attr({
										visibility:"visible"
									});
									if(zoneExpand[k]["zonerackcoordinates"][g]['rackpathstatus'] == 'on')
									zoneExpand[k]["zonerackcoordinates"][g]['rackpath'].attr({
										visibility:"visible"
									});
								}

							}
							zoneExpand[k]["zonetext"].attr({
									visibility:"visible"
								});
								zoneExpand[k]["zoneplus"].attr({
									visibility:"visible"
								});
						}
					});
					zoneExpand[z]["zonetext"].animate({transform:carStartMatrix}, animationduration, mina.easeinout);
					for(var j=0;j<zoneExpand[z]["zoneracks"].length;j++)
					{
						zoneExpand[z]["zoneracksGroup"][j].animate({transform:carStartMatrix}, animationduration, mina.easeinout);
						zoneExpand[z]["zonerackcoordinates"][j]['rackpath'].animate({transform:carStartMatrix}, animationduration, mina.easeinout);
					}
					
				}
				zoneExpand[z]["zoneminus"].attr({
						visibility:"hidden"
					});
				

			}

		});
	}	
}