
$(document).ready(function(){
    $('#locationlink').addClass('active');
    $('#racklink').addClass('active');
   $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
   $('#site').on('change', function(){
        $('#zone').empty().selectpicker('refresh');
        $.ajax({
          url: "/rack/getzones",
          type: "post",
          async: false,
          data: {'value':$('#site').val()} ,
          success: function (response) {
            if(response.error == true)
            {
              document.getElementById('message').innerHTML=
              '<div class="alert alert-warning" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                  +'<strong>'+response.message+'</strong>'
              +'</div>';
            }
            else
            {
              $selecthtml ='';
              for(var x in response.zones[0])
              {
                var zone = response.zones[0][x].ZONE_name;
                
                $selecthtml = $selecthtml+'<option>'+zone+'</option>';
              }
              
              $('#zone').html($selecthtml).selectpicker('refresh');
            }
          }
      });
    });
   oTable = $('.maintable').dataTable();
OTable =$('.maintable').DataTable();
  oTable.$('tr').click( function () {
        $("#data").removeClass('col-md-12');
        $("#data").addClass('col-md-8');
        OTable.rows( '.selected' ).nodes().to$() .removeClass( 'selected' );
        $(this).addClass('selected');
        $("#prop").removeClass("hidden").addClass("show");
        var title=$(this).find("td:nth-child(2)").html();
        $("#prop-title").text(title+' '+'properties');
        var title=$(this).find("td:nth-child(1)").html();

        var unitloadtypeName = $(this).find("td:nth-child(2)").html(); 
        var storagelocationtypeName = $(this).find("td:nth-child(3)").html(); 
        var allocation = $(this).find("td:nth-child(4)").html(); 
        var created_at = $(this).find("td:nth-child(5)").html(); 
        var updated_at = $(this).find("td:nth-child(6)").html();  

        $.get("getrole", function(data, status){

            $.post("rack",{id:title}, function(prop, status){
                //alert(prop);

            var table = document.getElementById("prop-table");
            var element2 = document.createElement("input");
            element2.type = "hidden";
            element2.name = "id";
           element2.value = title;
            table.appendChild(element2);
            $("#prop-table").find("tr:gt(0)").remove();


            var row = table.insertRow(1);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Name";
            var element1 = document.createElement("input"); 
            element1.setAttribute('class','form-control');           
            element1.type = "text";
            element1.name = "rack_name";
            element1.value=prop['rack']['RACK_name'];
            cell2.appendChild(element1);
            element1.readOnly = true; 

            var row = table.insertRow(2);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Zones";
            //cell2.innerHTML = data[0].wms_client;
            var element1 = document.createElement("select");
            element1.name = "zone_name";
            element1.setAttribute('class','form-control selectpicker show-tick');
            cell2.appendChild(element1);
            var arrtypes = prop['zones'];
            arrtypes.forEach(function(ind){
                var option = document.createElement("option");
                option.value = ind;
                option.text = ind;
                element1.appendChild(option);
            });

            $('select[name=zone_name]').val(prop['rack_zone']['ZONE_name']);
            $('.selectpicker').selectpicker('refresh')

            var row = table.insertRow(3);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "No.Rows";
            var element1 = document.createElement("input"); 
            element1.setAttribute('class','form-control');           
            element1.type = "text";
            element1.name = "row";
            element1.value=prop['rack']['RACK_row'];
            cell2.appendChild(element1);
            if(data['role'] == 'labor')
            {
                element1.readOnly = true;
            } 

            var row = table.insertRow(4);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "No.Columns";
            var element1 = document.createElement("input"); 
            element1.setAttribute('class','form-control');           
            element1.type = "text";
            element1.name = "column";
            element1.value=prop['rack']['RACK_column'];
            cell2.appendChild(element1);
            if(data['role'] == 'labor')
            {
                element1.readOnly = true;
            } 

            var row = table.insertRow(5);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Depth";
            var element1 = document.createElement("input"); 
            element1.setAttribute('class','form-control');           
            element1.type = "text";
            element1.name = "depth";
            element1.value=prop['rack']['RACK_depth'];
            cell2.appendChild(element1);
            if(data['role'] == 'labor')
            {
                element1.readOnly = true;
            }            


            document.getElementById("prop").style.maxHeight = document.getElementById("data").style.height
            if(data['role'] == 'labor' || data['role'] == 'chief_of_labors')
            {
                $("#zonesave").removeClass("show").addClass("hidden");
                $("#zonedelete").removeClass("show").addClass("hidden");
            }
            
            
            });

    });
 });

       $('.prop-remove').click(function(){
        $("#data").removeClass('col-md-8').addClass('col-md-12');
        $("#prop").removeClass("show").addClass("hidden");
        $("tr").removeClass('selected');
       });

       $('#create').click(function(){
        $("#create-panel").removeClass("hidden").addClass("show");
       });

});

// var jvalidate = $("#wmszone-form").validate({
//     ignore: [],
//     rules: {                                            
//         name:
//         {
//             required: true
//         }
//     }                                        
// });   


