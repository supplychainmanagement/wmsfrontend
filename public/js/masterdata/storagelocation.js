/**
 * Created by Khaled on 8/16/2016.
 */

$(document).ready(function(){
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	oTable = $('.maintable').dataTable({
        "sPaginationType": "full_numbers",
        "bServerSide": true,
        "sAjaxSource": "storagelocation/storaglocationtable",
        "sServerMethod": "POST",
        "iDisplayLength": 10
    });
    $('#locationlink').addClass('active');
    $('#locationslink').addClass('active');
    var Site;
    var Zone;
    var Rack;

    


    $('#STL_SITE').on('change',function(){
        $('#STL_ZONE').empty();
        $('#STL_RACK').empty();
        $('#STL_ZONE').selectpicker('refresh');
        $('#STL_RACK').selectpicker('refresh');
        Site = this.value;
        $.ajax({
            url: "storagelocation/getzonebysitename",
            type: "POST",
            data: {SITE_name:Site},
            success: function(response)
            {
                response.forEach(function(zone){
                    $('#STL_ZONE').append($('<option>',{
                        value:zone['ZONE_name'],
                        text: zone['ZONE_name']
                    }));
                    Zone = zone['ZONE_name'];
                    $('#STL_ZONE').selectpicker('refresh');
                });
            }
        });
    });



    $('#STL_ZONE').on('change',function(){
        $('#STL_RACK').empty();
        Zone = this.value;
        $.ajax({
            url: "storagelocation/getrackbyzonename",
            type: "POST",
            data: {ZONE_name:Zone},
            success: function(response)
            {
                response.forEach(function(rack){
                    $('#STL_RACK').append($('<option>',{
                        value: rack['RACK_name'],
                        text: rack['RACK_name']
                    }));
                    Rack = rack['RACK_name'];
                    $('#STL_RACK').selectpicker('refresh');
                });
            }
        });
    });
    $('.maintable input')
    .unbind('keypress keyup')
    .bind('keypress keyup', function(e){
      if ($(this).val().length < 3 && e.keyCode != 13) return;
      oTable.fnFilter($(this).val());
    });
    OTable =$('.maintable').DataTable();
    $('.tablebody').on("click", "tr", function(e) {
    	//alert('hi');
        $("#data").removeClass('col-md-12');
        $("#data").addClass('col-md-8');
        OTable.rows( '.selected' ).nodes().to$() .removeClass( 'selected' );
        $(this).addClass('selected');
        $("#prop").removeClass("hidden").addClass("show");
        var title=$(this).find("td:nth-child(2)").html();
        $("#prop-title").text(title+' '+'properties');
        var title=$(this).find("td:nth-child(1)").html();

        var typeName = $(this).find("td:nth-child(2)").html();
        var created_at = $(this).find("td:nth-child(3)").html();
        var updated_at = $(this).find("td:nth-child(4)").html();

            $.post('storagelocation',{id:title},function(data,status){

                var table = document.getElementById("prop-table");
                var element2 = document.createElement("input");
                element2.type = "hidden";
                element2.name = "id";
                element2.value = title;
                table.appendChild(element2);
                $("#prop-table").find("tr:gt(0)").remove();



                var row = table.insertRow(1);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                cell1.innerHTML = "Name";
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "STL_name";
                element1.value=typeName;
                element1.readOnly = true;
                cell2.appendChild(element1);


                var row = table.insertRow(2);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                cell1.innerHTML = "Status";
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "STL_Status";
                element1.value= data.STL_status;
                cell2.appendChild(element1);


                var row = table.insertRow(3);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                cell1.innerHTML = "Functional Area";
                var element1 = document.createElement("select");
                element1.setAttribute('class','form-control selectpicker show-tick');
                cell2.appendChild(element1);
                var arrtypes = data.functionalAreas;
                arrtypes.forEach(function(ind){
                    var option = document.createElement("option");
                    option.value = ind['FA_name'];
                    option.text = ind['FA_name'];
                    element1.appendChild(option);
                });
                element1.value = data.STL_FA;
                element1.text = data.STL_FA;
                element1.name = "STL_FA";

                var row = table.insertRow(4);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                cell1.innerHTML = "Type";
                var element1 = document.createElement("select");
                element1.setAttribute('class','form-control selectpicker show-tick');
                cell2.appendChild(element1);
                var arrtypes = data.types;
                arrtypes.forEach(function(ind){
                    var option = document.createElement("option");
                    option.value = ind['STL_Type_name'];
                    option.text = ind['STL_Type_name'];
                    element1.appendChild(option);
                });
                element1.value = data.STL_Type;
                element1.text = data.STL_Type;
                element1.name = "STL_Type";

                 var row = table.insertRow(5);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                cell1.innerHTML = "Rack";
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "STL_Rack";
                element1.value=data.STL_RACK;
                element1.readOnly = true;
                cell2.appendChild(element1);

                document.getElementById("prop").style.maxHeight = document.getElementById("data").style.height
                if(!data['permission'])
                {
                    $("#zonesave").removeClass("show").addClass("hidden");
                    $("#zonedelete").removeClass("show").addClass("hidden");
                }
            });
    });

    $('.prop-remove').click(function(){
        $("#data").removeClass('col-md-8').addClass('col-md-12');
        $("#prop").removeClass("show").addClass("hidden");
        $("tr").removeClass('selected');
    });

    $('#create').click(function(){
        $("#create-panel").removeClass("hidden").addClass("show");
    });

});