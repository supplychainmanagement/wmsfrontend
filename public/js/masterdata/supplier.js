/**
 * Created by Khaled on 8/9/2016.
 */
$(document).ready(function(){
    $('#inventorylink').addClass('active');
    $('#supplierlink').addClass('active');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    oTable = $('.maintable').dataTable();
    OTable =$('.maintable').DataTable();
    oTable.$('tr').click( function () {
        $("#data").removeClass('col-md-12');
        $("#data").addClass('col-md-8');
        OTable.rows( '.selected' ).nodes().to$() .removeClass( 'selected' );
        $(this).addClass('selected');
        $("#prop").removeClass("hidden").addClass("show");
        var title=$(this).find("td:nth-child(2)").html();
        $("#prop-title").text(title+' '+'properties');
        var title=$(this).find("td:nth-child(1)").html();

        $.get("getrole", function(permission, status) {

            var table = document.getElementById("prop-table");
            var element2 = document.createElement("input");
            element2.type = "hidden";
            element2.name = "id";
            element2.value = title;
            table.appendChild(element2);
            $("#prop-table").find("tr:gt(0)").remove();

            $.post("supplier",{id:title}, function(data, status){

                
                var table = document.getElementById("prop-table");
                var element2 = document.createElement("input");
                element2.type = "hidden";
                element2.name = "id";
                element2.value = data['supplier']['id'];
                table.appendChild(element2);
                $("#prop-table").find("tr:gt(0)").remove();



                var row = table.insertRow(1);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                cell1.innerHTML = "Name";
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "SUP_name";
                element1.value=data['supplier']['SUP_name'];
                cell2.appendChild(element1);
                if(permission['role']!='system_admin'&& permission['role']!='site_admin')
                {
                    element1.readOnly = true;
                }

    

                var row = table.insertRow(2);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                cell1.innerHTML = "SKU";
                if(permission['role'] == 'system_admin'||permission['role']=='site_admin')
                {
                    var arrtypes = data.skus;
                    arrtypes.forEach(function(ind){
                        var option = document.createElement("input");
                        option.type = "checkbox";
                        option.value = ind['SKU_name'];
                        option.text = ind['SKU_name'];
                        option.name = "SUP_SKU[]";
                        cell2.appendChild(option);
                        cell2.appendChild(document.createTextNode(ind['SKU_name']));
                        cell2.appendChild(document.createElement('br'));
                        var attached = data.supplier_skus;
                        attached.forEach(function(object)
                        {
                            if(object['SKU_name']==option.value)
                            {
                                option.checked = true;
                            }
                        });
                    });

                }
                else
                {
                    var element1 = document.createElement("input");
                    element1.setAttribute('class','form-control');
                    element1.type = "text";
                    element1.name = "SUP_SKU_name[]";
                    element1.readOnly = true;
                    element1.value=data.customer_skus['SKU_name'];
                }


                var row = table.insertRow(3);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                cell1.innerHTML = "Phone";
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "SUP_phone";
                element1.value=data['supplier']['SUP_phone'];
                cell2.appendChild(element1);
                if(permission['role']!='system_admin'&& permission['role']!='site_admin')
                {
                    element1.readOnly = true;
                }

                var row = table.insertRow(4);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                cell1.innerHTML = "Email";
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "SUP_email";
                element1.value=data['supplier']['SUP_email'];
                cell2.appendChild(element1);
                if(permission['role']!='system_admin'&& permission['role']!='site_admin')
                {
                    element1.readOnly = true;
                }

                var row = table.insertRow(5);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                cell1.innerHTML = "Address";
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "SUP_address";
                element1.value=data['supplier']['SUP_address'];
                cell2.appendChild(element1);
                if(permission['role']!='system_admin'&& permission['role']!='site_admin')
                {
                    element1.readOnly = true;
                }





                document.getElementById("prop-panel").style.cssText = "height: 500px !important";
                document.getElementById("mCSB_3").style.cssText = "height: 400px !important";
                if(permission['role']!='system_admin'&&permission['role']!='site_admin')
                {
                    $("#zonesave").removeClass("show").addClass("hidden");
                    $("#zonedelete").removeClass("show").addClass("hidden");
                }


            });
        });


    });
    $('.prop-remove').click(function(){
        $("#data").removeClass('col-md-8').addClass('col-md-12');
        $("#prop").removeClass("show").addClass("hidden");
        $("tr").removeClass('selected');
    });

    $('#create').click(function(){
        $("#create-panel").removeClass("hidden").addClass("show");
    });

});

// var jvalidate = $("#wmszone-form").validate({
//     ignore: [],
//     rules: {
//         name:
//         {
//             required: true
//         }
//     }
// });

