$(document).ready(function(){
    $("#zonesave").removeClass("show").addClass("hidden");
    $('#unitloadslink').addClass('active');
    $('#unitloadlink').addClass('active');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#UL_SITE').on('change', function(){
        $('#UL_ZONE').empty().selectpicker('refresh');
        $('#UL_RACK').empty().selectpicker('refresh');
        $.ajax({
          url: "unitload/getzones",
          type: "post",
          data: {'SITE_name':$("#UL_SITE").val()} ,
          success: function (response) {
              if(response.error == false)
              {
                $selecthtml ='';
                  for(var x in response.data)
                  {
                    var zone = response.data[x];
                    //alert(customer);
                    str = zone.replace(/\s+/g, '');
                    $selecthtml = $selecthtml+'<option id="'+str+'">'+zone+'</option>';
                  }
                  $('#UL_ZONE').html($selecthtml).selectpicker('refresh');
              }
          },
          error: function(jqXHR, textStatus, errorThrown) {
          }
        });

    });
     $('#UL_ZONE').on('change', function(){
        $('#UL_RACK').empty().selectpicker('refresh');
        $.ajax({
          url: "unitload/getracks",
          type: "post",
          data: {'SITE_name':$("#UL_SITE").val(),
                'ZONE_name':$('#UL_ZONE').val()} ,
          success: function (response) {
              if(response.error == false)
              {
                $selecthtml ='';
                  for(var x in response.data)
                  {
                    var zone = response.data[x];
                    //alert(customer);
                    str = zone.replace(/\s+/g, '');
                    $selecthtml = $selecthtml+'<option id="'+str+'">'+zone+'</option>';
                  }
                  $('#UL_RACK').html($selecthtml).selectpicker('refresh');
              }
          },
          error: function(jqXHR, textStatus, errorThrown) {
          }
        });

    });
    $('#UL_RACK').on('change', function(){
        $.ajax({
          url: "unitload/getlocations",
          type: "post",
          data: {'SITE_name':$("#UL_SITE").val(),
                'ZONE_name':$('#UL_ZONE').val(),
                'RACK_name':$('#UL_RACK').val()},
          success: function (response) {
              if(response.error == false)
              {
                $selecthtml ='';
                  for(var x in response.data)
                  {
                    var zone = response.data[x];
                    //alert(customer);
                    str = zone.replace(/\s+/g, '');
                    $selecthtml = $selecthtml+'<option id="'+str+'">'+zone+'</option>';
                  }
                  $('#UL_STL').html($selecthtml).selectpicker('refresh');
              }
          },
          error: function(jqXHR, textStatus, errorThrown) {
          }
        });
    });
    oTable = $('.maintable').dataTable();
    OTable =$('.maintable').DataTable();
    oTable.$('tr').click( function () {
        $("#data").removeClass('col-md-12');
        $("#data").addClass('col-md-8');
        OTable.rows( '.selected' ).nodes().to$() .removeClass( 'selected' );
        $(this).addClass('selected');
        $("#prop").removeClass("hidden").addClass("show");
        var title=$(this).find("td:nth-child(2)").html();
        $("#prop-title").text(title+' '+'properties');
        var title=$(this).find("td:nth-child(1)").html();

        $.get("getrole", function(permission, status) {

            var table = document.getElementById("prop-table");
            var element2 = document.createElement("input");
            element2.type = "hidden";
            element2.name = "id";
            element2.value = title;
            table.appendChild(element2);
            $("#prop-table").find("tr:gt(0)").remove();

        $.post("unitload",{id:title}, function(data, status){

            
            var table = document.getElementById("prop-table");
            var element2 = document.createElement("input");
            element2.type = "hidden";
            element2.name = "id";
            element2.value = data['unitload']['id'];
            table.appendChild(element2);
            $("#prop-table").find("tr:gt(0)").remove();



            var row = table.insertRow(1);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Name";
            var element1 = document.createElement("input");
            element1.setAttribute('class','form-control');
            element1.type = "text";
            element1.name = "UL_name";
            element1.value=data['unitload']['UL_name'];
            cell2.appendChild(element1);
            if(permission['role']!='system_admin'&& permission['role']!='site_admin')
            {
                element1.readOnly = true;
            }

            

            var row = table.insertRow(2);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Client";
            if(permission['role'] == 'system_admin'||permission['role']=='site_admin')
            {
                var element1 = document.createElement("select");
                element1.name = "UL_CLI";
                element1.setAttribute('class','form-control selectpicker show-tick');
                cell2.appendChild(element1);
                var arrtypes = data.clients;
                arrtypes.forEach(function(ind){
                    var option = document.createElement("option");
                    option.value = ind['CLI_name'];
                    option.text = ind['CLI_name'];
                    element1.appendChild(option);
                });
                element1.value = data.unitload_client;
                element1.text = data.unitload_client;
            }
            else
            {
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "UL_CLI";
                element1.readOnly = true;
                element1.value=data.unitload_client;
                cell2.appendChild(element1);
            }


            var row = table.insertRow(3);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Type";
            if(permission['role'] == 'system_admin'||permission['role'] == 'site_admin')
            {
                var element1 = document.createElement("select");
                element1.name = "UL_ULT_Type";
                element1.setAttribute('class','form-control selectpicker show-tick');
                cell2.appendChild(element1);
                var arrtypes = data.types;
                arrtypes.forEach(function(ind){
                    var option = document.createElement("option");
                    option.value = ind['ULT_Type_name'];
                    option.text = ind['ULT_Type_name'];
                    element1.appendChild(option);
                });
                element1.value = data.unitload_type;
                element1.text = data.unitload_type;
            }
            else
            {
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "UL_ULT_Type";
                element1.readOnly = true;
                element1.value=data.unitload_type;
                cell2.appendChild(element1);
            }


            var row = table.insertRow(4);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Allocation";
            if(permission['role']=='system_admin'||permission['role']=='site_admin')
            {
                var element1 = document.createElement("input");
                element1.type = "text";
                element1.name = "UL_allocation";
                element1.value = data['unitload']['UL_allocation'];
                element1.setAttribute('class','form-control');
                cell2.appendChild(element1);

            }
            else
            {
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "UL_allocation";
                element1.readOnly = true;
                element1.value=data['unitload']['UL_allocation'];
                cell2.appendChild(element1);
            }





            var row = table.insertRow(5);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "SKU";
            if(permission['role'] == 'system_admin'||permission['role']=='site_admin')
            {
                var element1 = document.createElement("select");
                element1.name = "UL_SKU";
                element1.setAttribute('class','form-control selectpicker show-tick');
                cell2.appendChild(element1);
                var arrtypes = data.skus;
                arrtypes.forEach(function(ind){
                    var option = document.createElement("option");
                    option.value = ind['SKU_name'];
                    option.text = ind['SKU_name'];
                    element1.appendChild(option);
                });
                element1.value = data.unitload_sku;
                element1.text = data.unitload_sku;
            }
            else
            {
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "UL_SKU";
                element1.readOnly = true;
                element1.value=data.unitload_sku;
                cell2.appendChild(element1);
            }



            var row = table.insertRow(6);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Amount";
            if(permission['role']=='system_admin'||permission['role']=='site_admin')
            {
                var element1 = document.createElement("input");
                element1.type = "text";
                element1.name = "UL_amount";
                element1.value = data['unitload']['UL_amount'];
                element1.setAttribute('class','form-control');
                cell2.appendChild(element1);

            }
            else
            {
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "UL_amount";
                element1.readOnly = true;
                element1.value=data['unitload']['UL_amount'];
                cell2.appendChild(element1);
            }


            var row = table.insertRow(7);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Location";
            if(permission['role'] == 'system_admin'||permission['role']=='site_admin')
            {
                var element1 = document.createElement("select");
                element1.name = "UL_STL";
                element1.setAttribute('class','form-control selectpicker show-tick');
                cell2.appendChild(element1);
                var arrtypes = data.locations;
                arrtypes.forEach(function(ind){
                    var option = document.createElement("option");
                    option.value = ind['STL_name'];
                    option.text = ind['STL_name'];
                    element1.appendChild(option);
                });
                element1.value = data.unitload_location;
                element1.text = data.unitload_location;
            }
            else
            {
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "UL_STL";
                element1.readOnly = true;
                element1.value=data.unitload_location;
                cell2.appendChild(element1);
            }


            var row = table.insertRow(8);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Status";
            if(permission['role']=='system_admin'||permission['role']=='site_admin')
            {
                var element1 = document.createElement("input");
                element1.type = "text";
                element1.name = "UL_status";
                element1.value = data['unitload']['UL_status'];
                element1.setAttribute('class','form-control');
                cell2.appendChild(element1);

            }
            else
            {
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "UL_status";
                element1.readOnly = true;
                element1.value=data['unitload']['UL_status'];
                cell2.appendChild(element1);
            }


            var row = table.insertRow(9);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Serial Number";
            if(permission['role']=='system_admin'||permission['role']=='site_admin')
            {
                var element1 = document.createElement("input");
                element1.type = "text";
                element1.name = "UL_serialNumber";
                element1.value = data['unitload']['UL_serialNumber'];
                element1.setAttribute('class','form-control');
                cell2.appendChild(element1);

            }
            else
            {
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "UL_serialNumber";
                element1.readOnly = true;
                element1.value=data['unitload']['UL_serialNumber'];
            }


            var row = table.insertRow(10);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Reserved";
            if(permission['role']=='system_admin'||permission['role']=='site_admin')
            {
                var element1 = document.createElement("input");
                element1.type = "text";
                element1.name = "UL_reserved";
                element1.value = data['unitload']['UL_reserved'];
                element1.setAttribute('class','form-control');
                cell2.appendChild(element1);

            }
            else
            {
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "UL_reserved";
                element1.readOnly = true;
                element1.value=data['unitload']['UL_reserved'];
                cell2.appendChild(element1);
            }


            var row = table.insertRow(11);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Expiry Date";
            if(permission['role']=='system_admin'||permission['role']=='site_admin')
            {
                var element1 = document.createElement("input");
                element1.type = "date";
                element1.name = "UL_expiryDate";
                element1.value = data['unitload']['UL_expiryDate'];
                element1.setAttribute('class','form-control');
                cell2.appendChild(element1);

            }
            else
            {
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "date";
                element1.name = "UL_expiryDate";
                element1.readOnly = true;
                element1.value=data['unitload']['UL_expiryDate'];
                cell2.appendChild(element1);
            }


            var row = table.insertRow(12);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = "Stocktaking Date";
            if(permission['role']=='system_admin'||permission['role']=='site_admin')
            {
                var element1 = document.createElement("input");
                element1.type = "date";
                element1.name = "UL_stocktakingDate";
                element1.value = data['unitload']['UL_stocktakingDate'];
                element1.setAttribute('class','form-control');
                cell2.appendChild(element1);

            }
            else
            {
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "date";
                element1.name = "UL_stocktakingDate";
                element1.readOnly = true;
                element1.value=data['unitload']['UL_stocktakingDate'];
                cell2.appendChild(element1);
            }






            document.getElementById("prop-panel").style.cssText = "height: 500px !important";
            document.getElementById("mCSB_3").style.cssText = "height: 400px !important";
            if(permission['role']!='system_admin'&&permission['role']!='site_admin')
            {
                $("#zonedelete").removeClass("show").addClass("hidden");
            }


        });
        });


    });
    $('.prop-remove').click(function(){
        $("#data").removeClass('col-md-8').addClass('col-md-12');
        $("#prop").removeClass("show").addClass("hidden");
        $("tr").removeClass('selected');
    });

    $('#create').click(function(){
        $("#create-panel").removeClass("hidden").addClass("show");
    });

});

// var jvalidate = $("#wmszone-form").validate({
//     ignore: [],
//     rules: {                                            
//         name:
//         {
//             required: true
//         }
//     }                                        
// });   


