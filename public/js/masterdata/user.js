/**
 * Created by Khaled on 8/9/2016.
 */
$(document).ready(function(){
    $('#systemlink').addClass('active');
    $('#userlink').addClass('active');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#USR_ROLE').on('change', function(){

        if($('#USR_ROLE').val() != 'SystemAdmin')

        {
            $('#siteinput').show();
        }

        else

        {
            $('#siteinput').hide();
        }
   });
    OTable =$('.maintable').DataTable();
    OTable.$('tr').click( function () {
        $("#data").removeClass('col-md-12');
        $("#data").addClass('col-md-8');
        OTable.rows( '.selected' ).nodes().to$() .removeClass( 'selected' );
        $(this).addClass('selected');
        $("#prop").removeClass("hidden").addClass("show");
        var title=$(this).find("td:nth-child(2)").html();
        $("#prop-title").text(title+' '+'properties');
        var title=$(this).find("td:nth-child(1)").html();

        $.get("getrole", function(permission, status) {

            var table = document.getElementById("prop-table");
            var element2 = document.createElement("input");
            element2.type = "hidden";
            element2.name = "id";
            element2.value = title;
            table.appendChild(element2);
            $("#prop-table").find("tr:gt(0)").remove();

            $.post("user",{id:title}, function(data, status){

                
                var table = document.getElementById("prop-table");
                var element2 = document.createElement("input");
                element2.type = "hidden";
                element2.name = "id";
                element2.value = data['user']['id'];
                table.appendChild(element2);
                $("#prop-table").find("tr:gt(0)").remove();



                var row = table.insertRow(1);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                cell1.innerHTML = "Username";
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "USR_name";
                element1.value=data['user']['USR_name'];
                cell2.appendChild(element1);
                if(permission['role']!='system_admin'&& permission['role']!='site_admin')
                {
                    element1.readOnly = true;
                }

                var row = table.insertRow(2);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                cell1.innerHTML = "Created";
                cell2.innerHTML = data['user'].created_at;

                var row = table.insertRow(3);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                cell1.innerHTML = "Modified";
                cell2.innerHTML = data['user'].updated_at

                var row = table.insertRow(4);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                cell1.innerHTML = "Email";
                var element1 = document.createElement("input");
                element1.setAttribute('class','form-control');
                element1.type = "text";
                element1.name = "USR_email";
                element1.value=data['user']['USR_email'];
                cell2.appendChild(element1);
                if(permission['role']!='system_admin'&& permission['role']!='site_admin')
                {
                    element1.readOnly = true;
                }

                var row = table.insertRow(5);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                cell1.innerHTML = "Client";
                if(permission['role'] == 'system_admin'||permission['role']=='site_admin')
                {
                    var element1 = document.createElement("select");
                    element1.name = "USR_CLI";
                    element1.setAttribute('class','form-control selectpicker show-tick');
                    cell2.appendChild(element1);
                    var arrtypes = data.clients;
                    arrtypes.forEach(function(ind){
                        var option = document.createElement("option");
                        option.value = ind['CLI_name'];
                        option.text = ind['CLI_name'];
                        element1.appendChild(option);
                    });
                    element1.value = data.user_client;
                    element1.text = data.user_client;
                }
                else
                {
                    var element1 = document.createElement("input");
                    element1.setAttribute('class','form-control');
                    element1.type = "text";
                    element1.name = "USR_CLI";
                    element1.readOnly = true;
                    element1.value=data.user_client;
                }


                var row = table.insertRow(6);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                cell1.innerHTML = "Role";
                if(permission['role'] == 'system_admin'||permission['role']=='site_admin')
                {
                    var element1 = document.createElement("select");
                    element1.name = "USR_ROLE";
                    element1.setAttribute('class','form-control selectpicker show-tick');
                    cell2.appendChild(element1);
                    var arrtypes = data.roles;
                    arrtypes.forEach(function(ind){
                        var option = document.createElement("option");
                        option.value = ind['ROLE_name'];
                        option.text = ind['ROLE_name'];
                        element1.appendChild(option);
                    });
                    element1.value = data.user_role;
                    element1.text = data.user_role;
                }
                else
                {
                    var element1 = document.createElement("input");
                    element1.setAttribute('class','form-control');
                    element1.type = "text";
                    element1.name = "USR_ROLE";
                    element1.readOnly = true;
                    element1.value=data.user_role;
                }


                var row = table.insertRow(7);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                cell1.innerHTML = "Site";
                if(permission['role'] == 'system_admin'||permission['role']=='site_admin')
                {
                    var element1 = document.createElement("select");
                    element1.name = "USR_SITE";
                    element1.setAttribute('class','form-control selectpicker show-tick');
                    cell2.appendChild(element1);
                    var arrtypes = data.sites;
                    arrtypes.forEach(function(ind){
                        var option = document.createElement("option");
                        option.value = ind['SITE_name'];
                        option.text = ind['SITE_name'];
                        element1.appendChild(option);
                    });
                    element1.value = data.user_site;
                    element1.text = data.user_site;
                }
                else
                {
                    var element1 = document.createElement("input");
                    element1.setAttribute('class','form-control');
                    element1.type = "text";
                    element1.name = "USR_SITE";
                    element1.readOnly = true;
                    element1.value=data.user_site;
                }






                document.getElementById("prop").style.maxHeight = document.getElementById("data").style.height
                if(permission['role']!='system_admin'&&permission['role']!='site_admin')
                {
                    $("#zonesave").removeClass("show").addClass("hidden");
                    $("#zonedelete").removeClass("show").addClass("hidden");
                }


            });
        });


    });
    $('.prop-remove').click(function(){
        $("#data").removeClass('col-md-8').addClass('col-md-12');
        $("#prop").removeClass("show").addClass("hidden");
        $("tr").removeClass('selected');
    });

    $('#create').click(function(){
        $("#create-panel").removeClass("hidden").addClass("show");
    });

});



