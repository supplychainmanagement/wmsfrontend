var pickinitems = new Array();
var pickoutitems = new Array();
function message(response,status)
{
    var list = "<ul>";
      for(var x=0 ;x<response.message.length;x++)
      {
        list += '<li>'+response.message[x]+'</li>';
      }
      list += "</ul>";
      document.getElementById('message').innerHTML=
      '<div class="alert alert-'+status+'" role="alert">'+
          '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
          +'<strong>'+list+'</strong>'
      +'</div>';
}
function getPickInUnitloadAmount(value)
{
  var amountFound = false;
  for(var x in pickinitems)
  {
      if(value == pickinitems[x].name)
      {
        if(pickinitems[x].amount != -1)
        {
          amountFound = true;
          var amount = parseInt($("#pick-in-knob").val());
          setPickInSlider(pickinitems[x].amount,amount);
        }
      }
  }
  if(amountFound == false)
  {
    $.ajax({
          url: "pickorder/getpickinunitloadamount",
          type: "post",
          data: {'value':value} ,
          success: function (response) {
              if(response.error == true)
              {
                message(response,'danger');
              }
              else
              {
                var amount = parseInt($("#pick-in-knob").val());
                for(var x in pickinitems)
                {
                  if(value == pickinitems[x].name)
                  {
                    pickinitems[x].amount = response.data[0];
                  }
                }
                setPickInSlider(response.data[0],amount);
              } 
          },
          error: function(jqXHR, textStatus, errorThrown) {
             

          }
      });
  }
  
}
function getPickOutUnitloadAmount(value)
{
  var amountFound = false;
  for(var x in pickoutitems)
  {
      if(value == pickoutitems[x].name)
      {
        if(pickoutitems[x].amount != -1)
        {
          amountFound = true;
          var amount = parseInt($("#pick-out-knob").val());
          setPickOutSlider(pickoutitems[x].amount,amount);
          
        }
      }
  }
  if(amountFound == false)
  {
    $.ajax({
          url: "pickorder/getpickoutunitloadamount",
          type: "post",
          data: {'value':value} ,
          success: function (response) {
              if(response.error == true)
              {
                  message(response,'danger');
              }
              else
              {
                  $('#pickoutsliderdiv').empty();
                  var amount = parseInt($("#pick-out-knob").val());
                  for(var x in pickoutitems)
                  {
                    if(value == pickoutitems[x].name)
                    {
                      pickoutitems[x].amount = response.data[0];
                    }
                  }
                  setPickOutSlider(response.data[0],amount);
              }
              
          },
          error: function(jqXHR, textStatus, errorThrown) {
             

          }
      });
  }
  
}
function getIssueOrderItems(value)
{
	$.ajax({
          url: "pickorder/getitems",
          type: "post",
          data: {'value':value} ,
          success: function (response) {
            if(response.error == true)
            {
                message(response,'danger');
            }
            else
            {
              $('#issueorderitem').empty();
              //
              response.data.issueorderitems_data.forEach(function(x)
                {

                    $('#issueorderitem').append($('<option>', {
                        value: x.issueitem.ISI_number,
                        text: (x.issueitem.ISI_number + "  | "+x.SKU_name+" => "+x.issueitem.ISI_amount+" |")
                    }));
                });
                $('#issueorderitem').selectpicker('refresh');

              // $selecthtml ='';
              // for(var x in response.issueorder.issueorderitems_data)
              // {
              //   var issueorderitem = response.issueorder.issueorderitems_data[x].issueitem.ISI_number;
              //   //alert(customer);
              //   str = issueorderitem.replace(/\s+/g, '');
              //   $selecthtml = $selecthtml+'<option id="'+str+'">'+issueorderitem+'</option>';
              // }
              // $('#issueorderitem').html($selecthtml).selectpicker('refresh');
          	}
          },
          error: function(jqXHR, textStatus, errorThrown) {
             

          }
      });
}
function createPickRequest(value)
{
  var isinumber = $('#issueorderitem').val();
  $.ajax({
          url: "pickorder/createpickrequest",
          type: "post",
          data: {'value':value,'isinumber':isinumber} ,
          success: function (response) {
            if(response.error == true)
            {
                message(response,'danger');
            }
            else
            {
              $('.message-box-success').addClass('open');
              $('#pickrequest').attr('disabled',true);
              var itemSelector = $('#issueorderitem');
              var itemSelectorOption = $('#issueorderitem option:selected');
              itemSelectorOption.remove();
              itemSelector.selectpicker('refresh');
              $('#pickinunitload').find('option:not(:first)').remove().selectpicker('refresh').attr('disabled',false);
              $('#pickoutunitload').find('option:not(:first)').remove().selectpicker('refresh').attr('disabled',false);
              $('#pickout-table').dataTable().fnClearTable();
              $('#pickin-table').dataTable().fnClearTable();
              intiation();

              
            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
             

          }
      });
}
function setPickOutSlider(unitloadamount,knobamount)
{
  $('#pickoutsliderdiv').empty();
  if(knobamount<unitloadamount)
  {
    var slider = new $add.SliderObj({
      min: 0,
      max: knobamount,
      value: 0,
      id: 'pickoutslider',
      step:1,
      fontsize: 14
    });
    slider.render("#pickoutsliderdiv", "append");
  }
  else
  {
    var slider = new $add.SliderObj({
      min: 0,
      max: unitloadamount,
      value: 0,
      id: 'pickoutslider',
      step:1,
      fontsize: 14
    });
    slider.render("#pickoutsliderdiv", "append");
  }
}
function setPickInSlider(unitloadamount,knobamount)
{
  $('#pickinsliderdiv').empty();
  if(knobamount<unitloadamount)
  {
    $('#pickinsliderdiv').empty();
    var slider = new $add.SliderObj({
      min: 0,
      max: knobamount,
      value: 0,
      id: 'pickinslider',
      step:1,
      fontsize: 14
    });
    slider.render("#pickinsliderdiv", "append");
  }
  else
  {
    $('#pickinsliderdiv').empty();
    var slider = new $add.SliderObj({
      min: 0,
      max: unitloadamount,
      value: 0,
      id: 'pickinslider',
      step:1,
      fontsize: 14
    });
    slider.render("#pickinsliderdiv", "append");
  }
}
function getPickOutUnitloads(value)
{
	$.ajax({
          url: "pickorder/getpickoutunitloads",
          type: "post",
          async: false,
          data: {'value':value} ,
          success: function (response) {
             if(response.error == true)
            {
                message(response,'danger');
            }
            else
            {
              if(response.data.length>0)
              {
                $('#pickoutunitload').empty();
                response.data.forEach(function(x)
                {
                  var unitload = x.unitload;
                  pickoutitems.push({
                  'name': unitload,
                  'amount': -1
                  });
                    $('#pickoutunitload').append($('<option>', {
                        value: unitload,
                        text: (unitload + "  | "+x.route+" |")
                    }));
                });
                $('#pickoutunitload').selectpicker('refresh');

              }
             
               else
              {
                 document.getElementById('message').innerHTML=
                '<div class="alert alert-danger" role="alert">'+
                    '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                    +'<strong>Issue Order Item Unpickable cause there isn\'t any available storage unitloads</strong>'
                +'</div>';
              }
          	}
          },
          error: function(jqXHR, textStatus, errorThrown) {
             

          }
      });
}



function getPickInUnitloads(value)
{

  $.ajax({
          url: "pickorder/getpickinunitloads",
          type: "post",
          async: false,
          data: {'value':value} ,
          success: function (response) {
             if(response.error == true)
            {
                message(response,'danger');
            }
            else
            {
              if(response.data.length>0)
              {
                $('#pickinunitload').empty();
                response.data.forEach(function(x)
                {
                  var unitload = x.unitload;
                  pickinitems.push({
                  'name': unitload,
                  'amount': -1
                  });
                    $('#pickinunitload').append($('<option>', {
                        value: unitload,
                        text: (unitload + "  | "+x.route+" |")
                    }));
                });
                $('#pickinunitload').selectpicker('refresh');
                
                // $('#pickinunitload').empty();
                // $selecthtml ='';
                // for(var x in response.pickinunitloads)
                // {
                  
                //   var unitload = response.pickinunitloads[x];
                //   pickinitems.push({
                //   'name': unitload,
                //   'amount': -1
                //   });
                //   str = unitload.replace(/\s+/g, '');
                //   $selecthtml = $selecthtml+'<option id="'+str+'">'+unitload+'</option>';
                // }
                // $('#pickinunitload').html($selecthtml).selectpicker('refresh');
              }
              else
              {
                 document.getElementById('message').innerHTML=
                '<div class="alert alert-danger" role="alert">'+
                    '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                    +'<strong>Issue Order Item Unpickable cause there isn\'t any available goods out unitloads</strong>'
                +'</div>';
              }
              
            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
             

          }
      });
}


function getIssueOrderItemAmount(value)
{
	$.ajax({
          url: "pickorder/getitemamount",
          type: "post",
          data: {'issueordernumber':$('#issueorder').val(),'issueorderitemnumber':value} ,
          success: function (response) {
            if(response.error == true)
            {
                message(response,'danger');
            }
            else
            {
             $('#pick-out-knob').trigger('configure', {
              	max: response.data.ISI_amount
	           });
  	         $("#pick-out-knob").val(response.data.ISI_amount);
  	         $("#pick-out-knob").trigger('change');
  	         $('#pick-in-knob').trigger('configure', {
                	max: response.data.ISI_amount
  	         });
             if(response.data.STRAT_name != null)
             {
                $('#suggestpickout').attr('disabled',false);
             }
  	         //alert(response.issueorderitem.ISI_amount);
  	         $("#pick-in-knob").val(response.data.ISI_amount);
  	         $("#pick-in-knob").trigger('change');
	     	   }
	      	 
          },
          error: function(jqXHR, textStatus, errorThrown) {
             

          }
      });
}

function suggestPickOut(value)
{
  $.ajax({
          url: "pickorder/suggestpickout",
          type: "post",
          data: {'issueorderitem':value} ,
          success: function (response) {
             if(response.error == true)
            {
                message(response,'danger');
            }
            else
            {
              var items = '';
              for(var x in response.data)
              {
                items = items+'<li> Unitload Name: '+response.data[x]['UL_name']+', Unitload Amount:'+response.data[x]['UL_amount']+'</li>';
              }
              document.getElementById('message').innerHTML=
              '<div class="alert alert-success" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                  +'<strong>'
                  +'<ul>'
                  +items
                  +'</ul>'
                  +'</strong>'
              +'</div>';
            }
           
          },
          error: function(jqXHR, textStatus, errorThrown) {
             

          }
      });
}
function intiation()
{
  $('#pickinadd').attr('disabled',true);
  $('#pickoutadd').attr('disabled',true);
  $('#pickrequest').attr('disabled',true);
  $('#pickoutunitload').attr('disabled',false);
  $('#pickinunitload').attr('disabled',false);
  $('#suggestpickout').attr('disabled',true);
}
$(document).ready(function(){
  intiation();
	$('#issueorder').on('change', function() {
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
    var value = this.value;
    getIssueOrderItems(value);

  });
	$('#issueorderitem').on('change', function() {
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
    var value = this.value;
    pickinitems = new Array();
    pickoutitems = new Array();
    $('#pickout-table').dataTable().fnClearTable();
    //$('#pickout-table').DataTable().draw();
    $('#pickin-table').dataTable().fnClearTable();
    //$('#pickin-table').DataTable().draw();
    $('#pickinunitload').find('option:not(:first)').remove().selectpicker('refresh').attr('disabled',false);
    $('#pickoutunitload').find('option:not(:first)').remove().selectpicker('refresh').attr('disabled',false);
    $('#pickrequest').attr('disabled',true);
    getIssueOrderItemAmount(value);
    getPickOutUnitloads(value);
    getPickInUnitloads(value);
    $('#pick-out').show();
    $('#pick-in').show();

    if($('#pickoutunitload option').size()>1 && $('#pickinunitload option').size()>1)
    {
      $('#pickinadd').attr('disabled',false);
      $('#pickoutadd').attr('disabled',false);
    }
  });

  $("#pickoutform").submit(function(e) {
      var unitloadname = $('#pickoutunitload').val();
      
      var amount = parseInt($('#pickoutslider input').attr('value'));
      
      if(amount == 0 || unitloadname == '' )
        document.getElementById('message').innerHTML=
              '<div class="alert alert-warning" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                  +'<strong>you can\'t add a unitload with 0 amount or an unselected unitload</strong>'
              +'</div>';
      else
      {
        var pickouttable = $('#pickout-table').DataTable();
        for(var x in pickoutitems)
        {
          if(pickoutitems[x].name == unitloadname)
          {
            pickoutitems[x].amount = pickoutitems[x].amount - amount;
            if(pickoutitems[x].amount == 0)
            {
              pickoutitems.splice(x,1);
              var itemSelector = $('#pickoutunitload');
              var itemSelectorOption = $('#pickoutunitload option:selected');
              itemSelectorOption.remove();
              itemSelector.selectpicker('refresh');
            }
            else
            {
              var amountKnob = parseInt($("#pick-out-knob").val());
              setPickOutSlider(pickoutitems[x].amount,amountKnob);
            }
          }
        }
        pickouttable.row.add
        ([
          unitloadname,
          amount,
          '<button type="button" class="btn btn-danger removepickoutunitload" disabled=true>Remove</button>'
        ]).draw();
        var knobNewAmount =parseInt($("#pick-out-knob").val())-parseInt(amount)-1; 
        $({animatedVal: parseInt($("#pick-out-knob").val())}).animate({animatedVal: knobNewAmount}, {
         duration: 500,
         easing: "swing", 
         step: function() { 
             $("#pick-out-knob").val(Math.ceil(this.animatedVal)).trigger("change"); 
         }
        });

        if(knobNewAmount <= 0)
        {
          $('#pickoutadd').attr('disabled',true);
          $('#pickoutunitload').attr('disabled',true);
        }
        var otherknob = parseInt($("#pick-in-knob").val());
        
        if( knobNewAmount <= 0  && otherknob <= 0)
        {
          $('#pickrequest').attr('disabled',false);
        }
      }
      e.preventDefault();
    });
  $("#pickinform").submit(function(e) {
      var unitloadname = $('#pickinunitload').val();
      var amount = parseInt($('#pickinslider input').attr('value'));
      if(amount == 0 || unitloadname == '')
        document.getElementById('message').innerHTML=
              '<div class="alert alert-warning" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                  +'<strong>you can\'t add a unitload with 0 amount or an unselected unitload</strong>'
              +'</div>';
      else
      {
          var pickintable = $('#pickin-table').DataTable();
          for(var x in pickinitems)
          {
            if(pickinitems[x].name == unitloadname)
            {
              pickinitems[x].amount = pickinitems[x].amount - amount;
              if(pickinitems[x].amount == 0)
              {
                pickinitems.splice(x,1);
                var itemSelector = $('#pickinunitload');
                var itemSelectorOption = $('#pickinunitload option:selected');
                itemSelectorOption.remove();
                itemSelector.selectpicker('refresh');
              }
              else
              {
                var amountKnob = parseInt($("#pick-in-knob").val());
                setPickInSlider(pickinitems[x].amount,amountKnob);
              }
            }
          }
          pickintable.row.add
          ([
            unitloadname,
            amount,
            '<button type="button" class="btn btn-danger removepickinunitload" disabled = true>Remove</button>'
          ]).draw();
      }
      var knobNewAmount =parseInt($("#pick-in-knob").val())-parseInt(amount)-1; 
        $({animatedVal: parseInt($("#pick-in-knob").val())}).animate({animatedVal: knobNewAmount}, {
         duration: 500,
         easing: "swing", 
         step: function() { 
             $("#pick-in-knob").val(Math.ceil(this.animatedVal)).trigger("change"); 
         }
        });

        if(knobNewAmount <= 0)
        {
          $('#pickinadd').attr('disabled',true);
          $('#pickinunitload').attr('disabled',true);
        }
        var otherknob = $("#pick-out-knob").val();
        
        if( knobNewAmount <= 0  && otherknob <= 0)
        {
          $('#pickrequest').attr('disabled',false);
        }
      e.preventDefault();
    });
  $(document).on("click",".pickrequest",function(e)
    {
      var pickoutknob = parseInt($("#pick-out-knob").val());
      var pickinknob = parseInt($("#pick-in-knob").val());
      if(pickinknob != 0 || pickoutknob!=0)
      {
        document.getElementById('message').innerHTML=
              '<div class="alert alert-warning" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                  +'<strong>Pick out and pick in must  be equal 0 before creating the pick request</strong>'
              +'</div>';
      }
      else
      {
        var pickoutunitloads = $('#pickout-table').DataTable();
        var pickoutunitloadsArray = new Array();
        pickoutunitloads.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
          var data = this.data();
          pickoutunitloadsArray.push({
            "unitload_name":data[0],
            "unitload_amount":data[1]
          });
        });

        var pickinunitloads = $('#pickin-table').DataTable();
        var pickinunitloadsArray = new Array();
        pickinunitloads.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
          var data = this.data();
          pickinunitloadsArray.push({
            "unitload_name":data[0],
            "unitload_amount":data[1]
          });
        });
        var pickrequest = new Array();
        pickrequest.push({
          "pickout":pickoutunitloadsArray,
          "pickin":pickinunitloadsArray
        });
        createPickRequest(pickrequest);

      }
    });

    $('#pickinunitload').on('change', function() {
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
    var value = this.value;
    getPickInUnitloadAmount(value);
    });
    $('#pickoutunitload').on('change', function() {
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
    var value = this.value;
    getPickOutUnitloadAmount(value);
    });


    $(document).on("click",".suggestpickout",function(e)
    {
      suggestPickOut($('#issueorderitem').val());
    });
});