function message(id,response,status)
{
    var list = "<ul>";
      for(var x=0 ;x<response.message.length;x++)
      {
        list += '<li>'+response.message[x]+'</li>';
      }
      list += "</ul>";
      document.getElementById(id).innerHTML=
      '<div class="alert alert-'+status+'" role="alert">'+
          '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
          +'<strong>'+list+'</strong>'
      +'</div>';
}

function getClientCustomers(client)
{
  $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
     //alert(client);
       $.ajax({
          url: "getcustomers",
          type: "post",
          async: false,
          data: {'value':client} ,
          success: function (response) {
            if(response.error == true)
            {
              message('message',response,'warning');
            }
            else
            {
              $('#customer').empty();
              $('#customer').attr('disabled',false);
              $selecthtml ='';
              for(var x in response.data)
              {
                var customer = response.data[x];
                //alert(customer);
                str = customer.replace(/\s+/g, '');
                $selecthtml = $selecthtml+'<option id="'+str+'">'+customer+'</option>';
              }
              $('#customer').html($selecthtml).selectpicker('refresh');
            }
          }
      });
}
function getClientSKUs(client)
{
   $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
     //alert(client);
       $.ajax({
          url: "getskus",
          type: "post",
          async: false,
          data: {'value':client} ,
          success: function (response) {
            if(response.error == true)
            {
              message('message',response,'warning');
            }
            else
            {
              $('#sku').empty();
              $('#sku').attr('disabled',false);
              $selecthtml ='';
              for(var x in response.data)
              {
                var sku = response.data[x];
                //alert(customer);
                str = sku.replace(/\s+/g, '');
                $selecthtml = $selecthtml+'<option id="'+str+'">'+sku+'</option>';
              }
              $('#sku').html($selecthtml).selectpicker('refresh');
            }
          }
      });
}
function getIssueOrderData(value)
{
  if(value == 'New Issue Order')
    {
      $('#client').selectpicker('val', 'Pick Something Please!');
      $('#site').selectpicker('val', 'Pick Something Please!');
      $('#customer').selectpicker('val', 'Pick Something Please!');
      $('#assigned-issueorderitems-table').dataTable().fnClearTable();
      $("#assigned-issueorderitems-title").text('No Issue Order Picked Yet');
    }
    else
    {
         document.getElementById('message').innerHTML=
              '<div class="alert alert-warning" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                  +'<strong>Loading ...</strong>'
              +'</div>';
       $("#assigned-issueorderitems-title").text(value+' Items');
       $.ajax({
          url: "getentry",
          type: "post",
          data: {'value':value} ,
          success: function (response) {
            if(response.error == true)
            {
              message('message',response,'warning');
            }
            else
            {
               document.getElementById('message').innerHTML=
              '<div class="alert alert-success" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
                  +'<strong>Loading Finished</strong>'
              +'</div>';
              $('#client').selectpicker('val', response.data.CLI_name);
              $('#site').selectpicker('val', response.data.SITE_name);
              getClientCustomers(response.data.CLI_name);
              getClientSKUs(response.data.CLI_name);
              $('#customer').selectpicker('val',response.data.CST_name);

             // update Issue Order Items
             var issueOrderItems = $('#assigned-issueorderitems-table').DataTable();
             $('#assigned-issueorderitems-table').dataTable().fnClearTable();
             
              for(var orderItem in response.data.issueorderitems_data)
              {
                if(response.data.issueorderitems_data[orderItem].STRAT_name != null && response.data.issueorderitems_data[orderItem].STRAT_name != '')
                {
                  issueOrderItems.row.add
                  ([
                    response.data.issueorderitems_data[orderItem].issueitem.ISI_number,
                    response.data.issueorderitems_data[orderItem].SKU_name,
                    response.data.issueorderitems_data[orderItem].issueitem.ISI_amount,
                    response.data.issueorderitems_data[orderItem].STRAT_name,
                    response.data.issueorderitems_data[orderItem].issueitem.ISI_shippingDate,
                    '<button type="button" class="btn btn-danger remove" disabled = true>Remove</button>'
                  ]).draw();
                }
                else
                {
                  issueOrderItems.row.add
                  ([
                    response.data.issueorderitems_data[orderItem].issueitem.ISI_number,
                    response.data.issueorderitems_data[orderItem].SKU_name,
                    response.data.issueorderitems_data[orderItem].issueitem.ISI_amount,
                    'No Strategy Assigned',
                    response.data.issueorderitems_data[orderItem].issueitem.ISI_shippingDate,
                    '<button type="button" class="btn btn-danger remove" disabled = true>Remove</button>'
                  ]).draw();
                }
              }
              issueOrderItems.draw();             
             $('#addissueorderitem').attr('disabled',false);
             $('#finish').attr('disabled',false);
             $('#issueordername').val(value);
             $('#issueordernamex').val(value);
            }                 
             
              ///////////////////////////////////////////////////////////////
             
          },
          error: function(jqXHR, textStatus, errorThrown) {
             

          }


      });
    }
}

$(document).ready(function(){
  $('#addissueorderitem').attr('disabled',true);
  $('#finish').attr('disabled',true);
  //get customers
  $('#client').on('change', function(){
    var client = $(this).find("option:selected").val();
    getClientCustomers(client);
    getClientSKUs(client); 
  });
  //get issue order data
  $('#issueorder').on('change', function() {
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
    var value = this.value;
    if(value == 'New Issue Order')
    {
      $('#addissueorderitem').attr('disabled',true);
      $('#finish').attr('disabled',true);
    }
    getIssueOrderData(value);

  });
  $("#saveissueorder").submit(function(e) {

     $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
     var sentdata = $("#saveissueorder").serialize();
     if($('#saveissueorder').valid())
     {

       $.ajax({
            url: "save",
            type: "post",
            data: sentdata ,
            success: function (response) {
              var issueordervalue = $('#issueorder').val();
               if(response.error==true){
                   message('message',response,'warning');
               }
               else
               {
                   message('message',response,'success');
                  if(response.state == 'new')
                  {
                      $('#issueorder').append($('<option>', {
                          value: response.data,
                          text: response.data
                      }));
                      $('#issueorder').selectpicker('refresh');              
                      $('#issueorder').selectpicker('val',response.data);
                  }
                  else if(response.state == 'old')
                  {
                    $('#issueorder').selectpicker('val',response.data);
                  }
                  $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
               if(response.state == 'new')
                  getIssueOrderData(response.data);
                else if(response.state == 'old')
                  getIssueOrderData(issueordervalue);
               }
            },
            error: function(jqXHR, textStatus, errorThrown) {
               
            }


        });
    }
      e.preventDefault();
  });

  $("#saveissueorderitem").submit(function(e) {

     $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
     var sentdata = $("#saveissueorderitem").serialize();
     if($('#saveissueorderitem').valid())
     {
       $.ajax({
            url: "saveitem",
            type: "post",
            data: sentdata ,
            success: function (response) {
               if(response.error==true){
                 message('message-step-2',response,'warning');
               }
               else
               {
                 message('message-step-2',response,'success');
                var issueorderitems = $('#assigned-issueorderitems-table').DataTable();
                issueorderitems.row.add
                ([
                  response.data[0].ISI_number,
                  $('#sku').val(),
                  $('#amount').val(),
                  $('#orderstrategy').val(),
                  response.data[0].ISI_shippingDate,
                  '<button type="button" class="btn btn-danger remove" disabled= "true">Remove</button>'
                ]).draw();
               }
            },
            error: function(jqXHR, textStatus, errorThrown) {
               
            }


        });
    }
      e.preventDefault();
  });
  $(document).on("click",".remove",function(e)
    {
      var issueorderitems = $('#assigned-issueorderitems-table').DataTable();
      var oTableApi = $('#assigned-issueorderitems-table').dataTable().api();
      var tr = $(this).closest('tr');
      cell1 = tr.find("td:first");
      var issueItem = oTableApi.cell(cell1).data();
      $.ajax({
          url: "removeitem",
          type: "post",
          data: {'issueorder':$("#issueordername").val(),
                 'issueorderitem':issueItem
                } ,
          success: function (response) {
              issueorderitems.row(tr).remove().draw();
          },
          error: function(jqXHR, textStatus, errorThrown) {
             

          }
      });
      
    });

});