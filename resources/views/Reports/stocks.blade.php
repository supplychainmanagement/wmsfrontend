@extends('main&templates.MainTemplateMasterdata')

@section('content')
<div class = "row">
<div class="panel panel-warning" id= "data-panel">
  <form role="form" class="form-horizontal" method="post" id="stockgenerate-form">
  {!! csrf_field() !!}
     <div class="panel-heading">                                
        <h3 class="panel-title" id = "table-title">Generate Stock Report</h3>
          <ul class="panel-controls">
              <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
              <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
               <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
           </ul>                                               
    </div>
    <div class="panel-body"> 

        <div class="row">

          	<div class="col-md-6">
                <div class="form-group">
                        <label class="col-md-4 control-label">Client</label>
                          <div class="col-md-8">
                              <select class="form-control selectpicker show-tick" name = "CLI_name" id= "CLI_name" title="All Clients" data-live-search="true">
                                @foreach ($clients as $client)
                                  <option>{{$client}}</option>
                                @endforeach
                        </select>
                          </div>
                    
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-5 control-label">day</label>
                    <div class="col-md-7">
                        <div class="input-group date" id="dp-2" data-date="{{$date}}" data-date-format="yyyy-mm-dd">
                            <input type="text" class="form-control datepicker" value="{{$date}}" id="day" name="day" />
                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                </div>
            </div>
        	</div>
        </div>                                
         <div class="panel-footer">
            <button type="submit" value= "Submit" id = "generate" class="btn btn-primary pull-right">Generate</button>
         </div>
         </form>
  </div>
  </div>
  <div class="row" id = "report-panel">
  <div class="panel panel-default" id= "data-panel">
	    <div class="panel-heading">
	        <h3 class="panel-title" id = "report-title"></h3>
	        <div class="btn-group pull-right">
	        </div>
	        <ul class="panel-controls">
	            <li><a href="#" id = "hoho"><span class="fa fa-plus"></span></a></li>
	            <li><a href="#" class="panel-collapse" ><span class="fa fa-angle-down"></span></a></li>
	            <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
	            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
	        </ul>
	    </div>
	    <div class="panel-body">
	        <table id="report-table" class="table">
	              <thead>
                    <tr>
                      <th>SKU</th>
                      <th>Client</th>
                      @foreach($sites as $site)
                        <th>
                          {{$site}}
                        </th>
                      @endforeach
                    </tr>                
                 </thead>
                 <tfoot>
                    <tr>
                      <th>SKU</th>
                      <th>Client</th>
                      @foreach($sites as $site)
                        <th>
                          {{$site}}
                        </th>
                      @endforeach
                    </tr>                
                 </tfoot>
                 <div class = "scroll">
                   <tbody>             
                   </tbody>
                   </div>
	        </table>

	    </div>
	</div>
  </div>

@stop

@section('Jsplugin')
<script type="text/javascript">
   $('#reportslink').addClass('active');
   $('#stocklink').addClass('active');
</script>
<script type="text/javascript" src="{{asset('js/reports/stocks.js')}}"></script>
@stop                       