@extends('main&templates.tables_layout')
@section('actionOfUpdate')
    customer/update
@stop

@section('actionOfDelete')
    customer/delete
@stop

@section('table-title')
    Customers
@stop


@section('table-head')
    <tr>
        <th style="display:none";>Customer ID</th>
        <th>Name</th>
        <th>Branch</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Address</th>
    </tr>
@stop

@section('table-body')

    @foreach($customers as $customer)
        <tr>
            <td style="display:none;">{{$customer['id']}}</td>
            <td>{{$customer['CST_name']}}</td>
            <td>{{$customer['CST_branch']}}</td>
            <td>{{$customer['CST_email']}}</td>
            <td>{{$customer['CST_phone']}}</td>
            <td>{{$customer['CST_address']}}</td>

        </tr>
    @endforeach

@stop

@section('prop-head')

    <th>Property</th>
    <th>Value</th>
@stop


@section('prop-body')


@stop
@section('message-body')
    @if(session()->has('message'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                        <p>{{session('message')}}</p>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif
@stop
@if(session('system_client') == true)
    @if(session('user_role') == 'system_admin'||session('user_role')=='site_admin')
@section('create-panel')

    <div id="data"class="col-md-12">

        <div class="panel panel-warning" id= "data-panel">
            <form role="form" class="form-horizontal"  action="{{route('customerCreate')}}" method="post" id="customer-form">
                {!! csrf_field() !!}
                <div class="panel-heading">
                    <h3 class="panel-title" id = "table-title">Create New Customer</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">

                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Name</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name = "CST_name" required/>
                                </div>
                            </div>
                        </div>

                        @if(session('system_client') == true)
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Client</label>
                                    <div class="col-md-10">
                                            @foreach ($clients as $client)
                                            <input type="checkbox" name = "CST_CLI[]"
                                                   value="{{$client['CLI_name']}}">{{$client['CLI_name']}}
                                                <br/>
                                            @endforeach
                                    </div>
                                </div>
                            </div>
                        @endif


                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Branch</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name = "CST_branch"/>
                                </div>
                            </div>
                        </div>

                    </div>

                    </br>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Email</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name = "CST_email"/>
                            </div>
                        </div>
                    </div>



                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Phone</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name = "CST_phone"/>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Address</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name = "CST_address"/>
                            </div>
                        </div>
                    </div>

                    <br/>

                </div>


        </div>
        <div class="panel-footer">
            <button type="submit" value= "Submit" class="btn btn-primary pull-right">Submit</button>
        </div>
        </form>
    </div>

    </div>

@stop

@endif
@endif


@section('Jsplugin')
    <script type="text/javascript" src="js/masterdata/customer.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
            var pathname = window.location.href;
            var position = pathname.indexOf("?");
            if(position != -1)
            {
                var getvalues = pathname.substring(position+1,pathname.length);
                var splited = getvalues.split('=');
                console.log(splited[0]);
                if(splited.length == 2)
                {
                    if(splited[0] == 'search')
                    {
                         $('.maintable').DataTable().search(splited[1]).draw();
                         console.log('i am here');
                    }
                } 
            }
        }); 
    </script>
@stop
