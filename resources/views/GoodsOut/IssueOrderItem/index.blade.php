@extends('main&templates.tables_layout')


@section('table-title')
    Issue Order Items
@stop


@section('table-head')
    <tr>
        <th style="display:none";>Issue Order Item ID</th>
        <th>Issue Order Item Number</th>
        <th>State</th>
        <th>Shipping Date</th>
        <th>Picked</th>
        <th>Shipped</th>
        <th>Amount</th>
        <th>Issue Order</th>
        <th>SKU</th>
        <th>Strategy</th>
        <th>Site</th>
    </tr>
@stop

@section('table-body')

    @foreach($issueItems as $issueItem)
        <tr>
            <td style="display:none;">{{$issueItem['issueitem']['id']}}</td>
            <td>{{$issueItem['issueitem']['ISI_number']}}</td>
            <td>{{$issueItem['issueitem']['ISI_state']}}</td>
            <td>{{$issueItem['issueitem']['ISI_shippingDate']}}</td>
            <td>{{$issueItem['issueitem']['ISI_picked']}}</td>
            <td>{{$issueItem['issueitem']['ISI_shipped']}}</td>
            <td>{{$issueItem['issueitem']['ISI_amount']}}</td>
            <td><a href="{{route('issueorder',array('search'=>$issueItem['issueorder']))}}">{{$issueItem['issueorder']}}</a></td>
            <td><a href="{{route('sku',array('search'=>$issueItem['sku']))}}">{{$issueItem['sku']}}</a></td>
            <td><a href="{{route('strategy',array('search'=>$issueItem['strategy']))}}">{{$issueItem['strategy']}}</a></td>
            <td><a href="{{route('site',array('search'=>$issueItem['site']))}}">{{$issueItem['site']}}</a></td>
        </tr>
    @endforeach

@stop

@section('message-body')
    @if(session()->has('message'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                        <p>{{session('message')}}</p>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif
@stop
@section('Jsplugin')
<script>
$('.maintable').dataTable();
        $('#goodsoutlink').addClass('active');
        $('#issueorderitemlink').addClass('active');
        $(document).ready(function(){
            var pathname = window.location.href;
            var position = pathname.indexOf("?");
            if(position != -1)
            {
                var getvalues = pathname.substring(position+1,pathname.length);
                var splited = getvalues.split('=');
                console.log(splited[0]);
                if(splited.length == 2)
                {
                    if(splited[0] == 'search')
                    {
                         $('.maintable').DataTable().search(splited[1]).draw();
                         console.log('i am here');
                    }
                } 
            }
        }); 
    </script>
@stop