@extends('main&templates.tables_layout')

@section('table-title')
    Pick Jobs
@stop


@section('table-head')
    <tr>
        <th style="display:none";>Pick Job ID</th>
        <th>Pick Job Number</th>
        <th>amount</th>
        <th>Unitload</th>
        <th>Pick Request Item</th>
        <th>Site</th>
    </tr>
@stop

@section('table-body')

    @foreach($pickJobs as $pickJob)
        <tr>
            <td style="display:none;">{{$pickJob['pickjob']['id']}}</td>
            <td>{{$pickJob['pickjob']['PJ_number']}}</td>
            <td>{{$pickJob['pickjob']['PJ_amount']}}</td>
            <td><a href="{{route('unitload',array('search'=>$pickJob['unitload']))}}">{{$pickJob['unitload']}}</a></td>
            <td><a href="{{route('pickorder.pickrequestitemsindex',array('search'=>$pickJob['pickrequestitem']))}}">{{$pickJob['pickrequestitem']}}</a></td>
            <td><a href="{{route('site',array('search'=>$pickJob['site']))}}">{{$pickJob['site']}}</a></td>
        </tr>
    @endforeach

@stop
@if(session('system_client') == true)
@if(session('user_role') != 'labor')
@section('create-panel')

  <div id="data" class="col-md-12">

            <div class="panel panel-warning" id= "data-panel">
              <form role="form" class="form-horizontal"  action="{{route('createPickJob')}}" method="post" id="pickjob-form">
              {!! csrf_field() !!}
                 <div class="panel-heading">                                
                    <h3 class="panel-title" id = "table-title">Create new Pick Job</h3>
                      <ul class="panel-controls">
                          <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                          <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                           <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                       </ul>                                               
                </div>
                <div class="panel-body"> 
                      
                      <div class="row">

                          <div class="col-md-6" style="margin-left: 25px;">
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Pick Job Number: </label>
                                  <label class="col-md-4 control-label" id="PJ_number">{{$PJ_number}}</label>
                              </div>
                          </div>
                      </div>
                    <br/>

                    <div class="row">

                      <div class="col-md-6">
                            <div class="form-group">
                                    <label class="col-md-4 control-label">Pick Request</label>
                                      <div class="col-md-8">
                                          <select class="form-control selectpicker show-tick" name = "pickrequest" id= "pickrequest" title="Pick Something Please!" data-live-search="true" required>
                                            @foreach ($pickrequests as $pickRequest)
                                              <option>{{$pickRequest}}</option>
                                            @endforeach
                                    </select>
                                      </div>
                                <input type="text" style="display: none;" name="PJ_number" value ={{$PJ_number}} >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                    <label class="col-md-4 control-label">Pick Request Item</label>
                                      <div class="col-md-8">
                                          <select class="form-control selectpicker show-tick" name = "pickrequestitem" id = "pickrequestitem" title="Pick Something Please!" data-live-search="true" required>
                                    </select>
                                      </div>
                            </div>
                        </div>
                    </div>
                    </div>                                
                     <div class="panel-footer">
                        <button type="submit" value= "Submit" id = "submitJob" disabled =true class="btn btn-primary pull-right">Submit</button>
                     </div>
                     </form>
              </div>
           
    </div>

    @stop
    
  @endif
  @endif
@section('message-body')
    @if(session()->has('message'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                        <h4 style="color:white;">{{session('message')}} if yoy have finished all the pick jobs for this pick request you may procced to the <a href="{{route('ShippingCreate')}}">Shipping</a> Step</h4>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif
@stop

@section('Jsplugin')
    <script type="text/javascript" src="{{asset('js/GoodsOut/pickjob.js')}}"></script>
  <script>
  $('.maintable').dataTable();
        $('#goodsoutlink').addClass('active');
        $('#pickjoblink').addClass('active');
    </script>
@stop   