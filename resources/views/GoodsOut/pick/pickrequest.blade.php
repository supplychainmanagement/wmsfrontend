@extends('main&templates.MainTemplateMasterdata')
@section('content')


<div class="row" style="margin-bottom:10px;margin-top:10px;">
  <div class="col-md-4">
    <div class="panel panel-warning">
      <div class="panel-heading">
          <h3 class="panel-title">Order</h3>
      </div>
      <div class="panel-body">
        <div class="row" style="margin-top: 10px">
          <div class="form-group">
            <label class="col-md-5 control-label">Orders</label>
            <div class="col-md-7">                                        
              <select class="form-control selectpicker show-tick" name = "issueorder" title="Pick Something Please!" id ="issueorder" onchange="" data-live-search="true">
                @foreach ($issueOrders as $issueOrder)
	              <option>{{$issueOrder}}</option>
	            @endforeach
              </select>
            </div>
          </div>
        </div>
        <div class="row" style="margin-top: 10px">
          <div class="form-group">
            <label class="col-md-5 control-label">Issue Order Item</label>
            <div class="col-md-7">                                        
              <select class="form-control selectpicker show-tick" name = "issueorderitem" title="Pick Something Please!" id = "issueorderitem" data-live-search="true">
              </select>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4 text-center" id= "pick-out" style="display: none;">
    <h5>Pick-Out</h5>
    <input class="knob" data-width="150" data-min="0" data-fgColor="#FD421C" data-displayPrevious=true value="0" id= "pick-out-knob" data-readOnly="true" id ='pick-out-knob'/>
  </div>
  <div class="col-md-4 text-center" id= "pick-in" style="display: none;" >
    <h5>Pick-In</h5>
    <input class="knob" data-width="150" data-min="0" data-fgColor="#FD421C" data-displayPrevious=true value="0" id= "pick-in-knob" data-readOnly="true" id ='pick-in-knob'/>
  </div>
</div>

<div class="row" style="margin-bottom:10px;margin-top:10px;">
  <div class="col-md-8" id="message">
  </div>
</div>


<div class="row" style="margin-bottom:10px;margin-top:10px;">
  <div class = "col-md-6">
    <div class="panel panel-warning">
      <div class="panel-heading">
          <h3 class="panel-title">Pick-Out</h3>
           <button class="btn btn-success pull-right suggestpickout" disabled= true id = "suggestpickout">Suggest</button>
      </div>
      <div class="panel-body">
        <div class = "row">
        <form role="form" class="form-horizontal" id = "pickoutform">
            <div class= row style="margin-bottom:10px;margin-top:10px;">
              <div class="form-group col-md-12">
              <div class ="row">
              <label class="col-md-5 control-label">Unitload</label>
              <div class="col-md-7">                                        
                <select class="form-control selectpicker show-tick" name = "pickoutunitload" title="Pick Something Please!" id ="pickoutunitload" onchange="" data-live-search="true">
                  <!-- for each unit load -->
                </select>
              </div>
              </div>
              </div>
              </div>
              <div class= row style="margin-bottom:10px;margin-top:30px;">
              <div class="form-group col-md-9" id ="pickoutsliderdiv">
              </div>
              <div class="col-md-3">
                <button  type="submit" value= "Submit" class="btn btn-success pull-right" class="pickoutadd" id = "pickoutadd" disabled=true>Add</button>
              </div>
            </div>
        </form>
        </div>
        <div class ="row" style="margin-bottom:10px;margin-top:25px;">
        <div class="col-md-12">
          <table class="table  datatable maintable" id = "pickout-table">
                  <thead>
                      <tr>
                        <th>Unitload</th>
                        <th>Amount</th>
                        <th>Remove</th>
                      </tr>                
                   </thead>
                   <div class = "scroll">
                     <tbody>             
                     </tbody>
                     </div>
          </table>                                        
        </div>
        </div>
      </div>
    </div>
  </div>

  <div class = "col-md-6">
    <div class="panel panel-warning">
      <div class="panel-heading">
          <h3 class="panel-title">Pick-In</h3>
          <button class="btn btn-success pull-right" disabled= true>Suggest</button>
      </div>
      <div class="panel-body">
        <div class = "row">
        <form role="form" class="form-horizontal" id = "pickinform">
            <div class= row style="margin-bottom:10px;margin-top:10px;">
              <div class="form-group col-md-12">
              <div class ="row">
              <label class="col-md-5 control-label">Unitload</label>
              <div class="col-md-7">                                        
                <select class="form-control selectpicker show-tick" name = "pickinunitload" title="Pick Something Please!" id ="pickinunitload" onchange="" data-live-search="true">
                  <!-- for each unit load -->
                </select>
              </div>
              </div>
              </div>
              </div>
              <div class= row style="margin-bottom:10px;margin-top:30px;">
              <div class="form-group col-md-9" id = "pickinsliderdiv">
              </div>
              <div class="col-md-3">
                <button  type="submit" value= "Submit" class="btn btn-success pull-right" disabled = true class="pickinadd" id = "pickinadd">Add</button>
              </div>
            </div>
        </form>
        </div>
        <div class ="row" style="margin-bottom:10px;margin-top:25px;">
        <div class="col-md-12">
          <table class="table  datatable maintable" id = "pickin-table">
                  <thead>
                      <tr>
                        <th>Unitload</th>
                        <th>Amount</th>
                        <th>Remove</th>
                      </tr>                
                   </thead>
                   <div class = "scroll">
                     <tbody>             
                     </tbody>
                     </div>
          </table>                                        
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-check"></span>Success</div>
                    <div class="mb-content">
                        <h4 style="color:white;">Pick Request Successfully created you may proceed to the <a href="{{route('pickorder.pickjobindex')}}">Pick Job</a> Step</h4>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>
<div class = "row">
 <div class="col-md-12">
  <button  class="btn btn-success btn-block pickrequest"  id= 'pickrequest' disabled=true>Generate Pick Request</button>
 </div>
</div>
@stop

@section('Jsplugin')
<script type="text/javascript" src="{{asset('js/GoodsOut/pickorder.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/rangeslider/jQAllRangeSliders-min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/addslider/addSlider.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/addslider/Obj.min.js')}}"></script>

@stop