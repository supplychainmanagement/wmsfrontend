@extends('main&templates.MainTemplateMasterdata')

@section('content')
    <div class="panel panel-default" id= "data-panel">
    <div class="panel-heading">
        <h3 class="panel-title" id = "table-title">Shipping {{$SHIP_number}}
            <button class = "pulse-button popover-dismiss"  data-content="Ship Pick Request Item" data-placement="bottom"></button>
        </h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>
    </div>

    <!-- Creation Body -->
    <!-- Khaled M.Fathy-->

    <div class="panel-body">
        <form role="form" class="form-horizontal"  action="{{route('SubmitShip')}}" method="post" id="ship-form">
            {!! csrf_field() !!}
            <div class="row">

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-6 control-label">Issue Order</label>
                        <div class="col-md-10">
                            <select name="ISO_name" id="ISO_name" class="form-control selectpicker show-tick"
                                    title="Choose Issue Order" form="ship-form" required>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
            <br/>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-6 control-label">Issue Order Item</label>
                        <div class="col-md-10">
                            <select class="form-control selectpicker show-tick" name="ISI_number" id="ISI_number"
                                    title="Choose Issue Order Item" form="ship-form" required>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <input type="text" form="ship-form" id="SHIP_number" name="SHIP_number" style="display: none;" value={{$SHIP_number}}>
            <br/>


            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-6 control-label">Shipping Agency</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name = "SHIP_shippingAgency" id="SHIP_shippingAgency"
                                   form ="ship-form" required/>
                        </div>
                    </div>
                </div>
            </div>
            <br/>


            <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="col-md-6 control-label">Date of Shipping</label>
                    <div class="col-md-10">
                        <div class="input-group date" id="dp-2" data-date="{{$date}}" data-date-format="yyyy-mm-dd">
                            <input type="text" class="form-control datepicker" value="{{$date}}" id="SHIP_date"
                                   name="SHIP_date" form ="ship-form" required/>
                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <br/>


            <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-6 control-label">Driver Name</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name = "CST_name" required/>
                            </div>
                        </div>
                    </div>
            </div>
            <br/>

            <div class ="row">
                 <div class="col-md-5" id="message">

                 </div>
            </div>
            <div class="panel-footer">
                <button type="submit" value= "Submit" class="btn btn-primary pull-right">Submit</button>
            </div>
        </form>
    </div>
    </div>
    <script type="text/javascript" src={{("../js/GoodsOut/ship.js")}}></script>

@stop

@if(session()->has('message'))
    <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                <div class="mb-content">
                    @if(session('messagetype') == 'danger' ||session('messagetype') == 'warning')
                    <ul>
                     @foreach(session('message') as $mess)
                        <li>{{$mess}}</li>
                    @endforeach
                    </ul>
                    @endif
                    @if(session('messagetype') == 'success')
                        <h4 style="color:white;"> Shipping Order Successfully Submitted Out-Bound Operation Done</h4>
                    @endif
                    <div class="mb-footer">
                        <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function showless() {
            //alert('i am here');
            document.getElementById("message-box-success").style.display="none";
        }
    </script>

@endif

