
 <form role="form" class="form-horizontal" id="saveissueorderitem" method="post">
    {!! csrf_field() !!}
    <input type="hidden" name="issueordername" id="issueordername">
    <div class="row" style="margin-bottom: 20px">
      <div class="col-md-6">
        <div class="form-group">

          <label class="col-md-5 control-label">SKU</label>
          <div class="col-md-7">
            <select class="form-control selectpicker show-tick" name = "sku" title="Pick Something Please!" id = "sku" data-live-search="true" required>
            <!-- SKU FOR EACH -->
            </select>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
            <label class="col-md-5 control-label">Amount</label>
              <div class="col-md-7">
                  <input type="number" class="form-control" name = "amount" id="amount" required/>
              </div>
        </div>
      </div>
      </div>
      <div class="row" style="margin-bottom: 20px">
      <div class="col-md-6">
        <div class="form-group">
          <label class="col-md-5 control-label">Order Strategy</label>
          <div class="col-md-7">                                        
            <select class="form-control selectpicker show-tick" name = "orderstrategy" title="Pick Something Please!" id = "orderstrategy" data-live-search="true">
            @foreach ($strategy_names as $strategy)
              <option>{{$strategy}}</option>
            @endforeach
            </select>
          </div>
        </div>
      </div>
      <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-5 control-label">Date of Shipping</label>
                <div class="col-md-7">
                    <div class="input-group date" id="dp-2" data-date="{{$date}}" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control datepicker" value="{{$date}}" id="from" name="shippingdate" required/>
                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-bottom:10px;margin-top:10px;">
      <div class="col-md-5" id="message">

      </div>
      <div class="col-md-offset-6 col-md-1">
      <button id="addissueorderitem" type="submit" value= "Submit" class="btn btn-success pull-right">Add</button>
      </div>
    </div>
</form>
 <div class="col-md-8" id="message-step-2" style = "margin-top:10px">

    </div>
<div class="row" id="assigned-issueorderitems">
  <div class="col-md-12">
            <div class="panel panel-warning" id= "assigned-issueorderitems-panel">
                 <div class="panel-heading">
                      <h3 class="panel-title" id = "assigned-issueorderitems-title">No Issue Order Selected</h3>                               
                      <ul class="panel-controls">
                          <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                           <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                       </ul>                                               
                   </div>
                    <div class="panel-body " >
                    <table class="table  datatable maintable" id = "assigned-issueorderitems-table">
                            <thead>
                                <tr>
                                  <th>Issue Order Item</th>
                                  <th>Item Data</th>
                                  <th>Amount</th>
                                  <th>Strategy</th>
                                  <th>Date of Shipping</th>
                                  <th>Remove</th>
                                </tr>                
                             </thead>
                             <div class = "scroll">
                               <tbody>             
                               </tbody>
                               </div>
                         </table>                                        
                     </div>
             </div>
    </div>
   
  </div>
  <form role="form" class="form-horizontal" id="finishissueorder" method="post" action="{{route('finishissueorder')}}">
  {!! csrf_field() !!}
  <div class="row" style="margin-bottom:10px;margin-top:10px;" >
    <input type="hidden" name="issueordernamex" id="issueordernamex"></input>
      <button  type="submit" value= "Submit" class="btn btn-success pull-right" id="finish">Finish</button>
  </div>
  </form>