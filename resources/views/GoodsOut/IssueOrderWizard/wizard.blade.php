@extends('main&templates.MainTemplateMasterdata')
<!-- START DEFAULT WIZARD -->
@section('content')
<div class = 'row'>
<div class="col-md-12">
<div class="block">
    <h4>Issue Order Wizard</h4>
    <div class="wizard">
        <ul>
            <li>
                <a href="#step-1">
                    <span class="stepNumber">1</span>
                    <span class="stepDesc">Create / Pick Issue Order<br /><small>Pick order strategy & customer & client</small></span>
                </a>
            </li>
            <li>
                <a href="#step-2">
                    <span class="stepNumber">2</span>
                    <span class="stepDesc">Create / Update Issue Order Items<br /><small>Add & Remove Issue Order Items</small></span>
                </a>
            </li>
            
        </ul>
        <div id="step-1">   
            <h4>Create / Pick Issue Order</h4>
            <div class="row">
            @include('GoodsOut.IssueOrderWizard.step1')
            </div>
       	</div>
        <div id="step-2">
            <h4>Create / Update Issue Order Items</h4>
            <div class="row">
            @include('GoodsOut.IssueOrderWizard.step2')
            </div>
        </div>                      
                                
   </div>
</div>
</div>
</div>
@stop   


@section('Jsplugin')
<script type="text/javascript" src="{{asset('js/plugins/smartwizard/jquery.smartWizard-2.0.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/GoodsOut/createissueorder.js')}}"></script>

<script type="text/javascript" src="{{asset('js/plugins/tour/bootstrap-tour.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/tour/bootstro.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/tour/tour.js')}}"></script>
@stop                                     
<!-- END DEFAULT WIZARD -->