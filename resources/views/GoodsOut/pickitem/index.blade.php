@extends('main&templates.tables_layout')

@section('table-title')
    Pick Request Items
@stop


@section('table-head')
    <tr>
        <th style="display:none";>Pick Request Item ID</th>
        <th>Pick Request Item Number</th>
        <th>amount</th>
        <th>type</th>
        <th>Unitload</th>
        <th>Pick Request</th>
        <th>Site</th>
    </tr>
@stop

@section('table-body')

    @foreach($pickRequestItems as $pickRequestItem)
        <tr>
            <td style="display:none;">{{$pickRequestItem['pickrequestitem']['id']}}</td>
            <td>{{$pickRequestItem['pickrequestitem']['PRI_number']}}</td>
            <td>{{$pickRequestItem['pickrequestitem']['PRI_amount']}}</td>
            <td>{{$pickRequestItem['pickrequestitem']['PRI_type']}}</td>
            <td><a href="{{route('unitload',array('search'=>$pickRequestItem['unitload']))}}">{{$pickRequestItem['unitload']}}</a></td>
            <td><a href="{{route('pickorder.pickrequestindex',array('search'=>$pickRequestItem['pickrequest']))}}">{{$pickRequestItem['pickrequest']}}</a></td>
            <td><a href="{{route('site',array('search'=>$pickRequestItem['site']))}}">{{$pickRequestItem['site']}}</a></td>
        </tr>
    @endforeach

@stop

@section('message-body')
    @if(session()->has('message'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                        <p>{{session('message')}}</p>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif
@stop

@section('Jsplugin')
<script>
$('.maintable').dataTable();
        $('#goodsoutlink').addClass('active');
        $('#pickrequestitemlink').addClass('active');
        $(document).ready(function(){
            var pathname = window.location.href;
            var position = pathname.indexOf("?");
            if(position != -1)
            {
                var getvalues = pathname.substring(position+1,pathname.length);
                var splited = getvalues.split('=');
                console.log(splited[0]);
                if(splited.length == 2)
                {
                    if(splited[0] == 'search')
                    {
                         $('.maintable').DataTable().search(splited[1]).draw();
                         console.log('i am here');
                    }
                } 
            }
        }); 
    </script>
@stop
