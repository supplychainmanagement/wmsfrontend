@extends('main&templates.MainTemplateMasterdata')
<!-- START DEFAULT WIZARD -->
@section('content')
<div class = 'row'>
<div class="col-md-12">
<div class="block">
    <h4>Purchase Order Wizard</h4>
    <div class="wizard">
        <ul>
            <li>
                <a href="#step-1">
                    <span class="stepNumber">1</span>
                    <span class="stepDesc">Create / Pick Purchase Order<br /><small>Pick order date of Delivery & client</small></span>
                </a>
            </li>
            <li>
                <a href="#step-2">
                    <span class="stepNumber">2</span>
                    <span class="stepDesc">Create / Update Purchase Order Items<br /><small>Add & Remove Purchase Order Items</small></span>
                </a>
            </li>
            
        </ul>
        <div id="step-1">

            <h4>Create / Pick Purchase Order</h4>
            <div class="row">
            @include('GoodsIn.PurchaseOrderWizard.step1')
            </div>
       	</div>
        <div id="step-2">
            <h4>Create / Update Purchase Order Items</h4>
            <div class="row">
            @include('GoodsIn.PurchaseOrderWizard.step2')
            </div>
        </div>                      
                                
   </div>
</div>
</div>
</div>
@stop   


@section('Jsplugin')
<script type="text/javascript" src="{{asset('js/plugins/smartwizard/jquery.smartWizard-2.0.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/GoodsIn/createpurchaseorder.js')}}"></script>

<script type='text/javascript' src='{{asset('js/plugins/icheck/icheck.min.js')}}'></script>
<script type="text/javascript" src="{{asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>

<script type="text/javascript" src="{{asset('js/plugins/tour/bootstrap-tour.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/tour/bootstro.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/tour/tour.js')}}"></script>


@stop                                     
<!-- END DEFAULT WIZARD -->