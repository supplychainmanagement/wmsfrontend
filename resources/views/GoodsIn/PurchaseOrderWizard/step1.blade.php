<button onclick="bootstro.start('.bootstro');" class="btn btn-danger">Start Tour</button>


<form role="form" class="form-horizontal" id="savepurchaseorder" method="post">
            {!! csrf_field() !!}

                      <div class="row" style="margin-top: 10px">



                        <div class="col-md-6">
                        <div class="form-group">
                            <div class="bootstro" data-bootstro-title="Purchase Order"
                                 data-bootstro-content="Choose (New Purchase Order) If You Want To Create New Order,
                                 Or Choose Any Existing Purchase Order To Update It."
                                 data-bootstro-placement="right" data-bootstro-width="400px">

                          <label class="col-md-5 control-label">Purchase Order</label>
                          <div class="col-md-7">
                            <select class="form-control selectpicker show-tick " name = "purchaseorder" title="Pick Something Please!" id ="purchaseorder" onchange="" data-live-search="true" required>
                             <option>New Purchase Order</option>
                             <option data-divider="true"></option>
                               @foreach ($purchaseorder_names as $purchaseorder)
                                <option>{{$purchaseorder}}</option>
                               @endforeach
                            </select>
                          </div>
                            </div>
                        </div>
                        </div>






                        <div class="col-md-6">
                          <div class="form-group">
                              <div class="bootstro" data-bootstro-title="Client"
                                   data-bootstro-content="Choose The Client That Will Do This Purchase Order.">
                            <label class="col-md-5 control-label">Client</label>
                            <div class="col-md-7">                                        
                              <select class="form-control selectpicker show-tick" name = "client" title="Pick Something Please!" data-live-search="true" id = "client" required>
                              @foreach ($client_names as $client)
                                <option>{{$client}}</option>
                              @endforeach
                              </select>
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row" style="margin-top: 10px">
                      @if(session('system_client') == true && session('user_role') == 'system_admin')
                         <div class="col-md-6">
                            <div class="form-group">
                                <div class="bootstro" data-bootstro-title="Site"
                                     data-bootstro-content="Choose Which Site of Your Warehouse Will Receive The Goods Assigned
                                      in This Purchase Order."
                                     data-bootstro-width="600px">
                              <label class="col-md-5 control-label">Site</label>
                              <div class="col-md-7">                                        
                                <select class="form-control selectpicker show-tick" name = "site" title="Pick Something Please!" id = "site" data-live-search="true" required>
                                @foreach ($site_names as $site)
                                <option>{{$site}}</option>
                                @endforeach
                                </select>
                              </div>
                                    </div>
                            </div>
                          </div>
                          @endif
                          <div class="col-md-6">
                            <div class="form-group">
                                <div class="bootstro" data-bootstro-title="Site"
                                     data-bootstro-content="Choose Which Date the Goods will be Received in the Warehouse."
                                     data-bootstro-width="600px">
                                <label class="col-md-5 control-label">Date of Delivery</label>
                                <div class="col-md-7">
                                    <div class="input-group date" id="dp-2" data-date="{{$date}}" data-date-format="yyyy-mm-dd">
                                        <input type="text" class="form-control datepicker" value="{{$date}}" id="from" name="dateofdelivery" required/>
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                                    </div>
                            </div>
                        </div>
                       
                      </div>
                      <div class="row" style="margin-bottom:10px;margin-top:10px;">
                          <div class="col-md-5" id="message">

                          </div>
                        <div class="col-md-offset-6 col-md-1">
                            <div class="bootstro" data-bootstro-title="Save"
                                 data-bootstro-content="After You Finish All Previous Steps, Don't Forget To Click Save">
                          <button  type="submit" value= "Submit" class="btn btn-success pull-right">Save</button>
                                </div>
                        </div>
                      </div>
</form>





