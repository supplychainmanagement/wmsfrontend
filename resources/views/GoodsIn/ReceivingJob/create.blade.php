@extends('main&templates.MainTemplateMasterdata')
@section('content')
    <div class = 'row'>
        <div class="col-md-12">
            {!! csrf_field() !!}
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title">Receiving Job {{$RCJ_number}}</h3>
                        </div>
                        <div class="panel-body">
                            <button onclick="bootstro.start('.bootstro');" class="btn btn-danger">Start Tour</button>

                            <div class="row" style="margin-top: 10px">
                                <br/>
                                <div class="form-group">
                                    <div class="bootstro" data-bootstro-title="Purchase Order"
                                         data-bootstro-content="Choose Which Purchase Order You've Received Its SKUs"
                                         data-bootstro-placement="right" data-bootstro-width="400px">
                                    <label class="col-md-5 control-label">Purchase Order</label>
                                    <div class="col-md-7">
                                        <select class="form-control selectpicker show-tick" name = "PO_name" title="Choose Purchase Order" id ="PO_name" onchange="" data-live-search="true">
                                            @foreach ($PO_names as $PO_name)
                                                <option>{{$PO_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                    </div>

                            </div>
                            <div class="row" style="margin-top: 10px">
                                <div class="form-group">
                                    <div class="bootstro" data-bootstro-title="Purchase Order Item"
                                         data-bootstro-content="Choose Which Purchase Order Item You've Received Its SKUs"
                                         data-bootstro-placement="right" data-bootstro-width="400px">
                                    <label class="col-md-5 control-label">Purchase Order Items</label>
                                    <div class="col-md-7">
                                        <select class="form-control selectpicker show-tick" name = "POI_name" title="Choose Purchase Order Item" id = "POI_name" data-live-search="true">
                                        </select>
                                    </div>
                                </div>
                                    </div>
                            </div>
                            <br/>
                            <div class="form-group">
                                <div class="bootstro" data-bootstro-title="Date of Production"
                                     data-bootstro-content="Specify The Production Date of The Received SKUs"
                                     data-bootstro-placement="right" data-bootstro-width="400px">
                                <label class="col-md-5 control-label">Date of Production</label>
                                <div class="col-md-7">
                                    <div class="input-group date" id="dp-2" data-date="{{Carbon\Carbon::now()->toDateString()}}" data-date-format="yyyy-mm-dd">
                                        <input type="text" class="form-control datepicker" value="{{Carbon\Carbon::now()->toDateString()}}" id="productiondatefrom" name="POI_productiondate"/>
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                                    </div>
                            </div>
                            <br/>
                            <br/>
                            <div class="bootstro" data-bootstro-title="Amount"
                                 data-bootstro-content="Specify The Amount of SKUs You've Received"
                                 data-bootstro-placement="right" data-bootstro-width="400px">
                            <label class="col-md-5 control-label">Receiving Job Amount</label>
                            <div class="col-md-5">
                                <form id="myForm">
                                <input type="text" name="RCJ_amount" class="form-control" id="RCJ_amount" required>
                                </form>
                                </div>
                            </div>
                            <div class="bootstro" data-bootstro-title="Add"
                                 data-bootstro-content="Don't Forget To Click The Add Button"
                                 data-bootstro-placement="right" data-bootstro-width="400px">
                            <button type="button" class="btn btn-warning disabled" id="RCJ_add">Add</button>
                        </div>
                            </div>
                    </div>
                </div>


                <div class="col-md-5 text-center hide" id= "knob-col" style="margin-top:27px;">
                    <input class="knob" data-min="0" data-fgColor="#FD421C" data-displayPrevious=true id= "requiredKnob" data-readOnly="true"
                           style="border: 0px; font-style: normal;
                             font-variant: normal; font-weight: bold; font-stretch: normal;
                              font-size: 37px; line-height: normal; font-family: Arial;
                              text-align: center; color: rgb(253, 66, 28); padding: 0px;
                              -webkit-appearance: none; background: none;"/>
                </div>

            </div>
            <div class = "row">
                <div class="col-md-5" id="message">
                    
                </div>
            </div>
                


{{--Purchase Order Info Div--}}
            <div class="row">
                <div class="col-md-6 hide" id ="POI_info">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title">Purchase Order Item Info</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group border-bottom">
                                <li class="list-group-item">Client<span class="badge badge-info" id ="client"></span></li>
                                <li class="list-group-item">Provider<span class="badge badge-info" id ="provider"></span></li>
                                <li class="list-group-item">Production Date<span class="badge badge-info" id ="productiondate"></span></li>
                            </ul>
                        </div>
                    </div>
                </div>

{{--Amount Info Div--}}
                <div class="col-md-6 hide" id ="amount-info">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title">Amount Info</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group border-bottom">
                                <li class="list-group-item">SKU<span class="badge badge-info" id="sku"></span></li>
                                <li class="list-group-item">Amount Required<span class="badge badge-info" id="amount"></span></li>
                                <li class="list-group-item">Amount Received<span class="badge badge-info" id="received"></span></li>

                            </ul>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>

    <div class="row" style="margin-bottom:10px;margin-top:10px;">
        <div class="col-md-5" id="message">
        </div>
    </div>


    {{--Available Unitloads Table--}}
    <div class="row collapse" style="margin-bottom:10px;margin-top:10px;" id="availableUnitloadsDiv">
        <div class = "col-md-12">
            <div class="panel panel-warning" id= "available-panel">
                <div class="panel-heading">
                    <h3 class="panel-title" id = "table-title">Available Unitloads</h3>
                </div>
                <div class="panel-body " >
                    <table class="table  datatable maintable" id="available_unitloads">
                        <thead>
                        <tr>
                            <th>Unitload</th>
                            <th>Route</th>
                            <th>Status</th>
                            <th>Location</th>
                            <th>Amount</th>
                            <th>add</th>
                        </tr>
                        </thead>
                        <div class = "scroll">
                            <tbody>
                            </tbody>
                        </div>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <div class="row hide" id="buttonsDiv">
        <div class="col-md-9">
            <button class="btn btn-success btn-block" style="margin-bottom: 40px;" id ="submit">Submit Receiving Job</button>
        </div>
        <div class="col-md-3">
            <button class="btn btn-danger btn-block " id ="clear">Clear</button>
        </div>
    </div>
    <div class="message-box message-box-success animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-check"></span>Success</div>
                    <div class="mb-content">
                        <h4 style="color:white;">Receiving Job Successfully created you may proceed to the <a href="{{route('StorageJobCreate')}}">Storage Job</a> Step</h4>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>


    <script type="text/javascript" src={{("../js/GoodsIn/receivingjob.js")}}></script>

    <script type="text/javascript" src="{{asset('js/plugins/tour/bootstrap-tour.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/tour/bootstro.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/tour/tour.js')}}"></script>
@stop





