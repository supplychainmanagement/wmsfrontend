@extends('main&templates.tables_layout')
@section('actionOfUpdate')
    purchaseorderitem/update
@stop

@section('actionOfDelete')
    purchaseorderitem/delete
@stop

@section('table-title')
    Purchase Order Items
@stop

@section('table_id')
"purchaseorderitemtable"
@stop


@section('table_data_val'){{$POI_number}}@stop


@section('table-head')
    <tr>
        <th style="display:none";>Purchase Order Item ID</th>
        <th>Purchase Order Item Number</th>
        <th>Purchase Order Number</th>
        <th>SKU</th>
        <th>Amount</th>
        <th>State</th>
        <th>Received Amount</th>
        <th>Production Date</th>
    </tr>
@stop

@section('table-body')

    @foreach($data as $POI)
        <tr>
            <td style="display:none;">{{$POI['id']}}</td>
            <td>{{$POI['POI_number']}}</td>
            <td><a href="{{route('purchaseorder')}}/{{$POI['PO_number']}}">{{$POI['PO_number']}}</a></td>
            <td>{{$POI['SKU_name']}}</td>
            <td>{{$POI['amount']}}</td>
            <td>{{$POI['state']}}</td>
            <td>{{$POI['received']}}</td>
            <td>{{$POI['productionDate']}}</td>
        </tr>
    @endforeach

@stop

@section('prop-head')

    <th>Property</th>
    <th>Value</th>
@stop


@section('prop-body')


@stop
@section('message-body')
    @if(session()->has('message'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                        <p>{{session('message')}}</p>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif
@stop



@section('Jsplugin')
    <script type="text/javascript">
        $('#goodsinlink').addClass('active');
        $('#purchaseorderitemlink').addClass('active');
    </script>
    @if($POI_number != null)
        <script>
            $(document).ready(function(){
                var temp = $('#purchaseorderitemtable').DataTable();
                var data_search= $('#purchaseorderitemtable').data('myval');
                temp.search(data_search).draw();
            });
        </script>
    @endif
    {{--<script type="text/javascript" src="js/GoodsIn/purchaseorderitem.js"></script>--}}
@stop
<!-- var data_search = $('#purchaseorderitemtable').data('myval');
    alert(data_search);
    if(data_search != null)
    {
        var temp = $('#purchaseorderitemtable').dataTable( {
            "oSearch": {
              "sSearch": data_search
            }
        });
    } -->