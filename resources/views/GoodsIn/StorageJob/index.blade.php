@extends('main&templates.tables_layout')
@section('actionOfUpdate')
    
@stop

@section('actionOfDelete')
    
@stop

@section('table-title')
    Storage Jobs
@stop


@section('table-head')
    <tr>
        <th style="display:none";>Storage Job Id</th>
        <th>Storage Job Name</th>
        <th>Storage Job State</th>
        <th>Unitload</th>
        <th>Storage Location</th>
        <th>Site</th>
    </tr>
@stop

@section('table-body')

    @foreach($storageJobs as $storageJob)
        <tr>
            <td style="display:none;">{{$storageJob['storagejob']['id']}}</td>
            <td>{$storageJob['storagejob']['STJ_name']}}</td>
            <td>{{$storageJob['receivingjob']['STJ_state']}}</td>
            <td><a href="{{route('unitload',array('search'=>$storageJob['UL_name']))}}">{{$storageJob['UL_name']}}</a></td>
            <td><a href="{{route('storagelocation',array('search'=>$storageJob['STL_name']))}}">{{$storageJob['STL_name']}}</a></td>
            <td><a href="{{route('site',array('search'=>$storageJob['SITE_name']))}}">{{$storageJob['SITE_name']}}</a></td>
        </tr>
    @endforeach

@stop

@section('message-body')
    @if(session()->has('message'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                        <p>{{session('message')}}</p>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif
@stop



@section('Jsplugin')
<script>
$('.maintable').dataTable();
        $('#goodsinlink').addClass('active');
        $('#storagejoblink').addClass('active');
        $(document).ready(function(){
            var pathname = window.location.href;
            var position = pathname.indexOf("?");
            if(position != -1)
            {
                var getvalues = pathname.substring(position+1,pathname.length);
                var splited = getvalues.split('=');
                console.log(splited[0]);
                if(splited.length == 2)
                {
                    if(splited[0] == 'search')
                    {
                         $('.maintable').DataTable().search(splited[1]).draw();
                         console.log('i am here');
                    }
                } 
            }
        }); 
    </script>
@stop