@extends('main&templates.tables_layout')
@section('actionOfUpdate')

@stop

@section('actionOfDelete')

@stop

@section('table-title')
    Strategies
@stop


@section('table-head')
    <tr>
        <th style="display:none";>Strategy ID</th>
        <th>Strategy Name</th>
        <th>Order Strategy</th>
        <th>Storage Strategy</th>
    </tr>
@stop

@section('table-body')

    @foreach($strategies as $strategy)
        <tr>
            <td style="display:none;">{{$strategy['id']}}</td>
            <td>{{$strategy['STRAT_name']}}</td>
            @if ($strategy['STRAT_order'] == 1)
                <td><span class="fa fa-check"></span></td>
            @elseif ($strategy['STRAT_order'] == 0)
                <td><span class="glyphicon glyphicon-remove"></span></td>
            @else
                <td>Not Set</span></td>
            @endif
            @if ($strategy['STRAT_storage'] == 1)
                <td><span class="fa fa-check"></span></td>
            @elseif ($strategy['STRAT_storage'] == 0)
                <td><span class="glyphicon glyphicon-remove"></span></td>
            @else
                <td>Not Set</span></td>
            @endif
        </tr>
    @endforeach

@stop

@section('prop-head')

    <th>Property</th>
    <th>Value</th>
@stop


@section('prop-body')


@stop
@section('message-body')
    @if(session()->has('message'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                        <p>{{session('message')}}</p>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif
@stop
@if(session('system_client')==true)
    @if(session('user_role')=='system_admin'||session('user_role')=='site_admin')
@section('create-panel')

    {{--<div id="data"class="col-md-12">--}}

        {{--<div class="panel panel-warning" id= "data-panel">--}}
            {{--<form role="form" class="form-horizontal"  action="{{route('strategyCreate')}}" method="post" id="strategy-form">--}}
                {{--{!! csrf_field() !!}--}}
                {{--<div class="panel-heading">--}}
                    {{--<h3 class="panel-title" id = "table-title">Create New Zone</h3>--}}
                    {{--<ul class="panel-controls">--}}
                        {{--<li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>--}}
                        {{--<li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>--}}
                        {{--<li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<div class="panel-body">--}}

                    {{--<div class="row">--}}

                        {{--<div class="col-md-4">--}}
                            {{--<div class="form-group">--}}
                                {{--<label class="col-md-2 control-label">Name</label>--}}
                                {{--<div class="col-md-10">--}}
                                    {{--<input type="text" class="form-control" name = "name" required/>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--@if($siteBased == false)--}}
                            {{--<div class="col-md-4">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="col-md-2 control-label">Site</label>--}}
                                    {{--<div class="col-md-10">--}}
                                        {{--<select class="form-control selectpicker show-tick" name = "site" required>--}}
                                            {{--@foreach ($sites as $site)--}}
                                                {{--<option>{{$site['SITE_name']}}</option>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--@endif--}}

                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="panel-footer">--}}
                    {{--<button type="submit" value= "Submit" class="btn btn-primary pull-right">Submit</button>--}}
                {{--</div>--}}
            {{--</form>--}}
        {{--</div>--}}

    {{--</div>--}}

@stop

@endif
@endif


@section('Jsplugin')
    <script type="text/javascript">
        $('#strategygrouplink').addClass('active');
        $('#strategylink').addClass('active');
        $(document).ready(function(){
            var pathname = window.location.href;
            var position = pathname.indexOf("?");
            if(position != -1)
            {
                var getvalues = pathname.substring(position+1,pathname.length);
                var splited = getvalues.split('=');
                console.log(splited[0]);
                if(splited.length == 2)
                {
                    if(splited[0] == 'search')
                    {
                         $('.maintable').DataTable().search(splited[1]).draw();
                         console.log('i am here');
                    }
                } 
            }
        }); 
    </script>
@stop

