<!DOCTYPE html>
<!--[if lt IE 7]><html class="ie ie6" lang="en"><![endif]-->
<!--[if IE 7]><html class="ie ie7" lang="en"><![endif]-->
<!--[if IE 8]><html class="ie ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if (gte IE 10)|!(IE)]><!--><html lang="en"><!--<![endif]-->

<head>

    <!-- Your Basic Site Informations -->
    <title>WMS++ Beta</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700" rel="stylesheet" type="text/css">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/jquery.fancybox.css">
    <link rel="stylesheet" href="css/style.css">

    <!-- Custom Colors -->
    <!--<link rel="stylesheet" href="css/colors/blue/color.css">-->
    <!--<link rel="stylesheet" href="css/colors/green/color.css">-->
    <!--<link rel="stylesheet" href="css/colors/pink/color.css">-->
    <!--<link rel="stylesheet" href="css/colors/purple/color.css">-->
    <!--<link rel="stylesheet" href="css/colors/yellow/color.css">-->

    <!--[if lt IE 9]>
    <script src="js/html5.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    <!--[if lt IE 8]>
    <link rel="stylesheet" href="css/ie-older.css">
    <![endif]-->

    <noscript><link rel="stylesheet" href="css/no-js.css"></noscript>
</head>
<body>

<!-- #body-wrap -->
<div id="body-wrap">

    <!-- #header -->
    <header id="header" data-parallax="scroll" data-speed="0.2" data-natural-width="1920" data-natural-height="1080" data-image-src="images/content/bg/1.jpg">

        <!-- .header-overlay -->
        <div class="header-overlay">

            <!-- #navigation -->
            <nav id="navigation" class="navbar scrollspy">

                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#header" class="smooth-scroll">Home</a></li>
                            <li><a href="#aboutus" class="smooth-scroll">About Us</a></li>
                            <li><a href="#features" class="smooth-scroll">Core Features</a></li>
                            <!--<li><a href="#testimonials" class="smooth-scroll">Testimonials</a></li>-->
                            <li><a href="#bottom" class="smooth-scroll">Invitation Code</a></li>
                            <li><a href="#rfq" class="smooth-scroll">RFQ</a></li>
                            <li><a href="#contact" class="smooth-scroll">Contact Us</a></li>
                            <li style="margin-right:40px;"><a href="{{route('supportcenter')}}">Support Center</a></li>
                            
                        </ul>

                <!-- .container -->
                <div class="container">

                    <div class="navbar-brand">

                        <a href="#"><img src="{{URL::asset('images/logo.png')}}" alt="Logo"></a> <!-- site logo -->
                    </div>
                    </div>
                    <!-- .container end -->

                </nav>
                <!-- #navigation end -->

                <!-- .header-content -->
                <div class="header-content">

                    <!-- .container -->
                    <div class="container">

                        <div class="header-heading-title">
                            <h1>Welcome To WMS++</h1>
                            <h4>Warehouse and Inventory Management System</h4>
                        </div>

                        <!-- .row -->
                        <div class="row">

                            <div class="col-sm-6 col-md-7 col-lg-6">
                            <br/>
                             <br/>
                              <br/>
                               <br/>
                                <br/>
                                <div class="header-txt">
                                    <h1>Flexible Flow </h1>
                                    <div class="header-txt-btn">
                                       <iframe width="560" height="315" src="https://www.youtube.com/embed/89cU2fgcW7Y" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-md-5 col-lg-offset-1">
                                <ul class="header-txt-list">
                                    <li>
                                        <i class="fa fa-money"></i>
                                        <h4>Software For Rent</h4>
                                        <p>You've Flexible Payment Plans That Fits Your Warehouse</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-balance-scale"></i>
                                        <h4>Flexible For Your Business Scale</h4>
                                        <p>Our System Provides Scalability For All Types of Warehouses</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-industry"></i>
                                        <h4>Adaptive For Different Industries</h4>
                                        <p>Our System Supports Multiple Industries</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-arrows"></i>
                                        <h4>Multiple Workflows</h4>
                                        <p>Our System Support Multiple Workflows According To Your Clients' Industries</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-tags"></i>
                                        <h4>RFID Tracking Integrated</h4>
                                        <p>Real Time Tracking and Monitoring Using RFID Technology</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-lock"></i>
                                        <h4>Easy and Secured</h4>
                                        <p>Your Data is Safe With Us</p>
                                    </li>
                                </ul>
                            </div>

                    </div>
                    <!-- .row end -->

                </div>
                <!-- .container end -->

            </div>
            <!-- .header-content end -->

        </div>
        <!-- .header-overlay end -->

    </header>
    <!-- #header end -->

    <!-- #main-wrap -->
    <div id="main-wrap">

        <!-- #features -->
        <div id="aboutus" class="padding-top80">

            <!-- .container -->
            <div class="container">

                <div class="post-heading-center">
                    <h2>About Us</h2>
                </div>

                <!-- .row -->
                <div class="row padding-bottom20">

                    <div class="col-sm-4"> <!-- 1 -->
                        <div class="affa-feature-img">
                            <img src="images/METI-logo.PNG" alt="METI">
                            <h4>METI</h4>
                            <p>WMS++ is one of the products that Micro Engineering Tech Inc
                                offers to help the supply chain management business.</p>
                        </div>
                    </div>

                    <div class="col-sm-4"> <!-- 2 -->
                        <div class="affa-feature-img">
                            <img src="images/warehouse.jpg" alt="WMS" width="150px;" height="150px;">
                            <h4>WMS</h4>
                            <p>WMS++ is dedicated to help managment of the inventory and the locationing system
                                in the warehouses providing a very powerful tool to manage warehouse transactions.</p>
                        </div>
                    </div>

                    <div class="col-sm-4"> <!-- 3 -->
                        <div class="affa-feature-img">
                            <img src="images/secure.jpg" alt="secure" width="150px;" height="150px;">
                            <h4>Secure</h4>
                            <p>WMS++ offers a secure, easy of use and flexible warehouse data management.</p>
                        </div>
                    </div>

                </div>
                <!-- .row end -->
            </div>
            <!-- .container end -->

        </div>
        <!-- #features end -->


        <div id="features" class="padding-top80">

            <!-- .container -->
            <div class="container">

                <div class="post-heading-center">
                    <h2>Features</h2>
                    <p>Take control over your Warehouse Completely.</p>
                </div>
                <!-- .row -->
                <div class="row padding-bottom20">

                    <div class="col-sm-6 col-md-5 margin-bottom40">
                    <br/>
                        <figure><img src={{URL::asset('images/unnamed.png')}} alt="Image"></figure>
                    </div>

                    <div class="col-sm-6 col-md-7 col-lg-6 col-lg-offset-1 margin-bottom-20" style="margin-top:-100px">
                        <div class="col-txt100">

                            <div class="post-heading-left post-heading-top-desc margin-bottom20">
                                <!-- <p>See this action in the real page</p> -->
                                <h2>What does <strong>WMS++</strong> offer</h2>
                            </div>
                            <div class="list-icon">
                                <ul>
                                    <li><i class="fa fa-check-square-o"></i>Inventory Managment</li>
                                    <li><i class="fa fa-check-square-o"></i>
                                        Location Control
                                    </li>
                                    <li><i class="fa fa-check-square-o"></i>Order Managment</li>
                                    <li><i class="fa fa-check-square-o"></i>Purchasing</li>
                                    <li><i class="fa fa-check-square-o"></i>Shipping Managment</li>
                                    <li><i class="fa fa-check-square-o"></i>Recieving/Putaway Managment</li>
                                    <li><i class="fa fa-check-square-o"></i>3PL Managment</li>
                                </ul>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- .row end -->
            </div>
            <!-- .container end -->

        </div>


        <!--&lt;!&ndash; #testimonials &ndash;&gt;-->
        <!--<div id="testimonials" class="bg-grey wrap-container80">-->
        <!-- -->
        <!--<div class="container">-->
        <!--<i class="fa fa-quote-left affa-testimonial-icon"></i>-->
        <!--</div>-->
        <!-- -->
        <!--&lt;!&ndash; .carousel-slider &ndash;&gt;-->
        <!--<div class="carousel-slider affa-testimonials-carousel">-->
        <!-- -->
        <!--<div class="slick-slide"> &lt;!&ndash; 1 &ndash;&gt;-->
        <!--<div class="container">-->
        <!--<div class="affa-testimonial">-->
        <!--<div class="testimonial-txt">-->
        <!--<p>"Lorem ipsum dolor consectetur adipiscing elit, sed do eiusmod tempor incididunt labore et dolore magna aliqua. Enim id minim veniam exercitation laboris consequat. Duis aute dolor reprehende ritin voluptate cillum fugiatos."</p>-->
        <!--</div>-->
        <!--<div class="testimonial-name">-->
        <!--<img src="images/content/avatar/1.jpg" alt="Avatar">-->
        <!--<h4>Amah Holland</h4>-->
        <!--<p>Social Media Manager of MG</p>-->
        <!--</div>-->
        <!--</div>-->
        <!--</div>-->
        <!--</div>-->
        <!-- -->
        <!--<div class="slick-slide"> &lt;!&ndash; 2 &ndash;&gt;-->
        <!--<div class="container">-->
        <!--<div class="affa-testimonial">-->
        <!--<div class="testimonial-txt">-->
        <!--<p>"Lorem ipsum dolor consectetur adipiscing elit, sed do eiusmod tempor incididunt labore et dolore magna aliqua. Enim id minim veniam exercitation laboris consequat. Duis aute dolor reprehende ritin voluptate cillum fugiatos."</p>-->
        <!--</div>-->
        <!--<div class="testimonial-name">-->
        <!--<img src="images/content/avatar/2.jpg" alt="Avatar">-->
        <!--<h4>Natsu Dragnell</h4>-->
        <!--<p>Lead Designer of DolanDolen</p>-->
        <!--</div>-->
        <!--</div>-->
        <!--</div>-->
        <!--</div>-->
        <!-- -->
        <!--<div class="slick-slide"> &lt;!&ndash; 3 &ndash;&gt;-->
        <!--<div class="container">-->
        <!--<div class="affa-testimonial">-->
        <!--<div class="testimonial-txt">-->
        <!--<p>"Lorem ipsum dolor consectetur adipiscing elit, sed do eiusmod tempor incididunt labore et dolore magna aliqua. Enim id minim veniam exercitation laboris consequat. Duis aute dolor reprehende ritin voluptate cillum fugiatos."</p>-->
        <!--</div>-->
        <!--<div class="testimonial-name">-->
        <!--<img src="images/content/avatar/3.jpg" alt="Avatar">-->
        <!--<h4>Denden Mushi</h4>-->
        <!--<p>Mobile Developer of Avelie</p>-->
        <!--</div>-->
        <!--</div>-->
        <!--</div>-->
        <!--</div>-->
        <!-- -->
        <!--</div>-->
        <!--&lt;!&ndash; .carousel-slider end &ndash;&gt;-->
        <!-- -->
        <!--</div>-->
        <!--&lt;!&ndash; #testimonials end &ndash;&gt;-->



        <!-- #map -->
        <div id="bottom" class="bg-img" style="background-color: white;">
            <div class="bg-overlay">
                <div class="container">

                    <div class="post-heading-center">
                        <h2>Invitation Code!</h2>
                        <p>Join Thousands of Satisfied Warehouse Owners By Entering your Invitation Code</p>
                    </div>

                    <form method="post" class="affa-form-subscribe" id = "verifycode">
                    {!! csrf_field() !!}    
                        <input type="text"  placeholder="Invitation Code...." name='code' id = "code" required>
                        <input type="submit" name="submit" value="Join Now">
                    </form>
                    <form method="post" class="affa-form-subscribe" hidden id="join">
                    {!! csrf_field() !!}
                        <input type="text"  placeholder="" name='codehidden' id = "codehidden" hidden required>
                        <input type="email"  placeholder="Email" id="emailjoin" name="emailjoin" required>
                        <input type="text"  placeholder="WareHouse Name" id="warehouseName" name="warehouseName" required>
                        <input type="text"  placeholder="Username" id="username" name="username" required>
                        <input type="password"  placeholder="Password" id="password" name="password" required>
                        <input type="submit" name="submit" value="Join Now"  style="margin-top:10px">
                    </form>
                    <div class="row" style="margin-bottom:10px;margin-top:10px;">
                      <div class="col-md-4 col-md-offset-4" id="messagecode">

                      </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- #map end -->


        <div id="rfq" class="padding-top80">

            <!-- .container -->
            <div class="container">

                <div class="post-heading-center">
                    <div class="post-heading-center">
                        <h2>Request For Quote</h2>
                        <p>Please Complete The Form Below and We'll Get Back To You ASAP</p>
                    </div>
                    <div class ="row">
                        <div class = "col-md-4 col-md-offset-4">
                    <form method="post" class="affa-form-subscribe" id="rfqform">
                    {!! csrf_field() !!}
                        <input type="text"  placeholder="Name" id="name" name="name" required>
                        <input type="email"  placeholder="Email" id="emailrfq" name="emailrfq" required>
                        <input type="text"  placeholder="Phone" id="phone" name="phone" required>
                        <input type="text"  placeholder="Company Name" id="companyName" name="companyName" required>
                        <input type="text"  placeholder="Industry" id="industry" name="industry" required>
                        <select  name="ERPPlatform" id="ERPPlatform" required>
                            <option selected >ERP Platform</option>
                            <option>SAP</option>
                            <option>Sales Force</option>
                            <option>Others</option>
                        </select>
                        <br/>
                        <br/>
                        <select name="noOfEmployees" id="noOfEmployees" required>
                            <option selected>Number of Employees</option>
                            <option>0-5</option>
                            <option>6-15</option>
                            <option>16-30</option>
                            <option>More</option>
                        </select>
                        <br/>
                        <br/>
                        
                        <select name="noOfSites" id="noOfSites" required>
                            <option selected  >Number of Sites</option>
                            <option>0-5</option>
                            <option>5-10</option>
                            <option>10-15</option>
                            <option>More</option>
                        </select>
                        <br/>
                        <br/>
                        <select name="noOfTransactionsPerMonth" id="noOfTransactionsPerMonth" required>
                            <option selected  >Number of Transactions Per Month</option>
                            <option>0-100</option>
                            <option>101-500</option>
                            <option>501-2000</option>
                            <option>More</option>
                        </select>
                        <input type="submit" name="submit" value="Join Now" style="margin-top:10px">
                    </form>
                    </div>
                    </div>
                    <div class="row" style="margin-bottom:10px;margin-top:10px;">
                      <div class="col-md-4 col-md-offset-4" id="messagerfq">

                      </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- #bottom -->
        <div id="bottom" class="bg-img">

            <!-- .bg-overlay -->
            <div class="bg-overlay">

                <!-- .container -->
                <section id="contact">
                    <div class="container">
                        <br/>
                        <br/>
                        <div class="row">
                            <div class="col-lg-8 col-lg-offset-2 text-center">
                                <h2 class="section-heading">Let's Get In Touch!</h2>
                                <hr class="primary">
                                <p>Ready to start your next project with us? That's great! Give us a call or send us an email and we will get back to you as soon as possible!</p>
                            </div>
                            <div class="col-lg-4 col-lg-offset-2 text-center">
                                <i class="fa fa-phone fa-3x sr-contact"></i>
                                <p>Tel: +1 (403) 457-3112</p>
                            </div>
                            <div class="col-lg-4 text-center">
                                <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                                <p><a href="mailto:your-email@your-domain.com">services@meng-tech.com</a></p>
                            </div>
                            <div class="col-lg-4 col-lg-offset-2 text-center">
                                <i class="fa fa-map-marker fa-3x sr-contact"></i>
                                <p>Address: Calgary, Alberta T2M 0L7
                                </p>
                            </div>
                            <div class="col-lg-4 text-center">
                                <i class="fa fa-map-pin fa-3x sr-contact"></i>
                                <p>Suite 400, 1716-16 Ave NW</p>
                            </div>

                        </div>
                        <br/>
                        <br/>
                    </div>
                </section>

                <!-- .container end -->

            </div>
            <!-- .bg-overlay -->

            <!-- background image -->

        </div>
        <!-- #bottom end -->

        <!-- #footer -->
        <!--<footer id="footer">-->
        <!-- -->
        <!--&lt;!&ndash; .container &ndash;&gt;-->
        <!--<div class="container">-->
        <!-- -->
        <!--<p class="copyright-txt">&copy; 2016 Copyrights by <a href="http://affapress.com" target="_blank">Affapress</a> - All rights reserved.</p>-->
        <!-- -->
        <!--<div class="socials">-->
        <!--<a href="#" title="Facebook" class="link-facebook"><i class="fa fa-facebook"></i></a>-->
        <!--<a href="#" title="Twitter" class="link-twitter"><i class="fa fa-twitter"></i></a>-->
        <!--<a href="#" title="Google Plus" class="link-google-plus"><i class="fa fa-google-plus"></i></a>-->
        <!--</div>-->
        <!-- -->
        <!--</div>-->
        <!--&lt;!&ndash; .container end &ndash;&gt;-->
        <!-- -->
        <!--</footer>-->
        <!-- #footer end -->

    </div>
    <!-- #main-wrap end -->

</div>
<!-- #body-wrap end -->

<!--[if lt IE 8]>
<div class="browser-notice">
    <div class="container">
        <div class="text">
            <h1>Internet Explorer Out To Date</h1>
            <p>Please update your Internet Explorer browser with a newer version (Internet Explorer 8 above) now!</p>
            <span>You can download it <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie" target="_blank">here....</a></span>
        </div>
    </div>
</div>
<![endif]-->

<!-- JavaScripts -->
<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.easing.min.js"></script>
<script type="text/javascript" src="js/smoothscroll.js"></script>
<script type="text/javascript" src="js/response.min.js"></script>
<script type="text/javascript" src="js/jquery.placeholder.min.js"></script>
<script type="text/javascript" src="js/jquery.fitvids.js"></script>
<script type="text/javascript" src="js/jquery.imgpreload.min.js"></script>
<script type="text/javascript" src="js/waypoints.min.js"></script>
<script type="text/javascript" src="js/slick.min.js"></script>
<script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="js/jquery.fancybox-media.js"></script>
<script type="text/javascript" src="js/jquery.counterup.min.js"></script>
<script type="text/javascript" src="js/parallax.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDE7s3AMgMt8XkGB3xZemuH4MU_HKNSpFw"></script>
<script type="text/javascript" src="js/gmaps.js"></script>
<script type="text/javascript" src="js/script.js"></script>
 <script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>
 <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>     
<script type="text/javascript" src="{{URL::asset('js/home/register.js')}}"></script>
<script type="text/javascript">
    var map;

    map = new window.GMaps({
        div: '#companyMap',
        lat: -12.0411925,
        lng: -77.0282043,
        scrollwheel: false,
        zoomControl: false,
        disableDoubleClickZoom: false,
        disableDefaultUI: true
    });

    map.addMarker({
        lat: -12.042,
        lng: -77.028333,
        title: 'Company Name',
        infoWindow: {
            content: 'Your Company Name'
        }
    });
</script>

</body>
</html>