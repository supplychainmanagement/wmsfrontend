<!DOCTYPE html>
<!--[if lt IE 7]><html class="ie ie6" lang="en"><![endif]-->
<!--[if IE 7]><html class="ie ie7" lang="en"><![endif]-->
<!--[if IE 8]><html class="ie ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if (gte IE 10)|!(IE)]><!--><html lang="en"><!--<![endif]-->

<head>
    
    <!-- Your Basic Site Informations -->
    <title>WMS++ Beta</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Conversi is a professional conversion landing page that built-in fully responsive template, premium design, elegant style and have a slew of features.">
    <meta name="keywords" content="Landing Page, One Page Template, Premium Design, Responsive Template, Conversion Landing Page, Conversion Template, Registration Form, Elegant Style, PSD, HTML5, CSS3, jQuery">
    <meta name="author" content="Affapress">
    
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700" rel="stylesheet" type="text/css">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/jquery.fancybox.css">
    <link rel="stylesheet" href="css/style.css">
    
    <!-- Custom Colors -->
    <!--<link rel="stylesheet" href="css/colors/blue/color.css">-->
    <!--<link rel="stylesheet" href="css/colors/green/color.css">-->
    <!--<link rel="stylesheet" href="css/colors/pink/color.css">-->
    <!--<link rel="stylesheet" href="css/colors/purple/color.css">-->
    <!--<link rel="stylesheet" href="css/colors/yellow/color.css">-->
    
    <!--[if lt IE 9]>
        <script src="js/html5.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 8]>
        <link rel="stylesheet" href="css/ie-older.css">
    <![endif]-->
    
    <noscript><link rel="stylesheet" href="css/no-js.css"></noscript>
    
    <!-- Favicons -->
    
    <style>
    .btn {
      background: #e34724;
      background-image: -webkit-linear-gradient(top, #e34724, #e34724);
      background-image: -moz-linear-gradient(top, #e34724, #e34724);
      background-image: -ms-linear-gradient(top, #e34724, #e34724);
      background-image: -o-linear-gradient(top, #e34724, #e34724);
      background-image: linear-gradient(to bottom, #e34724, #e34724);
      -webkit-border-radius: 28;
      -moz-border-radius: 28;
      border-radius: 28px;
      font-family: Arial;
      color: #ffffff;
      font-size: 20px;
      padding: 10px 20px 10px 20px;
      text-decoration: none;
    }

    .btn:hover {
      background: #de5338;
      background-image: -webkit-linear-gradient(top, #de5338, #eb7f6a);
      background-image: -moz-linear-gradient(top, #de5338, #eb7f6a);
      background-image: -ms-linear-gradient(top, #de5338, #eb7f6a);
      background-image: -o-linear-gradient(top, #de5338, #eb7f6a);
      background-image: linear-gradient(to bottom, #de5338, #eb7f6a);
      text-decoration: none;
    }
    </style>
</head>
<body>
    
    <!-- #body-wrap -->
    <div id="body-wrap">
        
        <!-- #header -->
        <header id="header" data-parallax="scroll" data-speed="0.2" data-natural-width="1920" data-natural-height="1080" data-image-src="images/content/bg/1.jpg">

            <!-- .header-overlay -->
            <div class="header-overlay">

                <!-- #navigation -->
                <nav id="navigation" class="navbar scrollspy">

                    <!-- .container -->
                    <div class="container">

                        <div class="navbar-brand">
                            <a href="#"><img src="{{URL::asset('images/logo.png')}}" alt="Logo"></a> <!-- site logo -->
                        </div>

                        <ul class="nav navbar-nav">
                            <li class="active"><a href="{{route('registerwarehouseview')}}" class="smooth-scroll">Home</a></li>
                            <li><a href="{{route('registerwarehouseview')}}#aboutus" class="smooth-scroll">About Us</a></li>
                            <li><a href="{{route('registerwarehouseview')}}#features" class="smooth-scroll">Core Features</a></li>
                            <!--<li><a href="#testimonials" class="smooth-scroll">Testimonials</a></li>-->
                            <li><a href="{{route('registerwarehouseview')}}#bottom" class="smooth-scroll">Invitation Code</a></li>
                            <li><a href="{{route('registerwarehouseview')}}#rfq" class="smooth-scroll">RFQ</a></li>
                            <li><a href="{{route('registerwarehouseview')}}#contact" class="smooth-scroll">Contact Us</a></li>
                            <li><a href="{{route('supportcenter')}}">Support Center</a></li>
                            
                        </ul>

                    </div>
                    <!-- .container end -->

                </nav>
                <!-- #navigation end -->

                <!-- .header-content -->
              
                <!-- .header-content end -->

            </div>
            <!-- .header-overlay end -->

        </header>
        <!-- #header end -->
        
        <!-- #sub-header -->
        <!-- #sub-header end -->
        
        <!-- #main-wrap -->
        <div id="main-wrap">
            
            <!-- #features -->
            <div id="features" class="padding-top80">
                
                <!-- .container -->
                <div class="container">
                    
                    <!-- .row -->
                    <div class="row padding-bottom20">
                        
                        <div class="col-sm-4"> <!-- 1 -->
                            <div class="affa-feature-img">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/CR3sNKmdNd8" frameborder="0" allowfullscreen></iframe>
                                <h4>System Overview</h4>
                                <p>Intorduction To Our System.</p>
                            </div>
                        </div>
                        
                        <div class="col-sm-4"> <!-- 2 -->
                            <div class="affa-feature-img">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/89cU2fgcW7Y" frameborder="0" allowfullscreen></iframe>
                                <h4>Warehouse Inbound Operations</h4>
                                <p>Step by Step Tutorial For In-Bound Operation</p>
                            </div>
                        </div>
                        
                        <div class="col-sm-4"> <!-- 3 -->
                            <div class="affa-feature-img">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/Xt242JzwJx4" frameborder="0" allowfullscreen></iframe>
                                <h4>Warehouse Outbound Operations</h4>
                                <p>Step by Step Tutorial For Out-Bound Operation.</p>
                            </div>
                        </div>
                        
                    </div>
                    <!-- .row end -->
                   <!--  <div class="row padding-bottom20">
                    <div class ="col-md-3 col-md-offset-3">
                    <a href="https://s3-us-west-2.amazonaws.com/wmsplusplus/manuals/Manual+Guide.pdf">
                    <button class = "btn">User Guide</button></a>
                    </div>
                    <div class ="col-md-3">
                    <a href="https://s3-us-west-2.amazonaws.com/wmsplusplus/manuals/Quick_Start_Guide.pdf">
                    <button class = "btn">Quick Start Guide</button></a>
                    </div>
                    </div> -->
            <!-- #footer -->
            <!-- #footer end -->
            
        </div>
        <!-- #main-wrap end -->
        
    </div>
    <!-- #body-wrap end -->
    
    <!--[if lt IE 8]>
    	<div class="browser-notice">
            <div class="container">
            	<div class="text">
                    <h1>Internet Explorer Out To Date</h1>
                    <p>Please update your Internet Explorer browser with a newer version (Internet Explorer 8 above) now!</p>
                    <span>You can download it <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie" target="_blank">here....</a></span>
                </div>
            </div>
        </div>
	<![endif]-->
    
    <!-- JavaScripts -->
	<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.easing.min.js"></script>
    <script type="text/javascript" src="js/smoothscroll.js"></script>
	<script type="text/javascript" src="js/response.min.js"></script>
	<script type="text/javascript" src="js/jquery.placeholder.min.js"></script>
	<script type="text/javascript" src="js/jquery.fitvids.js"></script>
	<script type="text/javascript" src="js/jquery.imgpreload.min.js"></script>
	<script type="text/javascript" src="js/waypoints.min.js"></script>
    <script type="text/javascript" src="js/slick.min.js"></script>
    <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="js/jquery.fancybox-media.js"></script>
    <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
    <script type="text/javascript" src="js/parallax.min.js"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDE7s3AMgMt8XkGB3xZemuH4MU_HKNSpFw"></script>
	<script type="text/javascript" src="js/gmaps.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
    
    <script type="text/javascript">
	var map;
	
	map = new window.GMaps({
		div: '#companyMap',
		lat: -12.0411925,
		lng: -77.0282043,
		scrollwheel: false,
		zoomControl: false,
		disableDoubleClickZoom: false,
		disableDefaultUI: true
	});
	
	map.addMarker({
		lat: -12.042,
		lng: -77.028333,
		title: 'Company Name',
		infoWindow: {
			content: 'Your Company Name'
		}
	});
    </script>
    
</body>
</html>