<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>WMS++</title>
        <meta charset="utf-8">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="{{URL::asset('css/theme-serenity.css')}}" rel="stylesheet" type="text/css" id="theme">
        <link href="{{URL::asset('css/theme-serenity-head-light.css')}}" rel="stylesheet" type="text/css" id="theme">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js" charset="utf-8"></script>         
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link rel="shortcut icon" href="{{URL::asset('images/favicon.png')}}">
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->                
        <link rel="stylesheet" type="text/css" id="theme" href="{{URL::asset('css/theme-default.css')}}"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.9.4/css/bootstrap-select.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/css/tooltipster.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/css/themes/tooltipster-punk.min.css" />
        <link rel="stylesheet" type="text/css" id="theme" href="{{URL::asset('css/addslider/addSlider.css')}}"/>
        <!-- EOF CSS INCLUDE -->
        <style >
        .selected
            {
                background-color: orange;
                color: #FFF;
                font-weight: bold;
            }
        .username

        {
            /*position: relative;*/
            font-size: 20px;
            color: white;
            left:36px;
            margin-top: 8px;
        }
        .profile-data{
            color: #FFFFFF;
        }
        .pulse-button {

            position: relative;
            width: 20px;
            height: 20px;
            border: none;
            border-radius: 50%;
            background-image: url('../images/info.png');
            background-size:cover;
            background-repeat: no-repeat;
            cursor: pointer;
            margin-left:5px;

            -webkit-animation: none;-moz-animation: none;-ms-animation: none;animation: none;
        }
        .pulse-button:hover
        {
            -webkit-animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
            -moz-animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
            -ms-animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
            animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
        }
        @-webkit-keyframes pulse {to {box-shadow: 0 0 0 12px rgba(254, 158, 25, 0);}}
@-moz-keyframes pulse {to {box-shadow: 0 0 0 12px rgba(254, 158, 25, 0);}}
@-ms-keyframes pulse {to {box-shadow: 0 0 0 12px rgba(254, 158, 25, 0);}}
@keyframes pulse {to {box-shadow: 0 0 0 12px rgba(254, 158, 25, 0);}}
        .unactive
        {
            visibility: hidden;
        }
        /* When the body has the loading class, we turn
           the scrollbar off with overflow:hidden */
        /*body.loading {
            overflow: hidden;   
        }

         Anytime the body has the loading class, our
           modal element will be visible 
        body.loading .modal {
            display: block;
        }*/
    .btn-disable
        {
        cursor: default;
        pointer-events: none;

        /*Button disabled - CSS color class*/
        color: #c0c0c0;
        background-color: #ffffff;

        }
        .upload-button {
            padding: 4px;
            border: 1px solid black;
            border-radius: 5px;
            display: inline-table;
        }
        #changeprofile {display: none;}
        #changepp a:hover + #changeprofile{ display: block;}
        </style>
        <script type="text/javascript" src="{{URL::asset('js/plugins/jquery/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('js/plugins/jquery/jquery-ui.min.js')}}"></script>
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar scroll" style="height:600px;">
                
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="{{route('home')}}"></a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>                                                                                 <li class="xn-profile">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="{{URL::asset('images/no.png')}}" alt="{{session('name')}}"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name">{{session('name')}}</div>
                                <div class="profile-data-title">{{session('user_role')}}</div>
                                <div class="profile-data-title">{{session('warehouseName')}} Warehouse
                                </div>
                            </div>
                        </div>                                                                        
                    </li>        
                    <li class="xn-title" style="color:white;">Warehouse Navigation</li>
                    <!-- System Tab -->                    
                    
                    <!-- Start Master Data -->

                            <li id='systemlink' class="xn-openable">
                            <a href="#"><span class="fa fa-cogs"></span> <span class="xn-text">System</span></a>
                            <ul>
                                <li id='clientlink'><a href="{{route('client')}}"><span class="fa fa-users"></span> Clients</a></li>
                                <li id='userlink'><a href="{{route('user')}}"><span class="fa fa-user"></span> Users</a></li>
                                <li id='customerlink'><a href="{{route('customer')}}"><span class="fa fa-users"></span> Customers</a></li>
                                <li id='providerlink'><a href="{{route('provider')}}"><span class="fa fa-users"></span> Providers</a></li>
                            </ul>  
                        </li>
                         <!-- Start Location -->
                            <li id='locationlink'class="xn-openable">
                                <a href="tables.html"><span class="fa fa-table"></span> <span class="xn-text">Location</span></a>
                                <ul>
                                    <li id='sitelink'><a href="{{route('site')}}"><span class="fa fa-align-justify"></span> Sites</a></li>
                                    <li id='zonelink'><a href="{{route('zone')}}"><span class="fa fa-align-justify"></span> Zones</a></li>
                                    <li id='racklink'><a href="{{route('rack')}}"><span class="fa fa-download"></span> Racks</a></li>
                                    <li id='locationslink'><a href="{{route('storagelocation')}}"><span class="fa fa-align-justify"></span> Locations</a></li>
                                    <li id='functionalarealink'><a href="{{route('functionalarea')}}"><span class="fa fa-download"></span> Functional Areas</a></li>
                                    <li id='storagelocationtypelink'><a href="{{route('storagelocationtype')}}"><span class="fa fa-align-justify"></span> Storage Location Types</a></li>
                                    <li id='capacitylocationlink'><a href="{{route('capacitylocation')}}"><span class="fa fa-sort-alpha-desc"></span> Location Capacities</a></li>

                                </ul>
                            </li>
                            <!-- End Location -->

                            <!-- Start Inventory Master Data -->
                             <li id='inventorylink' class="xn-openable">
                                <a href="tables.html"><span class="fa fa-table"></span> <span class="xn-text">Invertory Master Data</span></a>
                                <ul>                            
                                    <li id='skulink'><a href="sku"><span class="fa fa-sort-alpha-desc"></span>SKU</a></li>
                                    <li id='skutypelink'><a href="skutype"><span class="fa fa-sort-alpha-desc"></span>SKU Type</a></li>
                                    <li id='supplierlink'><a href="supplier"><span class="fa fa-sort-alpha-desc"></span>SKU Suppliers</a></li>
                                </ul>
                            </li>
                            <!-- End Inventory Master Data -->

                            <!-- Start Strategies -->
                            <li class="xn-openable" id='strategygrouplink'>
                                <a href="tables.html"><span class="fa fa-table"></span> <span class="xn-text">Strategies</span></a>
                                <ul>                            
                                    <li id='strategylink'><a href="{{route('strategy')}}"><span class="fa fa-align-justify"></span>Strategy</a></li>
                                </ul>
                            </li>


                            <li id='unitloadslink' class="xn-openable">
                                <a href="tables.html"><span class="fa fa-table"></span> <span class="xn-text">Unitloads</span></a>
                                <ul>
                                    <li id='unitloadlink'><a href="{{route('unitload')}}"><span class="fa fa-square-o"></span> Unit Loads</a></li>
                                    <li id='unitloadtypelink'><a href="{{route('unitloadtype')}}"><span class="fa fa-download"></span> Unit Load Types</a></li>
                                    <li id='capacityunitloadlink'><a href="{{route('capacityunitload')}}"><span class="fa fa-sort-alpha-desc"></span> Unitload Capacity</a></li>
                                    <li id='mismatcheslink'><a href="{{route('mismatches')}}"><span class="fa fa-sort-alpha-desc"></span>Mismatching Unitloads</a></li>
                                </ul>
                            </li>
                            
                            <!-- end Strategies -->

                    <!-- End Master Data -->


                    <!-- Start Goods In -->
                    <li id='goodsinlink' class="xn-openable">
                        <a href="#"><i class="fa fa-database" aria-hidden="true"></i> <span class="xn-text">Goods In</span></a>
                        <ul>
                            <li id='purchaseorderlink'><a href="{{route('purchaseorder')}}"><span class="fa fa-square-o"></span>Purchase Orders</a></li>
                            <li id='purchaseorderitemlink'><a href="{{route('purchaseorderitem')}}"><span class="fa fa-square-o"></span>Purchase Order Items</a></li>
                            <li id='receivingjoblink'><a href="{{route('receivingjob')}}"><span class="fa fa-square-o"></span>Receiving Jobs</a></li>
                            <li id='storagejoblink'><a href="{{route('storagejob')}}"><span class="fa fa-square-o"></span>Storage Jobs</a></li>
                        </ul>

                    </li>
                    <!-- End Goods in -->

                    <!-- Start Goods Out -->
                    <li class="xn-openable" id='goodsoutlink'>
                        <a href="#"><i class="fa fa-database" aria-hidden="true"></i> <span class="xn-text">Goods Out</span></a>
                        <ul>
                            <li id="issueorderlink"><a href="{{route('issueorder')}}"><span class="fa fa-square-o"></span>Issue Orders</a></li>
                            <li id="issueorderitemlink"><a href="{{route('issueorder.items')}}"><span class="fa fa-square-o"></span>Issue Order Items</a></li>
                            <li id='pickrequestlink'><a href="{{route('pickorder.pickrequestindex')}}"><span class="fa fa-square-o"></span>Pick Requests</a></li>
                            <li id='pickrequestitemlink'><a href="{{route('pickorder.pickrequestitemsindex')}}"><span class="fa fa-square-o"></span>Pick Requests Items</a></li>
                            <li id='pickjoblink'><a href="{{route('pickorder.pickjobindex')}}"><span class="fa fa-square-o"></span>Pick Jobs</a></li>
                        </ul>
                    </li>
                    <!-- End Goods Out -->


                    <li class="xn-openable" id='reportslink'>
                        <a href="#"><i class="fa fa-database" aria-hidden="true"></i> <span class="xn-text">Reports</span></a>
                        <ul>
                            <li id="transactionlink"><a href="{{route('transactionreport')}}"><span class="fa fa-square-o"></span>Transactions Report</a></li>
                            <li id="stocklink"><a href="{{route('stockreport')}}"><span class="fa fa-square-o"></span>Stock Report</a></li>
                            <li id='mintransactionlink'><a href="{{route('dailytransactionreport')}}"><span class="fa fa-square-o"></span>Daily Transactions Report</a></li>
                            <li id='orderlink'><a href="{{route('orderreport')}}"><span class="fa fa-square-o"></span>Not Finished Orders Report</a></li>
                        </ul>
                    </li>
                    <li id='monitoringlink'>
                                <a href="{{route('monitoring')}}"><span class="fa fa-table"></span> <span class="xn-text">Monitoring</span></a>
                    </li>
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel nav navbar-nav">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    
                    <li class="xn-icon-button pull-right">  
                         <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                          
                   </li>
                    <!-- END TOGGLE NAVIGATION -->

                    <!-- TASKS -->
                     <li class="xn-icon-button pull-right">
                        <a href="#" style="width: 200px;"><span class="fa fa-tasks"></span>Start Outbound Operation</a>
                        <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="fa fa-tasks"></span>Goods Out</h3>
                                <div class="pull-right">
                                </div>
                            </div>
                            <div class="panel-body list-group scroll" style="height: 200px;">
                                @if(session('user_role') == 'system_admin' || session('user_role') == 'site_admin')
                                <a class="list-group-item" href="{{route('issueorderwizard')}}">
                                    <strong>Create Issue Order</strong>

                                </a>
                                @endif
                                @if(session('user_role') != 'labor')
                                <a class="list-group-item" href="{{route('pickorder')}}">
                                    <strong>Create Pick Request</strong>

                                </a>
                                @endif
                                <a class="list-group-item" href="{{route('pickorder.pickjobindex')}}">
                                    <strong>Create Pick Job</strong>

                                </a>
                                <a class="list-group-item" href="{{route('ShippingCreate')}}">
                                    <strong>Create Ship Job</strong>

                                </a>
                            </div>
                        </div>
                    </li>


                    <li class="xn-icon-button pull-right">
                        <a href="#" style="width: 200px;"><span class="fa fa-tasks"></span>Start Inbound Operation</a>
                        <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="fa fa-tasks"></span>Goods In</h3>
                                <div class="pull-right">
                                </div>
                            </div>
                            <div class="panel-body list-group scroll" style="height: 200px;">
                             @if(session('user_role') == 'system_admin' || session('user_role') == 'site_admin')
                                 <a class="list-group-item" href="{{route('purchaseorderwizard')}}">
                                    <strong>Create Purchase Order</strong>

                                </a>
                                @endif
                                @if(session('user_role') != 'labor')
                                <a class="list-group-item" href="{{route('createrecevingjob')}}">
                                    <strong>Create Receiving Job</strong>

                                </a>
                                @endif
                                <a class="list-group-item" href="{{route('StorageJobCreate')}}">
                                    <strong>Create Storage Job</strong>

                                </a>
                            </div>
                        </div>
                    </li>

                   
                    <!-- END TASKS -->
                    

                    <!-- END TOGGLE NAVIGATION -->                    
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                                    
                <div class="page-content-wrap">
                            @yield('content')
                </div>
                <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">  
             <div class="mb-container">  
                 <div class="mb-middle">  
                     <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>  
                     <div class="mb-content">  
                        <p>Are you sure you want to log out?</p>                      
                         <p>Press No if youwant to continue work. Press Yes to logout current user.</p>  
                     </div>  
                     <div class="mb-footer">  
                         <div class="pull-right">  
                             <a href="/logout" class="btn btn-success btn-lg">Yes</a>  
                             <button class="btn btn-default btn-lg mb-control-close">No</button>  
                         </div>  
                     </div>  
                 </div> 
                 </div>
                 </div> 

            </div>
                <!-- END PAGE CONTENT WRAPPER -->                
        </div>            
            <!-- END PAGE CONTENT -->

        <!-- MESSAGE BOX-->
<!--         <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="pages-login.html" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- END MESSAGE BOX-->

        <!-- END PRELOADS -->  
                       

        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>        
        <script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.3.8/jquery.noty.min.js"></script>
            <script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.3.8/layouts/topCenter.min.js"></script>
            <script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.3.8/layouts/topLeft.min.js"></script>
            <script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.3.8/layouts/topRight.min.js"></script>            
            
            <script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.3.8/themes/default.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.9.4/js/bootstrap-select.min.js"></script>
            <script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
            <script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>
            <script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>  
        <!-- END PLUGINS -->                

        <!-- THIS PAGE PLUGINS -->
        <script type="text/javascript" src="{{URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
        
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
        <script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
        <script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
         <script type="text/javascript" src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
          <script type="text/javascript" src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
       <!--  <script type="text/javascript" src="{{URL::asset('js/plugins/datatables/dataTablenew.min.js')}}"></script>  -->   
        <!-- END PAGE PLUGINS -->

        <!-- START TEMPLATE -->
        
        <script type="text/javascript" src="{{URL::asset('js/plugins/bootstrap/ajax-bootstrap-select.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('js/plugins.js')}}"></script>        
        <script type="text/javascript" src="{{URL::asset('js/actions.js')}}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/js/jquery.tooltipster.min.js"></script>

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Knob/1.2.13/jquery.knob.min.js"></script>
        <script type="text/javascript">
            // console.log = function() {}
            // alert = function() {}
            // alert('hi');
            jQuery.validator.addMethod("lettersonly", function(value, element) 
            {
            return this.optional(element) || /^[A-Za-z]+$/i.test(value);
            }, "Letters and only please");
        </script> 
        @yield('Jsplugin')
        <script type="text/javascript">
            $(document).ready(function () {
                $('#create').click(function(e){
                    $("#create-panel").removeClass("hidden");
                    e.stopPropagation();
               });
            });
        </script>
    <!-- END SCRIPTS -->
    </body>
</html>