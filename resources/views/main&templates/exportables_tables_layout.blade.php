@extends('main&templates.MainTemplateMasterdata')
@section('content')

    <div class="row" id="data-row">
        <div id="data" class="col-md-12">
            <div class="panel panel-default" id= "data-panel">
                <div class="panel-heading">
                    <h3 class="panel-title" id = "table-title">@yield('table-title')</h3>
                    <div class="btn-group pull-right">
                    </div>
                    <ul class="panel-controls">
                        <li><a href="#" id = "hoho"><span class="fa fa-plus"></span></a></li>
                        <li><a href="#" class="panel-collapse" ><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table id="customers2" class="table datatable">
                        <thead>
                        <tr>
                            @yield('table-head')
                        </tr>
                        </thead>
                        <div class = "scroll">
                            <tbody>
                            @yield('table-body')
                            </tbody>
                        </div>
                    </table>

                </div>
            </div>
        </div>

        <!-- properities Table -->
        <div  id="prop"class="col-md-4 hidden">
            <form action="@yield('actionOfUpdate')" method="post">
                <div class="panel panel-warning" id = "prop-panel">
                    {!! csrf_field() !!}
                    <div class="panel-heading">
                        <h3 id="prop-title" class="panel-title"></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                            <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                            <li><a href="#" class="prop-remove"><span class="fa fa-times"></span></a></li>
                        </ul>
                    </div>
                    <div class="panel-body scroll" style="max-height: 500px;" id = 'panel-body-prop'>

                        <table class="table" id = "prop-table">
                            <thead >
                            @yield('prop-head')
                            </thead>
                            <tbody id="prop-body">
                            @yield('prop-body')
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer text-center">
                        <!-- <a href="#" class="btn btn-success btn-rounded" type>save</a> -->
                            <button type="submit" class= "btn btn-success btn-rounded"value="Save">Save</button>
                            <button type="submit" class="btn btn-danger btn-rounded" formaction="@yield('actionOfDelete')">Delete</button>
                    </div>
                </div>
            </form>
        </div>
        @yield('message-body')
    </div>
        <div class = "row hidden" id="create-panel">
            @yield('create-panel')
        </div>
@stop
