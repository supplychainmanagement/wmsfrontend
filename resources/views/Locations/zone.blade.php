@extends('main&templates.tables_layout')
@section('actionOfUpdate')
zone/update
@stop

@section('actionOfDelete')
zone/delete
@stop

@section('table-title')
    Zones
@stop


@section('table-head')
    <tr>
        <th style="display:none";>Zone ID</th>
        <th>Zone Name</th>
        <th>Zone Site</th>
    </tr>
@stop

@section('table-body')

    @foreach($zones as $zone)
        <tr>
            <td style="display:none;">{{$zone['id']}}</td>
            <td>{{$zone['ZONE_name']}}</td>
            <td>{{$zone['SITE_name']}}</td>

        </tr>
    @endforeach

@stop

@section('prop-head')

    <th>Property</th>
    <th>Value</th>
@stop


@section('prop-body')


@stop
@section('message-body')
    @if(session()->has('message'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                        <ul>
                            @foreach(session('message') as $mess)
                            <li>{{$mess}}</li>
                            @endforeach
                        </ul>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif
@stop
@if(session('system_client') == true)
    @if(session('user_role') == 'system_admin'||session('user_role')=='site_admin')
@section('create-panel')

  <div id="data" class="col-md-12">

            <div class="panel panel-warning" id= "data-panel">
              <form role="form" class="form-horizontal"  action="{{route('zoneCreate')}}" method="post" id="wmszone-form">
              {!! csrf_field() !!}
                 <div class="panel-heading">                                
                    <h3 class="panel-title" id = "table-title">Create New Zone</h3>
                      <ul class="panel-controls">
                          <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                          <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                           <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                       </ul>                                               
                </div>
                <div class="panel-body"> 
                      
                      <div class="row">

                      <div class="col-md-4">
                          <div class="form-group">
                            <label class="col-md-2 control-label">Name</label>
                            <div class="col-md-10">                                        
                              <input type="text" class="form-control" name = "name" required/>
                            </div>
                          </div>
                      </div>
                    @if($siteBased == false)
                        <div class="col-md-4">
                            <div class="form-group">
                                    <label class="col-md-2 control-label">Site</label>
                                      <div class="col-md-10">
                                          <select class="form-control selectpicker show-tick" name = "site" required>
                                            @foreach ($sites as $site)
                                              <option>{{$site['SITE_name']}}</option>
                                            @endforeach
                                    </select>
                                      </div>
                            </div>
                        </div>
                    @endif
                    
                    </div>
                    </div>                                
                     <div class="panel-footer">
                        <button type="submit" value= "Submit" class="btn btn-primary pull-right">Submit</button>
                     </div>
                     </form>
              </div>
           
    </div>

    @stop
    
  @endif
  @endif


@section('Jsplugin')
    <script type="text/javascript" src="js/masterdata/zone.js"></script>
@stop                                      

