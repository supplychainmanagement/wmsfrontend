@extends('main&templates.tables_layout')
@section('actionOfUpdate')
    storagelocation/update
@stop

@section('actionOfDelete')
    storagelocation/delete
@stop

@section('table-title')
    Storage Locations
@stop


@section('table-head')
    <tr>
        <th>Location ID</th>
        <th>Name</th>
        <th>Functional Area</th>
        <th>Allocation</th>
        <th>Type</th>
        <th>Stocktaking Date</th>
        <th>Rack</th>
    </tr>
@stop

@section('table-body')

    

@stop

@section('prop-head')

    <th>Property</th>
    <th>Value</th>
@stop


@section('prop-body')


@stop
@section('message-body')
    @if(session()->has('message'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                       <ul>
                            @foreach(session('message') as $mess)
                            <li>{{$mess}}</li>
                            @endforeach
                        </ul>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif
@stop



@section('Jsplugin')
    <script type="text/javascript" src="js/masterdata/storagelocation.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var pathname = window.location.href;
            var position = pathname.indexOf("?");
            if(position != -1)
            {
                var getvalues = pathname.substring(position+1,pathname.length);
                var splited = getvalues.split('=');
                console.log(splited[0]);
                if(splited.length == 2)
                {
                    if(splited[0] == 'search')
                    {
                         $('.maintable').DataTable().search(splited[1]).draw();
                         console.log('i am here');
                    }
                } 
            }
        }); 
    </script>
@stop                                      

