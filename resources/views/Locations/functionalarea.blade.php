@extends('main&templates.tables_layout')
@section('actionOfUpdate')
functionalarea/update
@stop

@section('actionOfDelete')
functionalarea/delete
@stop


@section('table-title')
    Functional Areas
@stop


@section('table-head')
    <tr>
        <th style="display:none";>Functional Area ID</th>
        <th >Functional Area Name</th>
        <th style="display:none";>Used For Goods In</th>
        <th style="display:none";>Used For Goods Out</th>
        <th style="display:none";>Used For Storage</th>
        <th>Used For Goods In</th>
        <th>Used For Goods Out</th>
        <th>Used For Storage</th>
    </tr>
@stop

@section('table-body')

    @foreach($functionalareas as $functionalarea)
        <tr>
            <td style="display:none;">{{$functionalarea['id']}}</td>
            <td>{{$functionalarea['area_name']}}</td>
            <td style="display:none;">{{$functionalarea['goodsin']}}</td>
            <td style="display:none;">{{$functionalarea['goodsout']}}</td>
            <td style="display:none;">{{$functionalarea['storage']}}</td>
            @if ($functionalarea['goodsin'] == 1)
                <td><span class="fa fa-check"></span></td>
            @elseif ($functionalarea['goodsin'] == 0)
                <td><span class="glyphicon glyphicon-remove"></span></td>
            @else
                <td>Not Set</span></td>
            @endif
            @if ($functionalarea['goodsout'] == 1)
                <td><span class="fa fa-check"></span></td>
            @elseif ($functionalarea['goodsout'] == 0)
                <td><span class="glyphicon glyphicon-remove"></span></td>
            @else
                <td>Not Set</span></td>
            @endif
            @if ($functionalarea['storage'] == 1)
                <td><span class="fa fa-check"></span></td>
            @elseif ($functionalarea['storage'] == 0)
                <td><span class="glyphicon glyphicon-remove"></span></td>
            @else
                <td>Not Set</span></td>
            @endif
        </tr>
    @endforeach

@stop

@section('prop-head')

    <th>Property</th>
    <th>Value</th>
@stop

@section('message-body')
    @if(session()->has('message'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                        <p>{{session('message')}}</p>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif
@stop
@if(session('system_client') == true)
@if(session('user_role') != 'labor')
@section('create-panel')

  <div id="data"class="col-md-12">

            <div class="panel panel-warning" id= "data-panel">
              <form role="form" class="form-horizontal"  action="{{route('functionalarea.formcreate')}}" method="post" id="wmszone-form">
              {!! csrf_field() !!}
                 <div class="panel-heading">                                
                    <h3 class="panel-title" id = "table-title">Create New Functional Area</h3>
                      <ul class="panel-controls">
                          <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                          <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                           <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                       </ul>                                               
                </div>
                <div class="panel-body"> 
                      
                      <div class="row">

                      <div class="col-md-4">
                          <div class="form-group">
                            <label class="col-md-2 control-label">Name</label>
                            <div class="col-md-10">                                        
                              <input type="text" class="form-control" name = "area_name" required/>
                            </div>
                          </div>
                      </div>
                    
                    </div>
                    <div class="row">
                      <div class="col-md-3">
                          <div class="form-group">
                            <label class="switch switch-small"><input type="checkbox" checked value="0" name = "goodsin"/><span></span>Goods In</label>
                            </div>
                        </div>   
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="switch switch-small"><input type="checkbox" checked value="0" name = "goodsout"/><span></span>Goodsout</label>
                            </div>
                        </div>   
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="switch switch-small"><input type="checkbox" checked value="0" name = "storage"/><span></span>Storage</label>
                            </div>
                        </div>                      
                    </div>
                    </div>                                
                     <div class="panel-footer">
                        <button type="submit" value= "Submit" class="btn btn-primary pull-right">Submit</button>
                     </div>
                     </form>
              </div>
           
    </div>

    @stop
    
  @endif
  @endif


@section('Jsplugin')
    <script type="text/javascript" src="js/masterdata/functionalarea.js"></script>
@stop                                      

