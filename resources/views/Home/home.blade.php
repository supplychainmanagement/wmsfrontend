@extends('main&templates.MainTemplateMasterdata')




@section('content')


    @if(session()->has('message'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                        <p>{{session('message')}}</p>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif


    <div class="row" style="margin-top:20px">
        <div class="col-md-3 col-md-offset-3">

            <div class="widget widget-warning widget-carousel">
                <div class="owl-carousel" id="skusWidget">
                    @foreach($skus as $sku)
                        <div>                                    
                        <div class="widget-title">{{$sku['sku']}}</div>
                        <div class="widget-subtitle">Item Number {{$sku['number']}}</div>
                        <div class="widget-int">{{$sku['amount']}}</div>
                    </div>
                    @endforeach
                </div>                            
                <div class="widget-controls">                                
                    <a href="#" class="widget-control-right"><span class="fa fa-times"></span></a>
                </div>                             
            </div>

        </div>
        <div class="col-md-3">

            <div class="widget widget-primary widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-user"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count">{{$clients['amount']}}</div>
                    <div class="widget-title">Registred Clients</div>
                    <div class="widget-subtitle">On this Subdomain</div>
                </div>
                <div class="widget-controls">                                
                    <a href="#" class="widget-control-right"><span class="fa fa-times"></span></a>
                </div>                            
            </div>

        </div>                           
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Issue Order Categories</h3>   
                </div>
                <div class="panel-body">
                    <div id="issueOrderCategories" style="height: 300px;"></div>
                </div>
            </div>

        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Issue Order Categories 2</h3>
                </div>
                <div class="panel-body">
                    <div id="issueOrderCategories2" style="height: 300px;"></div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Purchase Order Categories</h3>
                </div>
                <div class="panel-body">
                    <div id="purchaseOrderCategories" style="height: 300px;"></div>
                </div>
            </div>
        </div>

    </div>
    <div class ="row">
        <div class = "col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Received and Issued Amount Per Year</h3>
                </div>
                <div class="panel-body">
                    <div id="RJ_SJ" style="height: 300px;"></div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Empty Zones</h3>
                </div>
                <div class="panel-body">
                    <div id="empty-zones" style="height: 300px;"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- START THIS PAGE PLUGINS-->
    <script type="text/javascript" src="js/plugins/owl/owl.carousel.min.js"></script> 
    <script type="text/javascript" src="js/plugins/morris/raphael-min.js"></script>
    <script type="text/javascript" src="js/plugins/morris/morris.min.js"></script>
    <script type="text/javascript" src="js/home/home.js"></script>
    <!-- END THIS PAGE PLUGINS-->

@stop



