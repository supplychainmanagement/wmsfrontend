@extends('main&templates.tables_layout')
@section('actionOfUpdate')
    user/update
@stop

@section('actionOfDelete')
    user/delete
@stop

@section('table-title')
    Users
@stop


@section('table-head')
    <tr>
        <th style="display:none";>User ID</th>
        <th>Name</th>
        <th>Email</th>
        <th>Client</th>
        <th>Role</th>
        <th>Site</th>
    </tr>
@stop

@section('table-body')

    @foreach($users as $key=>$user)
        <tr>
            <td style="display:none;">{{$user['user']['id']}}</td>
            <td>{{$user['user']['USR_name']}}</td>
            <td>{{$user['user']['USR_email']}}</td>
            <td>{{$user['client']['CLI_name']}}</td>
            <td>{{$user['role']['ROLE_name']}}</td>
            @if($user['site'] != null)
                <td>{{$user['site']['SITE_name']}}</td>
            @else
                <td></td>
            @endif

        </tr>
    @endforeach

@stop

@section('prop-head')

    <th>Property</th>
    <th>Value</th>
@stop


@section('prop-body')


@stop
@section('message-body')
    @if(session()->has('message'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                        <p>{{session('message')}}</p>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif
@stop
@if(session('system_client') == true)
    @if(session('user_role') == 'system_admin'||session('user_role')=='site_admin')
@section('create-panel')

    <div id="data"class="col-md-12">

        <div class="panel panel-warning" id= "data-panel">
            <form role="form" class="form-horizontal"  action="{{route('userCreate')}}" method="post" id="user-form">
                {!! csrf_field() !!}
                <div class="panel-heading">
                    <h3 class="panel-title" id = "table-title">Create New User</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">

                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-2 control-label" style="margin-left:-5px;margin-right:5px;">Username</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name = "USR_name" required/>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-2 control-label" style="margin-left:-5px;margin-right:5px;">Password</label>
                                <div class="col-md-10">
                                    <input type="password" class="form-control" name = "password" required/>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Email</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name = "USR_email"/>
                                </div>
                            </div>
                        </div>
                        <br/>

                    </div>
                    <div class="row" style="margin-top:10px;margin-bottom:10px">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Role</label>
                            <div class="col-md-10">
                                <select class="form-control selectpicker show-tick" name = "USR_ROLE" id ="USR_ROLE" required>
                                    @foreach ($roles as $role)
                                        <option>{{$role['ROLE_name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4" hidden id = "siteinput">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Site</label>
                            <div class="col-md-10">
                                <select class="form-control selectpicker show-tick" name = "USR_SITE">
                                    @foreach ($sites as $site)
                                        <option>{{$site['SITE_name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    </div>
                    <br/>

                </div>
                  <div class="panel-footer">
                <button type="submit" value= "Submit" class="btn btn-primary pull-right">Submit</button>
                </div>
            </form>
        </div>
        <div class="panel-footer">
        </div>

    </div>

    </div>

@stop

@endif
@endif


@section('Jsplugin')
    <script type="text/javascript" src="js/masterdata/user.js"></script>
@stop
