@extends('main&templates.tables_layout')
@section('actionOfUpdate')
    supplier/update
@stop

@section('actionOfDelete')
    supplier/delete
@stop

@section('table-title')
    Suppliers
@stop


@section('table-head')
    <tr>
        <th style="display:none";>Supplier ID</th>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Address</th>
    </tr>
@stop

@section('table-body')

    @foreach($suppliers as $supplier)
        <tr>
            <td style="display:none;">{{$supplier['id']}}</td>
            <td>{{$supplier['SUP_name']}}</td>
            <td>{{$supplier['SUP_email']}}</td>
            <td>{{$supplier['SUP_phone']}}</td>
            <td>{{$supplier['SUP_address']}}</td>

        </tr>
    @endforeach

@stop

@section('prop-head')

    <th>Property</th>
    <th>Value</th>
@stop


@section('prop-body')


@stop
@section('message-body')
    @if(session()->has('message'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                        <p>{{session('message')}}</p>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif
@stop
@if(session('system_client') == true)
    @if(session('user_role') == 'system_admin'||session('user_role')=='site_admin')
@section('create-panel')

    <div id="data"class="col-md-12">

        <div class="panel panel-warning" id= "data-panel">
            <form role="form" class="form-horizontal"  action="{{route('supplierCreate')}}" method="post" id="supplier-form">
                {!! csrf_field() !!}
                <div class="panel-heading">
                    <h3 class="panel-title" id = "table-title">Create New Supplier</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">

                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Name</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name = "SUP_name" required/>
                                </div>
                            </div>
                        </div>

                        @if(session('system_client') == true)
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">SKUs</label>
                                    <div class="col-md-10">
                                        @foreach ($skus as $sku)
                                            <input type="checkbox" name = "SUP_SKU[]"
                                                   value="{{$sku['SKU_name']}}">{{$sku['SKU_name']}}
                                            <br/>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endif



                    </div>

                    </br>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Email</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name = "SUP_email"/>
                            </div>
                        </div>
                    </div>



                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Phone</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name = "SUP_phone"/>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Address</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name = "SUP_address"/>
                            </div>
                        </div>
                    </div>

                    <br/>

                </div>


        </div>
        <div class="panel-footer">
            <button type="submit" value= "Submit" class="btn btn-primary pull-right">Submit</button>
        </div>
        </form>
    </div>

    </div>

@stop

@endif
@endif


@section('Jsplugin')
    <script type="text/javascript" src="js/masterdata/supplier.js"></script>
@stop
