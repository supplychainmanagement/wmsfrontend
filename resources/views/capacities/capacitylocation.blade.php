@extends('main&templates.tables_layout')
@section('actionOfUpdate')
capacitylocation/update
@stop

@section('actionOfDelete')
capacitylocation/delete
@stop


@section('table-title')
    Capacity of Locations Rules
@stop


@section('table-head')
    <tr>
        <th style="display:none";>Capacity Location ID</th>
        <th>Unitload Type Name</th>
        <th>Storage Location Type Name</th>
        <th>Allocation</th>
    </tr>
@stop

@section('table-body')

    @foreach($capacitylocations as $capacitylocation)
        <tr>
            <td style="display:none;">{{$capacitylocation['id']}}</td>
            <td>{{$capacitylocation['unitloadtype_name']}}</td>
            <td>{{$capacitylocation['storagelocationtype_name']}}</td>
            <td>{{$capacitylocation['allocation']}}</td>
        </tr>
    @endforeach

@stop

@section('prop-head')

    <th>Property</th>
    <th>Value</th>
@stop

@section('message-body')
    @if(session()->has('message'))
        <div class="message-box open message-box-{{session('messagetype')}} animated fadeIn" id="message-box-success">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-{{session('messagetypeicon')}}"></span> {{session('messagetype')}}</div>
                    <div class="mb-content">
                        <p>{{session('message')}}</p>
                        <div class="mb-footer">
                            <button class="btn btn-default btn-lg pull-right" id ="close" onClick="showless()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showless() {
                //alert('i am here');
                document.getElementById("message-box-success").style.display="none";
            }
        </script>

    @endif
@stop
@if(session('system_client') == true)
@if(session('user_role') != 'labor')
@section('create-panel')

  <div id="data"class="col-md-12">

            <div class="panel panel-warning" id= "data-panel">
              <form role="form" class="form-horizontal"  action="{{route('capacitylocation.formcreate')}}" method="post" id="wmszone-form">
              {!! csrf_field() !!}
                 <div class="panel-heading">                                
                    <h3 class="panel-title" id = "table-title">Create New Storage Capacity Location Rule</h3>
                      <ul class="panel-controls">
                          <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                          <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                           <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                       </ul>                                               
                </div>
                <div class="panel-body"> 
                      
                      <div class="row">

                      <div class="col-md-4">
                          <div class="form-group">
                            <label class="col-md-2 control-label">Unitload Type Name</label>
                            <div class="col-md-10">                                        
                              <select class="form-control selectpicker show-tick" name = "unitloadtype_name" required>
                                        @foreach ($unitloadtypes as $unitloadtype)
                                          <option>{{$unitloadtype}}</option>
                                        @endforeach
                                    </select>
                            </div>
                          </div>
                      </div>

                    
                    </div>

                    <div class="row">

                      <div class="col-md-4">
                          <div class="form-group">
                            <label class="col-md-2 control-label">Storage Location Type Name</label>
                            <div class="col-md-10">                                        
                              <select class="form-control selectpicker show-tick" name = "storagelocationtype_name" required>
                                        @foreach ($storagelocationtypes as $storagelocationtype)
                                          <option>{{$storagelocationtype}}</option>
                                        @endforeach
                                    </select>
                            </div>
                          </div>
                      </div>
                    
                    </div>

                      <div class="row">

                      <div class="col-md-4">
                          <div class="form-group">
                            <label class="col-md-2 control-label">Allocation</label>
                            <div class="col-md-10">                                        
                              <input type="text" class="form-control" name = "allocation" required/>
                            </div>
                          </div>
                      </div>
                    
                    </div>
                    </div>                                
                     <div class="panel-footer">
                        <button type="submit" value= "Submit" class="btn btn-primary pull-right">Submit</button>
                     </div>
                     </form>
              </div>
           
    </div>

    @stop
    
  @endif
  @endif


@section('Jsplugin')
    <script type="text/javascript" src="js/masterdata/capacitylocation.js"></script>
@stop                                      

